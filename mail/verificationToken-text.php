<?php
use yii\helpers\Html;

/* @var $user yii\web\View */
/* @var $user common\models\User */
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify','id'=>$user->id, 'verify'=>$user->verify_token]);
?>
    Hello <?= Html::encode($user->nip) ?>,

    Follow the link below to verify your account:

    <?= Html::a(Html::encode($resetLink), $resetLink) ?>
