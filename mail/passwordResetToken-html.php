<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <p>Hello <?= Html::encode($user->name) ?>,</p>

    <p>Untuk mengubah password anda pada ID <?= Html::encode($user->nip) ?> di aplikasi CLIC, mohon ikuti petunjuk link beriku:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
