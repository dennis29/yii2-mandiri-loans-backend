<?php
use yii\helpers\Html;

/* @var $user yii\web\View */
/* @var $user common\models\User */
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify','id'=>$user->id, 'verify'=>$user->verify_token]);
?>
<div class="password-reset">
    <p>Semangat Pagi!</p>
    <br/>
    <p>Terima kasih telah mendaftar pada Aplikasi Consumer Loans Information Center</p>
    <p>Untuk mengaktifkan akun anda, sebagai berikut:</p>
    <br/>
    <p>Nama : <?= Html::encode($user->name) ?></p>
    <p>NIP/Sales Code : <?= Html::encode($user->nip) ?></p>
    <p>Tanggal Lahir : <?= Html::encode($user->dob) ?></p>
    <p>No. HP : <?= Html::encode($user->phone_number) ?></p>
    <p>E-mail : <?= Html::encode($user->email) ?></p>
    <br/>
    <p>Silahkan klik link berikut:</p>
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
    <p>(Setelah anda meng-klik link tersebut, anda sudah dapat melakukan login dengan NIP dan password diatas)</p>
    <br/>
    <p>Terima kasih.</p>
</div>
