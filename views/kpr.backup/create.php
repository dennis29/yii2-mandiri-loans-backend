<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Kpr */
$this->title = 'Create Kpr';


?>
<div class="kpr-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
