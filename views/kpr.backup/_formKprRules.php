<div class="form-group" id="add-kpr-rules">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'KprRules',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        // "id" => ['type' => TabularForm::INPUT_HIDDEN],
        'order_number' => ['type' => TabularForm::INPUT_TEXT],
        'suku_bunga_fixed' => ['type' => TabularForm::INPUT_TEXT],
        'tahun_fixed' => ['type' => TabularForm::INPUT_TEXT],
        'min_tenor' => ['type' => TabularForm::INPUT_TEXT],
        // "lock" => ['type' => TabularForm::INPUT_HIDDEN],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowKprRules(' . $key . '); return false;', 'id' => 'kpr-rules-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Kpr Rules', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowKprRules()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

