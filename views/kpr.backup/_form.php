<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Kpr */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'KprDeveloperTier', 
        'relID' => 'kpr-developer-tier', 
        'value' => \yii\helpers\Json::encode($model->kprDeveloperTiers),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'KprProfesiKpr', 
        'relID' => 'kpr-profesi-kpr', 
        'value' => \yii\helpers\Json::encode($model->kprProfesiKprs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'KprRules', 
        'relID' => 'kpr-rules', 
        'value' => \yii\helpers\Json::encode($model->kprRules),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'KprTarg3tMarket', 
        'relID' => 'kpr-targ3t-market', 
        'value' => \yii\helpers\Json::encode($model->kprTarg3tMarkets),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Proyek', 
        'relID' => 'proyek', 
        'value' => \yii\helpers\Json::encode($model->proyeks),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="kpr-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true, 'placeholder' => 'Nama']) ?>

    <?= $form->field($model, 'is_multiguna')->dropDownList([0=>'KPR',1=>'Multiguna'],['prompt' => 'Select'])->label('Tipe'); ?>

    <?= $form->field($model, 'is_reguler')->dropDownList([0=>'Non Reguler',1=>'Reguler'],['prompt' => 'Select'])->label('Kalkulasi'); ?>

    <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Developer Tier'),
            'content' => $this->render('_formKprDeveloperTier', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->kprDeveloperTiers),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Profesi Kpr'),
            'content' => $this->render('_formKprProfesiKpr', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->kprProfesiKprs),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Rules'),
            'content' => $this->render('_formKprRules', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->kprRules),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Target Market'),
            'content' => $this->render('_formKprTarg3tMarket', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->kprTarg3tMarkets),
            ]),
        ],
        // [
        //     'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Proyek'),
        //     'content' => $this->render('_formProyek', [
        //         'row' => \yii\helpers\ArrayHelper::toArray($model->proyeks),
        //     ]),
        // ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
