<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KprSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-kpr-search row">
    
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

<div class="col-md-9">

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true, 'placeholder' => 'Nama'])->label(false) ?>

    <?php $form->field($model, 'is_multiguna')->textInput(['placeholder' => 'Is Multiguna']) ?>

    <?php $form->field($model, 'is_reguler')->textInput(['placeholder' => 'Is Reguler']) ?>

    <?php $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php  echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']);  ?>
</div>
<div class="col-md-3">
    
    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>

</div>
    <?php ActiveForm::end(); ?>

</div>
