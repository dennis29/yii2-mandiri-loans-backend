<div class="form-group" id="add-kpr-developer-tier">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'KprDeveloperTier',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'developer_tier_id' => [
            'label' => 'Developer tier',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\DeveloperTier::find()->orderBy('nama')->asArray()->all(), 'id', 'nama'),
                'options' => ['placeholder' => 'Choose Developer tier'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        // "lock" => ['type' => TabularForm::INPUT_HIDDEN],
        // "id" => ['type' => TabularForm::INPUT_HIDDEN],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowKprDeveloperTier(' . $key . '); return false;', 'id' => 'kpr-developer-tier-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Kpr Developer Tier', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowKprDeveloperTier()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

