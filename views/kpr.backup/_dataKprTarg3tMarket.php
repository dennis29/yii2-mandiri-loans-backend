<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->kprTarg3tMarkets,
        'key' => function($model){
            return ['kpr_id' => $model->kpr_id, 'targ3t_market_id' => $model->targ3t_market_id];
        }
    ]);
    $gridColumns = [
        // ['class' => 'yii\grid\SerialColumn'],
        [
                'attribute' => 'targ3tMarket.name',
                'label' => 'Target Market',
                'headerOptions' => ['class'=>'hidden'],
            ],
        // [
        //     'class' => 'yii\grid\ActionColumn',
        //     'controller' => 'kpr-targ3t-market'
        // ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => false,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'summary' => '',
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
