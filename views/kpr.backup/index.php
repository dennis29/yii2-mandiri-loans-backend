<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\KprSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\alert\AlertBlock;

$this->title = 'Kpr';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>

<?php $form = ActiveForm::begin(['action'=>Url::to(['kpr/import'],true),'options' => ['enctype' => 'multipart/form-data']]); ?>
<?= $form->field($excelModel, 'id')->hiddenInput()->label(false) ?>
<?= $form->field($excelModel, 'file')->fileInput(['accept' => 'application/vnd.ms-excel', 'required'=>true]) ?>
<?= Html::submitButton('Import' , 
    [
        'class' =>'btn btn-primary',
        'data-confirm' => Yii::t('yii', 'Are you sure you want to export?'),
    ]) 
?>
<?=Html::a('Export',Url::to(['kpr/export'],true),
    [
        'class'=>'btn btn-default',
        'data-confirm' => Yii::t('yii', 'Are you sure you want to export?'),
    ])
?>
<?= 

 AlertBlock::widget([
    'type' => AlertBlock::TYPE_ALERT,
    'useSessionFlash' => true
]);
?>
<?php ActiveForm::end(); ?>

</br>
<div class="kpr-index ">
    <div class="row">
        
        <div class="col-md-6">

                <?= Html::a('Create Kpr', ['create'], ['class' => 'btn btn-success']) ?>
            
        </div>
        <div class="col-md-6">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>
    <?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        'nama',
        [
            'label'=>'Tipe',
            'attribute'=>'is_multiguna',
            'value' => function ($model, $key, $index, $column) {
                if($model->is_multiguna){
                    return "Multiguna";
                }else{
                    return "KPR";

                }
            },
        ],
        [
            'label' => 'Kalkulasi',
            'attribute' => 'is_reguler',
            'value' => function ($model, $key, $index, $column) {
                if($model->is_reguler){
                    return "Reguler";
                }else{
                    return "Non-Reguler";
                }
            }
        ],
        [
            // 'class' => 'kartik\grid\ExpandRowColumn',
            // 'width' => '50px',
            'format' => 'raw',
            'value' => function ($model, $key, $index, $column) {
            //     return GridView::ROW_COLLAPSED;
            // },
            // 'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_dataKprProfesiKpr', ['model' => $model]);
            },
            'header' => "Profesi",
            // 'headerOptions' => ['class' => 'kartik-sheet-style'],
            // 'expandOneOnly' => true
        ],
        [
            'format' => 'raw',
            'value' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_dataKprDeveloperTier', ['model' => $model]);
            },
            'header' => '
                <table style="">
                    <tr>
                        <td style=""> Developer </td>
                    </tr>
                </table>
            ',
        ],
        [
          'format' => 'raw',
            'value' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_dataKprTarg3tMarket', ['model' => $model]);
            },
            'header' => '
                <table style="">
                    <tr>
                        <td style=""> Target Market </td>
                    </tr>
                </table>
            ',  
        ],
        [
            'format' => 'raw',
            'value' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_dataKprRules', ['model' => $model]);
            },
            'header' => '
                <table style="">
                    <tr>
                        <td style=""> Suku Bunga Fixed &nbsp</td>
                        <td style=""> Tahun Fixed &nbsp</td>
                        <td style=""> Min Tenor &nbsp</td>
                    </tr>
                </table>
            ',

        ],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'keterangan',
            'contentOptions'=> ['style'=>'min-width:500px'],
        ],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'kartik\grid\ActionColumn',
            'noWrap' => true,
            'template' => '{update} {delete}',
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'striped' => false,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-kpr']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => '',
        // 'toolbar' => [
        //     '{export}',
        //     ExportMenu::widget([
        //         'dataProvider' => $dataProvider,
        //         'columns' => $gridColumn,
        //         'target' => ExportMenu::TARGET_BLANK,
        //         'fontAwesome' => true,
        //         'dropdownOptions' => [
        //             'label' => 'Full',
        //             'class' => 'btn btn-default',
        //             'itemsBefore' => [
        //                 '<li class="dropdown-header">Export All Data</li>',
        //             ],
        //         ],
        //         'exportConfig' => [
        //             ExportMenu::FORMAT_PDF => false
        //         ]
        //     ]) ,
        // ],
    ]); ?>

</div>
