<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Kpr */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Kpr', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpr-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Kpr'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'nama',
        'is_multiguna',
        'is_reguler',
        'keterangan:ntext',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerKprDeveloperTier->totalCount){
    $gridColumnKprDeveloperTier = [
        ['class' => 'yii\grid\SerialColumn'],
                        [
                'attribute' => 'developerTier.id',
                'label' => 'Developer Tier'
            ],
            ['attribute' => 'lock', 'visible' => false],
            ['attribute' => 'id', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerKprDeveloperTier,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-kpr-developer-tier']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Kpr Developer Tier'),
        ],
        'export' => false,
        'columns' => $gridColumnKprDeveloperTier
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerKprProfesiKpr->totalCount){
    $gridColumnKprProfesiKpr = [
        ['class' => 'yii\grid\SerialColumn'],
                        [
                'attribute' => 'profesiKpr.id',
                'label' => 'Profesi Kpr'
            ],
            ['attribute' => 'lock', 'visible' => false],
            ['attribute' => 'id', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerKprProfesiKpr,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-kpr-profesi-kpr']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Kpr Profesi Kpr'),
        ],
        'export' => false,
        'columns' => $gridColumnKprProfesiKpr
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerKprRules->totalCount){
    $gridColumnKprRules = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'order_number',
            'suku_bunga_fixed',
            'tahun_fixed',
            'min_tenor',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerKprRules,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-kpr-rules']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Kpr Rules'),
        ],
        'export' => false,
        'columns' => $gridColumnKprRules
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerKprTarg3tMarket->totalCount){
    $gridColumnKprTarg3tMarket = [
        ['class' => 'yii\grid\SerialColumn'],
                        [
                'attribute' => 'targ3tMarket.name',
                'label' => 'Targ3t Market'
            ],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerKprTarg3tMarket,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-kpr-targ3t-market']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Kpr Targ3t Market'),
        ],
        'export' => false,
        'columns' => $gridColumnKprTarg3tMarket
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerProyek->totalCount){
    $gridColumnProyek = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'nama',
            'tanggal_jatuh_tempo',
            [
                'attribute' => 'developer.id',
                'label' => 'Developer'
            ],
                        'min_harga',
            'max_harga',
            [
                'attribute' => 'region.name',
                'label' => 'Region'
            ],
            'longitude',
            'latitude',
            'location',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerProyek,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-proyek']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Proyek'),
        ],
        'export' => false,
        'columns' => $gridColumnProyek
    ]);
}
?>
    </div>
</div>
