<div class="form-group" id="add-kpr-profesi-kpr">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'KprProfesiKpr',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'profesi_kpr_id' => [
            'label' => 'Profesi kpr',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\ProfesiKpr::find()->orderBy('nama')->asArray()->all(), 'id', 'nama'),
                'options' => ['placeholder' => 'Choose Profesi kpr'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        // "lock" => ['type' => TabularForm::INPUT_HIDDEN],
        // "id" => ['type' => TabularForm::INPUT_HIDDEN],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowKprProfesiKpr(' . $key . '); return false;', 'id' => 'kpr-profesi-kpr-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Kpr Profesi Kpr', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowKprProfesiKpr()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

