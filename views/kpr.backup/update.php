<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kpr */

$this->title = 'Update Kpr: ' . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Kpr', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kpr-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
