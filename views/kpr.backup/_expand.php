<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Kpr'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Kpr Developer Tier'),
        'content' => $this->render('_dataKprDeveloperTier', [
            'model' => $model,
            'row' => $model->kprDeveloperTiers,
        ]),
    ],
                [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Kpr Profesi Kpr'),
        'content' => $this->render('_dataKprProfesiKpr', [
            'model' => $model,
            'row' => $model->kprProfesiKprs,
        ]),
    ],
                [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Kpr Rules'),
        'content' => $this->render('_dataKprRules', [
            'model' => $model,
            'row' => $model->kprRules,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Kpr Targ3t Market'),
        'content' => $this->render('_dataKprTarg3tMarket', [
            'model' => $model,
            'row' => $model->kprTarg3tMarkets,
        ]),
    ],
                [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Proyek'),
        'content' => $this->render('_dataProyek', [
            'model' => $model,
            'row' => $model->proyeks,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
