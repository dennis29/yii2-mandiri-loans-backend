<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->kprDeveloperTiers,
        'key' => function($model){
            return ['kpr_id' => $model->kpr_id, 'developer_tier_id' => $model->developer_tier_id];
        }
    ]);
    $gridColumns = [
        // ['class' => 'yii\grid\SerialColumn'],
        [
                'attribute' => 'developerTier.nama',
                'label' => 'Developer Tier',
                'headerOptions' => ['class'=>'hidden'],

            ],
        // [
        //     'class' => 'yii\grid\ActionColumn',
        //     'controller' => 'kpr-developer-tier'
        // ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => false,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'summary' => '',
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
