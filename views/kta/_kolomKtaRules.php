
<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->ktaRules,
        'key' => 'id'
    ]);
    $gridColumns = [
        // ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false, ],
        // 'min_tenor',
        // 'max_tenor',
        // 'suku_bunga_floating',
        // 'suku_bunga_flat',
        // 'max_dbr',
        // 'limit_min',
        // 'limit_max',
        // 'income_min',
        // 'income_max',
        [
            'attribute'=>$attribute,
            'format'=>['decimal'],
            'headerOptions' => ['class'=>'hidden'],    
        ],
       
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => false,
        'summary' => '',
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
