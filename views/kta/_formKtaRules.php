<div class="form-group" id="add-kta-rules">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'KtaRules',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'income_min' => ['type' => TabularForm::INPUT_TEXT, 'options'=>['placeholder'=>'default -1']],
        'income_max' => ['type' => TabularForm::INPUT_TEXT, 'options'=>['placeholder'=>'default -1']],
        'min_tenor' => ['type' => TabularForm::INPUT_TEXT, 'options'=>['placeholder'=>'default -1']],
        'max_tenor' => ['type' => TabularForm::INPUT_TEXT, 'options'=>['placeholder'=>'default -1']],
        'limit_min' => ['type' => TabularForm::INPUT_TEXT, 'options'=>['placeholder'=>'default -1']],
        'limit_max' => ['type' => TabularForm::INPUT_TEXT, 'options'=>['placeholder'=>'default -1']],
        'suku_bunga_floating' => ['type' => TabularForm::INPUT_TEXT, 'options'=>['placeholder'=>'default -1']],
        'suku_bunga_flat' => ['type' => TabularForm::INPUT_TEXT, 'options'=>['placeholder'=>'default -1']],
        'max_dbr' => ['type' => TabularForm::INPUT_TEXT, 'options'=>['placeholder'=>'default -1']],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowKtaRules(' . $key . '); return false;', 'id' => 'kta-rules-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Kta Rules', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowKtaRules()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

