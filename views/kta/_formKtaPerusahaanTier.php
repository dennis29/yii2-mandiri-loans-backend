<div class="form-group" id="add-kta-perusahaan-tier">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'KtaPerusahaanTier',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'perusahaan_tier_id' => [
            'label' => 'Perusahaan tier',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\PerusahaanTier::find()->orderBy('nama')->asArray()->all(), 'id', 'nama'),
                'options' => ['placeholder' => 'Choose Perusahaan tier'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowKtaPerusahaanTier(' . $key . '); return false;', 'id' => 'kta-perusahaan-tier-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Kta Perusahaan Tier', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowKtaPerusahaanTier()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

