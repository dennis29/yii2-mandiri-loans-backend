<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Kta */

?>
<div class="kta-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->nama) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'pop_up',
        [
            'format' => 'raw',
            'attribute'=>'keterangan',
        ],
        [
            'format' => 'raw',
            'attribute'=>'target_market_text',
            'label' => 'Target Market'
        ],
        [
            'format' => 'raw',
            'attribute'=>'ketentuan_umum_text',
            'label' => 'Ketentuan Umum'
        ],
        [
            'format' => 'raw',
            'attribute'=>'limit_kredit_text',
            'label' => 'Limit Kredit'
        ],
        [
            'format' => 'raw',
            'attribute'=>'jangka_waktu_kredit_text',
            'label' => 'Jangka Waktu Kredit'
        ],
        [
            'format' => 'raw',
            'attribute'=>'suku_bunga_text',
            'label' => 'Suku Bunga'
        ],
        [
            'format' => 'raw',
            'attribute'=>'ketentuan_dbr_text',
            'label' => 'Ketentuan DBR'
        ],
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>