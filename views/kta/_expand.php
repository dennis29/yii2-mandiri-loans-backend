<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Detail'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Kta Perusahaan Tier'),
        'content' => $this->render('_dataKtaPerusahaanTier', [
            'model' => $model,
            'row' => $model->ktaPerusahaanTiers,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Kta Target Market'),
        'content' => $this->render('_dataKtaTarg3tMarket', [
            'model' => $model,
            'row' => $model->ktaTarg3tMarkets,
        ]),
    ],
                [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Kta Rules'),
        'content' => $this->render('_dataKtaRules', [
            'model' => $model,
            'row' => $model->ktaRules,
        ]),
    ],
        ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
