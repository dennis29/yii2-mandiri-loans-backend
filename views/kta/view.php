<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Kta */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Kta', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kta-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Kta'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'nama',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerKtaPerusahaanTier->totalCount){
    $gridColumnKtaPerusahaanTier = [
        ['class' => 'yii\grid\SerialColumn'],
                        [
                'attribute' => 'perusahaanTier.id',
                'label' => 'Perusahaan Tier'
            ],
            ['attribute' => 'id', 'visible' => false],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerKtaPerusahaanTier,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-kta-perusahaan-tier']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Kta Perusahaan Tier'),
        ],
        'export' => false,
        'columns' => $gridColumnKtaPerusahaanTier
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerKtaRules->totalCount){
    $gridColumnKtaRules = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'min_tenor',
            'max_tenor',
            'suku_bunga_floating',
            'suku_bunga_flat',
            'max_dbr',
            'limit_min',
            'limit_max',
            'income_min',
            'income_max',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerKtaRules,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-kta-rules']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Kta Rules'),
        ],
        'export' => false,
        'columns' => $gridColumnKtaRules
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerKtaTarg3tMarket->totalCount){
    $gridColumnKtaTarg3tMarket = [
        ['class' => 'yii\grid\SerialColumn'],
                        [
                'attribute' => 'targ3tMarket.name',
                'label' => 'Targ3t Market'
            ],
            ['attribute' => 'id', 'visible' => false],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerKtaTarg3tMarket,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-kta-targ3t-market']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Kta Targ3t Market'),
        ],
        'export' => false,
        'columns' => $gridColumnKtaTarg3tMarket
    ]);
}
?>
    </div>
</div>
