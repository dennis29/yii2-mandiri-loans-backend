<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\KtaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\alert\AlertBlock;
use kartik\widgets\SwitchInput;
use yii\bootstrap\Modal;


$this->title = 'Kta';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>

<?php $form = ActiveForm::begin(['action'=>Url::to(['kta/import'],true),'options' => ['enctype' => 'multipart/form-data']]); ?>
<?= $form->field($excelModel, 'id')->hiddenInput()->label(false) ?>
<?= $form->field($excelModel, 'file')->fileInput(['accept' => 'application/vnd.ms-excel', 'required'=>true]) ?>
<?=Html::a('Export',Url::to(['kta/export'],true),
    [
        'class'=>'btn btn-default',
        // 'data-confirm' => Yii::t('yii', 'Are you sure you want to Export?'),
    ])
?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'header'=>'<h3>Konfirmasi Approver</h3>',
    'toggleButton' => ['label' => 'Import','class'=>'btn btn-primary'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'user_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->where(['status'=>10])->all(), 'id', 'name'),
            'options' => ['placeholder' => 'User Approver'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('User Approver'); ?>
    <div class="form-group">
        <?= Html::submitButton('Submit' , 
            [
                'class' =>'btn btn-primary',
                'data-confirm' => Yii::t('yii', 'Are you sure you want to Import?'),
            ]) 
        ?>
    </div>
<?php Modal::end(); ?>
<?php ActiveForm::end(); ?>
<?= 

 AlertBlock::widget([
    'type' => AlertBlock::TYPE_ALERT,
    'useSessionFlash' => true
]);
?>
</br>
<div class="kta-index">
    <div class="row">
        
        <div class="col-md-6">

                <?= Html::a('Create Kta', ['create'], ['class' => 'btn btn-success']) ?>
            
        </div>
    </div>
</br>
    <?php 
    $gridColumn = [
         ['class' => 'yii\grid\SerialColumn'],
         [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        'nama',
        [
            'attribute' => 'expired',
            'filterType'=> \kartik\grid\GridView::FILTER_DATE, 
            'filterWidgetOptions' => [
                    'options' => ['placeholder' => 'Select date'],
                    'pluginOptions' => [
                        'format' => 'dd-mm-yyyy',
                        'todayHighlight' => true
                    ]
            ],
        ],
        [
        // 'class' => 'kartik\grid\CheckboxColumn',
            'attribute' => 'is_approved',
            'label' => 'Status',
            'value' => function($model){
                if($model->is_approved == 1){
                    return "Publish";
                }
                if($model->is_approved == 0){
                    return "Pending";
                }
            },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => [0=>'Pending',1=>'Publish'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Status', 'id' => 'grid-kta-search-is_approved']
        ],
        
        // [
        //     'class' => 'kartik\grid\ExpandRowColumn',
        //     'width' => '50px',
        //     'value' => function ($model, $key, $index, $column) {
        //         return GridView::ROW_COLLAPSED;
        //     },
        //     'detail' => function ($model, $key, $index, $column) {
        //         return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
        //     },
        //     'headerOptions' => ['class' => 'kartik-sheet-style'],
        //     'expandOneOnly' => true
        // ],
        // [
        //     'format' => 'raw',
        //     'value' => function ($model, $key, $index, $column) {
        //         return Yii::$app->controller->renderPartial('_dataKtaPerusahaanTier', ['model' => $model]);
        //     },
        //     'header' => '
        //         <table style="">
        //             <tr>
        //                 <td style=""> Perusahaan </td>
        //             </tr>
        //         </table>
        //     ',
        // ],
        // [
        //   'format' => 'raw',
        //     'value' => function ($model, $key, $index, $column) {
        //         return Yii::$app->controller->renderPartial('_dataKtaTarg3tMarket', ['model' => $model]);
        //     },
        //     'header' => '
        //         <table style="">
        //             <tr>
        //                 <td style=""> Target Market </td>
        //             </tr>
        //         </table>
        //     ',  
        // ],
        // [
        //     'format' => 'raw',
        //     'value' => function ($model, $key, $index, $column) {
        //         return Yii::$app->controller->renderPartial('_dataKtaRules', ['model' => $model]);
        //     },
        //     'header' => '
        //         <table style="5000px">
        //             <tr>
        //                 <td style="width:500px"> Income Min &nbsp;</td>
        //                 <td style="width:500px"> income Max &nbsp;</td>
        //                 <td style="width:500px"> Min Tenor &nbsp;</td>
        //                 <td style="width:500px"> Max Tenor &nbsp;</td>
        //                 <td style="width:500px"> limit Min &nbsp;</td>
        //                 <td style="width:500px"> Limit Max &nbsp;</td>
        //                 <td style="width:500px"> Suku Bunga Floating &nbsp;</td>
        //                 <td style="width:500px"> Suku Bunga Flat &nbsp;</td>
        //                 <td style="width:500px"> Max Dbr &nbsp;</td>
        //             </tr>
        //         </table>
        //     ',

        // ],

        //  [
        //     'format' => 'raw',
        //     'value' => function ($model, $key, $index, $column) {
        //         return Yii::$app->controller->renderPartial('_kolomKtaRules', ['model' => $model,'attribute'=>'income_min']);
        //     },
        //     'header' => 'Income Min',

        // ],
        //  [
        //     'format' => 'raw',
        //     'value' => function ($model, $key, $index, $column) {
        //         return Yii::$app->controller->renderPartial('_kolomKtaRules', ['model' => $model,'attribute'=>'income_max']);
        //     },
        //     'header' => 'Income Max',

        // ],
        //  [
        //     'format' => 'raw',
        //     'value' => function ($model, $key, $index, $column) {
        //         return Yii::$app->controller->renderPartial('_kolomKtaRules', ['model' => $model,'attribute'=>'min_tenor']);
        //     },
        //     'header' => 'Tenor Min',

        // ],
        //  [
        //     'format' => 'raw',
        //     'value' => function ($model, $key, $index, $column) {
        //         return Yii::$app->controller->renderPartial('_kolomKtaRules', ['model' => $model,'attribute'=>'max_tenor']);
        //     },
        //     'header' => 'Tenor Max',

        // ],
        //  [
        //     'format' => 'raw',
        //     'value' => function ($model, $key, $index, $column) {
        //         return Yii::$app->controller->renderPartial('_kolomKtaRules', ['model' => $model,'attribute'=>'limit_min']);
        //     },
        //     'header' => 'Limit Min',

        // ],
        //  [
        //     'format' => 'raw',
        //     'value' => function ($model, $key, $index, $column) {
        //         return Yii::$app->controller->renderPartial('_kolomKtaRules', ['model' => $model,'attribute'=>'limit_max']);
        //     },
        //     'header' => 'Limit Max',

        // ],

        //  [
        //     'format' => 'raw',
        //     'value' => function ($model, $key, $index, $column) {
        //         return Yii::$app->controller->renderPartial('_kolomKtaRules', ['model' => $model,'attribute'=>'suku_bunga_floating']);
        //     },
        //     'header' => 'Suku Bunga Floating',

        // ],

        //  [
        //     'format' => 'raw',
        //     'value' => function ($model, $key, $index, $column) {
        //         return Yii::$app->controller->renderPartial('_kolomKtaRules', ['model' => $model,'attribute'=>'suku_bunga_flat']);
        //     },
        //     'header' => 'Suku Bunga Flat',

        // ],

        //  [
        //     'format' => 'raw',
        //     'value' => function ($model, $key, $index, $column) {
        //         return Yii::$app->controller->renderPartial('_kolomKtaRules', ['model' => $model,'attribute'=>'max_dbr']);
        //     },
        //     'header' => 'Max Dbr',

        // ],

        // [
        // // 'class' => 'kartik\grid\CheckboxColumn',
        //     'label' => 'Status',
        //     'value' => function($model){
        //         if($model->is_approved == 1){
        //             return "Approved";
        //         }
        //         if($model->is_approved == 0){
        //             return "Rejected";
        //         }
        //     }
            // 'width' => '20px',
            // 'options'=>[
            //     "data-toggle"=>"toggle",
            //     "class"=>"checkbox"
            // ],
            // // 'headerOptions' =>['style'=>'color: #3c8dbc;'],
            // 'format'=>'raw',
            // 'value'=> function($model){
            //     // return json_encode($model->sessions);
            //     // $current_session = Yii::$app->session->get('current-session');
            //     // $session_id = $current_session->ID;

            //     // $obj = \app\models\Pic::find()->joinWith(['sessions'])->where(['PIC.ID'=>$model->ID,'SESSION_.ID'=>$session_id])->one();
            //     if($model->is_approved == 1){
            //         $obj = true;
            //     }else{
            //         $obj = false;
            //     }
            //     // $far = Yii::$app->user->identity->status;
            //     // return $far;
            //     if(Yii::$app->user->identity->status == 10){
            //         return SwitchInput::widget([
            //             'name' => 'status_13',
            //             // 'disabled' => 'true',
            //             // 'value' => function($model){
            //             //     // if(empty($model->kpr->is_proved)){
            //             //     //     return $model->kpr->is_proved = 1;
            //             //     // }
            //             // },
            //             'options'=>[
            //                 'checked'=>$obj,
            //                 'onChange'=>'changeStatus("'.$model->id.'",this.checked)',
            //             ],
            //             'pluginOptions' => [
            //                 'onText' => 'Approved',
            //                 'offText' => 'Reject',
            //                 'onColor' => 'primary',
            //                 'offColor' => 'default',
            //                 ]
            //             ]);
            //     }else{
            //         return SwitchInput::widget([
            //             'name' => 'status_3',
            //             'disabled' => 'true',
            //             // 'value' => function($model){
            //             // },
            //             'options'=>[
            //                 'checked'=>$obj,
            //                 // 'onChange'=>'changeStatus("'.$model->ID.'",this.checked)',
            //             ],
            //             'pluginOptions' => [
            //                 'onText' => 'Approved',
            //                 'offText' => 'Reject',
            //                 'onColor' => 'primary',
            //                 'offColor' => 'default',
            //                 ]
            //             ]);
            //     }
            // }
            // 'visible' => $is_admin_only,
        
        // ],

        ['attribute' => 'id', 'visible' => false],
        ['attribute' => 'lock', 'visible' => false],
        [
            'header'=>'Actions',
            'headerOptions'=>[
                'style'=>'color:#3c8dbc'
            ],
            'value'=> function ($model, $key, $index, $column) {
                   $a = Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update','id'=>$model->id], ['class' => '', 'id' => 'popupModal']);      
                   $b =   Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete-page','id'=>$model->id], ['class' => '', 'id' => 'popupModal']);      
                   return $a.$b;
              },
             'format' => 'raw'
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-kta']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => '',
        // 'toolbar' => [
        //     '{export}',
        //     ExportMenu::widget([
        //         'dataProvider' => $dataProvider,
        //         'columns' => $gridColumn,
        //         'target' => ExportMenu::TARGET_BLANK,
        //         'fontAwesome' => true,
        //         'dropdownOptions' => [
        //             'label' => 'Full',
        //             'class' => 'btn btn-default',
        //             'itemsBefore' => [
        //                 '<li class="dropdown-header">Export All Data</li>',
        //             ],
        //         ],
        //         'exportConfig' => [
        //             ExportMenu::FORMAT_PDF => false
        //         ]
        //     ]) ,
        // ],
    ]); ?>

</div>

<!-- <script type="text/javascript">
    function changeStatus(id,is_approved)
    {
        $.ajax({
            url: '<?php echo Url::to('kta/change-approval') ?>',
           type: 'post',
           data: {
                id: id ,
                is_approved: is_approved
            },
           success: function (data) {
              // console.log(data);
              // var raw = JSON.parse(data);
              // var res = raw.result;
              console.log(data);
             
           }
 
      });
    }
</script> -->