<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Delete '.$model->nama;

?>
<?php $form = ActiveForm::begin(); ?>
<?= $form->errorSummary($model); ?>
<?= $form->field($model, 'user_id')->widget(\kartik\widgets\Select2::classname(), [
	'data' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->where(['status'=>10])->all(), 'id', 'name'),
	'options' => ['placeholder' => 'User Approver'],
	'pluginOptions' => [
	'allowClear' => true
	],
	])->label('User Approver'); ?>
	<div class="form-group">
		<?= Html::submitButton('Delete' , 
		[
			'class' => 'btn btn-danger',
		]) ?>
		<?= Html::a('Cancel' , ['index'],
		[
			'class' => 'btn btn-primary',
		]) ?>
	</div>
<?php ActiveForm::end(); ?>
