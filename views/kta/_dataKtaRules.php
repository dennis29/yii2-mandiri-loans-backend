<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->ktaRules,
        'key' => 'id'
    ]);
    $gridColumns = [
        // ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false, ],
        // 'min_tenor',
        // 'max_tenor',
        // 'suku_bunga_floating',
        // 'suku_bunga_flat',
        // 'max_dbr',
        // 'limit_min',
        // 'limit_max',
        // 'income_min',
        // 'income_max',
        [
            'attribute'=>'income_min',
            'label' => 'Income Min',
            // 'headerOptions' => ['class'=>'hidden'],    
        ],
        [
            'attribute'=>'income_max',
            'label' => 'Income Max',
            // 'headerOptions' => ['class'=>'hidden'],    
        ],
        [
            'attribute'=>'min_tenor',
            'label' => 'Min Tenor',
            // 'headerOptions' => ['class'=>'hidden'],    
        ],
        [
            'attribute'=>'max_tenor',
            'label' => 'Max Tenor',
            // 'headerOptions' => ['class'=>'hidden'],    
        ],
        [
            'attribute'=>'limit_min',
            'label' => 'Limit Min',
            // 'headerOptions' => ['class'=>'hidden'],    
        ],
        [
            'attribute'=>'limit_max',
            'label' => 'Limit Max',
            // 'headerOptions' => ['class'=>'hidden'],    
        ],
        [
            'attribute'=>'suku_bunga_floating',
            'label' => 'Suku Bunga Floating',
            // 'headerOptions' => ['class'=>'hidden'],    
        ],
        [
            'attribute'=>'suku_bunga_flat',
            'label' => 'Suku Bunga Flat',
            // 'headerOptions' => ['class'=>'hidden'],    
        ],
        [
            'attribute'=>'max_dbr',
            'label' => 'Max DBR',
            // 'headerOptions' => ['class'=>'hidden'],    
        ],
        // ['attribute' => 'lock', 'visible' => false],
        // [
        //     'class' => 'yii\grid\ActionColumn',
        //     'controller' => 'kta-rules'
        // ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => false,
        'summary' => '',
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
