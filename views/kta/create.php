<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Kta */

$this->title = 'Create Kta';
$this->params['breadcrumbs'][] = ['label' => 'Kta', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kta-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
