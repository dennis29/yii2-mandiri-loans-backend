<div class="form-group" id="add-kta-targ3t-market">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'KtaTarg3tMarket',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'targ3t_market_id' => [
            'label' => 'Target market',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Targ3tMarket::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Choose Target market'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowKtaTarg3tMarket(' . $key . '); return false;', 'id' => 'kta-targ3t-market-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Kta Target Market', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowKtaTarg3tMarket()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

