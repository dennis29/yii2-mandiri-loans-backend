<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kta */

$this->title = 'Update Kta: ' . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Kta', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kta-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
