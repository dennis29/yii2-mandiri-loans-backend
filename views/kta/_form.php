<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use dosamigos\tinymce\TinyMce;


/* @var $this yii\web\View */
/* @var $model app\models\Kta */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'KtaPerusahaanTier', 
        'relID' => 'kta-perusahaan-tier', 
        'value' => \yii\helpers\Json::encode($model->ktaPerusahaanTiers),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'KtaRules', 
        'relID' => 'kta-rules', 
        'value' => \yii\helpers\Json::encode($model->ktaRules),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'KtaTarg3tMarket', 
        'relID' => 'kta-targ3t-market', 
        'value' => \yii\helpers\Json::encode($model->ktaTarg3tMarkets),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="kta-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true, 'placeholder' => 'Nama']) ?>

    <?= $form->field($model, 'expired')->widget(\kartik\date\DatePicker::classname(), [
        'type' =>  \kartik\date\DatePicker::TYPE_COMPONENT_PREPEND,
        // 'saveFormat' => 'php:Y-m-d',
        'value' => '1990-01-01',
        // 'ajaxConversion' => true,
        
        'pluginOptions' => [
            'placeholder' => 'Choose Pilih tanggal expired',
            'autoclose' => true,
            'format' =>'dd-mm-yyyy',
        ]
        
    ])->label('Expired'); ?>

    <?= $form->field($model, 'pop_up')->textInput(['maxlength' => true, 'placeholder' => 'Pop up']) ?>

    <?= $form->field($model, 'keterangan')->widget(TinyMce::className(), [
        'options' => ['rows' => 12],
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>

    <?= $form->field($model, 'target_market_text')->widget(TinyMce::className(), [
        'options' => ['rows' => 12],
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>

    <?= $form->field($model, 'ketentuan_umum_text')->widget(TinyMce::className(), [
        'options' => ['rows' => 12],
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>

    <?= $form->field($model, 'limit_kredit_text')->widget(TinyMce::className(), [
        'options' => ['rows' => 12],
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>

    <?= $form->field($model, 'jangka_waktu_kredit_text')->widget(TinyMce::className(), [
        'options' => ['rows' => 12],
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>

    <?= $form->field($model, 'suku_bunga_text')->widget(TinyMce::className(), [
        'options' => ['rows' => 12],
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>

    <?= $form->field($model, 'ketentuan_dbr_text')->widget(TinyMce::className(), [
        'options' => ['rows' => 12],
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>
    
    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('KtaPerusahaanTier'),
            'content' => $this->render('_formKtaPerusahaanTier', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->ktaPerusahaanTiers),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('KtaRules'),
            'content' => $this->render('_formKtaRules', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->ktaRules),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('KtaTargetMarket'),
            'content' => $this->render('_formKtaTarg3tMarket', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->ktaTarg3tMarkets),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>

    <?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'header'=>'<h3>Konfirmasi Approver</h3>',
    'toggleButton' => ['label' => 'Submit','class'=>'btn btn-success'],
    "footer"=>"",// always need it for jquery plugin
])?>
    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'user_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->where(['status'=>10])->all(), 'id', 'name'),
            'options' => ['placeholder' => 'User Approver'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('User Approver'); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', 
            [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                                    
            ]) ?>
    </div>
<?php Modal::end(); ?>
<?php ActiveForm::end(); ?>
</div>
