
<?php
  // dmstr\web\MorrisAsset::register($this);
  // dmstr\web\JQueryUIAsset::register($this);
  // dmstr\web\WYSIAsset::register($this);
  // dmstr\web\KnobAsset::register($this);
  // dmstr\web\DateRangePickerAsset::register($this);
use miloschuman\highcharts\Highcharts;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;

use miloschuman\highcharts\SeriesDataHelper;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\helpers\Html;
$this->title="PIE";
?>
  <div class="form-group">
  <?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\KtaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\alert\AlertBlock;
use kartik\widgets\SwitchInput;
use yii\bootstrap\Modal;

$form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
    <?= $form->field($model,'file')->fileInput() ?>
   
    <div class="form-group">
        <?= Html::submitButton('Import',['class'=>'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end();
?>

</br>