<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Perusahaan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="perusahaan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'perusahaan_tier_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\PerusahaanTier::find()->orderBy('nama')->asArray()->all(), 'id', 'nama'),
            'options' => ['placeholder' => 'Tier'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('Perusahaan Tier') ?>    

    <?= $form->field($model, 'perusahaan_group_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\PerusahaanGroup::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
            'options' => ['placeholder' => 'Tier'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('Perusahaan Group') ?>    

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
