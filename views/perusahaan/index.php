<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Perusahaan;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
// use yii\bootstrap\Alert;
use kartik\alert\AlertBlock;
use kartik\alert\Alert;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PerusahaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Perusahaans';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);
?>

<?php $form = ActiveForm::begin(['action'=>Url::to(['perusahaan/import'],true),'options' => ['enctype' => 'multipart/form-data']]); ?>
<?= $form->field($excelModel, 'id')->hiddenInput()->label(false) ?>
<?= $form->field($excelModel, 'file')->fileInput(['accept' => 'application/vnd.ms-excel', 'required'=>true]) ?>
<?= Html::submitButton('Import' , 
    [
        'class' =>'btn btn-primary',
        'data-confirm' => Yii::t('yii', 'Are you sure you want to Import?'),
    ]) 
?>

<?=Html::a('Export',Url::to(['perusahaan/export'],true),
    [
        'class'=>'btn btn-default',
        'data-confirm' => Yii::t('yii', 'Are you sure you want to Export?'),
    ])
?>
<?= 

 AlertBlock::widget([
    'type' => AlertBlock::TYPE_ALERT,
    'useSessionFlash' => true
]);
?>
<?php ActiveForm::end(); ?>

<br>
<div class="perusahaan-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
                    ['role'=>'modal-remote','title'=> 'Create new Perusahaans','class'=>'btn btn-default']).
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    ''
                    // '{toggleData}'.
                    // '{export}'
                ],
            ],          
            'striped' => true,
            'condensed' => true,
            'responsive' => true,          
            'panel' => [
                'type' => 'primary', 
                'heading' => '<i class="glyphicon glyphicon-list"></i> Perusahaans listing',
                // 'before'=>'<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
                'after'=>BulkButtonWidget::widget([
                            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Delete Selected',
                                ["bulk-delete"] ,
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Are you sure?',
                                    'data-confirm-message'=>'Are you sure want to delete this item'
                                ]),
                        ]).'&nbsp;'.
                         Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Delete All ('.number_format($dataProvider->getTotalCount()).')',
                                ['delete-all'],
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    // 'role'=>'modal-remote-bulk',
                                    // 'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'name'=>'aksi',
                                    'value'=>'delete all',
                                    'type'=>'submit',
                                    // 'data-toggle'=>"modal",
                                    // 'data-target'=>'#ajaxCrudModal'
                                    // 'data-request-method'=>'post',
                                    // 'data-confirm-title'=>'Are you sure?',
                                    'data-confirm'=>'Are you sure want to Delete all these item'
                                ]
                        ).                        
                        '<div class="clearfix"></div>',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
