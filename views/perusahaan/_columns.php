<?php
use yii\helpers\Url;
use kartik\grid\GridView;


return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'max_limit',
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'max_tenor',
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'dbr',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'perusahaan_tier_id',
        'label'=>'Perusahaan Tier',
        // 'group' =>true,
        // 'subGroupOf'=>7,
        'value' => function($model) {
            if(empty($model->perusahaanTier)){
                return null;
            }   else{
                return $model->perusahaanTier->nama;
            }
        },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\PerusahaanTier::find()->asArray()->all(), 'id', 'nama'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Perusahaan Tier', 'id' => 'grid-perusahaan-search-tier_id']
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'perusahaan_group_id',
        'group'=>true,  // enable grouping,
        'groupedRow'=>true,      
         'value' => function($model) {
            if(!empty($model->perusahaanGroup->name)){

                return $model->perusahaanGroup->name;
            }
        }
    ],
    // [
    //         'label' => 'Status',
    //         'value' => function($model){
    //             if($model->is_approved == 1){
    //                 return "Publish";
    //             }
    //             if($model->is_approved == 0){
    //                 return "Pending";
    //             }
    //         }
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'lock',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_at',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_by',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_by',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'headerOptions'=>[
                'style'=>'color:#3c8dbc'
                ],
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template'=>'{update} {delete}',
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   