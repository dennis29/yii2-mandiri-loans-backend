<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Intro */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="intro-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <?= $form->field($model, 'order_number')->textinput(['type' => 'number']) ?>
    <?= Html::img($model->image,['style'=>'width:100px'])?>
    <?= $form->field($model, 'imageFile')->fileInput(['accept' => 'image/*']) ?>
    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
