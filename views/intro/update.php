<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Intro */
?>
<div class="intro-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
