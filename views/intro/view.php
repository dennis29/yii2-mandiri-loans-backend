<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Intro */
?>
<div class="intro-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'image',
            'text:ntext',
            'lock',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
