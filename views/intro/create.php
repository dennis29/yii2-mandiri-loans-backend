<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Intro */

?>
<div class="intro-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
