<?php
use yii\helpers\Url;
use app\models\Priviledge;
use app\models\User;

$status = Yii::$app->user->identity->status;
$statusStr = "";
if($status == 10){
    $statusStr = "Approver";
}
if($status == 12){
    $statusStr = "Admin Region";
}else{
    $statusStr = "Admin";
}

?>

<?php 
    if(empty(Yii::$app->user->identity->region)){
        $adminR = "";
    }else{
        $adminR = Yii::$app->user->identity->region->name;
    }
?>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->nip?></p>
                <p><?= $statusStr?></p>  
            </div>
                <br>
                <br>
        </div>


        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    [
                        'label' => 'Dashboard', 
                        'icon' => 'fa fa-dashboard', 
                        'url' => ['/dashboard'],
                        //cek table privilege
                        'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Dashboard'])->one()->status)||($status == User::STATUS_APPROVER)))
                    ],
					[
                        'label' => 'Export PIE</span><span class="pull-right-container"><small class="label pull-right bg-green">excel</small>', 
                        'icon' => 'fa fa-circle-o', 
                        'url' => ['/pie'],
                        'encode' => false,
                        'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Dashboard'])->one()->status)||($status == User::STATUS_APPROVER)))
                    ],
                    // ['label' => 'Program', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Proyek</span><span class="pull-right-container"><small class="label pull-right bg-green">excel</small>', 
                        'icon' => 'fa fa-circle-o', 
                        'url' => ['/proyek'],
                        'encode' => false,
                        'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Proyek'])->one()->status)||($status == User::STATUS_APPROVER)))

                    ],
                    [
                        'label' => 'Approval</span><span class="pull-right-container"><small class="label pull-right bg-red" id="counter-not-read">0</small>', 
                        'icon' => 'fa fa-circle-o', 
                        'url' => ['/notifikasi'],
                        'encode' => false,
                        'visible' => Yii::$app->user->identity->status == \app\models\User::STATUS_ACTIVE,
                    ],
                    ['label' => 'KTA', 'options' => [
                        'class' => 'header large',
                        
                        'style' => 'color:#FFF;text-transform: uppercase;'
                        ],
                        'visible'=>($status!= User::STATUS_REGION),
                        ],
                    [
                        'label' => 'Program KTA</span><span class="pull-right-container"><small class="label pull-right bg-green">excel</small>', 
                        'icon' => 'fa fa-circle-o', 
                        'url' => ['/kta'],
                        'encode'=> false,
                        'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'KTA'])->one()->status)||($status == User::STATUS_APPROVER)))
                    ],
                    ['label' => 'Perusahaan KTA', 'icon' => 'fa fa-circle-o', 'url' => ['#'],
                        'visible'=>($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Perusahaan List KTA'])->one()->status)||($status == User::STATUS_APPROVER))),
                        'items' => [
                            [
                                'label' => 'Perusahaan List</span><span class="pull-right-container"><small class="label pull-right bg-green">excel</small>', 
                                'icon' => 'fa fa-circle-o', 
                                'url' => ['/perusahaan'],
                                'encode'=> false,
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Perusahaan List KTA'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                            [
                                'label' => 'Perusahaan Group', 'icon' => 'fa fa-circle-o', 'url' => ['/perusahaan-group'],
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Perusahaan Group KTA'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                            [
                                'label' => 'Tier Perusahaan List', 'icon' => 'fa fa-circle-o', 'url' => ['/perusahaan-tier'],
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Tier Perusahaan List KTA'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                        ],
                    ],
                    [
                        'label' => 'KPR', 'options' => ['class' => 'header',
                        'style' => 'color:#FFF;text-transform: uppercase;'
                    ],
                        'visible'=>($status!= User::STATUS_REGION),

                    ],
                    [
                        'label' => 'Program KPR</span><span class="pull-right-container"><small class="label pull-right bg-green">excel</small>', 
                        'url' => ['/kpr'],
                        'icon' => 'fa fa-circle-o', 
                        'encode' => false,
                        'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'KPR'])->one()->status)||($status == User::STATUS_APPROVER)))

                    ],
                    [
                        'label' => 'Suku Bunga KPR</span><span class="pull-right-container"><small class="label pull-right bg-green">excel</small>', 
                        'icon' => 'fa fa-circle-o', 
                        'url' => ['/suku-bunga'],
                        'encode'=>false,
                        'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Suku Bunga KPR'])->one()->status)||($status == User::STATUS_APPROVER)))

                    ],
                    ['label' => 'Developer', 'icon' => 'fa fa-circle-o', 'url' => ['#'],
                        'visible'=>($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Developer List'])->one()->status)||($status == User::STATUS_APPROVER))),

                        'items' => [
                            [
                                'label' => 'Developer List</span><span class="pull-right-container"><small class="label pull-right bg-green">excel</small>', 
                                'icon' => 'fa fa-circle-o', 
                                'url' => ['/developer'],
                                'encode'=>false,
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Developer List'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                            [
                                'label' => 'Developer Group', 'icon' => 'fa fa-circle-o', 'url' => ['/developer-group'],
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Developer Group'])->one()->status)||($status == User::STATUS_APPROVER)))
                                
                            ],
                            [
                                'label' => 'Tier Developer List', 'icon' => 'fa fa-circle-o', 'url' => ['/developer-tier'],
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Tier Developer List'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                        ],
                    ],
                    ['label' => 'Perusahaan KPR', 'icon' => 'fa fa-circle-o', 'url' => ['#'],
                        'visible'=>($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Perusahaan List KPR'])->one()->status)||($status == User::STATUS_APPROVER))),

                        'items' => [
                            [
                                'label' => 'Perusahaan List</span><span class="pull-right-container"><small class="label pull-right bg-green">excel</small>', 
                                'icon' => 'fa fa-circle-o', 
                                'url' => ['/perusahaan-kpr'],
                                'encode'=> false,
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Perusahaan List KPR'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                            [
                                'label' => 'Tier Perusahaan List', 'icon' => 'fa fa-circle-o', 'url' => ['/perusahaan-kpr-tier'],
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Tier Perusahaan List KPR'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                        ],
                    ],
                    [
                        'label' => 'Promo', 
                        'icon' => 'fa fa-circle-o', 
                        'url' => ['/promo'],
                        'encode'=>false,
                        'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Promo'])->one()->status)||($status == User::STATUS_APPROVER)))

                    ],
                    [
                        'label' => 'KKB', 'options' => ['class' => 'header',
                        'style' => 'color:#FFF;text-transform: uppercase;'
                        ],
                        'visible'=>($status!= User::STATUS_REGION),

                    ],
                    [
                        'label' => 'KKB', 
                        'icon' => 'fa fa-circle-o', 
                        'url' => ['/kkb'],
                        'encode' => false,
                        'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'KKB'])->one()->status)||($status == User::STATUS_APPROVER)))
                    ],
                    [
                        'label' => 'Slider KKB', 'icon' => 'fa fa-circle-o', 'url' => ['/slider-kkb'],
                        'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Slider KKB'])->one()->status)||($status == User::STATUS_APPROVER)))
                        
                    ],
                    [
                        'label' => 'KKB Link', 'icon' => 'fa fa-circle-o', 'url' => ['/kkb-link'],
                        'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'KKB Link'])->one()->status)||($status == User::STATUS_APPROVER)))

                    ],
                    [
                        'label' => 'etc', 'options' => ['class' => 'header',
                        'style' => 'color:#FFF;text-transform: uppercase;'
                        ],
                        'visible'=>($status!= User::STATUS_REGION),
                    ],
                    ['label' => 'Master Data', 'icon' => 'fa fa-circle-o', 'url' => ['#'],
                        'visible'=>($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Intro Gallery'])->one()->status)||($status == User::STATUS_APPROVER))),

                         'items' => [
                            [
                                'label' => 'Intro Gallery', 'icon' => 'fa fa-circle-o', 'url' => ['/intro'],
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Intro Gallery'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                            [
                                'label' => 'Home Slider', 'icon' => 'fa fa-circle-o', 'url' => ['/slider'],
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Home Slider'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                            [
                                'label' => 'Reward', 'icon' => 'fa fa-circle-o', 'url' => ['/reward'],
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Reward'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                            [
                                'label' => 'Menu KTA', 'icon' => 'fa fa-circle-o', 'url' => ['/menu-kta'],
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Menu KTA'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                            [
                                'label' => 'Profesi', 'icon' => 'fa fa-circle-o', 'url' => ['/profesi-kpr'],
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Profesi'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                            [
                                'label' => 'Target Market', 'icon' => 'fa fa-circle-o', 'url' => ['/targ3t-market'],
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Target Market'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                            [
                                'label' => 'Master Pdf', 'icon' => 'fa fa-circle-o', 'url' => ['/pdf-master'],
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Master PDF'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                            [
                                'label' => 'Region', 
                                'icon' => 'fa fa-circle-o', 
                                'url' => ['/region'],
                                'encode'=> false,
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Region'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                            [
                                'label' => 'Area</span><span class="pull-right-container"><small class="label pull-right bg-green">excel</small>', 
                                'icon' => 'fa fa-circle-o', 
                                'url' => ['/area'],
                                'encode'=> false,
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Area'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                            // ['label' => 'Limit', 'icon' => 'fa fa-circle-o', 'url' => ['/limit'],],
                            // ['label' => 'Income', 'icon' => 'fa fa-circle-o', 'url' => ['/income'],],
                        ],
                    ],
                    // ['label' => 'User', 'icon' => 'fa fa-circle-o', 'url' => ['/user']],
                    ['label' => 'Users', 'icon' => 'fa fa-circle-o', 'url' => ['#'],
                        'visible'=>($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Admin'])->one()->status)||($status == User::STATUS_APPROVER))),

                        'items' => [
                            [
                                'label' => 'Developer List</span><span class="pull-right-container"><small class="label pull-right bg-green">excel</small>', 
                                'label' => 'Admin', 
                                'icon' => 'fa fa-circle-o', 
                                'url' => ['/user/index'],
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'Admin'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                            [
                                'label' => 'User Terdaftar', 'icon' => 'fa fa-circle-o', 'url' => ['/user/mobile'],
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'User Terdaftar'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                            [
                                'label' => 'Pic Mtf</span><span class="pull-right-container"><small class="label pull-right bg-green">excel</small>', 
                                'icon' => 'fa fa-circle-o', 
                                'url' => ['/user-mtf'],
                                'encode'=> false,
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'PIC MTF'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                            [
                                'label' => 'Pic Muf</span><span class="pull-right-container"><small class="label pull-right bg-green">excel</small>', 
                                'icon' => 'fa fa-circle-o', 
                                'url' => ['/user-muf'],
                                'encode'=> false,
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'PIC MUF'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                            [
                                'label' => 'Privilege', 
                                'icon' => 'fa fa-circle-o', 
                                'url' => ['/privilege'],
                                'encode'=> false,
                                'visible' => Yii::$app->user->identity->status == \app\models\User::STATUS_APPROVER,

                            ],
                            [
                                'label' => 'User White List</span><span class="pull-right-container"><small class="label pull-right bg-green">excel</small>', 
                                'icon' => 'fa fa-circle-o', 
                                'url' => ['/user-referensi'],
                                'encode'=> false,
                                'visible' => ($status!= User::STATUS_REGION && ((Priviledge::find('status')->where(['nama' => 'User White List'])->one()->status)||($status == User::STATUS_APPROVER)))

                            ],
                        ],
                    ],
                    [   
                        'label' =>'USER '. $adminR,
                        'icon' => 'fa fa-circle-o', 
                        'url' => ['/user-region'], 
                        'visible'=>($status == User::STATUS_REGION),
                    ],
                    [   
                        'label' => 'Logout', 
                        'url' => ['site/logout'], 
                        'visible' => !Yii::$app->user->isGuest,
                        'template' => '<a href="{url}" data-method="post"><i class="fa fa-sign-out"></i>{label}</a>'
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
<script type="text/javascript">
    // console.log("<?= Url::to(['notifikasi/count-all'],true)?>");
    window.setInterval(function(){



        $.ajax({
            url: "<?= Url::to(['notifikasi/count-all'],true)?>",
           type: 'post',
           data: {
                // id: id ,
                // is_approved: is_approved
            },
           success: function (data) {
              // console.log(data);
              // var raw = JSON.parse(data);
              // var res = raw.result;
              document.getElementById("counter-not-read").innerHTML=data;

             
           }
 
      });       
    }
    , 10000);
</script>