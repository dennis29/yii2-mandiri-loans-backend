<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MenuKta */
?>
<div class="menu-kta-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
