<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MenuKta */
?>
<div class="menu-kta-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'link',
            'title',
            'lock',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
