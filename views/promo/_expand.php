<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    // [
    //     'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Promo'),
    //     'content' => $this->render('_detail', [
    //         'model' => $model,
    //     ]),
    // ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Promo Developer Group'),
        'content' => $this->render('_dataPromoDeveloperGroup', [
            'model' => $model,
            'row' => $model->promoDeveloperGroups,
        ]),
    ],
        ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
