<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->promoDeveloperGroups,
        'key' => function($model){
            return ['promo_id' => $model->promo_id, 'developer_group_id' => $model->developer_group_id];
        }
    ]);
    $gridColumns = [
        // ['class' => 'yii\grid\SerialColumn'],
        [
                'attribute' => 'developerGroup.name',
                'label' => 'Developer Group'
        ],
        [
                'attribute' => 'text',
                'format' => 'raw',
                'label' => 'Text'
        ],
        // ['attribute' => 'lock', 'visible' => false],
        // ['attribute' => 'id', 'visible' => false],
        // [
        //     'class' => 'yii\grid\ActionColumn',
        //     'controller' => 'promo-developer-group'
        // ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'summary'=>'',
        'emptyText'=>'-',
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
