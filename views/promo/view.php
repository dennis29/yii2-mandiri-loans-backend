<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Promo */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Promo', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Promo'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'title',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerPromoDeveloperGroup->totalCount){
    $gridColumnPromoDeveloperGroup = [
        ['class' => 'yii\grid\SerialColumn'],
                        [
                'attribute' => 'developerGroup.name',
                'label' => 'Developer Group'
            ],
            ['attribute' => 'lock', 'visible' => false],
            ['attribute' => 'id', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPromoDeveloperGroup,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-promo-developer-group']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Promo Developer Group'),
        ],
        'export' => false,
        'columns' => $gridColumnPromoDeveloperGroup
    ]);
}
?>
    </div>
</div>
