<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SliderKkb */
?>
<div class="slider-kkb-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'image',
            'image_name',
            'order_number',
            'lock',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
