<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SliderKkb */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slider-kkb-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'order_number')->textInput() ?>

    <?= Html::img($model->image,['style'=>'width:100px'])?>
    
    <?= $form->field($model, 'imageSliderKkb')->fileInput(['accept' => 'image/*']) ?>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
