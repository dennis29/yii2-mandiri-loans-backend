<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SukuBunga */

$this->title = 'Update Suku Bunga: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Suku Bunga', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->kpr->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="suku-bunga-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
