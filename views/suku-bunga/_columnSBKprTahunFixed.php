<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->sukuBungaRulesKpr,
        'key' => 'id'
    ]);
    $gridColumns = [
        // ['class' => 'yii\grid\SerialColumn'],
        // ['attribute' => 'id', 'visible' => false],
        // 'is_multiguna',
        // 'order_number',
        // 'suku_bunga_fixed',
        // 'tahun_fixed',
        [
             'headerOptions' => ['class'=>'hidden'],
             'noWrap'=> true,
             
             'value' => function($model){
                return "fixed ".$model->tahun_fixed." tahun";
             }

        ]
        // ['attribute' => 'lock', 'visible' => false],
        // [
        //     'class' => 'yii\grid\ActionColumn',
        //     'controller' => 'suku-bunga-rules'
        // ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'emptyText' => '-',
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => false,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'summary'=>'',
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
