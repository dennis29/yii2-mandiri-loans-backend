<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\SukuBunga */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Suku Bunga', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suku-bunga-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Suku Bunga'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'nama',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerKprSukuBunga->totalCount){
    $gridColumnKprSukuBunga = [
        ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'kpr.id',
                'label' => 'Kpr'
            ],
                        ['attribute' => 'id', 'visible' => false],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerKprSukuBunga,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-kpr-suku-bunga']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Kpr Suku Bunga'),
        ],
        'export' => false,
        'columns' => $gridColumnKprSukuBunga
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerSukuBungaRules->totalCount){
    $gridColumnSukuBungaRules = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'is_multiguna',
            'order_number',
            'suku_bunga_fixed',
            'tahun_fixed',
            'min_tenor',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSukuBungaRules,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-suku-bunga-rules']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Suku Bunga Rules'),
        ],
        'export' => false,
        'columns' => $gridColumnSukuBungaRules
    ]);
}
?>
    </div>
</div>
