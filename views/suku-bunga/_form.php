<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\SukuBunga */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'KprSukuBunga', 
        'relID' => 'kpr-suku-bunga', 
        'value' => \yii\helpers\Json::encode($model->kprSukuBungas),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'SukuBungaRulesKpr', 
        'relID' => 'suku-bunga-rules-kpr', 
        'value' => \yii\helpers\Json::encode($model->sukuBungaRulesKpr),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'SukuBungaRulesMultiguna', 
        'relID' => 'suku-bunga-rules-multiguna', 
        'value' => \yii\helpers\Json::encode($model->sukuBungaRulesMultiguna),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="suku-bunga-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true, 'placeholder' => 'Nama']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        // [
        //     'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('KprSukuBunga'),
        //     'content' => $this->render('_formKprSukuBunga', [
        //         'row' => \yii\helpers\ArrayHelper::toArray($model->kprSukuBungas),
        //     ]),
        // ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('KPR'),
            'content' => $this->render('_formSukuBungaRulesKpr', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->sukuBungaRulesKpr),
                'is_multiguna' => \app\models\SukuBungaRules::TYPE_KPR,
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Multiguna'),
            'content' => $this->render('_formSukuBungaRulesMultiguna', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->sukuBungaRulesMultiguna),
                'is_multiguna' => \app\models\SukuBungaRules::TYPE_MULTIGUNA,
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'header'=>'<h3>Konfirmasi Approver</h3>',
    'toggleButton' => ['label' => 'Submit','class'=>'btn btn-success'],
    "footer"=>"",// always need it for jquery plugin
])?>
    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'user_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->where(['status'=>10])->all(), 'id', 'name'),
            'options' => ['placeholder' => 'User Approver'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('User Approver'); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', 
            [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                                    
            ]) ?>
    </div>
<?php Modal::end(); ?>
<?php ActiveForm::end(); ?>
</div>