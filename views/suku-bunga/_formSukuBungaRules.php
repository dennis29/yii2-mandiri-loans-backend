<div class="form-group" id="add-suku-bunga-rules">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;


$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'SukuBungaRules',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'is_multiguna' => [
            'type' => TabularForm::INPUT_RAW,
            'value' => function($m, $k, $i, $w) { 
                // return Html::textInput("details", $is_multiguna, ['class'=>'form-control']);
            }
        ],
        'order_number' => ['type' => TabularForm::INPUT_TEXT],
        'suku_bunga_fixed' => ['type' => TabularForm::INPUT_TEXT],
        'tahun_fixed' => ['type' => TabularForm::INPUT_TEXT],
        'min_tenor' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowSukuBungaRules(' . $key . '); return false;', 'id' => 'suku-bunga-rules-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Suku Bunga Rules', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowSukuBungaRules()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

