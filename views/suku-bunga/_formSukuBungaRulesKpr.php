<div class="form-group" id="add-suku-bunga-rules-kpr">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;
use app\models\SukuBungaRules;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'SukuBungaRules',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'is_multiguna' => [
            'type' => TabularForm::INPUT_RAW,
            // 'visible' =>false,
            'label' =>'',

            'value' => function($m, $k, $i, $w) { 
                return Html::textInput("SukuBungaRules[".($k+100)."][is_multiguna]", SukuBungaRules::TYPE_KPR, ['class'=>'form-control hidden']);
            }
        ],
       'order_number' => ['type' => TabularForm::INPUT_RAW,
             'value' => function($m, $k, $i, $w) { 

                $item = null;
                if(!empty($m['order_number'])){
                    $item = $m['order_number'];
                }
                return Html::textInput("SukuBungaRules[".($k+100)."][order_number]", $item, ['class'=>'form-control']);
            }
        ],
        'suku_bunga_fixed' => ['type' => TabularForm::INPUT_RAW,
             'value' => function($m, $k, $i, $w) { 
                $item = null;
                if(!empty($m['suku_bunga_fixed'])){
                    $item = $m['suku_bunga_fixed'];
                }
                return Html::textInput("SukuBungaRules[".($k+100)."][suku_bunga_fixed]",$item , ['class'=>'form-control']);
            }
        ],
        'tahun_fixed' => ['type' => TabularForm::INPUT_RAW,
             'value' => function($m, $k, $i, $w) { 
                $item = null;
                if(!empty($m['tahun_fixed'])){
                    $item = $m['tahun_fixed'];
                }
                return Html::textInput("SukuBungaRules[".($k+100)."][tahun_fixed]",$item , ['class'=>'form-control']);
            }
        ],
        'min_tenor' => ['type' => TabularForm::INPUT_RAW,
             'value' => function($m, $k, $i, $w) { 
                $item = 0;
                if(!empty($m['min_tenor'])){
                    $item = $m['min_tenor'];
                }
                return Html::textInput("SukuBungaRules[".($k+100)."][min_tenor]",$item , ['class'=>'form-control']);
            }
        ],
        // "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowSukuBungaRulesKpr(' . $key . '); return false;', 'id' => 'suku-bunga-rules-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Suku Bunga Rules', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowSukuBungaRulesKpr()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

