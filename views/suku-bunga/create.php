<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SukuBunga */

$this->title = 'Create Suku Bunga';
$this->params['breadcrumbs'][] = ['label' => 'Suku Bunga', 'url' => ['index']];
?>
<div class="suku-bunga-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
