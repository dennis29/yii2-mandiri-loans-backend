<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\SukuBungaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use kartik\alert\AlertBlock;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
use kartik\select2\Select2;

$this->title = 'Suku Bunga';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
// CrudAsset::register($this);

?>

<?php $form = ActiveForm::begin(['action'=>Url::to(['suku-bunga/import'],true),'options' => ['enctype' => 'multipart/form-data']]); ?>
<?= $form->field($excelModel, 'id')->hiddenInput()->label(false) ?>
<?= $form->field($excelModel, 'file')->fileInput(['accept' => 'application/vnd.ms-excel', 'required'=>true]) ?>
<?=Html::a('Export',Url::to(['suku-bunga/export'],true),
    [
        'class'=>'btn btn-default',
        // 'data-confirm' => Yii::t('yii', 'Are you sure you want to Export?'),
    ])
?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'header'=>'<h3>Konfirmasi Approver</h3>',
    'toggleButton' => ['label' => 'Import','class'=>'btn btn-primary'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'user_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->where(['status'=>10])->all(), 'id', 'name'),
            'options' => ['placeholder' => 'User Approver'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('User Approver'); ?>
    <div class="form-group">
        <?= Html::submitButton('Submit' , 
            [
                'class' =>'btn btn-primary',
                'data-confirm' => Yii::t('yii', 'Are you sure you want to Import?'),
            ]) 
        ?>
    </div>
<?php Modal::end(); ?>
<?php ActiveForm::end(); ?>
<?= 
 AlertBlock::widget([
    'type' => AlertBlock::TYPE_ALERT,
    'useSessionFlash' => true
]);
?>
</br>
<div class="suku-bunga-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Suku Bunga', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Advance Search', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        'nama',
        // [
        //     'format'=> 'raw',
        //     'value' => function ($model, $key, $index, $column) {
        //         if($model->is_reguler){
        //             return 'Reguler';
        //         }else{
        //             return 'Non-Reguler';
        //         }
        //     },
        //     'label' => 'tipe'
        //     // 'headerOptions' => ['class' => 'kartik-sheet-style','colspan'=>3],
        //     // 'label' => 'Multiguna Min Tenor'
        // ],
      
        [
            'format'=> 'raw',
            'value' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_columnSBKpr', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style','colspan'=>3],
            // 'headerOptions' => ['class' => 'kartik-sheet-style'],
            'label' => 'Suku Bunga KPR'
        ],
        [
            'format'=> 'raw',
            'value' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_columnSBKprTahunFixed', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'hidden'],
            // 'headerOptions' => ['class' => 'kartik-sheet-style'],
            // 'label' => 'Multiguna Tahun Fixed'
        ],
        [
            'format'=> 'raw',
            'value' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_columnSBKprMinTenor', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'hidden'],
            // 'headerOptions' => ['class' => 'kartik-sheet-style','colspan'=>3],
            // 'label' => 'Multiguna Min Tenor'
        ],
          [
            'format'=> 'raw',
            'value' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_columnSBMultiguna', ['model' => $model]);
            },
            
            'headerOptions' => ['class' => 'kartik-sheet-style','colspan'=>3],
            // 'headerOptions' => ['class' => 'kartik-sheet-style'],
            'label' => 'Suku Bunga Multiguna'
        ],
        [
            'format'=> 'raw',
            'value' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_columnSBMultigunaTahunFixed', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'hidden'],
            // 'label' => 'Multiguna Tahun Fixed'
        ],
        [
            'format'=> 'raw',
            'value' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_columnSBMultigunaMinTenor', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'hidden'],

            // 'headerOptions' => ['class' => 'kartik-sheet-style','colspan'=>3],
            // 'label' => 'Multiguna Min Tenor'
        ],
        [
            'label' => 'Status',
            'value' => function($model){
                if($model->is_approved == 1){
                    return "Publish";
                }
                if($model->is_approved == 0){
                    return "Pending";
                }
            }
        ],
        
        ['attribute' => 'id', 'visible' => false],
        ['attribute' => 'lock', 'visible' => false],
        [
            'header'=>'Actions',
            'value'=> function ($model, $key, $index, $column) {
                   $a = Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update','id'=>$model->id], ['class' => '', 'id' => 'popupModal']);      
                   $b =   Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete-page','id'=>$model->id], ['class' => '', 'id' => 'popupModal']);      
                   return $a.$b;
              },
             'format' => 'raw'
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'striped' => false,
        'hover'=>true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-suku-bunga']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => '',
        //     '{export}',
        //     ExportMenu::widget([
        //         'dataProvider' => $dataProvider,
        //         'columns' => $gridColumn,
        //         'target' => ExportMenu::TARGET_BLANK,
        //         'fontAwesome' => true,
        //         'dropdownOptions' => [
        //             'label' => 'Full',
        //             'class' => 'btn btn-default',
        //             'itemsBefore' => [
        //                 '<li class="dropdown-header">Export All Data</li>',
        //             ],
        //         ],
        //         'exportConfig' => [
        //             ExportMenu::FORMAT_PDF => false
        //         ]
        //     ]) ,
        // ],
    ]); ?>

</div>
