<div class="form-group" id="add-suku-bunga-rules-multiguna">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;
use app\models\SukuBungaRules;


$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'SukuBungaRules',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'is_multiguna' => [
            'label' =>'',
            'type' => TabularForm::INPUT_RAW,
            // 'visible' =>false,
            'value' => function($m, $k, $i, $w) { 
                return Html::textInput("SukuBungaRules[".$k."][is_multiguna]", SukuBungaRules::TYPE_MULTIGUNA, ['class'=>'form-control hidden']);
            }
        ],
       'order_number' => ['type' => TabularForm::INPUT_RAW,
             'value' => function($m, $k, $i, $w) { 

                $item = null;
                if(!empty($m['order_number'])){
                    $item = $m['order_number'];
                }
                return Html::textInput("SukuBungaRules[".$k."][order_number]", $item, ['class'=>'form-control']);
            }
        ],
        'suku_bunga_fixed' => ['type' => TabularForm::INPUT_RAW,
             'value' => function($m, $k, $i, $w) { 
                $item = null;
                if(!empty($m['suku_bunga_fixed'])){
                    $item = $m['suku_bunga_fixed'];
                }
                return Html::textInput("SukuBungaRules[".$k."][suku_bunga_fixed]",$item , ['class'=>'form-control']);
            }
        ],
        'tahun_fixed' => ['type' => TabularForm::INPUT_RAW,
             'value' => function($m, $k, $i, $w) { 
                $item = null;
                if(!empty($m['tahun_fixed'])){
                    $item = $m['tahun_fixed'];
                }
                return Html::textInput("SukuBungaRules[".$k."][tahun_fixed]",$item , ['class'=>'form-control']);
            }
        ],
        'min_tenor' => ['type' => TabularForm::INPUT_RAW,
             'value' => function($m, $k, $i, $w) { 
                $item = 0;
                if(!empty($m['min_tenor'])){
                    $item = $m['min_tenor'];
                }
                return Html::textInput("SukuBungaRules[".$k."][min_tenor]",$item , ['class'=>'form-control']);
            }
        ],
        // "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowSukuBungaRulesMultiguna(' . $key . '); return false;', 'id' => 'suku-bunga-rules-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Suku Bunga Rules', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowSukuBungaRulesMultiguna()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

