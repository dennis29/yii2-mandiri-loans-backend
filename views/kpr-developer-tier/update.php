<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KprDeveloperTier */

$this->title = 'Update Kpr Developer Tier: ' . ' ' . $model->kpr_id;
$this->params['breadcrumbs'][] = ['label' => 'Kpr Developer Tier', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kpr_id, 'url' => ['view', 'kpr_id' => $model->kpr_id, 'developer_tier_id' => $model->developer_tier_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kpr-developer-tier-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
