<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KprDeveloperTier */

$this->title = 'Create Kpr Developer Tier';
$this->params['breadcrumbs'][] = ['label' => 'Kpr Developer Tier', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpr-developer-tier-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
