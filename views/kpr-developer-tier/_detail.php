<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\KprDeveloperTier */

?>
<div class="kpr-developer-tier-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->kpr_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
            'attribute' => 'kpr.id',
            'label' => 'Kpr',
        ],
        [
            'attribute' => 'developerTier.id',
            'label' => 'Developer Tier',
        ],
        ['attribute' => 'lock', 'visible' => false],
        ['attribute' => 'id', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>