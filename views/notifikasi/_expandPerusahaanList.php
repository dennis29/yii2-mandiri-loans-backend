<?php
use app\models\Perusahaan;


$column = ['name'];
$obj = null;
if(!empty($model2['id'])){

$obj = Perusahaan::findOne($model2['id']);
}

?>
<table class="table table-bordered">
	
	<tr class="">
		<td class="">
		</td>
		<td class="">
		Sebelum
		</td>
		<td class="">
		Sesudah
			
		</td>
	</tr>
	<?php foreach ($column as $item) :?>
		<?php if($model1[$item] != $model2[$item]):?>
			<tr class="danger">
		<?php else:?>
			<tr class="">
		<?php endif?>
			<td class="">
				<?= $item?> 
				
			</td>
			<td class="">
				<?= $model1[$item]?> 
			</td>
			<td class="">
				<?= $model2[$item]?> 
				
			</td>
		</tr>
	<?php endforeach ?>

	<?php 
		$sebelumPT = "";
		$sesudahPT = "";
		if(!empty($model1['perusahaanTier']['nama'])){
			$sebelumPT = $model1['perusahaanTier']['nama'];
		}
		if(!empty($model2['perusahaanTier']['nama'])){
			$sesudahPT = $model2['perusahaanTier']['nama'];
		} 
	?>
	<?php if($sebelumPT!=$sesudahPT):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Perusahaan Tier
		</td>
		<td class="">
			<?= $sebelumPT?> 
		</td>
		<td class="">
			<?= $sesudahPT?> 
		</td>
	</tr>

	<?php 
		$sebelumPG = "";
		$sesudahPG = "";
		if(!empty($model1['perusahaanGroup']['name'])){
			$sebelumPG = $model1['perusahaanGroup']['name'];
		} 
		if(!empty($model2['perusahaanGroup']['name'])){
			$sesudahPG = $model2['perusahaanGroup']['name'];
		} 
	?>
	<?php if($sebelumPG!=$sesudahPG):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Perusahaan Group
		</td>
		<td class="">
			<?= $sebelumPG?> 
		</td>
		<td class="">
			<?= $sesudahPG?> 
		</td>
	</tr>
	
	
</table>

