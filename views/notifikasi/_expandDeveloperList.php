<?php
use app\models\Developer;


$column = ['nama'];
$obj = Developer::findOne($model2['id']);

?>
<table class="table table-bordered">
	
	<tr class="">
		<td class="">
		</td>
		<td class="">
		Sebelum
		</td>
		<td class="">
		Sesudah
			
		</td>
	</tr>
	<?php foreach ($column as $item) :?>
		<?php if($model1[$item] != $model2[$item]):?>
			<tr class="danger">
		<?php else:?>
			<tr class="">
		<?php endif?>
			<td class="">
				<?= $item?> 
				
			</td>
			<td class="">
				<?= $model1[$item]?> 
			</td>
			<td class="">
				<?= $model2[$item]?> 
				
			</td>
		</tr>
	<?php endforeach ?>
	
	<?php 
		$sebelumDT = "";
		$sesudahDT = "";
		if(!empty($model1['developerTier']['nama'])){
			$sebelumDT = $model1['developerTier']['nama'];
		}
		if(!empty($model2['developerTier']['nama'])){
			$sesudahDT = $model2['developerTier']['nama'];
		} 
	?>
	<?php if($sebelumDT!=$sesudahDT):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Developer Tier
		</td>
		<td class="">
			<?= $sebelumDT?> 
		</td>
		<td class="">
			<?= $sesudahDT?> 
		</td>
	</tr>

	<?php 
		$sebelumDG = "";
		$sesudahDG = "";
		if(!empty($model1['developerGroup']['name'])){
			$sebelumDG = $model1['developerGroup']['name'];
		} 
		if(!empty($model2['developerGroup']['name'])){
			$sesudahDG = $model2['developerGroup']['name'];
		} 
	?>
	<?php if($sebelumDG!=$sesudahDG):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Developer Group
		</td>
		<td class="">
			<?= $sebelumDG?> 
		</td>
		<td class="">
			<?= $sesudahDG?> 
		</td>
	</tr>
	
</table>

