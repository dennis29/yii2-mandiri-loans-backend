<?php
use app\models\Proyek;


$column = ['nama','tanggal_jatuh_tempo', 'min_harga', 'max_harga', 'longitude','latitude','website'];
// if(!empty($model2['id'])){
// 	$obj = Proyek::findOne($model2['id']);	
// }

?>
<table class="table table-bordered">
	
	<tr class="">
		<td class="">
		</td>
		<td class="">
		Sebelum
		</td>
		<td class="">
		Sesudah
			
		</td>
	</tr>
	<?php foreach ($column as $item) :?>
		<?php if($model1[$item] != $model2[$item]):?>
			<tr class="danger">
		<?php else:?>
			<tr class="">
		<?php endif?>
			<td class="">
				<?= $item?> 
				
			</td>
			<td class="">
				<?= $model1[$item]?> 
			</td>
			<td class="">
				<?= $model2[$item]?> 
				
			</td>
		</tr>
	<?php endforeach ?>

	<?php 
		$sebelumD = "";
		$sesudahD = "";
		if(!empty($model1['developer']['nama'])){
			$sebelumD = $model1['developer']['nama'];
		} 
		if(!empty($model2['developer']['nama'])){
			$sesudahD = $model2['developer']['nama'];
		} 
	?>
	<?php if($sebelumD!=$sesudahD):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Developer
		</td>
		<td class="">
			<?= $sebelumD?> 
		</td>
		<td class="">
			<?= $sesudahD?> 
		</td>
	</tr>

	<?php 
		$sebelumR = "";
		$sesudahR = "";
		if(!empty($model1['region']['name'])){
			$sebelumR = $model1['region']['name'];
		}
		if(!empty($model2['region']['name'])){
			$sesudahR = $model2['region']['name'];
		}
	?>
	<?php if($sebelumR!=$sesudahR):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Region
		</td>
		<td class="">
			<?= $sebelumR?> 
		</td>
		<td class="">
			<?= $sesudahR?> 
		</td>
	</tr>

	<?php 
		$sebelumA = "";
		$sesudahA = "";
		if(!empty($model1['area']['name'])){
			$sebelumA = $model1['area']['name'];
		}
		if(!empty($model2['area']['name'])){
			$sesudahA = $model2['area']['name'];
		} 
	?>
	<?php if($sebelumA!=$sesudahA):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Area
		</td>
		<td class="">
			<?= $sebelumA?> 
		</td>
		<td class="">
			<?= $sesudahA?> 
		</td>
	</tr>

</table>