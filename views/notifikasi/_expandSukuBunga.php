<?php
use app\models\SukuBunga;


$column = ['nama'];
$obj = SukuBunga::findOne($model2['id']);
$countSebelum = 0;
$countSesudah = 0;
if(!empty($model1['sukuBungaRules'])){
	$countSebelum = count($model1['sukuBungaRules']);
}
if(!empty($model2['sukuBungaRules'])){
	$countSesudah = count($model2['sukuBungaRules']);
}
?>
<table class="table table-bordered">
	
	<tr class="">
		<td class="">
		</td>
		<td class="" colspan="<?=$countSebelum?>">
		Sebelum
		</td>
		<td class="" colspan="<?=$countSesudah?>">
		Sesudah
		</td>
	</tr>
	<?php foreach ($column as $item) :?>
		<?php if($model1[$item] != $model2[$item]):?>
			<tr class="danger">
		<?php else:?>
			<tr class="">
		<?php endif?>
			<td class="">
				<?= $item?> 	
			</td>
			<td class="" colspan="<?=$countSebelum?>">
				<?= $model1[$item]?> 
			</td>
			<td class="" colspan="<?=$countSesudah?>">
				<?= $model2[$item]?> 
				
			</td>
		</tr>
	<?php endforeach ?>
	<?php 
		$sebelumIM = ""; 
		$sesudahIM = ""; 
		if(!empty($model1['sukuBungaRules'])){
			$data = [];
			foreach ($model1['sukuBungaRules'] as $item) {
				$data[] = $item['is_multiguna'];
			}
			$sebelumIM =  implode("</td><td>", $data);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['sukuBungaRules'])){
			$data = [];
			foreach ($model2['sukuBungaRules'] as $item) {
				$data[] = $item['is_multiguna'];
			}
			$sesudahIM =  implode("</td><td>", $data);
		}
	?>
	<?php if($sebelumIM!=$sesudahIM):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Is Multiguna
		</td>
		<td class="">
			<?= $sebelumIM?>
			<!-- <?php echo $sebelumIM == 0 ? 'Suku Bunga KPR':'Suku Bunga Multiguna';?>  -->
		</td>
		<td class="">
			<?= $sesudahIM?> 
		</td>
	</tr>
	
	<?php 
		$sebelumON = ""; 
		$sesudahON = ""; 
		if(!empty($model1['sukuBungaRules'])){
			$data = [];
			foreach ($model1['sukuBungaRules'] as $item) {
				$data[] = $item['order_number'];
			}
			$sebelumON =  implode("</td><td>", $data);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['sukuBungaRules'])){
			$data = [];
			foreach ($model2['sukuBungaRules'] as $item) {
				$data[] = $item['order_number'];
			}
			$sesudahON =  implode("</td><td>", $data);
		}
	?>
	<?php if($sebelumON!=$sesudahON):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Order Number
		</td>
		<td class="">
			<?= $sebelumON?> 
		</td>
		<td class="">
			<?= $sesudahON?> 
		</td>
	</tr>

	<?php 
		$sebelumSKf = ""; 
		$sesudahSKf = ""; 
		if(!empty($model1['sukuBungaRules'])){
			$data = [];
			foreach ($model1['sukuBungaRules'] as $item) {
				$data[] = $item['suku_bunga_fixed'];
			}
			$sebelumSKf =  implode("</td><td>", $data);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['sukuBungaRules'])){
			$data = [];
			foreach ($model2['sukuBungaRules'] as $item) {
				$data[] = $item['suku_bunga_fixed'];
			}
			$sesudahSKf =  implode("</td><td>", $data);
		}
	?>
	<?php if($sebelumSKf!=$sesudahSKf):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Suku Bunga Fixed
		</td>
		<td class="">
			<?= $sebelumSKf?> 
		</td>
		<td class="">
			<?= $sesudahSKf?> 
		</td>
	</tr>

	<?php 
		$sebelumTF = ""; 
		$sesudahTF = ""; 
		if(!empty($model1['sukuBungaRules'])){
			$data = [];
			foreach ($model1['sukuBungaRules'] as $item) {
				$data[] = $item['tahun_fixed'];
			}
			$sebelumTF =  implode("</td><td>", $data);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['sukuBungaRules'])){
			$data = [];
			foreach ($model2['sukuBungaRules'] as $item) {
				$data[] = $item['tahun_fixed'];
			}
			$sesudahTF =  implode("</td><td>", $data);
		}
	?>
	<?php if($sebelumTF!=$sesudahTF):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Tahun Fixed
		</td>
		<td class="">
			<?= $sebelumTF?> 
		</td>
		<td class="">
			<?= $sesudahTF?> 
		</td>
	</tr>

	<?php 
		$sebelumMT = ""; 
		$sesudahMT = ""; 
		if(!empty($model1['sukuBungaRules'])){
			$data = [];
			foreach ($model1['sukuBungaRules'] as $item) {
				$data[] = $item['min_tenor'];
			}
			$sebelumMT =  implode("</td><td>", $data);
			// $sebelumMT = implode(", ", $data);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['sukuBungaRules'])){
			$data = [];
			foreach ($model2['sukuBungaRules'] as $item) {
				$data[] = $item['min_tenor'];
			}
			$sesudahMT =  implode("</td><td>", $data);
		}
	?>
	<?php if($sebelumMT!=$sesudahMT):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Min Tenor
		</td>
		<td class="">
			<?= $sebelumMT?> 
		</td>
		<td class="">
			<?= $sesudahMT?> 
		</td>
	</tr>
</table>
