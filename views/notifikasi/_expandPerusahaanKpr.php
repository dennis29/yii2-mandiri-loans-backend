<?php
use app\models\PerusahaanKpr;


$column = ['name'];
$obj = PerusahaanKpr::findOne($model2['id']);

?>
<table class="table table-bordered">
	
	<tr class="">
		<td class="">
		</td>
		<td class="">
		Sebelum
		</td>
		<td class="">
		Sesudah
			
		</td>
	</tr>
	<?php foreach ($column as $item) :?>
		<?php if($model1[$item] != $model2[$item]):?>
			<tr class="danger">
		<?php else:?>
			<tr class="">
		<?php endif?>
			<td class="">
				<?= $item?> 
				
			</td>
			<td class="">
				<?= $model1[$item]?> 
			</td>
			<td class="">
				<?= $model2[$item]?> 
				
			</td>
		</tr>
	<?php endforeach ?>

	<?php 
		$sebelumG = "";
		$sesudahG = "";
		if(!empty($model1['group']['name'])){
			$sebelumG = $model1['group']['name'];
		}
		if(!empty($model2['group']['name'])){
			$sesudahG = $model2['group']['name'];
		} 
	?>
	<?php if($sebelumG!=$sesudahG):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Group
		</td>
		<td class="">
			<?= $sebelumG?> 
		</td>
		<td class="">
			<?= $sesudahG?> 
		</td>
	</tr>

	<?php 
		$sebelumPKT = "";
		$sesudahPKT = "";
		if(!empty($model1['perusahaanKprTier']['name'])){
			$sebelumPKT = $model1['perusahaanKprTier']['name'];
		}
		if(!empty($model2['perusahaanKprTier']['name'])){
			$sesudahPKT = $model2['perusahaanKprTier']['name'];
		} 
	?>
	<?php if($sebelumPKT!=$sesudahPKT):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Perusahaan KPR Tier
		</td>
		<td class="">
			<?= $sebelumPKT?> 
		</td>
		<td class="">
			<?= $sesudahPKT?> 
		</td>
	</tr>
	
	
</table>

