<?php
use app\models\Kta;


$column = ['nama', 'expired', 'keterangan','pop_up','target_market_text','ketentuan_umum_text','limit_kredit_text','jangka_waktu_kredit_text','suku_bunga_text','ketentuan_dbr_text'];
// if(!empty($model2['id'])){
// 	$obj = Kta::findOne($model2['id']);
// }
$countSebelum = 0;
$countSesudah = 0;
if(!empty($model1['ktaRules'])){
	$countSebelum = count($model1['ktaRules']);
}
if(!empty($model2['ktaRules'])){
	$countSesudah = count($model2['ktaRules']);
}
?>
<table class="table table-bordered">
	
	<tr class="">
		<td class="">
		</td>
		<td class="" colspan="<?=$countSebelum?>">
		Sebelum
		</td>
		<td class="" colspan="<?=$countSesudah?>">
		Sesudah
		</td>
	</tr>
	<?php foreach ($column as $item) :?>
		<?php if($model1[$item] != $model2[$item]):?>
			<tr class="danger">
		<?php else:?>
			<tr class="">
		<?php endif?>
			<td class="">
				<?= $item?> 	
			</td>
			<td class="" colspan="<?=$countSebelum?>">
				<?= $model1[$item]?> 
			</td>
			<td class="" colspan="<?=$countSesudah?>">
				<?= $model2[$item]?> 
				
			</td>
		</tr>
	<?php endforeach ?>
	<?php 
		$sebelumPT = ""; 
		$sesudahPT = ""; 
		if(!empty($model1['perusahaanTiers'])){
			$name = [];
			foreach ($model1['perusahaanTiers'] as $item) {
				$name[] = $item['nama'];
			}
			$sebelumPT =  implode(", ", $name);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['perusahaanTiers'])){
			$name = [];
			foreach ($model2['perusahaanTiers'] as $item) {
				$name[] = $item['nama'];
			}
			$sesudahPT =  implode(", ", $name);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
	?>
	<?php if($sebelumPT!=$sesudahPT):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			KTA Perusahaan Tier
		</td>
		<td class="" colspan="<?=$countSebelum?>">
			<?= $sebelumPT?> 
		</td>
		<td class="" colspan="<?=$countSesudah?>">
			<?= $sesudahPT?> 
		</td>
	</tr>
	<?php 
		$sebelumTM = ""; 
		$sesudahTM = ""; 
		if(!empty($model1['targ3tMarkets'])){
			$name = [];
			foreach ($model1['targ3tMarkets'] as $item) {
				$name[] = $item['name'];
			}
			$sebelumTM =  implode(", ", $name);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['targ3tMarkets'])){
			$name = [];
			foreach ($model2['targ3tMarkets'] as $item) {
				$name[] = $item['name'];
			}
			$sesudahTM =  implode(", ", $name);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
	?>
	<?php if($sebelumTM!=$sesudahTM):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			KTA Target Market
		</td>
		<td class="" colspan="<?=$countSebelum?>">
			<?= $sebelumTM?> 
		</td>
		<td class="" colspan="<?=$countSesudah?>">
			<?= $sesudahTM?> 
		</td>
	</tr>
	<?php 
		$sebelumMT = ""; 
		$sesudahMT = ""; 
		if(!empty($model1['ktaRules'])){
			$data = [];
			foreach ($model1['ktaRules'] as $item) {
				$data[] = $item['min_tenor'];
			}
			$sebelumMT =  implode("</td><td>", $data);
			// $sebelumMT = implode(", ", $data);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['ktaRules'])){
			$data = [];
			foreach ($model2['ktaRules'] as $item) {
				$data[] = $item['min_tenor'];
			}
			$sesudahMT =  implode("</td><td>", $data);
				// var_dump($data);
				// die();
			// implode(glue, pieces)
			// echo var_dump($name);
		}
	?>
	<?php if($sebelumMT!=$sesudahMT):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Min Tenor
		</td>
		<td class="">
			<?= $sebelumMT?> 
		</td>
		<td class="">
			<?= $sesudahMT?> 
		</td>
	</tr>
	<?php 
		$sebelumMxT = ""; 
		$sesudahMxT = ""; 
		if(!empty($model1['ktaRules'])){
			$data = [];
			foreach ($model1['ktaRules'] as $item) {
				$data[] = $item['max_tenor'];
			}
			$sebelumMxT =  implode("</td><td>", $data);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['ktaRules'])){
			$data = [];
			foreach ($model2['ktaRules'] as $item) {
				$data[] = $item['max_tenor'];
			}
			$sesudahMxT =  implode("</td><td>", $data);
				// var_dump($data);
				// die();
			// implode(glue, pieces)
			// echo var_dump($name);
		}
	?>
	<?php if($sebelumMxT!=$sesudahMxT):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Max Tenor
		</td>
		<td class="">
			<?= $sebelumMxT?> 
		</td>
		<td class="">
			<?= $sesudahMxT?> 
		</td>
	</tr>
	<?php 
		$sebelumSKf = ""; 
		$sesudahSKf = ""; 
		if(!empty($model1['ktaRules'])){
			$data = [];
			foreach ($model1['ktaRules'] as $item) {
				$data[] = $item['suku_bunga_floating'];
			}
			$sebelumSKf =  implode("</td><td>", $data);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['ktaRules'])){
			$data = [];
			foreach ($model2['ktaRules'] as $item) {
				$data[] = $item['suku_bunga_floating'];
			}
			$sesudahSKf =  implode("</td><td>", $data);
				// var_dump($data);
				// die();
			// implode(glue, pieces)
			// echo var_dump($name);
		}
	?>
	<?php if($sebelumSKf!=$sesudahSKf):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Suku Bunga Floating
		</td>
		<td class="">
			<?= $sebelumSKf?> 
		</td>
		<td class="">
			<?= $sesudahSKf?> 
		</td>
	</tr>
	<?php 
		$sebelumSK = ""; 
		$sesudahSK = ""; 
		if(!empty($model1['ktaRules'])){
			$data = [];
			foreach ($model1['ktaRules'] as $item) {
				$data[] = $item['suku_bunga_flat'];
			}
			$sebelumSK =  implode("</td><td>", $data);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['ktaRules'])){
			$data = [];
			foreach ($model2['ktaRules'] as $item) {
				$data[] = $item['suku_bunga_flat'];
			}
			$sesudahSK =  implode("</td><td>", $data);
				// var_dump($data);
				// die();
			// implode(glue, pieces)
			// echo var_dump($name);
		}
	?>
	<?php if($sebelumSK!=$sesudahSK):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Suku Bunga Flat
		</td>
		<td class="">
			<?= $sebelumSK?> 
		</td>
		<td class="">
			<?= $sesudahSK?> 
		</td>
	</tr>
	<?php 
		$sebelumLMn = ""; 
		$sesudahLMn = ""; 
		if(!empty($model1['ktaRules'])){
			$data = [];
			foreach ($model1['ktaRules'] as $item) {
				$data[] = $item['limit_min'];
			}
			$sebelumLMn =  implode("</td><td>", $data);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['ktaRules'])){
			$data = [];
			foreach ($model2['ktaRules'] as $item) {
				$data[] = $item['limit_min'];
			}
			$sesudahLMn =  implode("</td><td>", $data);
				// var_dump($data);
				// die();
			// implode(glue, pieces)
			// echo var_dump($name);
		}
	?>
	<?php if($sebelumLMn!=$sesudahLMn):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Limit Min
		</td>
		<td class="">
			<?= $sebelumLMn?> 
		</td>
		<td class="">
			<?= $sesudahLMn?> 
		</td>
	</tr>
	<?php 
		$sebelumLMx = ""; 
		$sesudahLMx = ""; 
		if(!empty($model1['ktaRules'])){
			$data = [];
			foreach ($model1['ktaRules'] as $item) {
				$data[] = $item['limit_max'];
			}
			$sebelumLMx =  implode("</td><td>", $data);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['ktaRules'])){
			$data = [];
			foreach ($model2['ktaRules'] as $item) {
				$data[] = $item['limit_max'];
			}
			$sesudahLMx =  implode("</td><td>", $data);
				// var_dump($data);
				// die();
			// implode(glue, pieces)
			// echo var_dump($name);
		}
	?>
	<?php if($sebelumLMx!=$sesudahLMx):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Limit Max
		</td>
		<td class="">
			<?= $sebelumLMx?> 
		</td>
		<td class="">
			<?= $sesudahLMx?> 
		</td>
	</tr>
	<?php 
		$sebelumIMn = ""; 
		$sesudahIMn = ""; 
		$data1 = [];
		$data2 = [];
		if(!empty($model1['ktaRules'])){
			// $data = [];
			foreach ($model1['ktaRules'] as $item) {
				$data1[] = $item['income_min'];
			}
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['ktaRules'])){
			$data = [];
			foreach ($model2['ktaRules'] as $item) {
				$data[] = $item['income_min'];
			}
			$sesudahIMn =  implode("</td><td>", $data);
				// var_dump($data);
				// die();
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		for ($i=0; $i < max(count($data1),count($data2)); $i++) {
			if(!empty($data1[$i])){
				if($i!=0){
					$sebelumIMn .=  "</td><td>";				
				}
				$sebelumIMn .=  $data1[$i];				
			} 
			if(!empty($data2[$i])){
				if($i!=0){
					$sesudahIMn .=  "</td><td>";				
				}

				$sesudahIMn .=  $data2[$i];				
			}
			// $sesudahIMn = implode("</td><td>", $data);

		}
	?>
	<?php if($sebelumIMn!=$sesudahIMn):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Income Min
		</td>
		<td class="">
			<?= $sebelumIMn?> 
		</td>
		<td class="">
			<?= $sesudahIMn?> 
		</td>
	</tr>
	<?php 
		$sebelumIMx = ""; 
		$sesudahIMx = ""; 
		if(!empty($model1['ktaRules'])){
			$data = [];
			foreach ($model1['ktaRules'] as $item) {
				$data[] = $item['income_max'];
				
			}
			$sebelumIMx =  implode("</td><td>", $data);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['ktaRules'])){
			$data = [];
			foreach ($model2['ktaRules'] as $item) {
				$data[] = $item['income_max'];
			}
			$sesudahIMx =  implode("</td><td>", $data);
				// var_dump($data);
				// die();
			// implode(glue, pieces)
			// echo var_dump($name);
		}
	?>
	<?php if($sebelumIMx!=$sesudahIMx):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Income Max
		</td>
		<td class="">
			<?= $sebelumIMx?> 
		</td>
		<td class="">
			<?= $sesudahIMx?> 
		</td>
	</tr>
	<?php 
		$sebelumMD = ""; 
		$sesudahMD = ""; 
		if(!empty($model1['ktaRules'])){
			$data = [];
			foreach ($model1['ktaRules'] as $item) {
				$data[] = $item['max_dbr'];
			}
			$sebelumMD =  implode("</td><td>", $data);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['ktaRules'])){
			$data = [];
			foreach ($model2['ktaRules'] as $item) {
				$data[] = $item['max_dbr'];
			}
			$sesudahMD =  implode("</td><td>", $data);
				// var_dump($data);
				// die();
			// implode(glue, pieces)
			// echo var_dump($name);
		}
	?>
	<?php if($sebelumMD!=$sesudahMD):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Max DBR
		</td>
		<td class="">
			<?= $sebelumMD?> 
		</td>
		<td class="">
			<?= $sesudahMD?> 
		</td>
	</tr>
</table>
