<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\models\Notifikasi;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

CrudAsset::register($this);


$this->title = 'Notifikasi';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="notifikasi-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <!-- <p>
        <?= Html::a('Create Notifikasi', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->
<?php 
    $gridColumn = [
        [
            'class' => 'kartik\grid\CheckboxColumn',
            'width' => '20px',
            // 'visible' => false
            'checkboxOptions'=> function($model,$key,$index,$column){
                if($model->status != 0){
                    return ['disabled'=>true];
                    
                }
            }
            
        ],
        ['class' => 'yii\grid\SerialColumn'],
         [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                if($model->code == 1){
                    // return $model->sebelum;
                    return Yii::$app->controller->renderPartial('_expand', ['model1' => json_decode($model->sebelum,true),'model2' => json_decode($model->sesudah,true)]);
                }if($model->code == 2){
                    return Yii::$app->controller->renderPartial('_expandKta', ['model1' => json_decode($model->sebelum,true),'model2' => json_decode($model->sesudah,true)]);
                }
                if($model->code == 3){
                    return Yii::$app->controller->renderPartial('_expandSukuBunga', ['model1' => json_decode($model->sebelum,true),'model2' => json_decode($model->sesudah,true)]);
                }
                if($model->code == 4){
                    return Yii::$app->controller->renderPartial('_expandProyek', ['model1' => json_decode($model->sebelum,true),'model2' => json_decode($model->sesudah,true)]);
                }
                // if($model->code == 5){
                //     return Yii::$app->controller->renderPartial('_expandPerusahaanList', ['model1' => json_decode($model->sebelum,true),'model2' => json_decode($model->sesudah,true)]);
                // }
                // if($model->code == 6){
                //     return Yii::$app->controller->renderPartial('_expandPerusahaanGroup', ['model1' => json_decode($model->sebelum,true),'model2' => json_decode($model->sesudah,true)]);
                // }
                // if($model->code == 7){
                //     return Yii::$app->controller->renderPartial('_expandPerusahaanTier', ['model1' => json_decode($model->sebelum,true),'model2' => json_decode($model->sesudah,true)]);
                // }if($model->code == 8){
                //     return Yii::$app->controller->renderPartial('_expandDeveloperList', ['model1' => json_decode($model->sebelum,true),'model2' => json_decode($model->sesudah,true)]);
                // }if($model->code == 9){
                //     return Yii::$app->controller->renderPartial('_expandDeveloperGroup', ['model1' => json_decode($model->sebelum,true),'model2' => json_decode($model->sesudah,true)]);
                // }if($model->code == 10){
                //     return Yii::$app->controller->renderPartial('_expandDeveloperTier', ['model1' => json_decode($model->sebelum,true),'model2' => json_decode($model->sesudah,true)]);
                // }if($model->code == 11){
                //     return Yii::$app->controller->renderPartial('_expandPerusahaanKpr', ['model1' => json_decode($model->sebelum,true),'model2' => json_decode($model->sesudah,true)]);
                // }if($model->code == 12){
                //     return Yii::$app->controller->renderPartial('_expandPerusahaanKprTier', ['model1' => json_decode($model->sebelum,true),'model2' => json_decode($model->sesudah,true)]);
                // }
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
                'expandOneOnly' => true,
        ],
        ['attribute' => 'id', 'visible' => false],
        // 'code',
        // [
        //     'label' => 'Status',
        //     'width' => '20px',
        //     'options'=>[
        //         "data-toggle"=>"toggle",
        //         "class"=>"checkbox"
        //     ],
        //     'format'=>'raw',
        //     'value'=> function($model){
        //         if($model->status == 1){
        //             $obj = true;
        //         }else{
        //             $obj = false;
        //         }
        //         if(Yii::$app->user->identity->status == 10){
        //             return SwitchInput::widget([
        //                 'name' => 'status_13',
        //                 'options'=>[
        //                     'checked'=>$obj,
        //                     'onChange'=>'changeStatus("'.$model->id.'",this.checked)',
        //                 ],
        //                 'pluginOptions' => [
        //                     'onText' => 'Approved',
        //                     'offText' => 'Reject',
        //                     'onColor' => 'primary',
        //                     'offColor' => 'default',
        //                     ]
        //                 ]);
        //         }else{
        //             return SwitchInput::widget([
        //                 'name' => 'status_3',
        //                 'disabled' => 'true',
        //                 'options'=>[
        //                     'checked'=>$obj,
        //                 ],
        //                 'pluginOptions' => [
        //                     'onText' => 'Approved',
        //                     'offText' => 'Reject',
        //                     'onColor' => 'primary',
        //                     'offColor' => 'default',
        //                     ]
        //                 ]);
        //         }
        //     }        
        // ],
        [
            'attribute' => 'code',
            'format' => 'raw',
                'value' => function($model, $key, $index, $column) {
                        if($model->code == 1){
                            return 'KPR';
                        }if($model->code == 2){
                            return 'KTA';
                        }if($model->code == 3){
                            return 'Suku Bunga';
                        }if($model->code == 4){
                            return 'Proyek';
                        }
                        // if($model->code == 5){
                        //     return 'Perusahaan List';
                        // }if($model->code == 6){
                        //     return 'Perusahaan Group';
                        // }if($model->code == 7){
                        //     return 'Perusahaan Tier';
                        // }if($model->code == 8){
                        //     return 'Developer List';
                        // }if($model->code == 9){
                        //     return 'Developer Group';
                        // }if($model->code == 10){
                        //     return 'Developer Tier';
                        // }if($model->code == 11){
                        //     return 'Perusahaan KPR';
                        // }if($model->code == 12){
                        //     return 'Perusahaan KPR Tier';
                        // }
                },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => [1=>'kpr',2=>'kta',3=>'suku bunga',4 =>'proyek'],
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Code']
        ],
        [
            'label' => 'Message',
            'format' => 'raw',
                'value' => function($model, $key, $index, $column) {
                        $obj = json_decode($model->sebelum,true);
                        if(in_array($model->code, [Notifikasi::Code_PERUSAHAANLIST,
                            Notifikasi::Code_PERUSAHAANGROUP,
                            Notifikasi::Code_DEVELOPERGROUP,
                            Notifikasi::Code_PERUSAHAANKPR,
                            Notifikasi::Code_PERUSAHAANKPRTIER
                            ]) ){//tambahin pake koma

                            $nama = $obj['name'];
                        }else{
                            $nama = $obj['nama'];
                            
                        }
                        if(!empty($obj)){
                            return "Terdapat perubahan pada ".$nama;
                        }else{
                                if($model->code == 1){
                                    return "Anda telah melakukan penambahan data pada KPR";
                                }
                                if($model->code == 2){
                                    return "Anda telah melakukan penambahan data pada KTA";
                                }
                                if($model->code == 3){
                                    return "Anda telah melakukan penambahan data pada Suku Bunga";
                                }
                                if($model->code == 4){
                                    return "Anda telah melakukan penambahan data pada Proyek";
                                }
                        }
                        // return json_encode($obj->name);
                }
        ],
        [
            'attribute' => 'status',
            'label' => 'Status',
            'format' => 'raw',
                'value' => function($model, $key, $index, $column) {
                    if($model->status==1){
                        return "approved";
                    }else
                    if($model->status==2){
                        return "rejected";
                    }else{
                        $modelSebelum = json_decode($model->sebelum,true);
                        return Html::a(
                            '<i class="">Approved</i>',
                            Url::to(['notifikasi/change-approved', 'id' => $model->id,'objId'=>$modelSebelum['id']]), 
                            [
                                'id'=>'grid-custom-button',
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to Approved?'),
                                // 'data-pjax'=>true,
                                'class'=>'button btn btn-success',
                            ]
                        ).'&nbsp;'.Html::a(
                            '<i class="">Reject</i>',
                            Url::to(['notifikasi/change-reject', 'id' => $model->id,'objId'=>$modelSebelum['id']]),
                            [
                                'id'=>'grid-custom-button',
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to Reject?'),
                                // 'data-pjax'=>true,
                                'class'=>'button btn btn-danger',
                            ]
                        );
                    }
                },

            'filterType' => GridView::FILTER_SELECT2,
            'filter' => [0=>'not set',1=>'approve',2=>'reject'],
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Status']

            // 'filterInputOptions' =>

        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
        ],
    ]; 
    ?>
    <form method="post" action="<?= Url::to(['notifikasi/bulk-respon'],true)?>">
    <input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>" value="<?=Yii::$app->request->csrfToken?>"/>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'filterModel' => $searchModel,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-notifikasi']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
            'after'=>Html::a('<i class="glyphicon glyphicon-ok"></i>&nbsp; Approve Selected',
                                ['bulk-approve'],
                                [
                                    "class"=>"btn btn-success btn-xs",
                                    // 'role'=>'modal-remote-bulk',
                                    // 'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    // 'data-request-method'=>'post',
                                    // 'data-confirm-title'=>'Are you sure?',
                                    'name'=>'aksi',
                                    'value'=>'approve',
                                    'type'=>'submit',
                                    // 'data-toggle'=>"modal",
                                    // 'data-target'=>'#ajaxCrudModal',
                                    'data-confirm'=>'Are you sure want to Approve selected  item',
                                    // 'data-confirm-message'=>'Are you sure want to approve this item'
                                ]
                        ).     "&nbsp;".
                        Html::a('<i class="glyphicon glyphicon-ok"></i>&nbsp; Reject Selected',
                                ['bulk-reject'],
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    // 'role'=>'modal-remote-bulk',
                                    // 'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'name'=>'aksi',
                                    'value'=>'reject',
                                    'type'=>'submit',
                                    // 'data-toggle'=>"modal",
                                    // 'data-target'=>'#ajaxCrudModal'
                                    // 'data-request-method'=>'post',
                                    // 'data-confirm-title'=>'Are you sure?',
                                    'data-confirm'=>'Are you sure want to Reject all selected item'
                                ]
                        ).'<br/>'. 
                         Html::a('<i class="glyphicon glyphicon-ok"></i>&nbsp; Approve All ('.number_format($dataProvider->getTotalCount()).')',
                                ['approve-all'],
                                [
                                    "class"=>"btn btn-success btn-xs",
                                    'style'=>'margin-top:10px;',
                                    // 'role'=>'modal-remote-bulk',
                                    // 'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    // 'data-toggle'=>"modal",
                                    // 'data-target'=>'#ajaxCrudModal'
                                    // 'data-request-method'=>'post',
                                    // 'data-confirm-title'=>'Are you sure?',
                                    'data-confirm'=>'Are you sure want to Approve all these item'
                                ]
                        ). '&nbsp;'. 
                         Html::a('<i class="glyphicon glyphicon-ok "></i>&nbsp; Reject All ('.number_format($dataProvider->getTotalCount()).')',
                                ['reject-all'],
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    'style'=>'margin-top:10px;',
                                    // 'role'=>'modal-remote-bulk',
                                    // 'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    // 'data-toggle'=>"modal",
                                    // 'data-target'=>'#ajaxCrudModal'
                                    // 'data-request-method'=>'post',
                                    // 'data-confirm-title'=>'Are you sure?',
                                    'data-confirm'=>'Are you sure want to Reject all these item'
                                ]
                        ).       
                        '<div class="clearfix"></div>', 
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
        ],
    ]); ?>
    </form>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'header'=>'<h3>Apakah Anda yakin?</h3>',
    "footer"=>"",// always need it for jquery plugin
])?>
 <div class="form-group">
        <?= Html::submitButton('Submit' , 
            [
                'class' =>'btn btn-primary',
                'name'=> 'aksi',
                'value' => 'approve'
            ]) 
        ?>
    </div>
<?php Modal::end(); ?>