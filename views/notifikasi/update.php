<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Notifikasi */

$this->title = 'Update Notifikasi: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Notifikasi', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="notifikasi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
