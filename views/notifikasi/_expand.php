<?php
use app\models\Kpr;

$counter = 0;
$column = ['order_number','nama', 'keterangan','profil_nasabah_text', 'peruntukan_text', 'market_text', 'benefit_text', 'suku_bunga_kpr_text', 'suku_bunga_multiguna_text', 'peserta_text'];
$column_name = ['order number','nama', 'Summary Kpr','Summary Multiguna', 'peruntukan text', 'market text', 'benefit text', 'suku bunga kpr', 'suku bunga multiguna text', 'peserta text'];
// if(!empty($model2['id'])){
// $obj = Kpr::findOne($model2['id']);
// }
?>
<table class="table table-bordered">
	
	<tr class="">
		<td class="">
		</td>
		<td class="">
		Sebelum
		</td>
		<td class="">
		Sesudah
			
		</td>
	</tr>
	<?php foreach ($column as $item) :?>
		<?php if($model1[$item] != $model2[$item]):?>
			<tr class="danger">
		<?php else:?>
			<tr class="">
		<?php endif?>
			<td class="">
				<?= $column_name[$counter];?> 
				
			</td>
			<td class="">
				<?= $model1[$item]?> 
			</td>
			<td class="">
				<?= $model2[$item]?> 
				
			</td>
		</tr>
	<?php $counter++;endforeach ?>
	<?php 
		$sebelumDT = ""; 
		$sesudahDT = ""; 
		if(!empty($model1['developerTiers'])){
			$name = [];
			foreach ($model1['developerTiers'] as $item) {
		$name[] = $item['nama'];
			}
			$sebelumDT =  implode(", ", $name);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['developerTiers'])){
			$name = [];
			foreach ($model2['developerTiers'] as $item) {
				$name[] = $item['nama'];
			}
			$sesudahDT =  implode(", ", $name);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		
	?>
	<?php if($sebelumDT!=$sesudahDT):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Developer Tier
		</td>
		<td class="">
			<?= $sebelumDT?> 
		</td>
		<td class="">
			<?= $sesudahDT?> 
		</td>
	</tr>
	<?php 
		$sebelumPK = ""; 
		$sesudahPK = ""; 
		if(!empty($model1['profesiKprs'])){
			$name = [];
			foreach ($model1['profesiKprs'] as $item) {
				$name[] = $item['nama'];
			}
			$sebelumPK =  implode(", ", $name);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['profesiKprs'])){
			$name = [];
			foreach ($model2['profesiKprs'] as $item) {
				$name[] = $item['nama'];
			}
			$sesudahPK =  implode(", ", $name);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
	?>
	<?php if($sebelumPK!=$sesudahPK):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Profesi KPR
		</td>
		<td class="">
			<?= $sebelumPK?> 
		</td>
		<td class="">
			<?= $sesudahPK?> 
		</td>
	</tr>
	<?php 
		$sebelumSK = ""; 
		$sesudahSK = ""; 
		if(!empty($model1['sukuBungas'])){
			$name = [];
			foreach ($model1['sukuBungas'] as $item) {
				$name[] = $item['nama'];
			}
			$sebelumSK =  implode(", ", $name);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['sukuBungas'])){
			$name = [];
			foreach ($model2['sukuBungas'] as $item) {
				$name[] = $item['nama'];
			}
			$sesudahSK =  implode(", ", $name);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
	?>
	<?php if($sebelumSK!=$sesudahSK):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Suku Bunga
		</td>
		<td class="">
			<?= $sebelumSK?> 
		</td>
		<td class="">
			<?= $sesudahSK?> 
		</td>
	</tr>
	<?php 
		$sebelumTM = ""; 
		$sesudahTM = ""; 
		if(!empty($model1['targ3tMarkets'])){
			$name = [];
			foreach ($model1['targ3tMarkets'] as $item) {
				$name[] = $item['name'];
			}
			$sebelumTM =  implode(", ", $name);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['targ3tMarkets'])){
			$name = [];
			foreach ($model2['targ3tMarkets'] as $item) {
				$name[] = $item['name'];
			}
			$sesudahTM =  implode(", ", $name);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
	?>
	<?php if($sebelumTM!=$sesudahTM):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Target Market
		</td>
		<td class="">
			<?= $sebelumTM?> 
		</td>
		<td class="">
			<?= $sesudahTM?> 
		</td>
	</tr>
	<?php 
		$sebelumPT = ""; 
		$sesudahPT = ""; 
		if(!empty($model1['perusahaanKprTiers'])){
			$name = [];
			foreach ($model1['perusahaanKprTiers'] as $item) {
				$name[] = $item['name'];
			}
			$sebelumPT =  implode(", ", $name);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		if(!empty($model2['perusahaanKprTiers'])){
			$name = [];
			foreach ($model2['perusahaanKprTiers'] as $item) {
				$name[] = $item['name'];
			}
			$sesudahPT =  implode(", ", $name);
			// implode(glue, pieces)
			// echo var_dump($name);
		}
		
	?>
	<?php if($sebelumPT!=$sesudahPT):?>
	<tr class="danger">
	<?php else:?>
	<tr class="">
	<?php endif?>
		<td class="">
			Perusahaan Tier
		</td>
		<td class="">
			<?= $sebelumPT?> 
		</td>
		<td class="">
			<?= $sesudahPT?> 
		</td>
	</tr>
</table>