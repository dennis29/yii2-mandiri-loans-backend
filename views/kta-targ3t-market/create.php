<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KtaTarg3tMarket */

$this->title = 'Create Kta Targ3t Market';
$this->params['breadcrumbs'][] = ['label' => 'Kta Targ3t Market', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kta-targ3t-market-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
