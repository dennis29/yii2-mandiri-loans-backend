<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KtaTarg3tMarket */

$this->title = 'Update Kta Targ3t Market: ' . ' ' . $model->kta_id;
$this->params['breadcrumbs'][] = ['label' => 'Kta Targ3t Market', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kta_id, 'url' => ['view', 'kta_id' => $model->kta_id, 'targ3t_market_id' => $model->targ3t_market_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kta-targ3t-market-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
