<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Reward */
?>
<div class="reward-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
