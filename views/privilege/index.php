<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


$this->title = 'Privilege';
echo 'Privilege ini berfungsi untuk memberikan hak akses terhadap menu-menu yang ada di web ini, berlaku untuk seluruh Admin biasa.';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<br/>
<br/>
<div class="privilege-index">

<?php $form = ActiveForm::begin(['action'=>Url::to(['privilege/save'],true)]); ?>

<?php foreach ($dataProvider->getModels() as $key) :?>
    <div class="col-md-4">
        <input type="Checkbox" name="nama[]" value="<?= $key->id?>" <?= ($key->status==0)?'':'checked'?>>
        <?php echo $key->nama?>        
    </div>
<?php endforeach ?>
<div class="col-md-12" style="margin-top:10px">
    
        <?= Html::button('Save', ['class' => 'btn btn-primary','type'=>'submit']) ?>
</div>
<div class="clearfix"></div>
<?php $form = ActiveForm::end(); ?>
