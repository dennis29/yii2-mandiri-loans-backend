<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Priviledge */

$this->title = 'Update Priviledge: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Priviledge', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="priviledge-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
