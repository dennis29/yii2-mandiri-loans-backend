<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PerusahaanKpr */

$this->title = 'Create Perusahaan Kpr';
$this->params['breadcrumbs'][] = ['label' => 'Perusahaan Kpr', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perusahaan-kpr-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
