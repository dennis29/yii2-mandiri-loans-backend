<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\PerusahaanKprSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use kartik\alert\AlertBlock;
use kartik\alert\Alert;
use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\bootstrap\Modal;

CrudAsset::register($this);

$this->title = 'Perusahaan Kpr';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<?php $form = ActiveForm::begin(['action'=>Url::to(['perusahaan-kpr/import'],true),'options' => ['enctype' => 'multipart/form-data']]); ?>
<?= $form->field($excelModel, 'id')->hiddenInput()->label(false) ?>
<?= $form->field($excelModel, 'file')->fileInput(['accept' => 'application/vnd.ms-excel', 'required'=>true]) ?>
<?= Html::submitButton('Import' , 
    [
        'class' =>'btn btn-primary',
        'data-confirm' => Yii::t('yii', 'Are you sure you want to import?'),
    ]) 
?>

<?=Html::a('Export',Url::to(['perusahaan-kpr/export'],true),
    [
        'class'=>'btn btn-default',
        'data-confirm' => Yii::t('yii', 'Are you sure you want to export?'),
    ])
?>
<?= 

 AlertBlock::widget([
    'type' => AlertBlock::TYPE_ALERT,
    'useSessionFlash' => true
]);
?>
<?php ActiveForm::end(); ?>
<br/>
<br/>
<div class="perusahaan-kpr-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Perusahaan Kpr', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php 
    $gridColumn = [
        [
            'class' => 'kartik\grid\CheckboxColumn',
            'width' => '20px',
        ],
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        ['attribute' => 'id', 'visible' => false],
        'name',
        [
                'attribute' => 'group_id',
                'label' => 'Group',
                'value' => function($model){
                    if ($model->group)
                    {return $model->group->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\PerusahaanKpr::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Perusahaan kpr', 'id' => 'grid-perusahaan-kpr-search-group_id']
        ],
        [
                'attribute' => 'perusahaan_kpr_tier_id',
                'label' => 'Perusahaan Kpr Tier',
                'value' => function($model){
                    if ($model->perusahaanKprTier)
                    {return $model->perusahaanKprTier->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\PerusahaanKprTier::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Perusahaan kpr tier', 'id' => 'grid-perusahaan-kpr-search-perusahaan_kpr_tier_id']
         ],
         // [
         //    'label' => 'Status',
         //    'value' => function($model){
         //        if($model->is_approved == 1){
         //            return "Publish";
         //        }
         //        if($model->is_approved == 0){
         //            return "Pending";
         //        }
         //    }
         // ],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}{delete}',
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-perusahaan-kpr']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
            'after'=>BulkButtonWidget::widget([
                            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Delete Selected',
                                ["bulk-delete"] ,
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Are you sure?',
                                    'data-confirm-message'=>'Are you sure want to delete this item'
                                ]),
                        ]).'&nbsp;'.
                         Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Delete All ('.number_format($dataProvider->getTotalCount()).')',
                                ['delete-all'],
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    // 'role'=>'modal-remote-bulk',
                                    // 'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'name'=>'aksi',
                                    'value'=>'delete all',
                                    'type'=>'submit',
                                    // 'data-toggle'=>"modal",
                                    // 'data-target'=>'#ajaxCrudModal'
                                    // 'data-request-method'=>'post',
                                    // 'data-confirm-title'=>'Are you sure?',
                                    'data-confirm'=>'Are you sure want to Delete all these item'
                                ]
                        ).
                        '<div class="clearfix"></div>',
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => '',
        //     '{export}',
        //     ExportMenu::widget([
        //         'dataProvider' => $dataProvider,
        //         'columns' => $gridColumn,
        //         'target' => ExportMenu::TARGET_BLANK,
        //         'fontAwesome' => true,
        //         'dropdownOptions' => [
        //             'label' => 'Full',
        //             'class' => 'btn btn-default',
        //             'itemsBefore' => [
        //                 '<li class="dropdown-header">Export All Data</li>',
        //             ],
        //         ],
        //         'exportConfig' => [
        //             ExportMenu::FORMAT_PDF => false
        //         ]
        //     ]) ,
        // ],
    ]); ?>

</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
