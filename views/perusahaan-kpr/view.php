<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\PerusahaanKpr */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Perusahaan Kpr', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perusahaan-kpr-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Perusahaan Kpr'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        [
            'attribute' => 'group.name',
            'label' => 'Group',
        ],
        [
            'attribute' => 'perusahaanKprTier.name',
            'label' => 'perusahaan Kpr Tier',
        ],
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerPerusahaanKpr->totalCount){
    $gridColumnPerusahaanKpr = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'name',
                        [
                'attribute' => 'perusahaanKprTier.name',
                'label' => 'perusahaan Kpr Tier'
            ],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPerusahaanKpr,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-perusahaan-kpr']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Perusahaan Kpr'),
        ],
        'export' => false,
        'columns' => $gridColumnPerusahaanKpr
    ]);
}
?>
    </div>
</div>
