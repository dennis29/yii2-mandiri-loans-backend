<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DeveloperTier */
?>
<div class="developer-tier-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nama',
            'lock',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
        ],
    ]) ?>

</div>
