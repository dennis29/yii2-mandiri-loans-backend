<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DeveloperTier */
?>
<div class="developer-tier-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
