<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DeveloperTier */

?>
<div class="developer-tier-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
