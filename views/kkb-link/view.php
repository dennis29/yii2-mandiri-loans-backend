<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\KkbLink */
?>
<div class="kkb-link-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'link',
            'lock',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
