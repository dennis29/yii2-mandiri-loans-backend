<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KkbLink */
?>
<div class="kkb-link-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
