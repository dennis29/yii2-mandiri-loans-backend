<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PerusahaanKprTier */

$this->title = 'Update Perusahaan Kpr Tier: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Perusahaan Kpr Tier', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="perusahaan-kpr-tier-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
