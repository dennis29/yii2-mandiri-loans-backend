<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PerusahaanKprTier */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'KprPerusahaanKprTier', 
        'relID' => 'kpr-perusahaan-kpr-tier', 
        'value' => \yii\helpers\Json::encode($model->kprPerusahaanKprTiers),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PerusahaanKpr', 
        'relID' => 'perusahaan-kpr', 
        'value' => \yii\helpers\Json::encode($model->perusahaanKprs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="perusahaan-kpr-tier-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        // [
        //     'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('KprPerusahaanKprTier'),
        //     'content' => $this->render('_formKprPerusahaanKprTier', [
        //         'row' => \yii\helpers\ArrayHelper::toArray($model->kprPerusahaanKprTiers),
        //     ]),
        // ],
        // [
        //     'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PerusahaanKpr'),
        //     'content' => $this->render('_formPerusahaanKpr', [
        //         'row' => \yii\helpers\ArrayHelper::toArray($model->perusahaanKprs),
        //     ]),
        // ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
