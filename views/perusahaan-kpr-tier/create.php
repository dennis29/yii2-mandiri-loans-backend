<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PerusahaanKprTier */

$this->title = 'Create Perusahaan Kpr Tier';
$this->params['breadcrumbs'][] = ['label' => 'Perusahaan Kpr Tier', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perusahaan-kpr-tier-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
