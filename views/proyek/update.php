<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Proyek */

$this->title = 'Update Proyek: ' . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Proyek', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="proyek-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
