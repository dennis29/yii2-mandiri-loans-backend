<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Proyek */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="proyek-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id',['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
    <?= $form->field($model, 'nama')->textInput(['maxlength' => true, 'placeholder' => 'Nama']) ?>

    <?= $form->field($model, 'tanggal_jatuh_tempo')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Tanggal Jatuh Tempo',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'developer_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Developer::find()->orderBy('id')->asArray()->all(), 'id', 'nama'),
        'options' => ['placeholder' => 'Choose Developer'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('Developer');; ?>

    <!-- <?= $form->field($model, 'kpr_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Kpr::find()->orderBy('id')->asArray()->all(), 'id', 'nama'),
        'options' => ['placeholder' => 'Choose Kpr'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('Kpr');; ?> -->

    <?php $catList=ArrayHelper::map(app\models\Region::find()->all(), 'id', 'name' ); ?> 
    <?= // Normal parent select
        $form->field($model, 'region_id')->dropDownList($catList); ?>
    <?= //Dependent Dropdown
        $form->field($model, 'area_id')->widget(DepDrop::classname(), [
         'options' => ['id'=>'area_id','placeholder' => 'Choose Area...'],
         'data' => \yii\helpers\ArrayHelper::map(app\models\Area::find()->all(), 'id', 'name'),
         'pluginOptions'=>[
             'depends'=>['proyek-region_id'],
             'placeholder' => 'Choose Area...',
             'url' => Url::to(['subcat']),
             'initialize' => true

         ]
     ]); ?>

    <?= $form->field($model, 'min_harga')->textInput(['placeholder' => 'Min Harga']) ?>

    <?= $form->field($model, 'max_harga')->textInput(['placeholder' => 'Max Harga']) ?>

    <!-- <?= $form->field($model, 'region_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Region::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => 'Choose Region'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('Region');; ?> -->

    <?= $form->field($model, 'longitude')->textInput(['placeholder' => 'Longitude']) ?>

    <?= $form->field($model, 'latitude')->textInput(['placeholder' => 'Latitude']) ?>


    <?= $form->field($model, 'website')->textInput(['placeholder' => 'Website']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>


    <?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'header'=>'<h3>Konfirmasi Approver</h3>',
    'toggleButton' => ['label' => 'Submit','class'=>'btn btn-success'],
    "footer"=>"",// always need it for jquery plugin
])?>
    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'user_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->where(['status'=>10])->all(), 'id', 'name'),
            'options' => ['placeholder' => 'User Approver'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('User Approver'); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', 
            [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                                    
            ]) ?>
    </div>
<?php Modal::end(); ?>
<?php ActiveForm::end(); ?>

</div>
