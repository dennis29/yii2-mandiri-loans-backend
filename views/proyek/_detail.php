<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Proyek */

?>
<div class="proyek-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'nama',
        'tanggal_jatuh_tempo',
        [
            'attribute' => 'developer.id',
            'label' => 'Developer',
        ],
        [
            'attribute' => 'kpr.id',
            'label' => 'Kpr',
        ],
        'min_harga',
        'max_harga',
        [
            'attribute' => 'region.name',
            'label' => 'Region',
        ],
        'longitude',
        'latitude',
        'location',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>