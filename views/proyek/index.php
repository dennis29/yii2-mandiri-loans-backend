<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProyekSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use kartik\alert\AlertBlock;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\daterange\DateRangePicker;


$this->title = 'Proyek';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>

<?php $form = ActiveForm::begin(['action'=>Url::to(['proyek/import'],true),'options' => ['enctype' => 'multipart/form-data']]); ?>
<?= $form->field($excelModel, 'id')->hiddenInput()->label(false) ?>
<?= $form->field($excelModel, 'file')->fileInput(['accept' => 'application/vnd.ms-excel', 'required'=>true]) ?>
<?=Html::a('Export',Url::to(['proyek/export'],true),
    [
        'class'=>'btn btn-default',
        // 'data-confirm' => Yii::t('yii', 'Are you sure you want to export?'),
    ])
?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'header'=>'<h3>Konfirmasi Approver</h3>',
    'toggleButton' => ['label' => 'Import','class'=>'btn btn-primary'],
    "footer"=>"",// always need it for jquery plugin
])?>
<?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'user_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->where(['status'=>10])->all(), 'id', 'name'),
            'options' => ['placeholder' => 'User Approver'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('User Approver'); ?>
    <div class="form-group">
        <?= Html::submitButton('Submit' , 
            [
                'class' =>'btn btn-primary',
                'data-confirm' => Yii::t('yii', 'Are you sure you want to Import?'),
            ]) 
        ?>
    </div>
<?php Modal::end(); ?>
<?php ActiveForm::end(); ?>
<?= 
 AlertBlock::widget([
    'type' => AlertBlock::TYPE_ALERT,
    'useSessionFlash' => true
]);
?>
</br>
<div class="proyek-index">
    <div class="row">
        
        <div class="col-md-6">

                <?= Html::a('Create Proyek', ['create'], ['class' => 'btn btn-success']) ?>
            
        </div>
    </div>
</br>
    
    <?php 
    $gridColumn = [
        ['class' => 'kartik\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'nama',
        [
            'attribute' => 'tanggal_jatuh_tempo',
            'filterType'=> \kartik\grid\GridView::FILTER_DATE, 
            'filterWidgetOptions' => [
                    'options' => ['placeholder' => 'Select date'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
            ],
        ],
        [
                'attribute' => 'developer_id',
                'label' => 'Developer',
                'value' => function($model){
                    if(!empty($model->developer)){
                        return $model->developer->nama;
                    }
                },
                // 'filterType' => GridView::FILTER_SELECT2,
                // 'filter' => \yii\helpers\ArrayHelper::map(\app\models\Developer::find()->asArray()->all(), 'id', 'nama'),
                // 'filterWidgetOptions' => [
                //     'pluginOptions' => ['allowClear' => true],
                // ],
                // 'filterInputOptions' => ['placeholder' => 'Developer', 'id' => 'grid-proyek-search-developer_id']
        ],
        // [
        //         'attribute' => 'kpr_id',
        //         'label' => 'Kpr',
        //         'value' => function($model){
        //             if(!empty($model->kpr)){

        //                 return $model->kpr->nama;
        //             }
        //         },
        //         'filterType' => GridView::FILTER_SELECT2,
        //         'filter' => \yii\helpers\ArrayHelper::map(\app\models\Kpr::find()->asArray()->all(), 'id', 'nama'),
        //         'filterWidgetOptions' => [
        //             'pluginOptions' => ['allowClear' => true],
        //         ],
        //         'filterInputOptions' => ['placeholder' => 'Kpr', 'id' => 'grid-proyek-search-kpr_id']
        // ],
        [
                'attribute' => 'region_id',
                'label' => 'Region',
                'value' => function($model){
                    if(!empty($model->region)){
                        
                    return $model->region->name;
                    }
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Region::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Region', 'id' => 'grid-proyek-search-region_id']
        ],
        [
                'attribute' => 'area_id',
                'label' => 'Area',
                'value' => function($model){
                    if(!empty($model->area)){
                        
                    return $model->area->name;
                    }
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Area::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Area', 'id' => 'grid-proyek-search-area_id']
        ],
        'min_harga',
        'max_harga',
        'website',
        // [
        //         'attribute' => 'region_id',
        //         'label' => 'Region',
        //         'value' => function($model){
        //             return $model->region->name;
        //         },
        //         'filterType' => GridView::FILTER_SELECT2,
        //         'filter' => \yii\helpers\ArrayHelper::map(\app\models\Region::find()->asArray()->all(), 'id', 'name'),
        //         'filterWidgetOptions' => [
        //             'pluginOptions' => ['allowClear' => true],
        //         ],
        //         'filterInputOptions' => ['placeholder' => 'Region', 'id' => 'grid-proyek-search-region_id']
        //     ],
        'longitude',
        'latitude',
        // 'location',
        [
        // 'class' => 'kartik\grid\CheckboxColumn',
            'attribute' => 'is_approved',
            'label' => 'Status',
            'value' => function($model){
                if($model->is_approved == 1){
                    return "Publish";
                }
                if($model->is_approved == 0){
                    return "Pending";
                }
            },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => [0=>'Pending',1=>'Publish'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Status', 'id' => 'grid-proyek-search-is_approved']
        ],
        ['attribute' => 'lock', 'visible' => false],
        [
            'header'=>'Actions',
            'headerOptions'=>[
                'style'=>'color:#3c8dbc'
            ],
            'value'=> function ($model, $key, $index, $column) {
                   $a = Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update','id'=>$model->id], ['class' => '', 'id' => 'popupModal']);      
                   $b =   Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete-page','id'=>$model->id], ['class' => '', 'id' => 'popupModal']);      
                   return $a.$b;
              },
             'format' => 'raw'
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-proyek']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        'showPageSummary'=>true,
        // your toolbar can include the additional full export menu
        'toolbar' => '',
        // 'toolbar' => [
        //     '{export}',
        //     ExportMenu::widget([
        //         'dataProvider' => $dataProvider,
        //         'columns' => $gridColumn,
        //         'target' => ExportMenu::TARGET_BLANK,
        //         'fontAwesome' => true,
        //         'dropdownOptions' => [
        //             'label' => 'Full',
        //             'class' => 'btn btn-default',
        //             'itemsBefore' => [
        //                 '<li class="dropdown-header">Export All Data</li>',
        //             ],
        //         ],
        //         'exportConfig' => [
        //             ExportMenu::FORMAT_PDF => false
        //         ]
        //     ]) ,
        // ],
    ]); ?>

</div>
