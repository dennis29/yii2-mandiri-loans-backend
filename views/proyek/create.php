<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Proyek */

$this->title = 'Create Proyek';
$this->params['breadcrumbs'][] = ['label' => 'Proyek', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proyek-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
