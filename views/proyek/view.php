<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Proyek */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Proyek', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proyek-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Proyek'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'nama',
        'tanggal_jatuh_tempo',
        [
            'attribute' => 'developer.id',
            'label' => 'Developer',
        ],
        [
            'attribute' => 'kpr.id',
            'label' => 'Kpr',
        ],
        'min_harga',
        'max_harga',
        [
            'attribute' => 'region.name',
            'label' => 'Region',
        ],
        'longitude',
        'latitude',
        'location',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
