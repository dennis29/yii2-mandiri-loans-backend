<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PdfMaster */
?>
<div class="pdf-master-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'code',
            'file_location',
            'file_name',
            'lock',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
