<?php
use yii\helpers\Url;

?>
<script>
    function hashBeforeSubmit() {
        var pass = $('#loginform-password');
        console.log("foo");
        // hash('sha256', md5($password));
        //hashing disini
        $.ajax({
            type: 'POST',
            url: '<?php echo Url::to(['/user/hash-this']); ?>',
            data: {password: pass.val()},
            success: function (data) {
                var hashed = data;
                pass.val(hashed);   
                console.log(pass.val());
                // console.log($('#loginform').submit());
                // return true;
            },
            // error: function(data){
            //     return false;
            // }
        });
        return false;
    }
    function submitMe() {
        // var pass = $('#loginform-password');
        // console.log(pass.val());
        // hashBeforeSubmit();
        var pass = $('#loginform-password');
        if (pass.val().length != 0) {   
            var hash1 = CryptoJS.MD5(pass.val());
            var hash2 = CryptoJS.SHA256(hash1.toString());
            pass.val(hash2);
            $('#login-form').submit();
        }
        // document.getElementById('login-form').submit();
           
    }
</script>
