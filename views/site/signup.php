<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
$model->dob = '1990-01-10';
?>
<div class="login-box" style="margin-top: 20px">

    <div class="">
        <h1><?= Html::encode($this->title) ?></h1>

        <p>Please fill out the following fields to signup:</p>

        <div class="row">
            <div class="col-lg-12">
                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                    <?= $form->field($model, 'nip')->textInput(['autofocus' => true]) ?>
                    <?= $form->field($model, 'name') ?>
                    <?= $form->field($model, 'dob')->widget(\kartik\date\DatePicker::classname(), [
                        'type' =>  \kartik\date\DatePicker::TYPE_COMPONENT_PREPEND,
                        // 'saveFormat' => 'php:Y-m-d',
                        'value' => '1990-01-01',
                        // 'ajaxConversion' => true,
                        
                        'pluginOptions' => [
                            'placeholder' => 'Choose Pilih tanggal lahir',
                            'autoclose' => true,
                            'format' =>'yyyy-mm-dd',
                        ]
                        
                    ])->label('Tanggal Lahir'); ?>
                    <?= $form->field($model, 'phone_number') ?>
                    <?= $form->field($model, 'region') ?>
                    <?= $form->field($model, 'area') ?>

                    <?= $form->field($model, 'email') ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <?= $form->field($model, 'retype_password')->passwordInput() ?>

                    <div class="form-group">
                        <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>