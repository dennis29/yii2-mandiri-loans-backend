<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LogDashboard */

$this->title = 'Create Log Dashboard';
$this->params['breadcrumbs'][] = ['label' => 'Log Dashboard', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-dashboard-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
