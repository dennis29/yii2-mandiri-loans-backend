<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DeveloperGroup */

?>
<div class="developer-group-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
