<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DeveloperGroup */
?>
<div class="developer-group-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'lock',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
