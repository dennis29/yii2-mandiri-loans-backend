<?php
use yii\helpers\Url;
use kartik\widgets\SwitchInput;
use kartik\grid\GridView;


return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'label'=>'Nama',
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'text',
    //     'label'=>'Text',
    //     'format'=>'raw'
    // ],
    //  [
    //     // 'class' => 'kartik\grid\CheckboxColumn',
    //         'attribute' => 'is_publish',
    //         'label' => 'Status Publish',
    //         'width' => '20px',
    //         'options'=>[
    //             "data-toggle"=>"toggle",
    //             "class"=>"checkbox"
    //         ],
    //         'format'=>'raw',
    //         'value'=> function($model){
    //             if($model->is_publish == 1){
    //                 $obj = true;
    //             }else{
    //                 $obj = false;
    //             }
    //             if(Yii::$app->user->identity->status == 10){
    //                 return SwitchInput::widget([
    //                     'name' => 'status_13',
    //                     'options'=>[
    //                         'checked'=>$obj,
    //                         'onChange'=>'changePublish("'.$model->id.'",this.checked)',
    //                     ],
    //                     'pluginOptions' => [
    //                         'onText' => 'Publish',
    //                         'offText' => 'Reject',
    //                         'onColor' => 'primary',
    //                         'offColor' => 'default',
    //                         ]
    //                     ]);
    //             }else{
    //                 return SwitchInput::widget([
    //                     'name' => 'status_3',
    //                     'disabled' => 'true',
    //                     // 'value' => function($model){
    //                     // },
    //                     'options'=>[
    //                         'checked'=>$obj,
    //                         // 'onChange'=>'changeStatus("'.$model->ID.'",this.checked)',
    //                     ],
    //                     'pluginOptions' => [
    //                         'onText' => 'Publish',
    //                         'offText' => 'Reject',
    //                         'onColor' => 'primary',
    //                         'offColor' => 'default',
    //                         ]
    //                     ]);
    //             }
    //         },
    //         'filterType' => GridView::FILTER_SELECT2,
    //             'filter' => [0=>'Pending',1=>'Publish'],
    //             'filterWidgetOptions' => [
    //                 'pluginOptions' => ['allowClear' => true],
    //             ],
    //             'filterInputOptions' => ['placeholder' => 'Status', 'id' => 'grid-develpoer-group-search-is_approved']
    //         // 'visible' => $is_admin_only,
        
    //     ],
        // [
        //     'label' => 'Status',
        //     'value' => function($model){
        //         if($model->is_approved == 1){
        //             return "Publish";
        //         }
        //         if($model->is_approved == 0){
        //             return "Pending";
        //         }
        //     }
        // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'lock',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_at',
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'created_by',
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'updated_by',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'headerOptions'=>[
                'style'=>'color:#3c8dbc'
            ],
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template'=>'{update} {delete}',
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   