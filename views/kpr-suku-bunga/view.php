<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\KprSukuBunga */

$this->title = $model->kpr_id;
$this->params['breadcrumbs'][] = ['label' => 'Kpr Suku Bunga', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpr-suku-bunga-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Kpr Suku Bunga'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'kpr_id' => $model->kpr_id, 'suku_bunga_id' => $model->suku_bunga_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'kpr_id' => $model->kpr_id, 'suku_bunga_id' => $model->suku_bunga_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
            'attribute' => 'kpr.id',
            'label' => 'Kpr',
        ],
        [
            'attribute' => 'sukuBunga.id',
            'label' => 'Suku Bunga',
        ],
        ['attribute' => 'id', 'visible' => false],
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
