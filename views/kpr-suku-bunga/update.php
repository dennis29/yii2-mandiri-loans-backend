<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KprSukuBunga */

$this->title = 'Update Kpr Suku Bunga: ' . ' ' . $model->kpr_id;
$this->params['breadcrumbs'][] = ['label' => 'Kpr Suku Bunga', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kpr_id, 'url' => ['view', 'kpr_id' => $model->kpr_id, 'suku_bunga_id' => $model->suku_bunga_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kpr-suku-bunga-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
