<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KprSukuBunga */

$this->title = 'Create Kpr Suku Bunga';
$this->params['breadcrumbs'][] = ['label' => 'Kpr Suku Bunga', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpr-suku-bunga-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
