<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserMtf */

$this->title = 'Create User Mtf';
$this->params['breadcrumbs'][] = ['label' => 'User Mtf', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-mtf-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
