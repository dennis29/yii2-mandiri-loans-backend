<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserMtf */

$this->title = 'Update User Mtf: ' . ' ' . $model->nip;
$this->params['breadcrumbs'][] = ['label' => 'User Mtf', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-mtf-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
