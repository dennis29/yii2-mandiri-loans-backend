<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Developer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="developer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'developer_tier_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\DeveloperTier::find()->all(), 'id', 'nama'),
            'options' => ['placeholder' => 'Develeoper Tier'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('Developer Tier'); ?>

    <?= $form->field($model, 'developer_group_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\DeveloperGroup::find()->all(), 'id', 'name'),
            'options' => ['placeholder' => 'Developer Group'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('Developer Group'); ?>


  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
