<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Developer */
?>
<div class="developer-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nama',
            'link',
            'developer_tier_id',
            'developer_group_id',
            'lock',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
