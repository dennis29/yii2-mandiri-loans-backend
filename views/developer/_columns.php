<?php
use yii\helpers\Url;
use kartik\grid\GridView;


return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'nama',
        'format' => 'raw'
    ],
   [
        'label'=>'Developer Tier',
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'developer_tier_id',
        // 'group' =>true,
        // 'subGroupOf'=>7,
        'value' => function($model) {
            if(!empty($model->developerTier)){
                return $model->developerTier->nama;
            }
        },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\DeveloperTier::find()->asArray()->all(), 'id', 'nama'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Perusahaan Tier', 'id' => 'grid-developer-search-tier_id']
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'developer_group_id',
        'group'=>true,  // enable grouping,
        'groupedRow'=>true,      
         'value' => function($model) {
            if(!empty($model->developerGroup)){
                return $model->developerGroup->name;
            }
        }
    ],
    // [
    //         'label' => 'Status',
    //         'value' => function($model){
    //             if($model->is_approved == 1){
    //                 return "Publish";
    //             }
    //             if($model->is_approved == 0){
    //                 return "Pending";
    //             }
    //         }
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'lock',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_at',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_by',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_by',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'headerOptions'=>[
                'style'=>'color:#3c8dbc'
                ],
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template'=>'{update} {delete}',
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   