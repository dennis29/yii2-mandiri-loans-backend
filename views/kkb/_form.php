<?php
use yii\helpers\Html;
use dosamigos\tinymce\TinyMce;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Kkb */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kkb-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'code')->textInput() ?>
    <?= $form->field($model, 'title')->textInput() ?>

    <?= Html::img($model->image,['style'=>'width:100px'])?>
    
    <?= $form->field($model, 'imageKkb')->fileInput(['accept' => 'image/*']) ?>

     <?= $form->field($model, 'text')->widget(TinyMce::className(), [
        'options' => ['rows' => 12],
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>
    <?= $form->field($model, 'link')->textInput()->label('Apply Link') ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
