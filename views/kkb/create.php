<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Kkb */

?>
<div class="kkb-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
