<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Slider */

?>
<div class="slider-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
