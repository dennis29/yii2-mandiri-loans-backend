<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Slider */
?>
<div class="slider-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'image',
            'lock',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
