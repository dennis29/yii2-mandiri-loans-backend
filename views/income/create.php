<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Income */

?>
<div class="income-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
