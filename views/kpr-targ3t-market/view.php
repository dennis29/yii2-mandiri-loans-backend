<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\KprTarg3tMarket */

$this->title = $model->kpr_id;
$this->params['breadcrumbs'][] = ['label' => 'Kpr Targ3t Market', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpr-targ3t-market-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Kpr Targ3t Market'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'kpr_id' => $model->kpr_id, 'targ3t_market_id' => $model->targ3t_market_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'kpr_id' => $model->kpr_id, 'targ3t_market_id' => $model->targ3t_market_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
            'attribute' => 'kpr.id',
            'label' => 'Kpr',
        ],
        [
            'attribute' => 'targ3tMarket.name',
            'label' => 'Targ3t Market',
        ],
        ['attribute' => 'id', 'visible' => false],
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
