<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KprTarg3tMarket */

$this->title = 'Create Kpr Targ3t Market';
$this->params['breadcrumbs'][] = ['label' => 'Kpr Targ3t Market', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpr-targ3t-market-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
