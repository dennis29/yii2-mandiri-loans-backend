<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KprTarg3tMarket */

$this->title = 'Update Kpr Targ3t Market: ' . ' ' . $model->kpr_id;
$this->params['breadcrumbs'][] = ['label' => 'Kpr Targ3t Market', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kpr_id, 'url' => ['view', 'kpr_id' => $model->kpr_id, 'targ3t_market_id' => $model->targ3t_market_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kpr-targ3t-market-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
