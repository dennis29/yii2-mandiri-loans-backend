<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\LogUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Log User';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="log-user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'nip',
        [
                'attribute' => 'user_id',
                'label' => 'User',
                'value' => function($model){
                    if ($model->user)
                    {return $model->user->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid-log-user-search-user_id']
        ],
        'ip_address',
        // 'is_success',
        [
                'attribute' => 'is_success',
                'label' => 'Hasil',
                'value' => function($model){
                    if ($model->is_success == 1)
                    {return "Sukses";}
                    else if ($model->is_success == 0)
                    {return "Gagal";}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => [1=>'Sukses',0=>'Gagal'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Hasil', 'id' => 'grid-log-user-search-is_success']
        ],
        // 'status',
        [
                'attribute' => 'status',
                'label' => 'Status',
                'value' => function($model){
                    if ($model->status == 0)
                    {return "Login berhasil";}
                    else if ($model->status == 1)
                    {return "Nip tidak ditemukan";}
                    else if ($model->status == 2)
                    {return "Password salah";}
                    else if ($model->status == 3)
                    {return "User whitelist tidak ditemukan";}
                    else if ($model->status == 4)
                    {return "Ganti password berhasil";}
                    else if ($model->status == 5)
                    {return "Ganti password gagal, password salah";}
                    else if ($model->status == 6)
                    {return "Ganti password gagal, user tidak ditemukan";}
                    else if ($model->status == 7)
                    {return "Admin telah berhasil membuat user baru";}
                    else if ($model->status == 8)
                    {return "Admin telah berhasil merubah user";}
                    else if ($model->status == 9)
                    {return "Admin telah berhasil hapus user";}
                    else if ($model->status == 10)
                    {return "Admin telah berhasil merubah role user";}
                    else if ($model->status == 11)
                    {return "Admin telah berhasil membuat user mobile baru";}
                    else if ($model->status == 15)
                    {return "Admin telah berhasil membuat user whitelist baru";}
                    else if ($model->status == 16)
                    {return "Admin telah berhasil merubah user whitelist";}
                    else if ($model->status == 17)
                    {return "Admin telah berhasil hapus user whitelist";}
                    else if ($model->status == 18)
                    {return "Admin telah berhasil import user whitelist baru";}
                    elseif ($model->status == 19) 
                    {return 'Admin telah berhasil merubah privilege';}
                    else if ($model->status == 20)
                    {return "Admin Region telah berhasil membuat user region baru";}
                    else if ($model->status == 21)
                    {return "Admin Region telah berhasil merubah user region";}
                    else if ($model->status == 22)
                    {return "Admin Region telah berhasil hapus user region";}
                    else if ($model->status == 23)
                    {return "Admin Region telah berhasil import user region baru";}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => [
                                0=>'Login berhasil',
                                1=>'Nip tidak ditemukan',
                                2=>'Password salah',
                                3=>'User whitelist tidak ditemukan',
                                4=>'Ganti password berhasil',
                                5=>'Ganti password gagal, password salah',
                                6=>'Ganti password gagal, user tidak ditemukan',
                                7=>'Admin telah berhasil membuat user baru',
                                8=>'Admin telah berhasil merubah user',
                                9=>'Admin telah berhasil hapus user',
                                10=>'Admin telah berhasil merubah role user',
                                11=>'Admin telah berhasil membuat user mobile baru',
                                15=>'Admin telah berhasil membuat user whitelist baru',
                                16=>'Admin telah berhasil merubah user whitelist',
                                17=>'Admin telah berhasil hapus user whitelist',
                                18=>'Admin telah berhasil import user whitelist baru',
                                19=>'Admin telah berhasil merubah privilege',
                                20=>'Admin Region telah berhasil membuat user region baru',
                                21=>'Admin Region telah berhasil merubah user region',
                                22=>'Admin Region telah berhasil hapus user region',
                                23=>'Admin Region telah berhasil import user region baru',
                ],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Status', 'id' => 'grid-log-user-search-status']
        ],
        [
            'attribute' => 'message',
            'label' => 'Message',
        ],
        [
            'attribute' => 'created_at',
            'label' => 'Timestamp'
        ],
        ['attribute' => 'lock', 'visible' => false],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-log-user']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            // '{export}',
            // ExportMenu::widget([
            //     'dataProvider' => $dataProvider,
            //     'columns' => $gridColumn,
            //     'target' => ExportMenu::TARGET_BLANK,
            //     'fontAwesome' => true,
            //     'dropdownOptions' => [
            //         'label' => 'Full',
            //         'class' => 'btn btn-default',
            //         'itemsBefore' => [
            //             '<li class="dropdown-header">Export All Data</li>',
            //         ],
            //     ],
            //     'exportConfig' => [
            //         ExportMenu::FORMAT_PDF => false
            //     ]
            // ]) ,
        ],
    ]); ?>

</div>
