<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KtaPerusahaanTier */

$this->title = 'Update Kta Perusahaan Tier: ' . ' ' . $model->kta_id;
$this->params['breadcrumbs'][] = ['label' => 'Kta Perusahaan Tier', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kta_id, 'url' => ['view', 'kta_id' => $model->kta_id, 'perusahaan_tier_id' => $model->perusahaan_tier_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kta-perusahaan-tier-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
