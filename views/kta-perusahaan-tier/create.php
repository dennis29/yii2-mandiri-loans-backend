<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KtaPerusahaanTier */

$this->title = 'Create Kta Perusahaan Tier';
$this->params['breadcrumbs'][] = ['label' => 'Kta Perusahaan Tier', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kta-perusahaan-tier-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
