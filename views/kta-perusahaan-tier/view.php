<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\KtaPerusahaanTier */

$this->title = $model->kta_id;
$this->params['breadcrumbs'][] = ['label' => 'Kta Perusahaan Tier', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kta-perusahaan-tier-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Kta Perusahaan Tier'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'kta_id' => $model->kta_id, 'perusahaan_tier_id' => $model->perusahaan_tier_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'kta_id' => $model->kta_id, 'perusahaan_tier_id' => $model->perusahaan_tier_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
            'attribute' => 'kta.id',
            'label' => 'Kta',
        ],
        [
            'attribute' => 'perusahaanTier.id',
            'label' => 'Perusahaan Tier',
        ],
        ['attribute' => 'id', 'visible' => false],
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
