<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\KtaRules */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Kta Rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kta-rules-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Kta Rules'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'kta.id',
            'label' => 'Kta',
        ],
        'min_tenor',
        'max_tenor',
        'suku_bunga_floating',
        'suku_bunga_flat',
        'max_dbr',
        'limit_min',
        'limit_max',
        'income_min',
        'income_max',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
