<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KtaRulesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-kta-rules-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'kta_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Kta::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Kta'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'min_tenor')->textInput(['placeholder' => 'Min Tenor']) ?>

    <?= $form->field($model, 'max_tenor')->textInput(['placeholder' => 'Max Tenor']) ?>

    <?= $form->field($model, 'suku_bunga_floating')->textInput(['placeholder' => 'Suku Bunga Floating']) ?>

    <?php /* echo $form->field($model, 'suku_bunga_flat')->textInput(['placeholder' => 'Suku Bunga Flat']) */ ?>

    <?php /* echo $form->field($model, 'max_dbr')->textInput(['placeholder' => 'Max Dbr']) */ ?>

    <?php /* echo $form->field($model, 'limit_min')->textInput(['placeholder' => 'Limit Min']) */ ?>

    <?php /* echo $form->field($model, 'limit_max')->textInput(['placeholder' => 'Limit Max']) */ ?>

    <?php /* echo $form->field($model, 'income_min')->textInput(['placeholder' => 'Income Min']) */ ?>

    <?php /* echo $form->field($model, 'income_max')->textInput(['placeholder' => 'Income Max']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
