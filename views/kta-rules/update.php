<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KtaRules */

$this->title = 'Update Kta Rules: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Kta Rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kta-rules-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
