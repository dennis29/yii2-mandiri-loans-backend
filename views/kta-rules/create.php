<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KtaRules */

$this->title = 'Create Kta Rules';
$this->params['breadcrumbs'][] = ['label' => 'Kta Rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kta-rules-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
