<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\UserReferensi */
/* @var $form yii\widgets\ActiveForm */
if(empty($model->dob)){
    $model->dob = '1980-01-01';
}
?>

<div class="user-referensi-form">

    <?php $form = ActiveForm::begin(['options'=>['class'=>'']]); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'nip')->textInput(['maxlength' => true, 'placeholder' => 'Nip']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Nama']) ?>

    <?= $form->field($model, 'contact')->textInput(['maxlength' => true, 'placeholder' => 'Contact']) ?>

    <?php $areaList=ArrayHelper::map(app\models\Area::find()->where(['region_id' => Yii::$app->user->identity->region_id])->all(), 'id', 'name' ); ?> 
    <?= //Dependent Dropdown
        $form->field($model, 'area_id')->dropDownList($areaList);
    ?>

     <?= $form->field($model, 'dob')->widget(\kartik\datecontrol\DateControl::classname(), [
      'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
      'saveFormat' => 'php:Y-m-d',
      'ajaxConversion' => true,
      'options' => [
              'placeholder' => 'Start Date',
          'pluginOptions' => [
              'autoclose' => true
          ]
      ],
    ]); ?>
    
    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
