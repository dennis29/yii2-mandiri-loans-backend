<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
$hiddenName = false;
if($this->context->action->id=="create-mobile"){
    $hiddenName = true;
}
if(!empty($status) && in_array($status, [20,30])){
    $hiddenName = true;
}

?>

<div class="user-form">

<div class="col-lg-12">
                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                    <?= $form->field($model, 'nip')->textInput(['autofocus' => true]) ?>
					<?= $form->field($model, 'imei')->textInput(['autofocus' => true]) ?>
                    <?php 
                        if(!$hiddenName){
                            echo $form->field($model, 'name')->textInput([]) ;
                            
                        }
                    ?>
                    <?= $form->field($model, 'dob')->widget(\kartik\date\DatePicker::classname(), [
                        'type' =>  \kartik\date\DatePicker::TYPE_COMPONENT_PREPEND,
                        // 'saveFormat' => 'php:Y-m-d',
                        'value' => '01-01-1990',
                        // 'ajaxConversion' => true,
                        
                        'pluginOptions' => [
                            'placeholder' => 'Choose Pilih tanggal lahir',
                            'autoclose' => true,
                            'format' =>'dd-mm-yyyy',
                        ]
                        
                    ])->label('Tanggal Lahir'); ?>
                    <?= $form->field($model, 'phone_number') ?>
                    <?php $catList=ArrayHelper::map(app\models\Region::find()->all(), 'id', 'name' ); ?> 
                   
                   <?= // Normal parent select

                        $form->field($model, 'region_id')->dropDownList($catList); ?>
                    <?= //Dependent Dropdown
                        $form->field($model, 'area_id')->widget(DepDrop::classname(), [
                         'options' => ['id'=>'area_id','placeholder' => 'Choose Area...'],
                         'data' => \yii\helpers\ArrayHelper::map(app\models\Area::find()->all(), 'id', 'name'),
                         'pluginOptions'=>[
                             'depends'=>['signupform-region_id'],
                             'placeholder' => 'Choose Area...',
                             'url' => Url::to(['subcat']),
                             'initialize' => true,
                         ]
                     ]); ?>
                    <?= $form->field($model, 'email') ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <?= $form->field($model, 'retype_password')->passwordInput() ?>

                    <div class="form-group">
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
</div>
