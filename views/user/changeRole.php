<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="col-lg-12">
	<?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
	<?= $form->field($model, 'nip')->textInput(['readonly' => true]); ?>

	 <?= $form->field($model, 'status')->dropDownList($list,['prompt' => 'Select'])->label('Role'); ?>
	<?php ActiveForm::end(); ?>
</div>
