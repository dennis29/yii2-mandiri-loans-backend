<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;


return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'nip',
        'label'=>'nip/sales code'
    ],
	[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'imei',
        'label'=>'imei'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        // 'visible' => ($this->context->action->id=="index")
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'password_hash',
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute'=>'password_reset_token',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'email',
    ],
    [
        'attribute'=>'dob',
        'format' => ['date', 'php:d-m-Y'],
        'filterType'=> \kartik\grid\GridView::FILTER_DATE, 
        'filterWidgetOptions' => [
                    'options' => ['placeholder' => 'Select date'],
                    'pluginOptions' => [
                        'format' => 'dd-mm-yyyy',
                        'todayHighlight' => true
                    ]
            ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'value'=>function($model){
            if($model->status==5){
                return "admin";

            }
            if($model->status==12){
                return "admin region";

            }
            if($model->status==10){
                return "approver";
            }else 
            if($model->status==30){
                return "verified";

            }else if($model->status==20){
                return "not verified";

            }
        },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => [5=>'admin',12=>'admin region',10=>'approver',30=>'verified',20=>'not verified'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Status', 'id' => 'grid-proyek-search-is_approved']
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'phone_number',
    ],

    [
            'attribute' => 'region_id',
            'label' => 'Region',
            'value' => function($model){
                // return $model->region_id;
                // return var_dump($model->region);
                if(!empty($model->region)){
                    return $model->region->name;
                }else{
                    return "";
                }
            },

            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\app\models\Region::find()->orderBy('Name')->asArray()->all(), 'id', 'name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Region', 'id' => 'grid-category-search-region']
    ],

    [
            'attribute' => 'area_id',
            'label' => 'Area',
            'value' => function($model) {
                if(!empty($model->area)){
                    return $model->area->name;
                }else{
                    return "";
                }
            },

            'filterType' => GridView::FILTER_SELECT2,
            'filter' => \yii\helpers\ArrayHelper::map(\app\models\Area::find()->orderBy('Name')->asArray()->all(), 'id', 'name'),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Area', 'id' => 'grid-category-search-area']
    ],
    [
        'label' => 'Timestamp',
        'attribute'=>'created_at',
        'format' => 'datetime'
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_at',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'headerOptions'=>[
                'style'=>'color:#3c8dbc'
            ],
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{role} {update} {delete}',
        'buttons' => [
            'role' => function ($url, $model) {   
                // return 'hha';  
                return Html::a('<span class="glyphicon glyphicon-transfer"></span>', $url, [
                    'role'=>'modal-remote','title'=>'Change Role', 'data-toggle'=>'tooltip']
                );
            },         
        ],
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   