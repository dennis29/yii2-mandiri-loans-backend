<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\ProfesiKpr */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Profesi Kpr', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profesi-kpr-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Profesi Kpr'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'nama',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerKprProfesiKpr->totalCount){
    $gridColumnKprProfesiKpr = [
        ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'kpr.id',
                'label' => 'Kpr'
            ],
                ];
    echo Gridview::widget([
        'dataProvider' => $providerKprProfesiKpr,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-kpr-profesi-kpr']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Kpr Profesi Kpr'),
        ],
        'export' => false,
        'columns' => $gridColumnKprProfesiKpr
    ]);
}
?>
    </div>
</div>
