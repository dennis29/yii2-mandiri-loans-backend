<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfesiKprSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\bootstrap\Modal;


CrudAsset::register($this);

$this->title = 'Profesi Kpr';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="profesi-kpr-index">
    <div class="row">
        
        <div class="col-md-6">

                <?= Html::a('Create Profesi Kpr', ['create'], ['class' => 'btn btn-success']) ?>
                
        </div>
        <div class="col-md-6">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>
    <?php 
    $gridColumn = [
        [
            'class' => 'kartik\grid\CheckboxColumn',
            'width' => '20px',
        ],
        ['class' => 'kartik\grid\SerialColumn',
        ],
        'nama',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'kartik\grid\ActionColumn',
            'template' => '{update} {delete}',
            'headerOptions'=>[
                'style'=>'color:#3c8dbc'
            ],
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-profesi-kpr']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
            'after'=>BulkButtonWidget::widget([
                            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Delete Selected',
                                ["bulk-delete"] ,
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Are you sure?',
                                    'data-confirm-message'=>'Are you sure want to delete this item'
                                ]),
                        ]).'&nbsp;'.
                         Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Delete All ('.number_format($dataProvider->getTotalCount()).')',
                                ['delete-all'],
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    // 'role'=>'modal-remote-bulk',
                                    // 'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'name'=>'aksi',
                                    'value'=>'delete all',
                                    'type'=>'submit',
                                    // 'data-toggle'=>"modal",
                                    // 'data-target'=>'#ajaxCrudModal'
                                    // 'data-request-method'=>'post',
                                    // 'data-confirm-title'=>'Are you sure?',
                                    'data-confirm'=>'Are you sure want to Delete all these item'
                                ]
                        ).
                        '<div class="clearfix"></div>',
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => '',
        // 'toolbar' => [
        //     '{export}',
        //     ExportMenu::widget([
        //         'dataProvider' => $dataProvider,
        //         'columns' => $gridColumn,
        //         'target' => ExportMenu::TARGET_BLANK,
        //         'fontAwesome' => true,
        //         'dropdownOptions' => [
        //             'label' => 'Full',
        //             'class' => 'btn btn-default',
        //             'itemsBefore' => [
        //                 '<li class="dropdown-header">Export All Data</li>',
        //             ],
        //         ],
        //         'exportConfig' => [
        //             ExportMenu::FORMAT_PDF => false
        //         ]
        //     ]) ,
        // ],
    ]); ?>

</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>