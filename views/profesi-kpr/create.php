<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProfesiKpr */

$this->title = 'Create Profesi Kpr';
$this->params['breadcrumbs'][] = ['label' => 'Profesi Kpr', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profesi-kpr-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
