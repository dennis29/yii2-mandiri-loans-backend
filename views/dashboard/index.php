<?php
  // dmstr\web\MorrisAsset::register($this);
  // dmstr\web\JQueryUIAsset::register($this);
  // dmstr\web\WYSIAsset::register($this);
  // dmstr\web\KnobAsset::register($this);
  // dmstr\web\DateRangePickerAsset::register($this);
use miloschuman\highcharts\Highcharts;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;

use miloschuman\highcharts\SeriesDataHelper;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\helpers\Html;
$this->title="Dashboard";
?>
  <div class="form-group">
  <?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\KtaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\alert\AlertBlock;
use kartik\widgets\SwitchInput;
use yii\bootstrap\Modal;

$form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
    <?= $form->field($model,'file')->fileInput() ?>
   
    <div class="form-group">
        <?= Html::submitButton('Import',['class'=>'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end();
?>

</br>
    <?php $form = ActiveForm::begin(['options'=>['class'=>'form-inline'],'method'=>'get']); ?>
     <?= $form->field($searchModel, 'start_date')->widget(\kartik\datecontrol\DateControl::classname(), [
      'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
      'saveFormat' => 'php:Y-m-d',
      'ajaxConversion' => true,
      'options' => [
              'class' => 'form-group',
              'placeholder' => 'Start Date',
          'pluginOptions' => [
              'autoclose' => true
          ]
      ],
    ])->label(false); ?>
    <i class="fa fa-arrows-h" aria-hidden="true"></i>

    <?= $form->field($searchModel, 'end_date')->widget(\kartik\datecontrol\DateControl::classname(), [
      'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
      'saveFormat' => 'php:Y-m-d',
      'ajaxConversion' => true,
      'options' => [
              'placeholder' => 'End Date',
          'pluginOptions' => [
              'autoclose' => true
          ]
      ],
    ])->label(false); ?>
    <?php $catList=ArrayHelper::map(app\models\Region::find()->all(), 'id', 'name' ); ?> 
    <?php // Normal parent select
        echo $form->field($searchModel, 'region_id')->dropDownList($catList,['prompt'=>'Select..']); 
        // echo $form->field($searchModel, 'farhan');
    ?>
    <?= //Dependent Dropdown
        $form->field($searchModel, 'area_id')->widget(DepDrop::classname(), [
         'options' => ['id'=>'area_id'],
         'pluginOptions'=>[
             'depends'=>['logdashboardsearch-region_id'],
             'placeholder' => 'Select...',
             'url' => Url::to(['/user-mtf/subcat'])
         ]
     ]); ?>
     <div class="form-group" style='vertical-align:top'>
       
      <?= Html::submitButton('<span class="glyphicon glyphicon-search"></span>' , 
            [
                'class' =>'btn btn-primary',
                // 'style'=>'vertical-align:top'
            ]) 
        ?>
     </div>

  <?php ActiveForm::end(); ?>

  </div>
    <section class="content">
      <!-- Small boxes (Stat box) -->
    
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?= $log_hit_kpr?></h3>

              <p>Total Hit KPR</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a class="small-box-footer" href="<?= Url::to(['/kpr'])?>">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?= $log_hit_kta?></h3>

              <p>Total Hit KTA</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a class="small-box-footer" href="<?= Url::to(['/kta'])?>">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?= $log_hit_kkb?></h3>

              <p>Total Hit KKB</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a class="small-box-footer" href="<?= Url::to(['/kkb'])?>">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?= $log_total?></h3>

              <p>Total Hit</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-xs-6">
          <div class="box box-primary">

            <div class="box-body">
               <?php echo Highcharts::widget([
                      'scripts' => [
                          'themes/grid-light',
                      ],
                      'options' => [
                          'title' => [
                              'text' => 'PIE',
                          ],
                          'labels' => [
                              'items' => [
                                  [
                                      
                                  ],
                              ],
                          ],
                          'series' => [
                              [
                                  'type' => 'pie',
                                  'name' => 'Total consumption',
                                  'data' => [
                                      [
                                          'name' => 'KPR',
                                          'y' => intval($log_hit_kpr),
                                          'color' => new JsExpression('Highcharts.getOptions().colors[0]'), // Jane's color
                                      ],
                                      [
                                          'name' => 'KTA',
                                          'y' => intval($log_hit_kta),
                                          'color' => new JsExpression('Highcharts.getOptions().colors[1]'), // John's color
                                      ],
                                      [
                                          'name' => 'KKB',
                                          'y' => intval($log_hit_kkb),
                                          'color' => new JsExpression('Highcharts.getOptions().colors[2]'), // Joe's color
                                      ],
                                  ],
                                  'showInLegend' => false,
                                  'dataLabels' => [
                                      'enabled' => true,
                                  ],
                              ],
                          ],
                      ]
                  ]);?>
            </div>
          </div>
        </div>
        <div class="col-xs-6">
          <div class="box box-primary">
              <div class="box-body">
           <?= Html::a('Export',Url::to(['dashboard/export'],true),
            [
                'class'=>'btn btn-success pull-right',
                'data-confirm' => Yii::t('yii', 'Are you sure you want to export?'),
            ])?>
                       <?php
                  echo Highcharts::widget([
             'options' => [
                'title' => ['text' => 'Grafik '],
                'xAxis' => [
                   'categories' => $categories_data
                ],
                'yAxis' => [
                   'title' => ['text' => 'grafik']
                    ],
                    'series' => [
                       ['name' => 'KPR', 'data' => $data_kpr],
                       ['name' => 'KTA', 'data' => $data_kta],
                       ['name' => 'KKB', 'data' => $data_kkb]
                    ]
                 ]
              ]);?>
              </div>
          </div>
        </div>
      </div>
        <!-- /.col -->
     
      <!-- /.row -->
      
     <!-- /.row -->
      
    </section>