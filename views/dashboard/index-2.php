<div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Perusahaan and Developer</h3>
               <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-4">
                  <p class="text-center">
                    <strong>Grafik Perusahaan</strong>
                  </p>
                                    <div class="progress-group">
                    <span class="progress-text">Perusahaan A</span>
                    <span class="progress-number"><b>2</b>/7</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="background-color:#0048BA ;width: 98.571428571429%"></div>
                    </div>
                  </div>
                                    <div class="progress-group">
                    <span class="progress-text">Perusahaan B</span>
                    <span class="progress-number"><b>22</b>/46</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="background-color:#FF7E00 ;width: 87.826086956522%"></div>
                    </div>
                  </div>
                                    <div class="progress-group">
                    <span class="progress-text">Perusahaan C</span>
                    <span class="progress-number"><b>8</b>/12</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="background-color:#D0FF14 ;width: 66.666666666667%"></div>
                    </div>
                  </div>
                                    <div class="progress-group">
                    <span class="progress-text">Perusahaan D</span>
                    <span class="progress-number"><b>8</b>/15</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="background-color:#9932CC ;width: 53.333333333333%"></div>
                    </div>
                  </div>
                                    <div class="progress-group">
                    <span class="progress-text">Perusahaan E</span>
                    <span class="progress-number"><b>16</b>/26</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="background-color:#00FFFF ;width: 41.538461538462%"></div>
                    </div>
                  </div>
                                    <div class="progress-group">
                    <span class="progress-text">Perusahaan F</span>
                    <span class="progress-number"><b>8</b>/15</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="background-color:#D70A53 ;width: 33.333333333333%"></div>
                    </div>
                  </div>
                                    <!-- /.progress-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <p class="text-center">
                    <strong>Grafik Developer</strong>
                  </p>
                                    <div class="progress-group">
                    <span class="progress-text">Developer A</span>
                    <span class="progress-number"><b>2</b>/7</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="background-color:#0048BA ;width: 28.571428571429%"></div>
                    </div>
                  </div>
                                    <div class="progress-group">
                    <span class="progress-text">Developer B</span>
                    <span class="progress-number"><b>22</b>/46</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="background-color:#FF7E00 ;width: 47.826086956522%"></div>
                    </div>
                  </div>
                                    <div class="progress-group">
                    <span class="progress-text">Developer C</span>
                    <span class="progress-number"><b>8</b>/12</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="background-color:#D0FF14 ;width: 66.666666666667%"></div>
                    </div>
                  </div>
                                    <div class="progress-group">
                    <span class="progress-text">Developer D</span>
                    <span class="progress-number"><b>8</b>/15</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="background-color:#9932CC ;width: 53.333333333333%"></div>
                    </div>
                  </div>
                                    <div class="progress-group">
                    <span class="progress-text">Developer E</span>
                    <span class="progress-number"><b>16</b>/26</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="background-color:#00FFFF ;width: 61.538461538462%"></div>
                    </div>
                  </div>
                                    <div class="progress-group">
                    <span class="progress-text">Developer F</span>
                    <span class="progress-number"><b>8</b>/15</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="background-color:#D70A53 ;width: 53.333333333333%"></div>
                    </div>
                  </div>
                                    <!-- /.progress-group -->
                </div>
                <div class="col-md-4">
                  <p class="text-center">
                    <strong>Grafik Region</strong>
                  </p>
                                    <div class="progress-group">
                    <span class="progress-text">Region 1</span>
                    <span class="progress-number"><b>2</b>/7</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="background-color:#0048BA ;width: 28.571428571429%"></div>
                    </div>
                  </div>
                                    <div class="progress-group">
                    <span class="progress-text">Region 2</span>
                    <span class="progress-number"><b>22</b>/46</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="background-color:#FF7E00 ;width: 47.826086956522%"></div>
                    </div>
                  </div>
                                    <div class="progress-group">
                    <span class="progress-text">Region 3</span>
                    <span class="progress-number"><b>8</b>/12</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="background-color:#D0FF14 ;width: 66.666666666667%"></div>
                    </div>
                  </div>
                                    <div class="progress-group">
                    <span class="progress-text">Region 4</span>
                    <span class="progress-number"><b>8</b>/15</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="background-color:#9932CC ;width: 73.333333333333%"></div>
                    </div>
                  </div>
                                    <div class="progress-group">
                    <span class="progress-text">Region 5</span>
                    <span class="progress-number"><b>16</b>/26</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="background-color:#00FFFF ;width: 81.538461538462%"></div>
                    </div>
                  </div>
                                    <div class="progress-group">
                    <span class="progress-text">Region 6</span>
                    <span class="progress-number"><b>8</b>/15</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="background-color:#D70A53 ;width: 93.333333333333%"></div>
                    </div>
                  </div>
                                    <!-- /.progress-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <h5 class="description-header">8</h5>
                    <span class="description-text">TOTAL 1</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <h5 class="description-header">64</h5>
                    <span class="description-text">TOTAL 2</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block border-right">
                    <h5 class="description-header">9</h5>
                    <span class="description-text">TOTAL 3</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3 col-xs-6">
                  <div class="description-block">
                    <h5 class="description-header">121</h5>
                    <span class="description-text">TOTAL 4</span>
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

 <div class="row">

        <div class="col-md-6">
          <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">KTA terbaru</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <ul class="products-list product-list-in-box">
                  <?php for ($i=0; $i<5; $i++ ) :?>
                  <li class="item">
                    
                    <div class="product-info">
                      <a href="" class="product-title">judul KTA <?= $i?></a>
                          <span class="product-description">
                            Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and printing in place of English to emphasise design elements over content. It's also called placeholder (or filler) text. 
                          </span>
                    </div>
                  </li>
                  <?php endfor?>
                  <!-- /.item -->
                </ul>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-center">
                <a href="<?= \Yii::$app->request->BaseUrl?>/index.php/task" class="uppercase">View All KTA</a>
              </div>
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-6">
          <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">KPR Terbaru</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <ul class="products-list product-list-in-box">
                  <?php for ($i = 0;$i<5;$i++ ) :?>
                  <li class="item">
                    
                    <div class="product-info">
                      <a href="" class="product-title">judul <?= $i?></a>
                          <span class="product-description">
                            Lorem ipsum is a pseudo-Latin text used in web design, typography, layout, and printing in place of English to emphasise design elements over content. It's also called placeholder (or filler) text. 
                          </span>
                    </div>
                  </li>
                  <?php endfor?>
                  <!-- /.item -->
                 
                  <!-- /.item -->
                </ul>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-center">
                <a href="<?= \Yii::$app->request->BaseUrl?>/index.php/trouble-ticket" class="uppercase">View All KPR</a>
              </div>
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>