<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Limit */

?>
<div class="limit-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
