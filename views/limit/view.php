<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Limit */
?>
<div class="limit-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'min',
            'max',
            'lock',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
