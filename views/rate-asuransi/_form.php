<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RateAsuransi */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="rate-asuransi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'umur')->textInput(['placeholder' => 'Umur']) ?>

    <?= $form->field($model, 'tenor_1')->textInput(['placeholder' => 'Tenor 1']) ?>

    <?= $form->field($model, 'tenor_2')->textInput(['placeholder' => 'Tenor 2']) ?>

    <?= $form->field($model, 'tenor_3')->textInput(['placeholder' => 'Tenor 3']) ?>

    <?= $form->field($model, 'tenor_4')->textInput(['placeholder' => 'Tenor 4']) ?>

    <?= $form->field($model, 'tenor_5')->textInput(['placeholder' => 'Tenor 5']) ?>

    <?= $form->field($model, 'tenor_6')->textInput(['placeholder' => 'Tenor 6']) ?>

    <?= $form->field($model, 'tenor_7')->textInput(['placeholder' => 'Tenor 7']) ?>

    <?= $form->field($model, 'tenor_8')->textInput(['placeholder' => 'Tenor 8']) ?>

    <?= $form->field($model, 'tenor_9')->textInput(['placeholder' => 'Tenor 9']) ?>

    <?= $form->field($model, 'tenor_10')->textInput(['placeholder' => 'Tenor 10']) ?>

    <?= $form->field($model, 'tenor_11')->textInput(['placeholder' => 'Tenor 11']) ?>

    <?= $form->field($model, 'tenor_12')->textInput(['placeholder' => 'Tenor 12']) ?>

    <?= $form->field($model, 'tenor_13')->textInput(['placeholder' => 'Tenor 13']) ?>

    <?= $form->field($model, 'tenor_14')->textInput(['placeholder' => 'Tenor 14']) ?>

    <?= $form->field($model, 'tenor_16')->textInput(['placeholder' => 'Tenor 16']) ?>

    <?= $form->field($model, 'tenor_15')->textInput(['placeholder' => 'Tenor 15']) ?>

    <?= $form->field($model, 'tenor_18')->textInput(['placeholder' => 'Tenor 18']) ?>

    <?= $form->field($model, 'tenor_20')->textInput(['placeholder' => 'Tenor 20']) ?>

    <?= $form->field($model, 'tenor_19')->textInput(['placeholder' => 'Tenor 19']) ?>

    <?= $form->field($model, 'tenor_17')->textInput(['placeholder' => 'Tenor 17']) ?>

    <?= $form->field($model, 'tenor_21')->textInput(['placeholder' => 'Tenor 21']) ?>

    <?= $form->field($model, 'tenor_23')->textInput(['placeholder' => 'Tenor 23']) ?>

    <?= $form->field($model, 'tenor_22')->textInput(['placeholder' => 'Tenor 22']) ?>

    <?= $form->field($model, 'tenor_25')->textInput(['placeholder' => 'Tenor 25']) ?>

    <?= $form->field($model, 'tenor_24')->textInput(['placeholder' => 'Tenor 24']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
