<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RateAsuransi */

$this->title = 'Create Rate Asuransi';
$this->params['breadcrumbs'][] = ['label' => 'Rate Asuransi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rate-asuransi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
