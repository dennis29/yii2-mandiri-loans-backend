<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\RateAsuransi */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rate Asuransi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rate-asuransi-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Rate Asuransi'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'umur',
        'tenor_1',
        'tenor_2',
        'tenor_3',
        'tenor_4',
        'tenor_5',
        'tenor_6',
        'tenor_7',
        'tenor_8',
        'tenor_9',
        'tenor_10',
        'tenor_11',
        'tenor_12',
        'tenor_13',
        'tenor_14',
        'tenor_16',
        'tenor_15',
        'tenor_18',
        'tenor_20',
        'tenor_19',
        'tenor_17',
        'tenor_21',
        'tenor_23',
        'tenor_22',
        'tenor_25',
        'tenor_24',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
