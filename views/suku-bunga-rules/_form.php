<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SukuBungaRules */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="suku-bunga-rules-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'suku_bunga_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\SukuBunga::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Suku bunga'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'order_number')->textInput(['placeholder' => 'Order Number']) ?>

    <?= $form->field($model, 'suku_bunga_fixed')->textInput(['placeholder' => 'Suku Bunga Fixed']) ?>

    <?= $form->field($model, 'tahun_fixed')->textInput(['placeholder' => 'Tahun Fixed']) ?>

    <?= $form->field($model, 'min_tenor')->textInput(['placeholder' => 'Min Tenor']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
