<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SukuBungaRules */

$this->title = 'Create Suku Bunga Rules';
$this->params['breadcrumbs'][] = ['label' => 'Suku Bunga Rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="suku-bunga-rules-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
