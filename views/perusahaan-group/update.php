<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PerusahaanGroup */
?>
<div class="perusahaan-group-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
