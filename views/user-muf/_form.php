<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\UserMuf */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="user-muf-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'nip')->textInput(['maxlength' => true, 'placeholder' => 'Name'])->label('Name') ?>

    <?= $form->field($model, 'contact')->textInput(['maxlength' => true, 'placeholder' => 'Contact']) ?>

    <?php $catList=ArrayHelper::map(app\models\Region::find()->all(), 'id', 'name' ); ?> 
    <?= // Normal parent select
        $form->field($model, 'region_id')->dropDownList($catList); ?>
    <?= //Dependent Dropdown
        $form->field($model, 'area_id')->widget(DepDrop::classname(), [
        'options' => ['id'=>'area_id','placeholder' => 'Choose Area...'],
         'data' => \yii\helpers\ArrayHelper::map(app\models\Area::find()->all(), 'id', 'name'),
         'pluginOptions'=>[
             'depends'=>['usermuf-region_id'],
             'placeholder' => 'Choose Area...',
             'url' => Url::to(['subcat']),
             'initialize' => true
         ]
     ]); ?>

    <?= $form->field($model, 'slm')->textInput(['maxlength' => true, 'placeholder' => 'Slm']) ?>
    
    <?= $form->field($model, 'slm_contact')->textInput(['maxlength' => true, 'placeholder' => 'Slm Contact']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
