<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserMuf */

$this->title = 'Create User Muf';
$this->params['breadcrumbs'][] = ['label' => 'User Muf', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-muf-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
