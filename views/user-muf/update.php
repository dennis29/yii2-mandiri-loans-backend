<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserMuf */

$this->title = 'Update User Muf: ' . ' ' . $model->nip;
$this->params['breadcrumbs'][] = ['label' => 'User Muf', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-muf-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
