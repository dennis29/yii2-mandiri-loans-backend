<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Targ3tMarket */

$this->title = 'Update Target Market: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Targ3t Market', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="targ3t-market-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
