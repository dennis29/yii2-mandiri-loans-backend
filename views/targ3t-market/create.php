<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Targ3tMarket */

$this->title = 'Create Target Market';
$this->params['breadcrumbs'][] = ['label' => 'Targ3t Market', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="targ3t-market-create">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
