<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\Targ3tMarketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use kartik\alert\AlertBlock;
use yii\helpers\Url;
use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\bootstrap\Modal;


CrudAsset::register($this);


$this->title = 'Target Market';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>

<?php $form = ActiveForm::begin(['action'=>Url::to(['targ3t-market/import'],true),'options' => ['enctype' => 'multipart/form-data']]); ?>
<?= $form->field($excelModel, 'id')->hiddenInput()->label(false) ?>
<?= $form->field($excelModel, 'file')->fileInput(['accept' => 'application/vnd.ms-excel', 'required'=>true]) ?>
<?= Html::submitButton('Import' , 
    [
        'class' =>'btn btn-primary',
        'data-confirm' => Yii::t('yii', 'Are you sure you want to export?'),
    ]) 
?>
<?=Html::a('Export',Url::to(['targ3t-market/export'],true),
    [
        'class'=>'btn btn-default',
        'data-confirm' => Yii::t('yii', 'Are you sure you want to export?'),
    ])
?>
<?= 

 AlertBlock::widget([
    'type' => AlertBlock::TYPE_ALERT,
    'useSessionFlash' => true
]);
?>
<?php ActiveForm::end(); ?>

</br>
<div class="targ3t-market-index">
    <div class="row">
        
        <div class="col-md-6">

                <?= Html::a('Create Target Market', ['create'], ['class' => 'btn btn-success']) ?>
            
        </div>
        <div class="col-md-6">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>

    
    <?php 
    $gridColumn = [
        [
            'class' => 'kartik\grid\CheckboxColumn',
            'width' => '20px',
        ],
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'name',
            'label' => 'Nama',
        ],
        // 'name',
        // 'desc:ntext',
        [
            'attribute' => 'desc',
            'label' => 'Description',
        ],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'kartik\grid\ActionColumn',
            'noWrap' => true,
            'template' => '{update}{delete}',
            'headerOptions'=>[
                'style'=>'color:#3c8dbc'
            ],
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-targ3t-market']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
            'after'=>BulkButtonWidget::widget([
                            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Delete Selected',
                                ["bulk-delete"] ,
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    'role'=>'modal-remote-bulk',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Are you sure?',
                                    'data-confirm-message'=>'Are you sure want to delete this item'
                                ]),
                        ]).'&nbsp;'.
                         Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Delete All ('.number_format($dataProvider->getTotalCount()).')',
                                ['delete-all'],
                                [
                                    "class"=>"btn btn-danger btn-xs",
                                    // 'role'=>'modal-remote-bulk',
                                    // 'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'name'=>'aksi',
                                    'value'=>'delete all',
                                    'type'=>'submit',
                                    // 'data-toggle'=>"modal",
                                    // 'data-target'=>'#ajaxCrudModal'
                                    // 'data-request-method'=>'post',
                                    // 'data-confirm-title'=>'Are you sure?',
                                    'data-confirm'=>'Are you sure want to Delete all these item'
                                ]
                        ).
                        '<div class="clearfix"></div>',
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => '',
        // 'toolbar' => [
        //     '{export}',
        //     ExportMenu::widget([
        //         'dataProvider' => $dataProvider,
        //         'columns' => $gridColumn,
        //         'target' => ExportMenu::TARGET_BLANK,
        //         'fontAwesome' => true,
        //         'dropdownOptions' => [
        //             'label' => 'Full',
        //             'class' => 'btn btn-default',
        //             'itemsBefore' => [
        //                 '<li class="dropdown-header">Export All Data</li>',
        //             ],
        //         ],
        //         'exportConfig' => [
        //             ExportMenu::FORMAT_PDF => false
        //         ]
        //     ]) ,
        // ],
    ]); ?>

</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>