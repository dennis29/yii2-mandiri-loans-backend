<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Targ3tMarket */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Targ3t Market', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="targ3t-market-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Targ3t Market'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'desc:ntext',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerKprTarg3tMarket->totalCount){
    $gridColumnKprTarg3tMarket = [
        ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'kpr.id',
                'label' => 'Kpr'
            ],
                        ['attribute' => 'id', 'visible' => false],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerKprTarg3tMarket,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-kpr-targ3t-market']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Kpr Targ3t Market'),
        ],
        'export' => false,
        'columns' => $gridColumnKprTarg3tMarket
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerKtaTarg3tMarket->totalCount){
    $gridColumnKtaTarg3tMarket = [
        ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'kta.id',
                'label' => 'Kta'
            ],
                        ['attribute' => 'id', 'visible' => false],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerKtaTarg3tMarket,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-kta-targ3t-market']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Kta Targ3t Market'),
        ],
        'export' => false,
        'columns' => $gridColumnKtaTarg3tMarket
    ]);
}
?>
    </div>
</div>
