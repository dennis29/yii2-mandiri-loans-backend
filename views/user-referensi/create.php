<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserReferensi */

$this->title = 'Create User Referensi';
$this->params['breadcrumbs'][] = ['label' => 'User Referensi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-referensi-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
