<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserReferensi */

$this->title = 'Update User Referensi: ' . ' ' . $model->nip;
$this->params['breadcrumbs'][] = ['label' => 'User Referensi', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-referensi-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
