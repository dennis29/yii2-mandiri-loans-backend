<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Kpr */

?>
<div class="kpr-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->nama) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        // 'nama',
        // 'keterangan',
        [
            'format' => 'raw',
            'attribute'=>'profil_nasabah_text',
        ],
        [
            'format' => 'raw',
            'attribute'=>'peruntukan_text',
        ],
        [
            'format' => 'raw',
            'attribute'=>'market_text',
            'label'=> 'Target Market Text'
        ],
        [
            'class'=>'kartik\grid\DataColumn',
            'format' => 'raw',
            'headerOptions'=>['style'=>'width:20%'],
            'attribute'=>'benefit_text',
            'label' => 'Benefit View Kpr text'
        ],
        [
            'format' => 'raw',
            'attribute'=>'suku_bunga_kpr_text',
        ],
        [
            'format' => 'raw',
            'attribute'=>'suku_bunga_multiguna_text',
        ],
        [
            'format' => 'raw',
            'attribute'=>'peserta_text',
        ],
        [
            'format' => 'raw',
            'attribute'=>'keterangan',
        ],
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>