<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->kprProfesiKprs,
        'key' => function($model){
            return ['kpr_id' => $model->kpr_id, 'profesi_kpr_id' => $model->profesi_kpr_id];
        }
    ]);
    $gridColumns = [
        // ['class' => 'yii\grid\SerialColumn'],
        // [
        //         'attribute' => 'profesiKpr.id',
        //         'label' => 'Profesi Kpr'
        //     ],
        // ['attribute' => 'lock', 'visible' => false],
        // ['attribute' => 'id', 'visible' => false],
        // [
        //     'class' => 'yii\grid\ActionColumn',
        //     'controller' => 'kpr-profesi-kpr'
        // ],
        [
                'attribute' => 'profesiKpr.nama',
                'label' => 'Target Market',
                'headerOptions' => ['class'=>'hidden'],
            ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'summary'=>'',
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
