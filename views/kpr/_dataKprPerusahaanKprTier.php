<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->kprPerusahaanKprTiers,
        'key' => function($model){
            return ['kpr_id' => $model->kpr_id, 'perusahaan_kpr_tier_id' => $model->perusahaan_kpr_tier_id];
        }
    ]);
    $gridColumns = [
        // ['class' => 'yii\grid\SerialColumn'],
        [
                'attribute' => 'perusahaanKprTier.name',
                'label' => 'Perusahaan Kpr Tier',
                'headerOptions'=>[
                    'class'=>'hidden'
                ],

            ],
        ['attribute' => 'id', 'visible' => false],
        ['attribute' => 'lock', 'visible' => false],
        // [
        //     'class' => 'yii\grid\ActionColumn',
        //     'controller' => 'kpr-perusahaan-kpr-tier'
        // ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'summary'=>'',
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
