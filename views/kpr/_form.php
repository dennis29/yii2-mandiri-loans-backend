<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Kpr */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'KprDeveloperTier', 
        'relID' => 'kpr-developer-tier', 
        'value' => \yii\helpers\Json::encode($model->kprDeveloperTiers),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'KprProfesiKpr', 
        'relID' => 'kpr-profesi-kpr', 
        'value' => \yii\helpers\Json::encode($model->kprProfesiKprs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'KprSukuBunga', 
        'relID' => 'kpr-suku-bunga', 
        'value' => \yii\helpers\Json::encode($model->kprSukuBungas),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'KprTarg3tMarket', 
        'relID' => 'kpr-targ3t-market', 
        'value' => \yii\helpers\Json::encode($model->kprTarg3tMarkets),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Proyek', 
        'relID' => 'proyek', 
        'value' => \yii\helpers\Json::encode($model->proyeks),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,  
   'viewParams' => [ 
       'class' => 'KprPerusahaanKprTier',  
       'relID' => 'kpr-perusahaan-kpr-tier',  
       'value' => \yii\helpers\Json::encode($model->kprPerusahaanKprTiers), 
       'isNewRecord' => ($model->isNewRecord) ? 1 : 0
   ]
]);
?>

<div class="kpr-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'order_number')->textInput(['type' => 'number', 'placeholder' => 'Order']) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true, 'placeholder' => 'Nama']) ?>


    <?= $form->field($model, 'expired')->widget(\kartik\date\DatePicker::classname(), [
        'type' =>  \kartik\date\DatePicker::TYPE_COMPONENT_PREPEND,
        
        'pluginOptions' => [
            'placeholder' => 'Choose Pilih tanggal expired',
            'autoclose' => true,
            'format' =>'dd-mm-yyyy',
        ]
        
    ])->label('Expired'); ?>
    
    <?= $form->field($model, 'keterangan')->widget(TinyMce::className(), [
        'options' => ['rows' => 12],
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ])->label('Summary KPR');?>
    
    <?= $form->field($model, 'profil_nasabah_text')->widget(TinyMce::className(), [
        'options' => ['rows' => 12],
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ])->label('Summary Multiguna');?>
    
    <?= $form->field($model, 'peruntukan_text')->widget(TinyMce::className(), [
        'options' => ['rows' => 12],
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>

    <?= $form->field($model, 'market_text')->widget(TinyMce::className(), [
        'options' => ['rows' => 12],
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>

    <?= $form->field($model, 'benefit_text')->widget(TinyMce::className(), [
        'options' => ['rows' => 12],
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>

    <?= $form->field($model, 'suku_bunga_kpr_text')->widget(TinyMce::className(), [
        'options' => ['rows' => 12],
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>


    <?= $form->field($model, 'suku_bunga_multiguna_text')->widget(TinyMce::className(), [
        'options' => ['rows' => 12],
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>    

    <?= $form->field($model, 'peserta_text')->widget(TinyMce::className(), [
        'options' => ['rows' => 12],
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>


    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Developer Tier'),
            'content' => $this->render('_formKprDeveloperTier', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->kprDeveloperTiers),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Profesi Kpr'),
            'content' => $this->render('_formKprProfesiKpr', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->kprProfesiKprs),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Suku Bunga'),
            'content' => $this->render('_formKprSukuBunga', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->kprSukuBungas),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Target Market'),
            'content' => $this->render('_formKprTarg3tMarket', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->kprTarg3tMarkets),
            ]),
        ],
        [
           'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Perusahaan'),
           'content' => $this->render('_formKprPerusahaanKprTier', [
               'row' => \yii\helpers\ArrayHelper::toArray($model->kprPerusahaanKprTiers),
           ]),
       ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'header'=>'<h3>Konfirmasi Approver</h3>',
    'toggleButton' => ['label' => 'Submit','class'=>'btn btn-success'],
    "footer"=>"",// always need it for jquery plugin
])?>
    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'user_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->where(['status'=>10])->all(), 'id', 'name'),
            'options' => ['placeholder' => 'User Approver'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('User Approver'); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', 
            [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                                    
            ]) ?>
    </div>
<?php Modal::end(); ?>
<?php ActiveForm::end(); ?>

</div>
