<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\KprSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\alert\AlertBlock;
use kartik\widgets\SwitchInput;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;


$this->title = 'Kpr';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
    $('.search-form').toggle(1000);
    return false;
});";
$this->registerJs($search);
?>

<?php $form = ActiveForm::begin(['action'=>Url::to(['kpr/import'],true),'options' => ['enctype' => 'multipart/form-data']]); ?>
<?= $form->field($excelModel, 'id')->hiddenInput()->label(false) ?>
<?= $form->field($excelModel, 'file')->fileInput(['accept' => 'application/vnd.ms-excel', 'required'=>true]) ?>
<?=Html::a('Export',Url::to(['kpr/export'],true),
    [
        'class'=>'btn btn-default',
        // 'data-confirm' => Yii::t('yii', 'Are you sure you want to Export?'),
    ])
?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'header'=>'<h3>Konfirmasi Approver</h3>',
    'toggleButton' => ['label' => 'Import','class'=>'btn btn-primary'],
    "footer"=>"",// always need it for jquery plugin
])?>

    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'user_id')->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->where(['status'=>10])->all(), 'id', 'name'),
            'options' => ['placeholder' => 'User Approver'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label('User Approver'); ?>
    <div class="form-group">
        <?= Html::submitButton('Submit' , 
            [
                'class' =>'btn btn-primary',
                'data-confirm' => Yii::t('yii', 'Are you sure you want to Import?'),
            ]) 
        ?>
    </div>
<?php Modal::end(); ?>
<?php ActiveForm::end(); ?>
<?= 

 AlertBlock::widget([
    'type' => AlertBlock::TYPE_ALERT,
    'useSessionFlash' => true
]);
?>
<br/>
<div class="kpr-index">
    
    <p>
        <?= Html::a('Create Kpr', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
         [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        'nama',
        [
            'attribute' => 'expired',
            'filterType'=> \kartik\grid\GridView::FILTER_DATE, 
            'filterWidgetOptions' => [
                    'options' => ['placeholder' => 'Select date'],
                    'pluginOptions' => [
                        'format' => 'dd-mm-yyyy',
                        'todayHighlight' => true
                    ]
            ],
        ],
        [
        // 'class' => 'kartik\grid\CheckboxColumn',
            'attribute' => 'is_approved',
            'label' => 'Status',
            'value' => function($model){
                if($model->is_approved == 1){
                    return "Publish";
                }
                if($model->is_approved == 0){
                    return "Pending";
                }
            },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => [0=>'Pending',1=>'Publish'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Status', 'id' => 'grid-proyek-search-is_approved']
        ],
        // [
        // 'class' => 'kartik\grid\CheckboxColumn',
            // 'label' => 'Status',
            // 'value' => function($model){
            //     if($model->is_approved == 1){
            //         return "Publish";
            //     }
            //     if($model->is_approved == 0){
            //         return "Pending";
            //     }
            // }
            // 'width' => '20px',
            // 'options'=>[
            //     "data-toggle"=>"toggle",
            //     "class"=>"checkbox"
            // ],
            // // 'headerOptions' =>['style'=>'color: #3c8dbc;'],
            // 'format'=>'raw',
            // 'value'=> function($model){
            //     // return json_encode($model->sessions);
            //     // $current_session = Yii::$app->session->get('current-session');
            //     // $session_id = $current_session->ID;

            //     // $obj = \app\models\Pic::find()->joinWith(['sessions'])->where(['PIC.ID'=>$model->ID,'SESSION_.ID'=>$session_id])->one();
            //     if($model->is_approved == 1){
            //         $obj = true;
            //     }else{
            //         $obj = false;
            //     }
            //     // $far = Yii::$app->user->identity->status;
            //     // return $far;
            //     if(Yii::$app->user->identity->status == 10){
            //         return SwitchInput::widget([
            //             'name' => 'status_13',
            //             // 'disabled' => 'true',
            //             // 'value' => function($model){
            //             //     // if(empty($model->kpr->is_proved)){
            //             //     //     return $model->kpr->is_proved = 1;
            //             //     // }
            //             // },
            //             'options'=>[
            //                 'checked'=>$obj,
            //                 'onChange'=>'changeStatus("'.$model->id.'",this.checked)',
            //             ],
            //             'pluginOptions' => [
            //                 'onText' => 'Approved',
            //                 'offText' => 'Reject',
            //                 'onColor' => 'primary',
            //                 'offColor' => 'default',
            //                 ]
            //             ]);
            //     }else{
            //         return SwitchInput::widget([
            //             'name' => 'status_3',
            //             'disabled' => 'true',
            //             // 'value' => function($model){
            //             // },
            //             'options'=>[
            //                 'checked'=>$obj,
            //                 // 'onChange'=>'changeStatus("'.$model->ID.'",this.checked)',
            //             ],
            //             'pluginOptions' => [
            //                 'onText' => 'Approved',
            //                 'offText' => 'Reject',
            //                 'onColor' => 'primary',
            //                 'offColor' => 'default',
            //                 ]
            //             ]);
            //     }
            // }
            // 'visible' => $is_admin_only,
        
        // ],
        // [
        //     'format' => 'raw',
        //     'attribute'=>'profil_nasabah_text',
        // ],
        // [
        //     'format' => 'raw',
        //     'attribute'=>'peruntukan_text',
        // ],
        // [
        //     'format' => 'raw',
        //     'attribute'=>'market_text',
        //     'label'=> 'Target Market Text'
        // ],
        // [
        //     'class'=>'kartik\grid\DataColumn',
        //     'format' => 'raw',
        //     'headerOptions'=>['style'=>'width:20%'],
        //     'attribute'=>'benefit_text',
        //     'label' => 'Benefit View Kpr text'
        // ],
        // [
        //     'format' => 'raw',
        //     'attribute'=>'suku_bunga_kpr_text',
        // ],
        // [
        //     'format' => 'raw',
        //     'attribute'=>'suku_bunga_multiguna_text',
        // ],
        // [
        //     'format' => 'raw',
        //     'attribute'=>'peserta_text',
        // ],
        // [
        //     'format' => 'raw',
        //     'attribute'=>'keterangan',
        // ],
        
        // [
        //   'format' => 'raw',
        //     'value' => function ($model, $key, $index, $column) {
        //         return Yii::$app->controller->renderPartial('_dataKprTarg3tMarket', ['model' => $model]);
        //     },
        //     'header' => '
        //         <table style="">
        //             <tr>
        //                 <td style=""> Target Market </td>
        //             </tr>
        //         </table>
        //     ',  
        // ],
        //  [
        //   'format' => 'raw',
        //     'value' => function ($model, $key, $index, $column) {
        //         return Yii::$app->controller->renderPartial('_dataKprProfesiKpr', ['model' => $model]);
        //     },
        //     'header' => '
        //         <table style="">
        //             <tr>
        //                 <td style=""> Profesi </td>
        //             </tr>
        //         </table>
        //     ',  
        // ],

        // [
        //   'format' => 'raw',
        //     'value' => function ($model, $key, $index, $column) {
        //         return Yii::$app->controller->renderPartial('_dataKprSukuBunga', ['model' => $model]);
        //     },
        //     'header' => '
        //         <table style="">
        //             <tr>
        //                 <td style=""> Suku Bunga</td>
        //             </tr>
        //         </table>
        //     ',  
        // ],
        // [
        //   'format' => 'raw',
        //     'value' => function ($model, $key, $index, $column) {
        //         return Yii::$app->controller->renderPartial('_dataKprDeveloperTier', ['model' => $model]);
        //     },
        //     'header' => '
        //         <table style="">
        //             <tr>
        //                 <td style=""> Developer</td>
        //             </tr>
        //         </table>
        //     ',  
        // ],
        // [
        //   'format' => 'raw',
        //     'value' => function ($model, $key, $index, $column) {
        //         return Yii::$app->controller->renderPartial('_dataKprPerusahaanKprTier', ['model' => $model]);
        //     },
        //     'header' => '
        //         <table style="">
        //             <tr>
        //                 <td style=""> Perusahaan </td>
        //             </tr>
        //         </table>
        //     ',  
        // ],
        ['attribute' => 'id', 'visible' => false],

        ['attribute' => 'lock', 'visible' => false],
        [
            'header'=>'Actions',
            'headerOptions'=>[
                'style'=>'color:#3c8dbc'
            ],
            'value'=> function ($model, $key, $index, $column) {
                   $a = Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update','id'=>$model->id], ['class' => '', 'id' => 'popupModal']);      
                   $b =   Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete-page','id'=>$model->id], ['class' => '', 'id' => 'popupModal']);      
                   return $a.$b;
              },
             'format' => 'raw'
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        // 'pjax' => true,
        // 'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-kpr']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            // '{export}',
            // ExportMenu::widget([
            //     'dataProvider' => $dataProvider,
            //     'columns' => $gridColumn,
            //     'target' => ExportMenu::TARGET_BLANK,
            //     'fontAwesome' => true,
            //     'dropdownOptions' => [
            //         'label' => 'Full',
            //         'class' => 'btn btn-default',
            //         'itemsBefore' => [
            //             '<li class="dropdown-header">Export All Data</li>',
            //         ],
            //     ],
            //     'exportConfig' => [
            //         ExportMenu::FORMAT_PDF => false
            //     ]
            // ]) ,
        ],
    ]); ?>
</div>

<script type="text/javascript">
    function changeStatus(id,is_approved)
    {
        $.ajax({
            url: '<?php echo Url::to('kpr/change-approval') ?>',
           type: 'post',
           data: {
                id: id ,
                is_approved: is_approved
            },
           success: function (data) {
              // console.log(data);
              // var raw = JSON.parse(data);
              // var res = raw.result;
              console.log(data);
             
           }
 
      });
    }
</script>