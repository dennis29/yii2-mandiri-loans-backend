<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->kprSukuBungas,
        'key' => function($model){
            return ['kpr_id' => $model->kpr_id, 'suku_bunga_id' => $model->suku_bunga_id];
        }
    ]);
    $gridColumns = [
        // ['class' => 'yii\grid\SerialColumn'],
        // [
        //         'attribute' => 'sukuBunga.id',
        //         'label' => 'Suku Bunga'
        //     ],
        // ['attribute' => 'id', 'visible' => false],
        // ['attribute' => 'lock', 'visible' => false],
        // [
        //     'class' => 'yii\grid\ActionColumn',
        //     'controller' => 'kpr-suku-bunga'
        // ],
    [
                'attribute' => 'sukuBunga.nama',
                'label' => 'Suku Bunga',
                'headerOptions' => ['class'=>'hidden'],
            ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'summary'=>'',
        
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
