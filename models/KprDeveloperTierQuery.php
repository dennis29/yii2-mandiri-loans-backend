<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[KprDeveloperTier]].
 *
 * @see KprDeveloperTier
 */
class KprDeveloperTierQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return KprDeveloperTier[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return KprDeveloperTier|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}