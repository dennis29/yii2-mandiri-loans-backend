<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[KtaTarg3tMarket]].
 *
 * @see KtaTarg3tMarket
 */
class KtaTarg3tMarketQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return KtaTarg3tMarket[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return KtaTarg3tMarket|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}