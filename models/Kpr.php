<?php

namespace app\models;
use Yii;
use \app\models\base\Kpr as BaseKpr;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "kpr".
 */
class Kpr extends BaseKpr
{
    /**
     * @inheritdoc
     */
    const Type_KPR_Takeover = 1;
    const Type_KPR_Top_Up = 2;
    const Type_Multiguna_Takeover = 3;
    const Type_Multiguna_Top_Up = 4;

    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nama'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['nama', 'keterangan','profil_nasabah_text', 'peruntukan_text', 'market_text', 'benefit_text', 'suku_bunga_kpr_text', 'suku_bunga_multiguna_text', 'peserta_text','user_id'], 'required'],
            [['lock','order_number'], 'integer'],
            [['id', 'nama', 'expired','created_by', 'updated_by', 'created_at', 'updated_at'], 'string', 'max' => 255],
            [['keterangan'], 'string'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }

    // public function triggerNotifikasi($sesudah){
    //     $model  = Kpr::findOne($this->id);
    //     $sebelum = ArrayHelper::toArray($model);
    //     $sebelum['developerTiers'] = ArrayHelper::toArray($model->developerTiers);
    //     $sebelum['profesiKprs'] = ArrayHelper::toArray($model->profesiKprs);
    //     $sebelum['sukuBungas'] = ArrayHelper::toArray($model->sukuBungas);
    //     $sebelum['targ3tMarkets'] = ArrayHelper::toArray($model->targ3tMarkets);
    //     $sebelum['perusahaanKprTiers'] = ArrayHelper::toArray($model->perusahaanKprTiers);


    //     $notifikasi = new Notifikasi();
    //     $notifikasi->code = Notifikasi::Code_KPR;
    //     $notifikasi->sebelum = json_encode($sebelum);
    //     $notifikasi->sesudah = $sesudah;
    //     $notifikasi->status = 0;
    //     $notifikasi->is_read = 0;
    //     if(!$notifikasi->save()){
    //         // echo 'asd123';
    //         echo json_encode($notifikasi->getErrors());
    //         die();
    //     }
    // }

    public function triggerNotifikasi($sesudah,$state = 'update'){

        if($state == 'delete' || $state == 'update'){
        $model  = Kpr::findOne($this->id);
        $sebelum = ArrayHelper::toArray($model);
        $sebelum['developerTiers'] = ArrayHelper::toArray($model->developerTiers);
        $sebelum['profesiKprs'] = ArrayHelper::toArray($model->profesiKprs);
        $sebelum['sukuBungas'] = ArrayHelper::toArray($model->sukuBungas);
        $sebelum['targ3tMarkets'] = ArrayHelper::toArray($model->targ3tMarkets);
        $sebelum['perusahaanKprTiers'] = ArrayHelper::toArray($model->perusahaanKprTiers);
        }
        $notifikasi = new Notifikasi();
        $notifikasi->code = Notifikasi::Code_KPR;
        if($state == 'delete' || $state == 'update'){
            $notifikasi->sebelum = json_encode($sebelum);
        }
        if($state == 'create' || $state == 'update'){
            $notifikasi->sesudah = $sesudah;
        }
        $notifikasi->status = 0;
        $notifikasi->is_read = 0;
        $request = Yii::$app->request;
        $post = $request->post();
        $sesudah = $post['Kpr'];
        $notifikasi->user_id = $sesudah["user_id"];
        if(!$notifikasi->save()){
            var_dump($notifikasi->getErrors());
            die();
        }
    }
}
