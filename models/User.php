<?php
namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_OPERATOR = 5;
    const STATUS_ADMIN_BIASA = 5;
    const STATUS_ACTIVE = 10;
    const STATUS_APPROVER = 10;
    const STATUS_MOBILE = 20;
    const STATUS_VERIFIED = 30;
    const STATUS_REGION = 12;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [
                self::STATUS_ACTIVE, 
                self::STATUS_DELETED,
                self::STATUS_MOBILE,
                self::STATUS_VERIFIED,
                self::STATUS_OPERATOR,
                self::STATUS_REGION
                ]],
            ['verify_token','string']
        ];
    }

    public function getIsVerified()
    {
        if($this->status == self::STATUS_VERIFIED){
            return 1;
        }else{
            return 0;
        }
        // return $this->status == self::STATUS_VERIFIED;
    }

    public function sendVerification($resend = false){
        if(!$resend){
            $token = $this->generateVerifyEmailToken();
        }

        /* @var $user User */
       // echo 'asd';die();
        // return \Yii::$app
        //     ->mailer
        //     ->compose(
        //         ['html' => 'verificationToken-html'],
        //         ['user' => $this]
        //     )
        //     ->setFrom('noreply@clgbankmandiri.com')
        //     ->setTo($this->email)
        //     ->setSubject('Verify email mandiri pocl')
        //     // ->setHtmlBody('<b>'.Html::a('Verify',[Url::to(['site/verify','id'=>$this->id, 'verify'=>$this->verify_token],true)]).'</b>')
        //     ->send();
        return \Yii::$app
            ->mailer
            ->compose(
                ['html' => 'verificationToken-html'],
                ['user' => $this]
            )
            ->setFrom('noreply@clg-bankmandiri.com')
            ->setTo($this->email)
            ->setSubject('Verifikasi e-mail untuk Aplikasi Consumer Loans Information Center')
            // ->setSubject('Terdapat perubahan pada ')
            // ->setHtmlBody('<b>'.Html::a('Verify',[Url::to(['site/verify','id'=>$this->id, 'verify'=>$this->verify_token],true)]).'</b>')
            ->send();   
    
    }
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => [self::STATUS_ACTIVE,self::STATUS_OPERATOR,self::STATUS_REGION]]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::find()->where(['nip' => $username])->andWhere(['in','status',[self::STATUS_ACTIVE,self::STATUS_MOBILE,self::STATUS_VERIFIED,self::STATUS_OPERATOR,self::STATUS_REGION]])->one();
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            // 'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        // if (empty($token)) {
        //     return false;
        // }

        // $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        // $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        // return $timestamp + $expire >= time();
        return true;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        $password_hashing = hash('sha256', md5($password));
        // $password_hashed = Yii::$app->security->generatePasswordHash($password_hashing);
        // $password_hashedd = hash('sha256', md5('123456'));
        // var_dump($password);
        // echo "<br/>";
        // var_dump($this->password_hash);
        // die();
        return Yii::$app->security->validatePassword($password, $this->password_hash);
        // return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function validatePasswordMobile($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function generateVerifyEmailToken()
    {
        $this->verify_token = Yii::$app->security->generateRandomString() . '_' . time();
        $this->save(false);
        return $this->verify_token;
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(\app\models\Region::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArea()
    {
        return $this->hasOne(\app\models\Area::className(), ['id' => 'area_id']);
    }

}
