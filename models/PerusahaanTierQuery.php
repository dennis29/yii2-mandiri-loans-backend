<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PerusahaanTier]].
 *
 * @see PerusahaanTier
 */
class PerusahaanTierQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PerusahaanTier[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PerusahaanTier|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}