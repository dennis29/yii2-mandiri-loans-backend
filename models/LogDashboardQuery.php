<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[LogDashboard]].
 *
 * @see LogDashboard
 */
class LogDashboardQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return LogDashboard[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return LogDashboard|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}