<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Reward]].
 *
 * @see Reward
 */
class RewardQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Reward[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Reward|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}