<?php

namespace app\models;

use \app\models\base\Notifikasi as BaseNotifikasi;

/**
 * This is the model class for table "notifikasi".
 */
class Notifikasi extends BaseNotifikasi
{
    /**
     * @inheritdoc
     */
    const Code_KPR = 1;
    const Code_KTA = 2;
    const Code_SB = 3;
    const Code_PROYEK = 4;
    const Code_PERUSAHAANLIST = 5;
    const Code_PERUSAHAANGROUP = 6;
    const Code_PERUSAHAANTIER = 7;
    const Code_DEVELOPERLIST = 8;
    const Code_DEVELOPERGROUP = 9;
    const Code_DEVELOPERTIER = 10;
    const Code_PERUSAHAANKPR = 11;
    const Code_PERUSAHAANKPRTIER = 12;


    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['code',  'status', 'is_read'], 'required'],
            [['code', 'status', 'is_read'], 'integer'],
            [['sebelum', 'sesudah'], 'string'],
            [['id'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
        ]);
    }

    public function beforeSave($insert)
    {
        $this->sendMail();
        return parent::beforeSave($insert);
    }

    private function sendMail(){
        $user = User::findOne($this->user_id);
        return \Yii::$app
            ->mailer
            ->compose(
            ['html' => 'notifikasi-html'],
                ['notifikasi' => $this]
            )
            ->setFrom('noreply@clg-bankmandiri.com')
            ->setTo($user->email)
            ->setSubject('Terdapat perubahan pada '.$this->codeStr())
            // ->setHtmlBody('<b>'.Html::a('Verify',[Url::to(['site/verify','id'=>$this->id, 'verify'=>$this->verify_token],true)]).'</b>')
            ->send();
    }

    public function codeStr(){
        if($this->code==Notifikasi::Code_KPR){
            return "KPR";
        }else
         if($this->code==Notifikasi::Code_KTA){
            return "KTA";
        }else
        if($this->code==Notifikasi::Code_SB){
            return "Suku Bunga";
        }else
        if($this->code==Notifikasi::Code_PERUSAHAANLIST){
            return "Perusahaan";
        }else
        if($this->code==Notifikasi::Code_PROYEK){
            return "Proyek";
        }else
        {

            return "Perusahaan";
        }       
    }
	
}
