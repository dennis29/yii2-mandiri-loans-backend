<?php

namespace app\models;

use \app\models\base\DeveloperGroup as BaseDeveloperGroup;
// use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "developer_group".
 */
class DeveloperGroup extends BaseDeveloperGroup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [[ 'name'], 'required'],
            [['lock','is_publish'], 'integer'],
            [['id', 'name', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'string', 'max' => 255],
            [['text'],'string'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }

    // public function triggerNotifikasi($sesudah){
    //     $model  = DeveloperGroup::findOne($this->id);
    //     $sebelum = ArrayHelper::toArray($model);
        
    //     $notifikasi = new Notifikasi();
    //     $notifikasi->code = Notifikasi::Code_DEVELOPERGROUP;
    //     $notifikasi->sebelum = json_encode($sebelum);
    //     $notifikasi->sesudah = $sesudah;
    //     $notifikasi->status = 0;
    //     $notifikasi->is_read = 0;
    //     if(!$notifikasi->save()){
    //         var_dump($notifikasi->getErrors());
    //         die();
    //     }
    // }
	
}
