<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PerusahaanKprTier]].
 *
 * @see PerusahaanKprTier
 */
class PerusahaanKprTierQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PerusahaanKprTier[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PerusahaanKprTier|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}