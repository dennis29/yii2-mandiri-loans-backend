<?php

namespace app\models;

use Yii;
use \app\models\base\MenuKta as BaseMenuKta;
use yii\web\UploadedFile;


/**
 * This is the model class for table "menu_kta".
 */
class MenuKta extends BaseMenuKta
{
    /**
     * @inheritdoc
     */

    public $imageMenu;

    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['title'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [[], 'required'],
            [['lock'], 'integer'],
            [['imageMenu'], 'safe'],
            [['id', 'image', 'link', 'title', 'text'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['link'],'url'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    public function upload()
    {
        if ($this->validate()) {
            $this->imageMenu = UploadedFile::getInstance($this, 'imageMenu');
            if (!empty($this->imageMenu)){
                $this->imageMenu->saveAs(Yii::$app->basePath . '/web/uploads/menu-kta/' . md5($this->imageMenu->baseName . date("H:i:s")) . '.' . $this->imageMenu->extension);
                $this->image = '@web/uploads/menu-kta/' . md5($this->imageMenu->baseName . date("H:i:s")) . '.' . $this->imageMenu->extension;
                $this->image_name = $this->imageMenu->baseName . '.' . $this->imageMenu->extension;
                $this->imageMenu = null;
                $this->save();
            }
            return true;
        } else { 
            return false;
        }
    }
}
