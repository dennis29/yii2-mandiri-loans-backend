<?php

namespace app\models;

use Yii;
use \app\models\base\Slider as BaseSlider;
use yii\web\UploadedFile;

/**
 * This is the model class for table "slider".
 */
class Slider extends BaseSlider
{
    /**
     * @inheritdoc
     */
    public $imageSlider;
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [[], 'required'],
            [['lock','order_number'], 'integer'],
            [['id', 'image'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['imageSlider'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, png'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->imageSlider = UploadedFile::getInstance($this, 'imageSlider');
            if(($this->imageSlider)){
                $this->imageSlider->saveAs(Yii::$app->basePath . '/web/uploads/slider/' . md5($this->imageSlider->baseName . date("H:i:s")) . '.' . $this->imageSlider->extension);
                $this->image = '@web/uploads/slider/' . md5($this->imageSlider->baseName . date("H:i:s")) . '.' . $this->imageSlider->extension;
                $this->image_name = $this->imageSlider->baseName . '.' . $this->imageSlider->extension;
                $this->imageSlider = null;
                $this->save();
            }
            return true;
        } else { 
            return false;
        }
    }
	
}
