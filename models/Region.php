<?php

namespace app\models;

use \app\models\base\Region as BaseRegion;

/**
 * This is the model class for table "region".
 */
class Region extends BaseRegion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['name'], 'required'],
            [['lock'], 'integer'],
            [['id', 'name', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }

    public function getAreaOut(){
        $data=\app\models\Area::find()
       ->where(['region_id'=>$this->id])
       ->select(['id','name'])->asArray()->all();

        return $data;
    }
}
