<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Kta]].
 *
 * @see Kta
 */
class KtaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Kta[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Kta|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}