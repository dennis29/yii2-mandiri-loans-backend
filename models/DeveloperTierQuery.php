<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[DeveloperTier]].
 *
 * @see DeveloperTier
 */
class DeveloperTierQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return DeveloperTier[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DeveloperTier|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}