<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KprSukuBunga;

/**
 * app\models\KprSukuBungaSearch represents the model behind the search form about `app\models\KprSukuBunga`.
 */
 class KprSukuBungaSearch extends KprSukuBunga
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kpr_id', 'id', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'safe'],
            [['suku_bunga_id', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KprSukuBunga::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'suku_bunga_id' => $this->suku_bunga_id,
            'lock' => $this->lock,
        ]);

        $query->andFilterWhere(['like', 'kpr_id', $this->kpr_id])
            ->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at]);

        return $dataProvider;
    }
}
