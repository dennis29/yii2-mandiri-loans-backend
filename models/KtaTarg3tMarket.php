<?php

namespace app\models;

use \app\models\base\KtaTarg3tMarket as BaseKtaTarg3tMarket;

/**
 * This is the model class for table "kta_targ3t_market".
 */
class KtaTarg3tMarket extends BaseKtaTarg3tMarket
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['kta_id', 'targ3t_market_id'], 'required'],
            [['lock'], 'integer'],
            [['kta_id', 'targ3t_market_id', 'id', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
