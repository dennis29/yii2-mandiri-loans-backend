<?php

namespace app\models;

use \app\models\base\KtaPerusahaanTier as BaseKtaPerusahaanTier;

/**
 * This is the model class for table "kta_perusahaan_tier".
 */
class KtaPerusahaanTier extends BaseKtaPerusahaanTier
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['kta_id', 'perusahaan_tier_id'], 'required'],
            [['lock'], 'integer'],
            [['kta_id', 'perusahaan_tier_id', 'id', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
