<?php

namespace app\models;

use \app\models\base\SukuBungaRules as BaseSukuBungaRules;

/**
 * This is the model class for table "suku_bunga_rules".
 */
class SukuBungaRules extends BaseSukuBungaRules
{
    /**
     * @inheritdoc
     */
     const TYPE_KPR = 0;
     const TYPE_MULTIGUNA = 1;


    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [[ 'suku_bunga_id', 'is_multiguna', 'order_number', 'suku_bunga_fixed', 'tahun_fixed', 'min_tenor'], 'required'],
            [['is_multiguna', 'order_number', 'tahun_fixed', 'min_tenor', 'lock'], 'integer'],
            [['suku_bunga_fixed'], 'double'],
            [['id', 'suku_bunga_id', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
