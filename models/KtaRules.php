<?php

namespace app\models;

use \app\models\base\KtaRules as BaseKtaRules;

/**
 * This is the model class for table "kta_rules".
 */
class KtaRules extends BaseKtaRules
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['kta_id', 'min_tenor', 'max_tenor', 'suku_bunga_floating', 'suku_bunga_flat', 'max_dbr', 'limit_min', 'limit_max', 'income_min', 'income_max'], 'required'],
            [['min_tenor', 'max_tenor', 'lock'], 'integer'],
            [['suku_bunga_floating', 'suku_bunga_flat', 'max_dbr', 'limit_min', 'limit_max', 'income_min', 'income_max'], 'number'],
            [['id', 'kta_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
