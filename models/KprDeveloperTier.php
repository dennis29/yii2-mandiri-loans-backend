<?php

namespace app\models;

use \app\models\base\KprDeveloperTier as BaseKprDeveloperTier;

/**
 * This is the model class for table "kpr_developer_tier".
 */
class KprDeveloperTier extends BaseKprDeveloperTier
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['kpr_id', 'developer_tier_id'], 'required'],
            [['lock'], 'integer'],
            [['kpr_id', 'developer_tier_id', 'updated_at', 'created_at', 'updated_by', 'created_by', 'id'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
