<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KtaTarg3tMarket;

/**
 * app\models\KtaTarg3tMarketSearch represents the model behind the search form about `app\models\KtaTarg3tMarket`.
 */
 class KtaTarg3tMarketSearch extends KtaTarg3tMarket
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kta_id', 'targ3t_market_id', 'id', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'safe'],
            [['lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KtaTarg3tMarket::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'lock' => $this->lock,
        ]);

        $query->andFilterWhere(['like', 'kta_id', $this->kta_id])
            ->andFilterWhere(['like', 'targ3t_market_id', $this->targ3t_market_id])
            ->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at]);

        return $dataProvider;
    }
}
