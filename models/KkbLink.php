<?php

namespace app\models;

use \app\models\base\KkbLink as BaseKkbLink;

/**
 * This is the model class for table "kkb_link".
 */
class KkbLink extends BaseKkbLink
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['title'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['title','link'], 'required'],
            [['lock'], 'integer'],
            [['link'], 'url'],
            [['id', 'title', 'link', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
