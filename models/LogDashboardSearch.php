<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogDashboard;

/**
 * app\models\LogDashboardSearch represents the model behind the search form about `app\models\LogDashboard`.
 */
 class LogDashboardSearch extends LogDashboard
{
    /**
     * @inheritdoc
     */
    public $start_date;
    public $end_date;
    public $region_id;
    public $area_id;
    public function rules()
    {
        return [
            [['id', 'ip_address', 'program_id', 'created_at', 'created_by', 'updated_at', 'updated_by','start_date','end_date','region_id','area_id'], 'safe'],
            [['code', 'user_id', 'platform', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogDashboard::find()->joinWith(['user']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'code' => $this->code,
            'user_id' => $this->user_id,
            'platform' => $this->platform,
            'lock' => $this->lock,
            // 'user.region_id' => $this->region_id,
            // 'user.area_id' => $this->area_id,
        ]);
        if(!empty($this->region_id)){
            $query->andFilterWhere(['user.region_id'=>$this->region_id]);

        }
        if(!empty($this->area_id)){
            $query->andFilterWhere(['user.area_id'=>$this->area_id]);
        }
        if(!empty($this->end_date)){
            $end_date_format = date_create($this->end_date);
            $end_date_plus_one = strtotime('+2 day',date_timestamp_get($end_date_format));
            $query
            ->andFilterWhere(['<=', 'log_dashboard.created_at', date("Y-m-d",$end_date_plus_one)]);
        }

        $query->andFilterWhere(['like', 'log_dashboard.id', $this->id])
            ->andFilterWhere(['=', 'user.area_id', $this->area_id])
            ->andFilterWhere(['=', 'user.region_id', $this->region_id])
            // ->andFilterWhere(['between', 'log_dashboard.created_at', $this->start_date,$this->end_date])
            ->andFilterWhere(['>', 'log_dashboard.created_at', $this->start_date])
            ->andFilterWhere(['like', 'log_dashboard.created_at', $this->created_at])
            ->andFilterWhere(['like', 'log_dashboard.created_by', $this->created_by])
            ->andFilterWhere(['like', 'log_dashboard.updated_at', $this->updated_at])
            ->andFilterWhere(['like', 'log_dashboard.updated_by', $this->updated_by]);

        return $dataProvider;
    }
}
