<?php

namespace app\models;

use \app\models\base\PerusahaanKpr as BasePerusahaanKpr;
// use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "perusahaan_kpr".
 */
class PerusahaanKpr extends BasePerusahaanKpr
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['name'], 'required'],
            [['lock'], 'integer'],
            [['id', 'name', 'group_id', 'perusahaan_kpr_tier_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['perusahaan_kpr_tier_id'],'default', 'value' => null],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }

    // public function triggerNotifikasi($sesudah){
    //     $model  = PerusahaanKpr::findOne($this->id);
    //     $sebelum = ArrayHelper::toArray($model);
    //     $sebelum['group'] = ArrayHelper::toArray($model->group);
    //     $sebelum['perusahaanKprTier'] = ArrayHelper::toArray($model->perusahaanKprTier);
    //     $notifikasi = new Notifikasi();
    //     $notifikasi->code = Notifikasi::Code_PERUSAHAANKPR;
    //     $notifikasi->sebelum = json_encode($sebelum);
    //     $notifikasi->sesudah = $sesudah;
    //     $notifikasi->status = 0;
    //     $notifikasi->is_read = 0;
    //     if(!$notifikasi->save()){
    //         var_dump($notifikasi->getErrors());
    //         die();
    //     }
    // }
	
}
