<?php

namespace app\models;

use Yii;
use \app\models\base\SliderKkb as BaseSliderKkb;
use yii\web\UploadedFile;

/**
 * This is the model class for table "slider_kkb".
 */
class SliderKkb extends BaseSliderKkb
{
    /**
     * @inheritdoc
     */
    public $imageSliderKkb;
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [[], 'required'],
            [['order_number', 'lock'], 'integer'],
            [['id', 'image'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['imageSliderKkb'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, png'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }

	public function upload()
    {
        if ($this->validate()) {
            $this->imageSliderKkb = UploadedFile::getInstance($this, 'imageSliderKkb');
            if(!empty($this->imageSliderKkb)){
                $this->imageSliderKkb->saveAs(Yii::$app->basePath . '/web/uploads/sliderKkb/' . md5($this->imageSliderKkb->baseName . date("H:i:s")) . '.' . $this->imageSliderKkb->extension);
                $this->image = '@web/uploads/sliderKkb/' . md5($this->imageSliderKkb->baseName . date("H:i:s")) . '.' . $this->imageSliderKkb->extension;
                $this->image_name = $this->imageSliderKkb->baseName . '.' . $this->imageSliderKkb->extension;
                $this->imageSliderKkb = null;
                $this->save();
            }
            return true;
        } else { 
            return false;
        }
    }
}
