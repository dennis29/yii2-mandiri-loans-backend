<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UserMuf]].
 *
 * @see UserMuf
 */
class UserMufQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return UserMuf[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserMuf|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}