<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UserMtf]].
 *
 * @see UserMtf
 */
class UserMtfQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return UserMtf[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserMtf|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}