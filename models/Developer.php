<?php

namespace app\models;

use \app\models\base\Developer as BaseDeveloper;
// use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "developer".
 */
class Developer extends BaseDeveloper
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nama'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['nama'], 'required'],
            [['lock'], 'integer'],
            [['id', 'nama', 'developer_tier_id', 'developer_group_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'string', 'max' => 255],
            [['developer_tier_id', 'developer_group_id'],'default', 'value' => null],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }

    // public function triggerNotifikasi($sesudah){
    //     $model  = Developer::findOne($this->id);
    //     $sebelum = ArrayHelper::toArray($model);
    //     $sebelum['developerTier'] = ArrayHelper::toArray($model->developerTier);
    //     $sebelum['developerGroup'] = ArrayHelper::toArray($model->developerGroup);
        
    //     $notifikasi = new Notifikasi();
    //     $notifikasi->code = Notifikasi::Code_DEVELOPERLIST;
    //     $notifikasi->sebelum = json_encode($sebelum);
    //     $notifikasi->sesudah = $sesudah;
    //     $notifikasi->status = 0;
    //     $notifikasi->is_read = 0;
    //     if(!$notifikasi->save()){
    //         var_dump($notifikasi->getErrors());
    //         die();
    //     }
    // }
	
}
