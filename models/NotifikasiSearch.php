<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Notifikasi;

/**
 * app\models\NotifikasiSearch represents the model behind the search form about `app\models\Notifikasi`.
 */
 class NotifikasiSearch extends Notifikasi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sebelum', 'sesudah', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'safe'],
            [['code', 'status', 'is_read', 'user_id', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Notifikasi::find()->orderBy(['created_at' => SORT_DESC])->where(['user_id' => Yii::$app->user->identity->id,'status'=>0]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 150,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'code' => $this->code,
            'status' => $this->status,
            'is_read' => $this->is_read,
            'user_id' => $this->user_id,
            'lock' => $this->lock,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'sebelum', $this->sebelum])
            ->andFilterWhere(['like', 'sesudah', $this->sesudah])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
}
