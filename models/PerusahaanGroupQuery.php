<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PerusahaanGroup]].
 *
 * @see PerusahaanGroup
 */
class PerusahaanGroupQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PerusahaanGroup[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PerusahaanGroup|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}