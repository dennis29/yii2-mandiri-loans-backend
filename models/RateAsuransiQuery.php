<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RateAsuransi]].
 *
 * @see RateAsuransi
 */
class RateAsuransiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return RateAsuransi[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RateAsuransi|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}