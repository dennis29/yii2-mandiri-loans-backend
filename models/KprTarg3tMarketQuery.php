<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[KprTarg3tMarket]].
 *
 * @see KprTarg3tMarket
 */
class KprTarg3tMarketQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return KprTarg3tMarket[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return KprTarg3tMarket|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}