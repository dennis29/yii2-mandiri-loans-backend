<?php

namespace app\models;

use \app\models\base\KprTarg3tMarket as BaseKprTarg3tMarket;

/**
 * This is the model class for table "kpr_targ3t_market".
 */
class KprTarg3tMarket extends BaseKprTarg3tMarket
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['kpr_id', 'targ3t_market_id'], 'required'],
            [['kpr_id', 'targ3t_market_id'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
