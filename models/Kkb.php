<?php

namespace app\models;

use Yii;
use \app\models\base\Kkb as BaseKkb;
use yii\web\UploadedFile;

/**
 * This is the model class for table "kkb".
 */
class Kkb extends BaseKkb
{
    /**
     * @inheritdoc
     */
    public $imageKkb;
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['code','title'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [[], 'required'],
            [['text','code','link','link_utama','title'], 'string'],
            [['link','link_utama'], 'url'],
            [['lock'], 'integer'],
            [['id', 'image'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['imageKkb'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->imageKkb = UploadedFile::getInstance($this, 'imageKkb');
            if(!empty($this->imageKkb)){

                $this->imageKkb->saveAs(Yii::$app->basePath . '/web/uploads/kkb/' . md5($this->imageKkb->baseName . date("H:i:s")) . '.' . $this->imageKkb->extension);
                $this->image = '@web/uploads/kkb/' . md5($this->imageKkb->baseName . date("H:i:s")) . '.' . $this->imageKkb->extension;
                $this->image_name = $this->imageKkb->baseName . '.' . $this->imageKkb->extension;
                $this->imageKkb = null;
                $this->save();
            }
            return true;
        } else { 
            return false;
        }
    }
	
}
