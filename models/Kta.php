<?php

namespace app\models;
use Yii;
use \app\models\base\Kta as BaseKta;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "kta".
 */
class Kta extends BaseKta
{
    /**
     * @inheritdoc
     */
    const ALL = 2;
    const KARYAWAN = 1;
    const WIRASWASTA = 0;
    
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nama','pop_up'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['nama','user_id'], 'required'],
            [['keterangan','target_market_text','ketentuan_umum_text','limit_kredit_text','jangka_waktu_kredit_text','suku_bunga_text','ketentuan_dbr_text'], 'string'],
            [['lock'], 'integer'],
            [['id', 'nama','expired', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }

    // public function triggerNotifikasi($sesudah){
    //     $model  = Kta::findOne($this->id);
    //     $sebelum = ArrayHelper::toArray($model);
    //     $sebelum['perusahaanTiers'] = ArrayHelper::toArray($model->perusahaanTiers);
    //     $sebelum['targ3tMarkets'] = ArrayHelper::toArray($model->targ3tMarkets);
    //     $sebelum['ktaRules'] = ArrayHelper::toArray($model->ktaRules);

    //     $notifikasi = new Notifikasi();
    //     $notifikasi->code = Notifikasi::Code_KTA;
    //     $notifikasi->sebelum = json_encode($sebelum);
    //     $notifikasi->sesudah = $sesudah;
    //     $notifikasi->status = 0;
    //     $notifikasi->is_read = 0;
    //     if(!$notifikasi->save()){
    //         // echo 'asd123';
    //         echo json_encode($notifikasi->getErrors());
    //         die();
    //     }
    // }

    public function triggerNotifikasi($sesudah,$state = 'update'){

        if($state == 'delete' || $state == 'update'){
            $model  = Kta::findOne($this->id);
            $sebelum = ArrayHelper::toArray($model);
            $sebelum['perusahaanTiers'] = ArrayHelper::toArray($model->perusahaanTiers);
            $sebelum['targ3tMarkets'] = ArrayHelper::toArray($model->targ3tMarkets);
            $sebelum['ktaRules'] = ArrayHelper::toArray($model->ktaRules);
        }
        $notifikasi = new Notifikasi();
        $notifikasi->code = Notifikasi::Code_KTA;
        if($state == 'delete' || $state == 'update'){
            $notifikasi->sebelum = json_encode($sebelum);
        }
        if($state == 'create' || $state == 'update'){
            $notifikasi->sesudah = $sesudah;
        }
        $notifikasi->status = 0;
        $notifikasi->is_read = 0;
        // $notifikasi->scenario = 'userHarusAda';
        $request = Yii::$app->request;
        $post = $request->post();
		if(isset($post['Kta']))
		{	
			$sesudah = $post['Kta'];
			$notifikasi->user_id = $sesudah["user_id"];
			if(!$notifikasi->save()){
				var_dump($notifikasi->getErrors());
				die();
			}
		}
    }
	
}
