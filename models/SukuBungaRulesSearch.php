<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SukuBungaRules;

/**
 * app\models\SukuBungaRulesSearch represents the model behind the search form about `app\models\SukuBungaRules`.
 */
 class SukuBungaRulesSearch extends SukuBungaRules
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'safe'],
            [['suku_bunga_id', 'order_number', 'tahun_fixed', 'min_tenor', 'lock'], 'integer'],
            [['suku_bunga_fixed'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SukuBungaRules::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'suku_bunga_id' => $this->suku_bunga_id,
            'order_number' => $this->order_number,
            'suku_bunga_fixed' => $this->suku_bunga_fixed,
            'tahun_fixed' => $this->tahun_fixed,
            'min_tenor' => $this->min_tenor,
            'lock' => $this->lock,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at]);

        return $dataProvider;
    }
}
