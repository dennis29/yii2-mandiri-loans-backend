<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Proyek;

/**
 * app\models\ProyekSearch represents the model behind the search form about `app\models\Proyek`.
 */
 class ProyekSearch extends Proyek
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nama', 'tanggal_jatuh_tempo','developer_id', 'kpr_id', 'area_id','region_id', 'is_approved', 'created_at', 'updated_at', 'created_by', 'updated_by','website'], 'safe'],
            [['min_harga', 'max_harga'], 'number'],
            [['location', 'lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Proyek::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'tanggal_jatuh_tempo' => $this->tanggal_jatuh_tempo,
            // 'min_harga' => $this->min_harga,
            // 'max_harga' => $this->max_harga,
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
            'location' => $this->location,
            'area_id' => $this->area_id,
            'is_approved' => $this->is_approved,
            'lock' => $this->lock,
        ]);

        if(!empty($this->developer_id)){
            $query->joinWith(['developer'])
                           ->andFilterWhere(['like','developer.nama', $this->developer_id]);
        }
        if(!empty($this->min_harga)){
            $query->andFilterWhere(['>=', 'min_harga', $this->min_harga]);
        }
        if(!empty($this->max_harga)){
            $query->andFilterWhere(['<=', 'max_harga', $this->max_harga]);
        }
        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'nama', $this->nama])
            // ->andFilterWhere(['like', 'developer_id', $this->developer_id])
            ->andFilterWhere(['like', 'kpr_id', $this->kpr_id])
            ->andFilterWhere(['like', 'region_id', $this->region_id])
            ->andFilterWhere(['like', 'website', $this->website])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
}
