<?php

namespace app\models;

use \app\models\base\PerusahaanTier as BasePerusahaanTier;
// use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "perusahaan_tier".
 */
class PerusahaanTier extends BasePerusahaanTier
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nama'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['nama'], 'required'],
            [['id', 'nama', 'lock', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }

    // public function triggerNotifikasi($sesudah){
    //     $model  = PerusahaanTier::findOne($this->id);
    //     $sebelum = ArrayHelper::toArray($model);
    //     // var_dump($sebelum);
    //     // die();
    //     $notifikasi = new Notifikasi();
    //     $notifikasi->code = Notifikasi::Code_PERUSAHAANTIER;
    //     $notifikasi->sebelum = json_encode($sebelum);
    //     $notifikasi->sesudah = $sesudah;
    //     $notifikasi->status = 0;
    //     $notifikasi->is_read = 0;
    //     if(!$notifikasi->save()){
    //         var_dump($notifikasi->getErrors());
    //         die();
    //     }
    // }
	
}
