<?php

namespace app\models;

use \app\models\base\UserMuf as BaseUserMuf;

/**
 * This is the model class for table "user_muf".
 */
class UserMuf extends BaseUserMuf
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nip','slm'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['nip', 'contact'], 'required'],
            [['lock','contact','slm_contact'], 'integer'],
            [['id', 'nip', 'contact','area_id','region_id','slm','slm_contact','created_by', 'updated_by', 'created_at', 'updated_at'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }

    public static function getAreaDep($region_id) {
        $data=\app\models\Area::find()
       ->where(['region_id'=>$region_id])
       ->select(['id','name'])->asArray()->all();

        return $data;
    }
}
