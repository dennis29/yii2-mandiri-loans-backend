<?php

namespace app\models;

use \app\models\base\LogUser as BaseLogUser;
use Yii;

/**
 * This is the model class for table "log_user".
 */
class LogUser extends BaseLogUser
{
    Const SUCCESS = 1;
    Const FAIL = 0;

    Const STATUS_SUCCESS = 0;
    Const NIP_NOT_FOUND = 1;
    Const WRONG_PASSWORD = 2;
    Const USER_WHITELIST_NOT_FOUND = 3;
    Const CHANGE_PASSWORD_SUCCESS = 4;
    Const CHANGE_PASSWORD_FAIL_P = 5;
    Const CHANGE_PASSWORD_FAIL_U = 6;
    Const CREATE_ADMIN = 7;
    Const UPDATE_ADMIN = 8;
    Const DELETE_ADMIN = 9;
    Const CHANGEROLE_ADMIN = 10;
    Const CREATE_MOBILE = 11;
    Const UPDATE_MOBILE = 12;
    Const DELETE_MOBILE = 13;
    Const CHANGEROLE_MOBILE = 14;
    Const CREATE_WHITELIST = 15;
    Const UPDATE_WHITELIST = 16;
    Const DELETE_WHITELIST = 17;
    Const IMPORT_WHITELIST = 18;
    Const EDIT_PRIVILEGE = 19;
    Const CREATE_USERREGION = 20;
    Const UPDATE_USERREGION = 21;
    Const DELETE_USERREGION = 22;
    Const IMPORT_USERREGION = 23;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return parent::rules();
        
    }

    public function logHitUser($status,$is_success,$nip,$user_id="",$message=""){
        $obj = new LogUser();
        $obj->nip = $nip;
        $obj->user_id = $user_id;
        $obj->ip_address = Yii::$app->getRequest()->getUserIP();
        $obj->status = $status;
        $obj->is_success = $is_success;
        $obj->message = $message;
        $obj->save(false);
    }
}
