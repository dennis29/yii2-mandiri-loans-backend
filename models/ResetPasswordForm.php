<?php
namespace app\models;

use yii\base\Model;
use yii\base\InvalidParamException;
use app\models\User;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    public $password;
    public $retype_password;

    /**
     * @var \app\models\User
     */
    private $_user;


    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Password reset token cannot be blank.');
        }
        $this->_user = User::findByPasswordResetToken($token);
        if (!$this->_user) {
            throw new InvalidParamException('Wrong password reset token.');
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => 8],
            ['password', 'match', 'pattern' => '/[^a-zA-Z\d]/', 'message'=>'setidaknya memiliki 1 karakter spesial (!@#$%^&*-) untuk password baru'],
            ['password', 'match', 'pattern' => '/\d/', 'message'=>'setidaknya memiliki 1 angka untuk password baru'],
            ['password', 'match', 'pattern' => '/^.*(?=.*[a-z])(?=.*[A-Z]).*$/', 'message'=>'setidaknya memiliki 1 huruf kapital dan 1 huruf kecil untuk password baru'],
            ['retype_password', 'required'],
            ['retype_password', 'compare', 'compareAttribute' => 'password'],

        ];
    }

    /**
     * Resets password.
     *
     * @return bool if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;
        $password_hashed = hash('sha256', (md5($this->password)));
        $user->setPassword($password_hashed);
        $user->removePasswordResetToken();

        return $user->save(false);
    }
}
