<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PromoDeveloperGroup]].
 *
 * @see PromoDeveloperGroup
 */
class PromoDeveloperGroupQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PromoDeveloperGroup[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PromoDeveloperGroup|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}