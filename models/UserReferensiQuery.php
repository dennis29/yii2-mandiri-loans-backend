<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UserReferensi]].
 *
 * @see UserReferensi
 */
class UserReferensiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return UserReferensi[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserReferensi|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}