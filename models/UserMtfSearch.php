<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserMtf;

/**
 * app\models\UserMtfSearch represents the model behind the search form about `app\models\UserMtf`.
 */
 class UserMtfSearch extends UserMtf
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nip', 'contact', 'area_id','region_id','slm', 'slm_contact', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'safe'],
            [['lock'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserMtf::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'lock' => $this->lock,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'nip', $this->nip])
            ->andFilterWhere(['like', 'contact', $this->contact])
            ->andFilterWhere(['like', 'area_id', $this->area_id])
            ->andFilterWhere(['like', 'region_id', $this->region_id])
            ->andFilterWhere(['like', 'slm', $this->slm])
            ->andFilterWhere(['like', 'slm_contact', $this->slm_contact])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at]);

        return $dataProvider;
    }
}
