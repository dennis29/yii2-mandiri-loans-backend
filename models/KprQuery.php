<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Kpr]].
 *
 * @see Kpr
 */
class KprQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Kpr[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Kpr|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}