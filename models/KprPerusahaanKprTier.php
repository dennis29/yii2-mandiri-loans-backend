<?php

namespace app\models;

use \app\models\base\KprPerusahaanKprTier as BaseKprPerusahaanKprTier;

/**
 * This is the model class for table "kpr_perusahaan_kpr_tier".
 */
class KprPerusahaanKprTier extends BaseKprPerusahaanKprTier
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['kpr_id', 'perusahaan_kpr_tier_id', 'lock'], 'required'],
            [['lock'], 'integer'],
            [['kpr_id', 'perusahaan_kpr_tier_id', 'id', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }

	
}
