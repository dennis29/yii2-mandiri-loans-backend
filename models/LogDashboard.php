<?php

namespace app\models;

use \app\models\base\LogDashboard as BaseLogDashboard;

/**
 * This is the model class for table "log_dashboard".
 */
class LogDashboard extends BaseLogDashboard
{
    /**
     * @inheritdoc
     */
    const Code_KTA = 1;
    const Code_KPR = 2;
    const Code_KKB = 3;


    const Platform_web = 1;
    const PLatform_android = 2;

    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['code', 'ip_address', 'user_id'], 'required'],
            [['code', 'user_id', 'lock','platform'], 'integer'],
            [['id', 'ip_address', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
