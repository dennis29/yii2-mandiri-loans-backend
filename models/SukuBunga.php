<?php

namespace app\models;

use \app\models\base\SukuBunga as BaseSukuBunga;
use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "suku_bunga".
 */
class SukuBunga extends BaseSukuBunga
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nama'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['nama','user_id'], 'required'],
            [['lock','is_reguler'], 'integer'],
            [['id', 'nama', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    public function saveRules($rules=null){
        // echo json_encode($rules);
        $oldRules = sukuBungaRules::find()->where(['suku_bunga_id'=>$this->id])->all();
        if(!empty($oldRules)){

            foreach ( $oldRules as $key ) {
                $key->delete();
            }
        }
        if(!empty($rules)){

            foreach ($rules as $rule) {
                $r = new sukuBungaRules();
                $r->attributes= $rule;
                $r->save();
                $this->link('sukuBungaRules',$r);
            }
        }
    }

    // public function triggerNotifikasi($sesudah){
    //     $model  = SukuBunga::findOne($this->id);
    //     $sebelum = ArrayHelper::toArray($model);
    //     $sebelum['sukuBungaRules'] = ArrayHelper::toArray($model->sukuBungaRules);

    //     $notifikasi = new Notifikasi();
    //     $notifikasi->code = Notifikasi::Code_SB;
    //     $notifikasi->sebelum = json_encode($sebelum);
    //     $notifikasi->sesudah = $sesudah;
    //     $notifikasi->status = 0;
    //     $notifikasi->is_read = 0;
    //     if(!$notifikasi->save()){
    //         // echo 'asd123';
    //         echo json_encode($notifikasi->getErrors());
    //         die();
    //     }
    // }
    
    public function triggerNotifikasi($sesudah,$state = 'update'){

        if($state == 'delete' || $state == 'update'){
            $model  = SukuBunga::findOne($this->id);
        $sebelum = ArrayHelper::toArray($model);
        // var_dump($model);die();
        $sebelum['sukuBungaRules'] = ArrayHelper::toArray($model->sukuBungaRules);
        }
        $notifikasi = new Notifikasi();
        $notifikasi->code = Notifikasi::Code_SB;
        if($state == 'delete' || $state == 'update'){
            $notifikasi->sebelum = json_encode($sebelum);
        }
        if($state == 'create' || $state == 'update'){
            $notifikasi->sesudah = $sesudah;
        }
        $notifikasi->status = 0;
        $notifikasi->is_read = 0;
        $request = Yii::$app->request;
        $post = $request->post();
        // if($status != 'delete'){
            $sesudah = $post['SukuBunga'];
        // }
        $notifikasi->user_id = $sesudah["user_id"];
        if(!$notifikasi->save()){
            var_dump($notifikasi->getErrors());
            die();
        }
    }
}
