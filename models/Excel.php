<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "excel".
 *
 * @property integer $id
 * @property string $class
 * @property string $location_file
 */
class Excel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'excel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['file'], 'required'],
            [['class', 'location_file','file'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'class' => 'Class',
            'location_file' => 'Location File',
        ];
    }

    public function upload()
    {
        // try{

            $this->file = UploadedFile::getInstance($this, 'file');
        if(empty($this->file)){
            return false;
        }
            $this->file->saveAs(Yii::$app->basePath . '/web/uploads/excels/' .$this->location_file);
            $this->file = null;
            if($this->save()){
                return true;
                
            }else{
                return false;
            }
        // }catch(\yii\base\ErrorException $e){
        //     return false;
        // }
    }

    public function importExcel(){
        if (!file_exists(Yii::$app->basePath . '/web/uploads/excels')) {
            mkdir(Yii::$app->basePath . '/web/uploads/excels', 0777, true);
        }
        $fileExcel = Yii::getAlias("@webroot").'/uploads/excels/'.$this->location_file;
         $data = \moonland\phpexcel\Excel::import($fileExcel, [
                'setFirstRecordAsKeys' => true, // if you want to set the keys of record column with first record, if it not set, the header with use the alphabet column on excel. 
                'setIndexSheetByName' => true, // set this if your excel data with multiple worksheet, the index of array will be set with the sheet name. If this not set, the index will use numeric. 
                'getOnlySheet' => 'sheet1', // you can set this property if you want to get the specified sheet from the excel data with multiple worksheet.
            ]);

         return $data;
    }
}
