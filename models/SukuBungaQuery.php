<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SukuBunga]].
 *
 * @see SukuBunga
 */
class SukuBungaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SukuBunga[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SukuBunga|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}