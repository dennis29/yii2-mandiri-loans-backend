<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PdfMaster]].
 *
 * @see PdfMaster
 */
class PdfMasterQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PdfMaster[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PdfMaster|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}