<?php

namespace app\models;

use \app\models\base\KprSukuBunga as BaseKprSukuBunga;

/**
 * This is the model class for table "kpr_suku_bunga".
 */
class KprSukuBunga extends BaseKprSukuBunga
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['kpr_id', 'suku_bunga_id'], 'required'],
            [['lock'], 'integer'],
            [['kpr_id', 'suku_bunga_id', 'id', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
