<?php
namespace app\models;

use yii\base\Model;
use app\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $nip;
	public $imei;
    public $email;
    public $name;
    public $dob;
    public $phone_number;
    public $region_id;
    public $area_id;
    public $password;
    public $retype_password;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nip','name'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            ['nip', 'trim'],
            ['nip', 'required','message'=>'nip wajib diisi'],
            ['nip', 'unique', 'targetClass' => '\app\models\User', 'message' => 'NIP/Sales Code yang anda masukkan sudah pernah terdaftar pada database kami'],
            ['nip', 'string', 'min' => 2, 'max' => 255],
			
			[['imei','name'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            ['imei', 'trim'],
            ['imei', 'required','message'=>'nip wajib diisi'],
            ['imei', 'unique', 'targetClass' => '\app\models\User', 'message' => 'IMEI yang anda masukkan sudah pernah terdaftar pada database kami'],
            ['imei', 'string', 'min' => 2, 'max' => 255],
            
            ['name', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required','message'=>'email wajib diisi'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'email sudah pernah terdaftar'],

            // ['name', 'required','message'=>'wajib diisi'],
            ['dob', 'required','message'=>'tanggal lahir wajib diisi'],
            ['phone_number', 'required','message'=>'handphone wajib diisi'],
            ['phone_number', 'integer'],
            ['region_id', 'required','message'=>'region wajib diisi'],
            ['area_id', 'required','message'=>'area wajib diisi'],
            ['password', 'required','message'=>'password wajib diisi'],
            ['password', 'string', 'min' => 6],
            ['retype_password', 'required','message'=>'wajib diisi'],
            ['retype_password', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->nip = $this->nip;
		$user->imei = $this->imei;
        $user->email = $this->email;
        $user->name = $this->name;
        $user->dob = $this->dob;
        $user->phone_number = $this->phone_number;
        $user->area_id = $this->area_id;
        $user->region_id = $this->region_id;
        $password_hashed = hash('sha256', (md5($this->password)));
        $user->setPassword($password_hashed);
        // $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }

    public function signupMobileApi()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->nip = $this->nip;
		$user->imei = $this->imei;
        $user->email = $this->email;
        $user->name = $this->name;
        $user->dob = $this->dob;
        $user->phone_number = $this->phone_number;
        $user->area_id = $this->area_id;
        $user->region_id = $this->region_id;
        $user->status= 20;
        // $password_hashed = hash('sha256', (md5($this->password)));
        // $user->setPassword($password_hashed);
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save(false) ? $user : null;
    }

    public function signupMobile()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->nip = $this->nip;
		$user->imei = $this->imei;
        $user->email = $this->email;
        $user->name = $this->name;
        $user->dob = $this->dob;
        $user->phone_number = $this->phone_number;
        $user->area_id = $this->area_id;
        $user->region_id = $this->region_id;
        $user->status= 20;
        $password_hashed = hash('sha256', (md5($this->password)));
        $user->setPassword($password_hashed);
        // $user->setPassword($this->password);
        $user->password = $this->password;
        $user->generateAuthKey();
        
        return $user->save(false) ? $user : null;
    }

     public function myupdate($id)
    {
        
        $user = User::findOne($id);
        $user->nip = $this->nip;
		$user->imei = $this->imei;
        $user->email = $this->email;
        $user->name = $this->name;
        $user->dob = $this->dob;
        $user->phone_number = $this->phone_number;
        $user->area_id = $this->area_id;
        $user->region_id = $this->region_id;
        if($this->password){
            $password_hashed = hash('sha256', (md5($this->password)));
            // echo $this->password;
            // echo "<br/>";
            // var_dump($password_hashed);die();
            // $user->setPassword($password_hashed);
            $user->setPassword($password_hashed);
            $user->generateAuthKey();
        }
        
        return $user->save(false) ? $user : null;
    }
}
