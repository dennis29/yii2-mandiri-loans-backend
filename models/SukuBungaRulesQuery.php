<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SukuBungaRules]].
 *
 * @see SukuBungaRules
 */
class SukuBungaRulesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SukuBungaRules[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SukuBungaRules|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}