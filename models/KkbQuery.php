<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Kkb]].
 *
 * @see Kkb
 */
class KkbQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Kkb[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Kkb|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}