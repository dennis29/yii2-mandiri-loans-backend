<?php

namespace app\models;

use \app\models\base\Priviledge as BasePriviledge;

/**
 * This is the model class for table "priviledge".
 */
class Priviledge extends BasePriviledge
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nama', 'status'], 'required'],
            [['lock', 'status'], 'integer'],
            [['id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'nama'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
