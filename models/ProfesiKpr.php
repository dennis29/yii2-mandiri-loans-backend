<?php

namespace app\models;

use \app\models\base\ProfesiKpr as BaseProfesiKpr;

/**
 * This is the model class for table "profesi_kpr".
 */
class ProfesiKpr extends BaseProfesiKpr
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nama'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [[ 'nama'], 'required'],
            [['lock'], 'integer'],
            [['id', 'nama', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
