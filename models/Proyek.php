<?php

namespace app\models;
use Yii;
use \app\models\base\Proyek as BaseProyek;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "proyek".
 */
class Proyek extends BaseProyek
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nama'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['nama','user_id'], 'required'],
            [['tanggal_jatuh_tempo'], 'safe'],
            [['min_harga', 'max_harga', 'longitude', 'latitude'], 'number'],
            [['location', 'lock'], 'integer'],
            // [['min_harga', 'max_harga'], 'number'],
            [['id', 'nama', 'developer_id', 'kpr_id','area_id', 'region_id','website'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['website'], 'url'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }

    // public function triggerNotifikasi($sesudah){
    //     $model  = Proyek::findOne($this->id);
    //     $sebelum = ArrayHelper::toArray($model);
    //     $sebelum['developer'] = ArrayHelper::toArray($model->developer);
    //     $sebelum['region'] = ArrayHelper::toArray($model->region);
    //     $sebelum['area'] = ArrayHelper::toArray($model->area);

    //     $notifikasi = new Notifikasi();
    //     $notifikasi->code = Notifikasi::Code_PROYEK;
    //     $notifikasi->sebelum = json_encode($sebelum);
    //     $notifikasi->sesudah = $sesudah;
    //     $notifikasi->status = 0;
    //     $notifikasi->is_read = 0;
    //     if(!$notifikasi->save()){
    //         var_dump($notifikasi->getErrors());
    //         die();
    //     }
    // }

    public function triggerNotifikasi($sesudah,$state = 'update'){

        if($state == 'delete' || $state == 'update'){
            $model  = Proyek::findOne($this->id);
            $sebelum = ArrayHelper::toArray($model);
            $sebelum['developer'] = ArrayHelper::toArray($model->developer);
            $sebelum['region'] = ArrayHelper::toArray($model->region);
            $sebelum['area'] = ArrayHelper::toArray($model->area);
        }
        // var_dump($sebelum);die();
        $notifikasi = new Notifikasi();
        $notifikasi->code = Notifikasi::Code_PROYEK;
        if($state == 'delete' || $state == 'update'){
            $notifikasi->sebelum = json_encode($sebelum);
        }
        if($state == 'create' || $state == 'update'){
            $notifikasi->sesudah = $sesudah;
        }
        $notifikasi->status = 0;
        $notifikasi->is_read = 0;
        $request = Yii::$app->request;
        $post = $request->post();
        $sesudah = $post['Proyek'];
        $notifikasi->user_id = $sesudah["user_id"];
        if(!$notifikasi->save()){
            var_dump($notifikasi->getErrors());
            die();
        }
    }

    public static function getAreaDep($region_id="") {
        $data=\app\models\Area::find()
       ->where(['region_id'=>$region_id])
       ->select(['id','name'])->asArray()->all();

        return $data;
    }
	
}
