<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Targ3tMarket]].
 *
 * @see Targ3tMarket
 */
class Targ3tMarketQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Targ3tMarket[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Targ3tMarket|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}