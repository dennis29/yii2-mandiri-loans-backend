<?php

namespace app\models;

use Yii;
use \app\models\base\Intro as BaseIntro;
use yii\web\UploadedFile;

/**
 * This is the model class for table "intro".
 */
class Intro extends BaseIntro
{
    /**
     * @inheritdoc
     */
    public $imageFile;

    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [[], 'required'],
            [['text'], 'string'],
            [['lock','order_number'], 'integer'],
            [['imageFile'], 'safe'],
            [['id', 'image'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, png'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    public function upload()
    {
        if ($this->validate()) {
            $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
            if (!empty($this->imageFile)){
                $this->imageFile->saveAs(Yii::$app->basePath . '/web/uploads/intro/' . md5($this->imageFile->baseName . date("H:i:s")) . '.' . $this->imageFile->extension);
                $this->image = '@web/uploads/intro/' . md5($this->imageFile->baseName . date("H:i:s")) . '.' . $this->imageFile->extension;
                $this->image_name = $this->imageFile->baseName . '.' . $this->imageFile->extension;
                $this->imageFile = null;
                $this->save();
            }
            return true;
        } else { 
            return false;
        }
    }
}
