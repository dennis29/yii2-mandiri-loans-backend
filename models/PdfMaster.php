<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use \app\models\base\PdfMaster as BasePdfMaster;

/**
 * This is the model class for table "pdf_master".
 */
class PdfMaster extends BasePdfMaster
{
    /**
     * @inheritdoc
     */
    public $file;

    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['code'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['code'], 'required'],
            [['lock'], 'integer'],
            [['id', 'code'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'apk, pdf, xlsx, xls, doc, docx, jpg, png, mp4, mkv', 'maxSize' => '104857600'],
        ]);
    }


    public function upload()
    {
        if (!file_exists(Yii::$app->basePath . '/web/uploads/pdf/'.$this->code)) {
            mkdir(Yii::$app->basePath . '/web/uploads/pdf/'.$this->code, 0777, true);
        }
        $this->file = UploadedFile::getInstance($this, 'file');
        $this->file->saveAs(Yii::$app->basePath . '/web/uploads/pdf/'.$this->code.'/' . md5($this->file->baseName . date("H:i:s")) . '.' . $this->file->extension);
        $this->file_location = '@web/uploads/pdf/'.$this->code.'/'. md5($this->file->baseName . date("H:i:s")) . '.' . $this->file->extension;
        $this->file_name = $this->file->baseName . '.' . $this->file->extension;
        $this->file = null;
        if(!$this->save()){
            echo json_encode($this->errors);die();            
        }
        return true;
    }
	
}
