<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[KtaPerusahaanTier]].
 *
 * @see KtaPerusahaanTier
 */
class KtaPerusahaanTierQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return KtaPerusahaanTier[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return KtaPerusahaanTier|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}