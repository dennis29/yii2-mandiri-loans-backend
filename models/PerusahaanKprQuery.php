<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PerusahaanKpr]].
 *
 * @see PerusahaanKpr
 */
class PerusahaanKprQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PerusahaanKpr[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PerusahaanKpr|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}