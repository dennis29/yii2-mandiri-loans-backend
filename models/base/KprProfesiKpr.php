<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "kpr_profesi_kpr".
 *
 * @property string $kpr_id
 * @property string $profesi_kpr_id
 *
 * @property \app\models\Kpr $kpr
 * @property \app\models\ProfesiKpr $profesiKpr
 */
class KprProfesiKpr extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kpr_id', 'profesi_kpr_id'], 'required'],
            [['kpr_id', 'profesi_kpr_id'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpr_profesi_kpr';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kpr_id' => 'Kpr ID',
            'profesi_kpr_id' => 'Profesi Kpr ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKpr()
    {
        return $this->hasOne(\app\models\Kpr::className(), ['id' => 'kpr_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfesiKpr()
    {
        return $this->hasOne(\app\models\ProfesiKpr::className(), ['id' => 'profesi_kpr_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\KprProfesiKprQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\KprProfesiKprQuery(get_called_class());
    }
}
