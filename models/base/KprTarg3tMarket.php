<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "kpr_targ3t_market".
 *
 * @property string $kpr_id
 * @property string $targ3t_market_id
 *
 * @property \app\models\Kpr $kpr
 * @property \app\models\Targ3tMarket $targ3tMarket
 */
class KprTarg3tMarket extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kpr_id', 'targ3t_market_id'], 'required'],
            [['kpr_id', 'targ3t_market_id'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpr_targ3t_market';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kpr_id' => 'Kpr ID',
            'targ3t_market_id' => 'Targ3t Market ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKpr()
    {
        return $this->hasOne(\app\models\Kpr::className(), ['id' => 'kpr_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarg3tMarket()
    {
        return $this->hasOne(\app\models\Targ3tMarket::className(), ['id' => 'targ3t_market_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\KprTarg3tMarketQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\KprTarg3tMarketQuery(get_called_class());
    }
}
