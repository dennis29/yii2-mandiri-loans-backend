<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "developer".
 *
 * @property string $id
 * @property string $nama
 * @property string $link
 * @property string $developer_tier_id
 * @property string $developer_group_id
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 *
 * @property \app\models\DeveloperTier $developerTier
 * @property \app\models\DeveloperGroup $developerGroup
 * @property \app\models\Proyek[] $proyeks
 */
class Developer extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['nama'], 'required'],
            [['lock'], 'integer'],
            [['id', 'nama', 'developer_tier_id', 'developer_group_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'string', 'max' => 255],
            [['developer_tier_id', 'developer_group_id'],'default', 'value' => null],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'developer';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'developer_tier_id' => 'Developer Tier ID',
            'developer_group_id' => 'Developer Group ID',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeveloperTier()
    {
        return $this->hasOne(\app\models\DeveloperTier::className(), ['id' => 'developer_tier_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeveloperGroup()
    {
        return $this->hasOne(\app\models\DeveloperGroup::className(), ['id' => 'developer_group_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyeks()
    {
        return $this->hasMany(\app\models\Proyek::className(), ['developer_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\DeveloperQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\DeveloperQuery(get_called_class());
    }
}
