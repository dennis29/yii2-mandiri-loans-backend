<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "kpr".
 *
 * @property string $id
 * @property string $nama
 * @property string $keterangan
 * @property integer $lock
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 *
 * @property \app\models\KprDeveloperTier[] $kprDeveloperTiers
 * @property \app\models\DeveloperTier[] $developerTiers
 * @property \app\models\KprProfesiKpr[] $kprProfesiKprs
 * @property \app\models\ProfesiKpr[] $profesiKprs
 * @property \app\models\KprSukuBunga[] $kprSukuBungas
 * @property \app\models\SukuBunga[] $sukuBungas
 * @property \app\models\KprTarg3tMarket[] $kprTarg3tMarkets
 * @property \app\models\Targ3tMarket[] $targ3tMarkets
 * @property \app\models\Proyek[] $proyeks
 */
class Kpr extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['nama', 'keterangan','profil_nasabah_text', 'peruntukan_text', 'market_text', 'benefit_text', 'suku_bunga_kpr_text', 'suku_bunga_multiguna_text', 'peserta_text','user_id'], 'required'],

            [['lock','order_number'], 'integer'],
            [['id', 'nama', 'expired','created_by', 'updated_by', 'created_at', 'updated_at'], 'string', 'max' => 255],
            [['keterangan']],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpr';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'keterangan' => 'Keterangan',
            'lock' => 'Lock',
        ];
    }
    public function getKprPerusahaanKprTiers()
           {
               return $this->hasMany(\app\models\KprPerusahaanKprTier::className(), ['kpr_id' => 'id']);
           }
               
   /**
    * @return \yii\db\ActiveQuery
    */
   public function getPerusahaanKprTiers()
   {
       return $this->hasMany(\app\models\PerusahaanKprTier::className(), ['id' => 'perusahaan_kpr_tier_id'])->viaTable('kpr_perusahaan_kpr_tier', ['kpr_id' => 'id']);
   }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKprDeveloperTiers()
    {
        return $this->hasMany(\app\models\KprDeveloperTier::className(), ['kpr_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeveloperTiers()
    {
        return $this->hasMany(\app\models\DeveloperTier::className(), ['id' => 'developer_tier_id'])->viaTable('kpr_developer_tier', ['kpr_id' => 'id'])->orderBy('nama');
    }

    public function getDeveloperTiersName()
    {
        return $this->hasMany(\app\models\DeveloperTier::className(), ['id' => 'developer_tier_id'])->viaTable('kpr_developer_tier', ['kpr_id' => 'id'])->select(['id','nama']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKprProfesiKprs()
    {
        return $this->hasMany(\app\models\KprProfesiKpr::className(), ['kpr_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfesiKprs()
    {
        return $this->hasMany(\app\models\ProfesiKpr::className(), ['id' => 'profesi_kpr_id'])->viaTable('kpr_profesi_kpr', ['kpr_id' => 'id']);
    }
        
    public function getProfesiKprsName()
    {
        return $this->hasMany(\app\models\ProfesiKpr::className(), ['id' => 'profesi_kpr_id'])->viaTable('kpr_profesi_kpr', ['kpr_id' => 'id'])->select(['id','nama']);
    }
        
            /**
     * @return \yii\db\ActiveQuery
     */
    public function getKprSukuBungas()
    {
        return $this->hasMany(\app\models\KprSukuBunga::className(), ['kpr_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSukuBungas()
    {
        return $this->hasMany(\app\models\SukuBunga::className(), ['id' => 'suku_bunga_id'])->viaTable('kpr_suku_bunga', ['kpr_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKprTarg3tMarkets()
    {
        return $this->hasMany(\app\models\KprTarg3tMarket::className(), ['kpr_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarg3tMarkets()
    {
        return $this->hasMany(\app\models\Targ3tMarket::className(), ['id' => 'targ3t_market_id'])->viaTable('kpr_targ3t_market', ['kpr_id' => 'id']);
    }


    public function getTarg3tMarketsName()
    {
        return $this->hasMany(\app\models\Targ3tMarket::className(), ['id' => 'targ3t_market_id'])->viaTable('kpr_targ3t_market', ['kpr_id' => 'id'])->select(['id','name']);
    }        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyeks()
    {
        return $this->hasMany(\app\models\Proyek::className(), ['kpr_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\KprQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\KprQuery(get_called_class());
    }
}
