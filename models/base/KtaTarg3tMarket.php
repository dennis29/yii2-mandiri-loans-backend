<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "kta_targ3t_market".
 *
 * @property string $kta_id
 * @property string $targ3t_market_id
 * @property string $id
 * @property integer $lock
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 *
 * @property \app\models\Kta $kta
 * @property \app\models\Targ3tMarket $targ3tMarket
 */
class KtaTarg3tMarket extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kta_id', 'targ3t_market_id'], 'required'],
            [['lock'], 'integer'],
            [['kta_id', 'targ3t_market_id', 'id', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kta_targ3t_market';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kta_id' => 'Kta ID',
            'targ3t_market_id' => 'Targ3t Market ID',
            'id' => 'ID',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKta()
    {
        return $this->hasOne(\app\models\Kta::className(), ['id' => 'kta_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarg3tMarket()
    {
        return $this->hasOne(\app\models\Targ3tMarket::className(), ['id' => 'targ3t_market_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\KtaTarg3tMarketQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\KtaTarg3tMarketQuery(get_called_class());
    }
}
