<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "targ3t_market".
 *
 * @property string $id
 * @property string $name
 * @property string $desc
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 *
 * @property \app\models\KprTarg3tMarket[] $kprTarg3tMarkets
 * @property \app\models\Kpr[] $kprs
 * @property \app\models\KtaTarg3tMarket[] $ktaTarg3tMarkets
 * @property \app\models\Kta[] $ktas
 */
class Targ3tMarket extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['name', 'desc'], 'required'],
            [['desc'], 'string'],
            [['lock'], 'integer'],
            [['id', 'name', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'targ3t_market';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'desc' => 'Desc',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKprTarg3tMarkets()
    {
        return $this->hasMany(\app\models\KprTarg3tMarket::className(), ['targ3t_market_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKprs()
    {
        return $this->hasMany(\app\models\Kpr::className(), ['id' => 'kpr_id'])->viaTable('kpr_targ3t_market', ['targ3t_market_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKtaTarg3tMarkets()
    {
        return $this->hasMany(\app\models\KtaTarg3tMarket::className(), ['targ3t_market_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKtas()
    {
        return $this->hasMany(\app\models\Kta::className(), ['id' => 'kta_id'])->viaTable('kta_targ3t_market', ['targ3t_market_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\Targ3tMarketQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\Targ3tMarketQuery(get_called_class());
    }
}
