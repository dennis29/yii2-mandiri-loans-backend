<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "perusahaan_kpr_tier".
 *
 * @property string $id
 * @property string $name
 * @property string $lock
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property \app\models\KprPerusahaanKprTier[] $kprPerusahaanKprTiers
 * @property \app\models\Kpr[] $kprs
 * @property \app\models\PerusahaanKpr[] $perusahaanKprs
 */
class PerusahaanKprTier extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['name'], 'required'],
            [['id', 'name', 'lock', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'perusahaan_kpr_tier';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKprPerusahaanKprTiers()
    {
        return $this->hasMany(\app\models\KprPerusahaanKprTier::className(), ['perusahaan_kpr_tier_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKprs()
    {
        return $this->hasMany(\app\models\Kpr::className(), ['id' => 'kpr_id'])->viaTable('kpr_perusahaan_kpr_tier', ['perusahaan_kpr_tier_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerusahaanKprs()
    {
        return $this->hasMany(\app\models\PerusahaanKpr::className(), ['perusahaan_kpr_tier_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\PerusahaanKprTierQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PerusahaanKprTierQuery(get_called_class());
    }
}
