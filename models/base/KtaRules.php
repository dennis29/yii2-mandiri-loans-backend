<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "kta_rules".
 *
 * @property string $id
 * @property string $kta_id
 * @property integer $min_tenor
 * @property integer $max_tenor
 * @property double $suku_bunga_floating
 * @property double $suku_bunga_flat
 * @property double $max_dbr
 * @property double $limit_min
 * @property double $limit_max
 * @property double $income_min
 * @property double $income_max
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 *
 * @property \app\models\Kta $kta
 */
class KtaRules extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kta_id', 'min_tenor', 'max_tenor', 'suku_bunga_floating', 'suku_bunga_flat', 'max_dbr', 'limit_min', 'limit_max', 'income_min', 'income_max'], 'required'],
            [['min_tenor', 'max_tenor', 'lock'], 'integer'],
            [['suku_bunga_floating', 'suku_bunga_flat', 'max_dbr', 'limit_min', 'limit_max', 'income_min', 'income_max'], 'number'],
            [['id', 'kta_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kta_rules';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kta_id' => 'Kta ID',
            'min_tenor' => 'Min Tenor',
            'max_tenor' => 'Max Tenor',
            'suku_bunga_floating' => 'Suku Bunga Floating',
            'suku_bunga_flat' => 'Suku Bunga Flat',
            'max_dbr' => 'Max Dbr',
            'limit_min' => 'Limit Min',
            'limit_max' => 'Limit Max',
            'income_min' => 'Income Min',
            'income_max' => 'Income Max',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKta()
    {
        return $this->hasOne(\app\models\Kta::className(), ['id' => 'kta_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\KtaRulesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\KtaRulesQuery(get_called_class());
    }
}
