<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "perusahaan".
 *
 * @property string $id
 * @property string $name
 * @property double $max_limit
 * @property double $max_tenor
 * @property double $dbr
 * @property string $perusahaan_tier_id
 * @property string $perusahaan_group_id
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 *
 * @property \app\models\KprRules[] $kprRules
 * @property \app\models\PerusahaanTier $perusahaanTier
 * @property \app\models\PerusahaanGroup $perusahaanGroup
 */
class Perusahaan extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [[ 'name', 'perusahaan_tier_id', 'perusahaan_group_id'], 'required'],
            [['max_limit', 'max_tenor', 'dbr'], 'number'],
            [['lock'], 'integer'],
            [['id', 'name', 'perusahaan_tier_id', 'perusahaan_group_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'perusahaan';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'max_limit' => 'Max Limit',
            'max_tenor' => 'Max Tenor',
            'dbr' => 'Dbr',
            'perusahaan_tier_id' => 'Perusahaan Tier ID',
            'perusahaan_group_id' => 'Perusahaan Group ID',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKprRules()
    {
        return $this->hasMany(\app\models\KprRules::className(), ['perusahaan_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerusahaanTier()
    {
        return $this->hasOne(\app\models\PerusahaanTier::className(), ['id' => 'perusahaan_tier_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerusahaanGroup()
    {
        return $this->hasOne(\app\models\PerusahaanGroup::className(), ['id' => 'perusahaan_group_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\PerusahaanQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PerusahaanQuery(get_called_class());
    }
}
