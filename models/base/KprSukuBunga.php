<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "kpr_suku_bunga".
 *
 * @property string $kpr_id
 * @property string $suku_bunga_id
 * @property string $id
 * @property integer $lock
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 *
 * @property \app\models\Kpr $kpr
 * @property \app\models\SukuBunga $sukuBunga
 */
class KprSukuBunga extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kpr_id', 'suku_bunga_id'], 'required'],
            [['lock'], 'integer'],
            [['kpr_id', 'suku_bunga_id', 'id', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpr_suku_bunga';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kpr_id' => 'Kpr ID',
            'suku_bunga_id' => 'Suku Bunga ID',
            'id' => 'ID',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKpr()
    {
        return $this->hasOne(\app\models\Kpr::className(), ['id' => 'kpr_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSukuBunga()
    {
        return $this->hasOne(\app\models\SukuBunga::className(), ['id' => 'suku_bunga_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\KprSukuBungaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\KprSukuBungaQuery(get_called_class());
    }
}
