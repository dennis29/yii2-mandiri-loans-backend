<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "rate_asuransi".
 *
 * @property integer $id
 * @property integer $umur
 * @property double $tenor_1
 * @property double $tenor_2
 * @property double $tenor_3
 * @property double $tenor_4
 * @property double $tenor_5
 * @property double $tenor_6
 * @property double $tenor_7
 * @property double $tenor_8
 * @property double $tenor_9
 * @property double $tenor_10
 * @property double $tenor_11
 * @property double $tenor_12
 * @property double $tenor_13
 * @property double $tenor_14
 * @property double $tenor_16
 * @property double $tenor_15
 * @property double $tenor_18
 * @property double $tenor_20
 * @property double $tenor_19
 * @property double $tenor_17
 * @property double $tenor_21
 * @property double $tenor_23
 * @property double $tenor_22
 * @property double $tenor_25
 * @property double $tenor_24
 */
class RateAsuransi extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['umur', 'tenor_1', 'tenor_2', 'tenor_3', 'tenor_4', 'tenor_5', 'tenor_6', 'tenor_7', 'tenor_8', 'tenor_9', 'tenor_10', 'tenor_11', 'tenor_12', 'tenor_13', 'tenor_14', 'tenor_16', 'tenor_15', 'tenor_18', 'tenor_20', 'tenor_19', 'tenor_17', 'tenor_21', 'tenor_23', 'tenor_22', 'tenor_25', 'tenor_24'], 'required'],
            [['umur'], 'integer'],
            [['tenor_1', 'tenor_2', 'tenor_3', 'tenor_4', 'tenor_5', 'tenor_6', 'tenor_7', 'tenor_8', 'tenor_9', 'tenor_10', 'tenor_11', 'tenor_12', 'tenor_13', 'tenor_14', 'tenor_16', 'tenor_15', 'tenor_18', 'tenor_20', 'tenor_19', 'tenor_17', 'tenor_21', 'tenor_23', 'tenor_22', 'tenor_25', 'tenor_24'], 'number'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rate_asuransi';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'umur' => 'Umur',
            'tenor_1' => 'Tenor 1',
            'tenor_2' => 'Tenor 2',
            'tenor_3' => 'Tenor 3',
            'tenor_4' => 'Tenor 4',
            'tenor_5' => 'Tenor 5',
            'tenor_6' => 'Tenor 6',
            'tenor_7' => 'Tenor 7',
            'tenor_8' => 'Tenor 8',
            'tenor_9' => 'Tenor 9',
            'tenor_10' => 'Tenor 10',
            'tenor_11' => 'Tenor 11',
            'tenor_12' => 'Tenor 12',
            'tenor_13' => 'Tenor 13',
            'tenor_14' => 'Tenor 14',
            'tenor_16' => 'Tenor 16',
            'tenor_15' => 'Tenor 15',
            'tenor_18' => 'Tenor 18',
            'tenor_20' => 'Tenor 20',
            'tenor_19' => 'Tenor 19',
            'tenor_17' => 'Tenor 17',
            'tenor_21' => 'Tenor 21',
            'tenor_23' => 'Tenor 23',
            'tenor_22' => 'Tenor 22',
            'tenor_25' => 'Tenor 25',
            'tenor_24' => 'Tenor 24',
        ];
    }

/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\RateAsuransiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\RateAsuransiQuery(get_called_class());
    }
}
