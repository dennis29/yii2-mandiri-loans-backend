<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "kta".
 *
 * @property string $id
 * @property string $nama
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 *
 * @property \app\models\KtaPerusahaanTier[] $ktaPerusahaanTiers
 * @property \app\models\PerusahaanTier[] $perusahaanTiers
 * @property \app\models\KtaRules[] $ktaRules
 * @property \app\models\KtaTarg3tMarket[] $ktaTarg3tMarkets
 * @property \app\models\Targ3tMarket[] $targ3tMarkets
 */
class Kta extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama','pop_up'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['nama','pop_up','user_id'], 'required'],
            // [['nama'], 'required'],
            [['keterangan','target_market_text','ketentuan_umum_text','limit_kredit_text','jangka_waktu_kredit_text','suku_bunga_text','ketentuan_dbr_text'], 'string'],
            [['lock'], 'integer'],
            [['id', 'nama', 'expired', 'pop_up','created_at', 'updated_at', 'created_by', 'updated_by'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kta';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'keterangan' => 'Keterangan',
            'pop_up' => 'Pop up',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKtaPerusahaanTiers()
    {
        return $this->hasMany(\app\models\KtaPerusahaanTier::className(), ['kta_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerusahaanTiers()
    {
        return $this->hasMany(\app\models\PerusahaanTier::className(), ['id' => 'perusahaan_tier_id'])->viaTable('kta_perusahaan_tier', ['kta_id' => 'id']);
    }
    public function getPerusahaanTiersName()
    {
        return $this->hasMany(\app\models\PerusahaanTier::className(), ['id' => 'perusahaan_tier_id'])->viaTable('kta_perusahaan_tier', ['kta_id' => 'id'])->select(['id','nama']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKtaRules()
    {
        return $this->hasMany(\app\models\KtaRules::className(), ['kta_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKtaTarg3tMarkets()
    {
        return $this->hasMany(\app\models\KtaTarg3tMarket::className(), ['kta_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarg3tMarkets()
    {
        return $this->hasMany(\app\models\Targ3tMarket::className(), ['id' => 'targ3t_market_id'])->viaTable('kta_targ3t_market', ['kta_id' => 'id']);
    }

    public function getTarg3tMarketsName()
    {
        return $this->hasMany(\app\models\Targ3tMarket::className(), ['id' => 'targ3t_market_id'])->viaTable('kta_targ3t_market', ['kta_id' => 'id'])
            ->select(['id','name']);
        ;
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\KtaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\KtaQuery(get_called_class());
    }
}
