<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "suku_bunga".
 *
 * @property string $id
 * @property string $nama
 * @property integer $lock
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 *
 * @property \app\models\KprSukuBunga[] $kprSukuBungas
 * @property \app\models\Kpr[] $kprs
 * @property \app\models\SukuBungaRules[] $sukuBungaRules
 */
class SukuBunga extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['nama','user_id'], 'required'],
            [['lock','is_reguler'], 'integer'],
            [['id', 'nama', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suku_bunga';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKprSukuBungas()
    {
        return $this->hasMany(\app\models\KprSukuBunga::className(), ['suku_bunga_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKprs()
    {
        return $this->hasMany(\app\models\Kpr::className(), ['id' => 'kpr_id'])->viaTable('kpr_suku_bunga', ['suku_bunga_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSukuBungaRules()
    {
        return $this->hasMany(\app\models\SukuBungaRules::className(), ['suku_bunga_id' => 'id']);
    }


    public function getSukuBungaRulesKpr()
    {
        return $this->hasMany(\app\models\SukuBungaRules::className(), ['suku_bunga_id' => 'id'])->where(['is_multiguna'=>\app\models\SukuBungaRules::TYPE_KPR]);
    }    

    public function getSukuBungaRulesMultiguna()
    {
        return $this->hasMany(\app\models\SukuBungaRules::className(), ['suku_bunga_id' => 'id'])->where(['is_multiguna'=>\app\models\SukuBungaRules::TYPE_MULTIGUNA]);
    }    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\SukuBungaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\SukuBungaQuery(get_called_class());
    }
}
