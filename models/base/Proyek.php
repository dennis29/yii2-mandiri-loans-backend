<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "proyek".
 *
 * @property string $id
 * @property string $nama
 * @property string $tanggal_jatuh_tempo
 * @property string $developer_id
 * @property string $kpr_id
 * @property double $min_harga
 * @property double $max_harga
 * @property string $region_id
 * @property double $longitude
 * @property double $latitude
 * @property integer $location
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 *
 * @property \app\models\Developer $developer
 * @property \app\models\Kpr $kpr
 * @property \app\models\Region $region
 */
class Proyek extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['nama','user_id'], 'required'],
            [['tanggal_jatuh_tempo'], 'safe'],
            [['min_harga', 'max_harga', 'longitude', 'latitude'], 'number'],
            [['location', 'lock'], 'integer'],
            [['id', 'nama', 'developer_id', 'kpr_id','area_id','website'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proyek';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'tanggal_jatuh_tempo' => 'Tanggal Jatuh Tempo',
            'developer_id' => 'Developer ID',
            'kpr_id' => 'Kpr ID',
            'min_harga' => 'Min Harga',
            'max_harga' => 'Max Harga',
            'region_id' => 'Region ID',
            'area_id' => 'Area ID',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
            'location' => 'Location',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeveloper()
    {
        return $this->hasOne(\app\models\Developer::className(), ['id' => 'developer_id']);
    }

    public function getDeveloperTier()
    {
        return $this->hasOne(\app\models\DeveloperTier::className(), ['id' => 'developer_tier_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKpr()
    {
        return $this->hasOne(\app\models\Kpr::className(), ['id' => 'kpr_id']);
    }

    public function getArea()
    {
        return $this->hasOne(\app\models\Area::className(), ['id' => 'area_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(\app\models\Region::className(), ['id' => 'region_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\ProyekQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ProyekQuery(get_called_class());
    }
}
