<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "kpr_developer_tier".
 *
 * @property string $kpr_id
 * @property string $developer_tier_id
 * @property string $updated_at
 * @property string $created_at
 * @property string $updated_by
 * @property string $created_by
 * @property integer $lock
 * @property string $id
 *
 * @property \app\models\DeveloperTier $developerTier
 * @property \app\models\Kpr $kpr
 */
class KprDeveloperTier extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kpr_id', 'developer_tier_id'], 'required'],
            [['lock'], 'integer'],
            [['kpr_id', 'developer_tier_id', 'updated_at', 'created_at', 'updated_by', 'created_by', 'id'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpr_developer_tier';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kpr_id' => 'Kpr ID',
            'developer_tier_id' => 'Developer Tier ID',
            'lock' => 'Lock',
            'id' => 'ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeveloperTier()
    {
        return $this->hasOne(\app\models\DeveloperTier::className(), ['id' => 'developer_tier_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKpr()
    {
        return $this->hasOne(\app\models\Kpr::className(), ['id' => 'kpr_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\KprDeveloperTierQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\KprDeveloperTierQuery(get_called_class());
    }
}
