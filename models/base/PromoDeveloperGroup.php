<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "promo_developer_group".
 *
 * @property string $promo_id
 * @property string $developer_group_id
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property integer $lock
 * @property string $id
 * @property string $text
 *
 * @property \app\models\Promo $promo
 * @property \app\models\DeveloperGroup $developerGroup
 */
class PromoDeveloperGroup extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promo_id', 'developer_group_id'], 'required'],
            [['lock'], 'integer'],
            [['text'], 'string'],
            [['promo_id', 'developer_group_id', 'created_at', 'created_by', 'updated_at', 'updated_by', 'id'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promo_developer_group';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'promo_id' => 'Promo ID',
            'developer_group_id' => 'Developer Group ID',
            'lock' => 'Lock',
            'id' => 'ID',
            'text' => 'Text',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromo()
    {
        return $this->hasOne(\app\models\Promo::className(), ['id' => 'promo_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeveloperGroup()
    {
        return $this->hasOne(\app\models\DeveloperGroup::className(), ['id' => 'developer_group_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\PromoDeveloperGroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PromoDeveloperGroupQuery(get_called_class());
    }
}
