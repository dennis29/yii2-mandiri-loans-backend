<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "suku_bunga_rules".
 *
 * @property string $id
 * @property string $suku_bunga_id
 * @property integer $is_multiguna
 * @property integer $order_number
 * @property integer $suku_bunga_fixed
 * @property integer $tahun_fixed
 * @property integer $min_tenor
 * @property integer $lock
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_at
 * @property string $updated_at
 *
 * @property \app\models\SukuBunga $sukuBunga
 */
class SukuBungaRules extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['suku_bunga_id', 'is_multiguna', 'order_number', 'suku_bunga_fixed', 'tahun_fixed', 'min_tenor'], 'required'],
            [['is_multiguna', 'order_number',  'tahun_fixed', 'min_tenor', 'lock'], 'integer'],
            [['suku_bunga_fixed'], 'double'],
            [['id', 'suku_bunga_id', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suku_bunga_rules';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'suku_bunga_id' => 'Suku Bunga ID',
            'is_multiguna' => 'Is Multiguna',
            'order_number' => 'Order Number',
            'suku_bunga_fixed' => 'Suku Bunga Fixed',
            'tahun_fixed' => 'Tahun Fixed',
            'min_tenor' => 'Min Tenor',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSukuBunga()
    {
        return $this->hasOne(\app\models\SukuBunga::className(), ['id' => 'suku_bunga_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\SukuBungaRulesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\SukuBungaRulesQuery(get_called_class());
    }
}
