<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[KprProfesiKpr]].
 *
 * @see KprProfesiKpr
 */
class KprProfesiKprQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return KprProfesiKpr[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return KprProfesiKpr|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}