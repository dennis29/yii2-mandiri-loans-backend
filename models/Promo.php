<?php

namespace app\models;

use \app\models\base\Promo as BasePromo;

/**
 * This is the model class for table "promo".
 */
class Promo extends BasePromo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['title'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['order_number','title'], 'required'],
            [['order_number','lock'], 'integer'],
            [['id', 'title', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
