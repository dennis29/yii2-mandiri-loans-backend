<?php

namespace app\models;

use yii\helpers\ArrayHelper;
use \app\models\base\Perusahaan as BasePerusahaan;

/**
 * This is the model class for table "perusahaan".
 */
class Perusahaan extends BasePerusahaan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['name',  'perusahaan_tier_id', 'perusahaan_group_id'], 'required'],
            [['max_limit', 'max_tenor', 'dbr'], 'number'],
            [['lock'], 'integer'],
            [['id', 'name', 'perusahaan_tier_id', 'perusahaan_group_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }

    public function importExcel(){
        $fileExcel = Yii::getAlias("@webroot").'/uploads/excels/perusahaan.xls' ;
        if(!file_exists ( $fileExcel)){
            echo Yii::getAlias("@webroot").'/uploads/excels/perusahaan.xls';
            die();
        }

         $data = \moonland\phpexcel\Excel::import($fileExcel, [
                'setFirstRecordAsKeys' => true, // if you want to set the keys of record column with first record, if it not set, the header with use the alphabet column on excel. 
                'setIndexSheetByName' => true, // set this if your excel data with multiple worksheet, the index of array will be set with the sheet name. If this not set, the index will use numeric. 
                'getOnlySheet' => 'sheet1', // you can set this property if you want to get the specified sheet from the excel data with multiple worksheet.
            ]);
    }

    public function triggerNotifikasi($sesudah,$state = 'update'){

        if($state == 'delete' || $state == 'update'){
            $model  = Perusahaan::findOne($this->id);
            $sebelum = ArrayHelper::toArray($model);
            $sebelum['perusahaanTier'] = ArrayHelper::toArray($model->perusahaanTier);
            $sebelum['perusahaanGroup'] = ArrayHelper::toArray($model->perusahaanGroup);
        }
        $notifikasi = new Notifikasi();
        $notifikasi->code = Notifikasi::Code_PERUSAHAANLIST;
        if($state == 'delete' || $state == 'update'){
            $notifikasi->sebelum = json_encode($sebelum);
        }
        if($state == 'create' || $state == 'update'){
            $notifikasi->sesudah = $sesudah;
        }
        $notifikasi->status = 0;
        $notifikasi->is_read = 0;
        if(!$notifikasi->save()){
            var_dump($notifikasi->getErrors());
            die();
        }
    }
}
