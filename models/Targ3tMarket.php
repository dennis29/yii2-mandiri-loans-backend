<?php

namespace app\models;

use \app\models\base\Targ3tMarket as BaseTarg3tMarket;

/**
 * This is the model class for table "targ3t_market".
 */
class Targ3tMarket extends BaseTarg3tMarket
{
    /**
     * @inheritdoc
     */
    const Code_Nasabah_Prioritas = '81cc8131fd8a11e6acda94dbc9b18a6e';
    const Code_Nasabah_Payroll = '905ca29ef4f711e69e6094dbc9b18a6e';
    const Code_Nasabah_Non_Payroll = '55f2412be19b11e698c794dbc9b18a6e';
    const Code_Pegawai = '0a338d09e19b11e698c794dbc9b18a6e';
    const Code_Wiraswasta = '91bd0364e19b11e698c794dbc9b18a6e';

    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['name', 'desc'], 'required'],
            [['desc'], 'string'],
            [['lock'], 'integer'],
            [['id', 'name', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
