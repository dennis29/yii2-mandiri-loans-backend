<?php

namespace app\models;

use \app\models\base\RateAsuransi as BaseRateAsuransi;

/**
 * This is the model class for table "rate_asuransi".
 */
class RateAsuransi extends BaseRateAsuransi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['umur', 'tenor_1', 'tenor_2', 'tenor_3', 'tenor_4', 'tenor_5', 'tenor_6', 'tenor_7', 'tenor_8', 'tenor_9', 'tenor_10', 'tenor_11', 'tenor_12', 'tenor_13', 'tenor_14', 'tenor_16', 'tenor_15', 'tenor_18', 'tenor_20', 'tenor_19', 'tenor_17', 'tenor_21', 'tenor_23', 'tenor_22', 'tenor_25', 'tenor_24'], 'required'],
            [['umur'], 'integer'],
            [['tenor_1', 'tenor_2', 'tenor_3', 'tenor_4', 'tenor_5', 'tenor_6', 'tenor_7', 'tenor_8', 'tenor_9', 'tenor_10', 'tenor_11', 'tenor_12', 'tenor_13', 'tenor_14', 'tenor_16', 'tenor_15', 'tenor_18', 'tenor_20', 'tenor_19', 'tenor_17', 'tenor_21', 'tenor_23', 'tenor_22', 'tenor_25', 'tenor_24'], 'number'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
