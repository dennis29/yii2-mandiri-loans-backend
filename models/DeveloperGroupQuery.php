<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[DeveloperGroup]].
 *
 * @see DeveloperGroup
 */
class DeveloperGroupQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return DeveloperGroup[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DeveloperGroup|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}