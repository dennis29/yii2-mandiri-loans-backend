<?php

namespace app\models;

use Yii;
use \app\models\base\Reward as BaseReward;
use yii\web\UploadedFile;

/**
 * This is the model class for table "reward".
 */
class Reward extends BaseReward
{
    /**
     * @inheritdoc
     */
    public $imageReward;

    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['title'], 'match', 'pattern' => '/^[0-9a-zA-Z_&()\-\s.,!\/%]*$/i', 'message' => 'Allows only numbers, alphabets, underscore, hyphen, ampersand, round brackets, slash, comma, dot, exclamation mark, percent and space.'],
            [['title'], 'required'],
            [['text', 'title'], 'string'],
            [['lock'], 'integer'],
            [['imageReward'], 'safe'],
            [['id', 'image'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['imageReward'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
     public function upload()
    {
        if ($this->validate()) {
            $this->imageReward = UploadedFile::getInstance($this, 'imageReward');
            if(!empty($this->imageReward)){
                $this->imageReward->saveAs(Yii::$app->basePath . '/web/uploads/reward/' . md5($this->imageReward->baseName . date("H:i:s")) . '.' . $this->imageReward->extension);
                $this->image = '@web/uploads/reward/' . md5($this->imageReward->baseName . date("H:i:s")) . '.' . $this->imageReward->extension;
                $this->image_name = $this->imageReward->baseName . '.' . $this->imageReward->extension;
                $this->imageReward = null;
                $this->save();
            }
            return true;
        } else { 
            return false;
        }
    }
}
