<?php

namespace app\models;

use \app\models\base\KprProfesiKpr as BaseKprProfesiKpr;

/**
 * This is the model class for table "kpr_profesi_kpr".
 */
class KprProfesiKpr extends BaseKprProfesiKpr
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['kpr_id', 'profesi_kpr_id'], 'required'],
            [['kpr_id', 'profesi_kpr_id'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
