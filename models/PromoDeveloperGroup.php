<?php

namespace app\models;

use \app\models\base\PromoDeveloperGroup as BasePromoDeveloperGroup;

/**
 * This is the model class for table "promo_developer_group".
 */
class PromoDeveloperGroup extends BasePromoDeveloperGroup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['promo_id', 'developer_group_id'], 'required'],
            [['lock'], 'integer'],
            [['text'], 'string'],
            [['promo_id', 'developer_group_id', 'created_at', 'created_by', 'updated_at', 'updated_by', 'id'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
