<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[LogUser]].
 *
 * @see LogUser
 */
class LogUserQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return LogUser[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return LogUser|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}