<?php

namespace app\models;
use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "kta".
 */
class Staging_table extends ActiveRecord
{   
    public static function tableName()
    {
        return '{{staging_table}}';
    }
 
    public static function getAll($arr=[])
    {
		 $conn = Yii::$app->getDb();
		 $where="";
		 
		 
		 if($arr){

			 	 foreach($arr as $k=>$v)
				 {
					 if($i==0)
					 {
						$where .= "where ".$v->field." LIKE '%".$v->value."%'";  
					 }
					 else
					 {
						$where .= " and ".$v->field." LIKE '%".$v->value."%'"; 
					 }	 
					 $i++;
				 }
		 }
         return $conn->createCommand('select * from staging_table '.$where)->queryAll();
	      
    }

    
}
