<?php

namespace app\controllers;

use Yii;
use app\models\Excel;
use app\models\PerusahaanKpr;
use app\models\PerusahaanKprTier;
use app\models\PerusahaanKprSearch;
use app\models\Priviledge;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use \yii\web\Response;
use yii\filters\VerbFilter;

/**
 * PerusahaanKprController implements the CRUD actions for PerusahaanKpr model.
 */
class PerusahaanKprController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],

                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rules,$action){
                            return $this->isAccepted($rules,$action);
                        }
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    private function isAccepted($rules,$action){
        //jika dia admin cek table priviledge
        $status = Yii::$app->user->identity->status;
        if(in_array($status, [5])){
            $cek = Priviledge::find()->where(['nama' => 'Perusahaan List KPR','status'=>1])->one();
            if(!empty($cek)){
                return true;
            }else{
                return false;
            }
        }
        //jika approver return true
        if(in_array($status, [10])){
            return true;
        }else{
        //jika selain itu return false
            return false;
        }
        
    }
    /**
     * Lists all PerusahaanKpr models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PerusahaanKprSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $excelModel = Excel::find()->where(['class'=>'perusahaanKpr'])->one();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'excelModel' => $excelModel,
        ]);
    }

    /**
     * Displays a single PerusahaanKpr model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerPerusahaanKpr = new \yii\data\ArrayDataProvider([
            'allModels' => $model->perusahaanKprs,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerPerusahaanKpr' => $providerPerusahaanKpr,
        ]);
    }

    /**
     * Creates a new PerusahaanKpr model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PerusahaanKpr();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PerusahaanKpr model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if ($model->loadAll($post) && $model->save()) {
            // $sesudah= $post['PerusahaanKpr'];
            // if(!empty($post['PerusahaanKpr'])){
            //     $sesudah['group'] = PerusahaanKpr::find()->where(['id'=>$sesudah['group_id']])->asArray()->one();
            // }
            // if(!empty($sesudah['perusahaan_kpr_tier_id'])){
            //     $sesudah['perusahaanKprTier'] = PerusahaanKprTier::find()->where(['id'=>$sesudah['perusahaan_kpr_tier_id']])->asArray()->one();
            // }
            // $model->triggerNotifikasi(json_encode($sesudah));
            // $model2 = $this->findModel($id);
            // $model2->is_approved = 0;
            // $model2->save();

            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');  
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PerusahaanKpr model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    public function actionBulkDelete()
    {        
        try{

            $request = Yii::$app->request;
            $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
            foreach ( $pks as $pk ) {
                $model = $this->findModel($pk);
                $model->delete();
            }

            if($request->isAjax){
                /*
                *   Process for ajax request
                */
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['forceClose'=>true,'forceReload'=>'#kv-pjax-container-perusahaan-kpr'];
            }else{
                /*
                *   Process for non-ajax request
                */
                return $this->redirect(['index']);
            }
        }catch(\yii\db\IntegrityException $e){
            \Yii::$app->getSession()->setFlash('danger', 'Cannot delete because linked');   
            
            return $this->redirect(['index']);
        }
       
    }

    public function actionDeleteAll()
    {
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $model = null;
        try{
            $models = PerusahaanKpr::find()->all();
            foreach ($models as $item) {
                # code...
                $model = $item;
                $item->delete();
            }
            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');
            $transaction->commit();
        } catch (\Exception $e) {
            if(!empty($model)){

                \Yii::$app->getSession()->setFlash('error', $model->nama." gagal dihapus");   
            }else{
                
                \Yii::$app->getSession()->setFlash('error', "error");   
            }
            $transaction->rollBack();
            // throw $e;
        } catch (\Throwable $e) {
            \Yii::$app->getSession()->setFlash('error', $e->getMessage());   
            $transaction->rollBack();
            // throw $e;
        }
        return $this->redirect(['index']);
    }

    
    /**
     * Finds the PerusahaanKpr model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PerusahaanKpr the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PerusahaanKpr::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for PerusahaanKpr
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddPerusahaanKpr()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('PerusahaanKpr');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formPerusahaanKpr', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImport(){
        $request = Yii::$app->request;
        if($request->isGet){
            return $this->redirect(['index']);

        }else{
            $modelExcel = Excel::findOne($request->post('Excel')['id']);
            $modelExcel->load($request->post());
            if(!$modelExcel->upload()){
                \Yii::$app->getSession()->setFlash('error', 'Upload failed');   
                return $this->redirect(['index']);
            }

            $data = $modelExcel->importExcel();
            if(empty($data)){
                \Yii::$app->getSession()->setFlash('error', 'wrong format');   
                
            }
            // var_dump($data);
            // die();
            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try{
                PerusahaanKpr::deleteAll();
                $this->importDataInit($data);
                \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');   
                $transaction->commit();
            } catch (\Exception $e) {
                \Yii::$app->getSession()->setFlash('error', 'error');   
                $transaction->rollBack();
                // throw $e;
            } catch (\Throwable $e) {
                \Yii::$app->getSession()->setFlash('error', 'error');   
                $transaction->rollBack();
                // throw $e;
            }
            return $this->redirect(['index']);
        }
    }

    private function importDataInit($data){
         $groupModel = null;
        foreach ($data as $item) {
            $item["perusahaan"] = trim($item["perusahaan"]);
            $item["tier"] = trim($item["tier"]);
            if(!empty($item["no"])){
                $groupModel = PerusahaanKpr::find()->where(['name'=>$item["perusahaan"]])->one();
                if(empty($groupModel)){
                    $groupModel = new PerusahaanKpr();
                }
                $groupModel->name = $item["perusahaan"];
                $this->saveTier($item,$groupModel);
                $groupModel->save(false);
            }else{
                $model = PerusahaanKpr::find()->where(['name'=>$item["perusahaan"]])->one();
                if(empty($model)){
                    $model = new PerusahaanKpr();
                }
                $model->name = $item["perusahaan"];
                $model->group_id = $groupModel->id;
                $this->saveTier($item,$model);
                $model->save(false);
            }
        }
    }

    private function saveTier($item,$model){
        $tierModel  = PerusahaanKprTier::find()->where(['name'=>$item["tier"]])->one();
        if(empty($tierModel)){
            $tierModel = new PerusahaanKprTier();
            $tierModel->name= $item["tier"];
            if(!$tierModel->save()){
                echo json_encode($tierModel->getErrors());
                die();
            }
        }
        $model->perusahaan_kpr_tier_id = $tierModel->id;
        $model->save();
    }

    public function actionExport(){
        ob_end_clean();
        \moonland\phpexcel\Excel::export([
            'models' => PerusahaanKpr::find()->with(['perusahaanKprTier'])->orderBy('created_at')->all(),
            'fileName' => 'perusahaanKpr',
                'columns' => [
                    [
                            // 'attribute' => '',
                            'header' => 'no',
                            'format' => 'text',
                            'value' => function($model) {
                                if(empty($model->group_id)){
                                    return "1";
                                }else{
                                    return "";
                                }
                            },
                    ],
                    [
                            'attribute' => 'name',
                            'header' => 'perusahaan',
                            'format' => 'raw',
                            // 'value' => function($model) {
                            //     return ExampleClass::removeText('example', $model->content);
                            // },
                    ],
                    [
                            'attribute' => 'group_id',
                            'header' => 'group',
                            'format' => 'raw',
                            'value' => function($model){
                                if ($model->group){
                                    return $model->group->name;
                                }
                                else{
                                    return "";
                                }
                            }
                    ],
                    [
                            'header' => 'tier',
                            'format' => 'raw',
                             'value' => function($model) {
                                 if(empty($model->perusahaanKprTier)){
                                    return "";
                                }   else{
                                    return $model->perusahaanKprTier->name;
                                }
                            }
                    ],
                ],
                'headers' => [
                    'created_at' => 'Date Created Content',
                ],
        ]);        
    }
}