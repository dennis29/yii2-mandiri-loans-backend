<?php

namespace app\controllers;

use Yii;
use app\models\Kpr;
use app\models\KprSearch;
use app\models\PerusahaanKprTier;
use app\models\DeveloperTier;
use app\models\ProfesiKpr;
use app\models\SukuBunga;
use app\models\Targ3tMarket;
use app\models\KprTarg3tMarket;
use app\models\Excel;
use app\models\User;
use app\models\Priviledge;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/*
 * KprController implements the CRUD actions for Kpr model.
 */
class KprController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'add-kpr-developer-tier', 'add-kpr-profesi-kpr', 
                            'export','import','change-approval','test-kpr-dev-tier',
                            'add-kpr-suku-bunga', 'add-kpr-targ3t-market','add-kpr-perusahaan-kpr-tier', 'add-proyek','delete-page'],
                        'roles' => ['@'],
                        'matchCallback' => function($rules,$action){
                            return $this->isAccepted($rules,$action);
                        }
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    private function isAccepted($rules,$action){
        //jika dia admin cek table priviledge
        $status = Yii::$app->user->identity->status;
        if(in_array($status, [5])){
            $cek = Priviledge::find()->where(['nama' => 'KPR','status'=>1])->one();
            if(!empty($cek)){
                return true;
            }else{
                return false;
            }
        }
        //jika approver return true
        if(in_array($status, [10])){
            return true;
        }else{
        //jika selain itu return false
            return false;
        }
        
    }

    /**
     * Lists all Kpr models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KprSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $excelModel = Excel::find()->where(['class'=>'kpr'])->one();
        $modelLink = new Kpr();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'excelModel' => $excelModel,
            'model' => $modelLink
        ]);
    }

    /**
     * Displays a single Kpr model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->redirect(['index']);
        
        $model = $this->findModel($id);
        $providerKprDeveloperTier = new \yii\data\ArrayDataProvider([
            'allModels' => $model->kprDeveloperTiers,
        ]);
        $providerKprProfesiKpr = new \yii\data\ArrayDataProvider([
            'allModels' => $model->kprProfesiKprs,
        ]);
        $providerKprSukuBunga = new \yii\data\ArrayDataProvider([
            'allModels' => $model->kprSukuBungas,
        ]);
        $providerKprTarg3tMarket = new \yii\data\ArrayDataProvider([
            'allModels' => $model->kprTarg3tMarkets,
        ]);
        $providerProyek = new \yii\data\ArrayDataProvider([
            'allModels' => $model->proyeks,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerKprDeveloperTier' => $providerKprDeveloperTier,
            'providerKprProfesiKpr' => $providerKprProfesiKpr,
            'providerKprSukuBunga' => $providerKprSukuBunga,
            'providerKprTarg3tMarket' => $providerKprTarg3tMarket,
            'providerProyek' => $providerProyek,
        ]);
    }

    /**
     * Creates a new Kpr model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Kpr();
        $post = Yii::$app->request->post();
        // if(!empty($post["KprPerusahaanKprTier"])){
        //     $tempKprTier =  $post["KprPerusahaanKprTier"];
        //     $post["KprPerusahaanKprTier"] = [];
        // }

        if ($model->loadAll($post) /*&& $model->saveAll()*/) {
            
            // if(!empty($post["KprPerusahaanKprTier"])){

            //     foreach ($tempKprTier as $item) {
            //         // echo var_dump($item['perusahaan_kpr_tier_id']).'   ';
            //         $modelTier = PerusahaanKprTier::findOne($item["perusahaan_kpr_tier_id"]);
            //         $model->link("perusahaanKprTiers",$modelTier);                                
            //     }
            // }
            $sesudah= $post['Kpr'];
            if(!empty($post['KprDeveloperTier'])){
                $ids = [];
                foreach ($post['KprDeveloperTier'] as $item) {
                    $ids[] = $item['developer_tier_id'];
                }
                $items = DeveloperTier::find()->where(['id'=>$ids])->all();
                $sesudah['developerTiers'] = ArrayHelper::toArray($items);
            }
            if(!empty($post['KprProfesiKpr'])){
                $ids = [];
                foreach ($post['KprProfesiKpr'] as $item) {
                    $ids[] = $item['profesi_kpr_id'];
                }
                $items = ProfesiKpr::find()->where(['id'=>$ids])->all();
                $sesudah['profesiKprs'] = ArrayHelper::toArray($items);
            }
            if(!empty($post['KprSukuBunga'])){
                $ids = [];
                foreach ($post['KprSukuBunga'] as $item) {
                    $ids[] = $item['suku_bunga_id'];
                }
                $items = SukuBunga::find()->where(['id'=>$ids])->all();
                
                $sesudah['sukuBungas'] = ArrayHelper::toArray($items);
            }
            if(!empty($post['KprTarg3tMarket'])){
                $ids = [];
                foreach ($post['KprTarg3tMarket'] as $item) {
                    $ids[] = $item['targ3t_market_id'];
                }
                $items = Targ3tMarket::find()->where(['id'=>$ids])->all();
                $sesudah['targ3tMarkets'] = ArrayHelper::toArray($items);
                
            }
            if(!empty($post['KprPerusahaanKprTier'])){
                $ids = [];
                foreach ($post['KprPerusahaanKprTier'] as $item) {
                    $ids[] = $item['perusahaan_kpr_tier_id'];
                }
                $items = PerusahaanKprTier::find()->where(['id'=>$ids])->all();
                $sesudah['perusahaanKprTiers'] = ArrayHelper::toArray($items);
            }
            
            $model->triggerNotifikasi(json_encode($sesudah),'create');
            
            // \Yii::$app->getSession()->setFlash('success', 'Your data has been created');   

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Kpr model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if ($model->loadAll($post)) {
            // $model->saveAll();
            
            // $sesudah = [];
            // array_merge($sesudah, $post['Kpr']);
            $sesudah= $post['Kpr'];
            if(!empty($post['KprDeveloperTier'])){
                $ids = [];
                foreach ($post['KprDeveloperTier'] as $item) {
                    $ids[] = $item['developer_tier_id'];
                }
                $items = DeveloperTier::find()->where(['id'=>$ids])->all();
                $sesudah['developerTiers'] = ArrayHelper::toArray($items);
            }
            if(!empty($post['KprProfesiKpr'])){
                $ids = [];
                foreach ($post['KprProfesiKpr'] as $item) {
                    $ids[] = $item['profesi_kpr_id'];
                }
                $items = ProfesiKpr::find()->where(['id'=>$ids])->all();
                $sesudah['profesiKprs'] = ArrayHelper::toArray($items);
            }
            if(!empty($post['KprSukuBunga'])){
                $ids = [];
                foreach ($post['KprSukuBunga'] as $item) {
                    $ids[] = $item['suku_bunga_id'];
                }
                $items = SukuBunga::find()->where(['id'=>$ids])->all();
                
                $sesudah['sukuBungas'] = ArrayHelper::toArray($items);
            }
            if(!empty($post['KprTarg3tMarket'])){
                $ids = [];
                foreach ($post['KprTarg3tMarket'] as $item) {
                    $ids[] = $item['targ3t_market_id'];
                }
                $items = Targ3tMarket::find()->where(['id'=>$ids])->all();
                $sesudah['targ3tMarkets'] = ArrayHelper::toArray($items);
                
            }
            if(!empty($post['KprPerusahaanKprTier'])){
                $ids = [];
                foreach ($post['KprPerusahaanKprTier'] as $item) {
                    $ids[] = $item['perusahaan_kpr_tier_id'];
                }
                $items = PerusahaanKprTier::find()->where(['id'=>$ids])->all();
                $sesudah['perusahaanKprTiers'] = ArrayHelper::toArray($items);
            }
            
            $model->triggerNotifikasi(json_encode($sesudah));
            $model2 = $this->findModel($id);
            $model2->is_approved = 0;
            $model2->save();
            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');  
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }


    }

    /**
     * Deletes an existing Kpr model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // $this->findModel($id)->deleteWithRelated();
        $this->findModel($id)->triggerNotifikasi("",'delete');
        return $this->redirect(['index']);
    }

    public function actionDeletePage($id){
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if ($model->loadAll($post)) {
            $sesudah= $post['Kpr'];
            
            $model->triggerNotifikasi("",'delete');  
            return $this->redirect(['index']);
            } else {
                return $this->render('delete-page', [
                    'model' => $model,
                ]);
            }
    }

    
    /**
     * Finds the Kpr model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Kpr the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kpr::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModelWithRelation($id)
    {
        if (($model = Kpr::find()->with(['kprDeveloperTiers','targ3tMarkets','profesiKprs','sukuBungas','perusahaanKprTiers'])
            ->where(['id'=>$id])
            ->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    /**
    * Action to load a tabular form grid
    * for KprDeveloperTier
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddKprDeveloperTier()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('KprDeveloperTier');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formKprDeveloperTier', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for KprProfesiKpr
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddKprProfesiKpr()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('KprProfesiKpr');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formKprProfesiKpr', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for KprSukuBunga
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddKprSukuBunga()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('KprSukuBunga');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formKprSukuBunga', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for KprTarg3tMarket
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddKprTarg3tMarket()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('KprTarg3tMarket');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formKprTarg3tMarket', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionAddKprPerusahaanKprTier() 
           { 
               if (Yii::$app->request->isAjax) { 
                   $row = Yii::$app->request->post('KprPerusahaanKprTier'); 
                   if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add') 
                       $row[] = []; 
                   return $this->renderAjax('_formKprPerusahaanKprTier', ['row' => $row]); 
               } else { 
                   throw new NotFoundHttpException('The requested page does not exist.'); 
               } 
           } 
    /**
    * Action to load a tabular form grid
    * for Proyek
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddProyek()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Proyek');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formProyek', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionExport(){
        ob_end_clean();
        
        \moonland\phpexcel\Excel::export([
            'models' => Kpr::find()->orderBy('order_number')->all(),
            'fileName' => 'KPR',
                'columns' => [
                    // [
                    //         'attribute' => 'id',
                    //         'header' => 'id',
                    //         'format' => 'raw',
                    // ],
                    [
                            'attribute' => 'order_number',
                            'header' => 'order_number',
                            'format' => 'text',
                    ],
                    [
                            'attribute' => 'nama',
                            'header' => 'nama',
                            'format' => 'text',
                    ],
                    [
                            'attribute' => 'expired',
                            'header' => 'expired',
                            'format' => 'text',
                    ],
                    [
                            'header' => 'developerTiers',
                            'format' => 'raw',
                            'value' => function($model) {
                                 if(empty($model->developerTiers)){
                                    return "";
                                }   else{
                                    $temp = [];
                                    foreach ($model->developerTiers as $key) {
                                        $temp[] = $key->nama;
                                    }
                                    return implode(", ", $temp);
                                    // return $model->developerTiers->nama;
                                }
                            }
                    ],
                    [
                            'header' => 'profesiKprs',
                            'format' => 'text',
                            'value' => function($model) {
                                 if(empty($model->profesiKprs)){
                                    return "";
                                }   else{
                                    $temp = [];
                                    foreach ($model->profesiKprs as $key) {
                                        $temp[] = $key->nama;
                                    }
                                    return implode(", ", $temp);
                                    // return $model->profesiKprs->nama;
                                }
                            }
                    ],
                    [
                            'header' => 'sukuBungas',
                            'format' => 'raw',
                            'value' => function($model) {
                                 if(empty($model->sukuBungas)){
                                    return "";
                                }   else{
                                    $temp = [];
                                    foreach ($model->sukuBungas as $key) {
                                        $temp[] = $key->nama;
                                    }
                                    return implode(", ", $temp);
                                }
                            }
                    ],
                    [
                            'header' => 'targ3tMarkets',
                            'format' => 'text',
                            'value' => function($model) {
                                 if(empty($model->targ3tMarkets)){
                                    return "";
                                }   else{
                                    $temp = [];
                                    foreach ($model->targ3tMarkets as $key) {
                                        $temp[] = $key->name;
                                    }
                                    return implode(", ", $temp);
                                }
                            }
                    ],
                    [
                            'header' => 'perusahaanKprTiers',
                            'format' => 'text',
                            'value' => function($model) {
                                 if(empty($model->perusahaanKprTiers)){
                                    return "";
                                }   else{
                                    $temp = [];
                                    foreach ($model->perusahaanKprTiers as $key) {
                                        $temp[] = $key->name;
                                    }
                                    return implode(", ", $temp);
                                }
                            }
                    ],
                    [
                            'attribute' => 'profil_nasabah_text',
                            'header' => 'profil_nasabah_text',
                            'format' => 'raw',
                            // 'value' => function($model) {
                            //     return $model->developerGroup->name;
                            // }
                    ],
                    [
                            'attribute' => 'peruntukan_text',
                            'header' => 'peruntukan_text',
                            'format' => 'raw',
                    ],
                    [
                            'attribute'=>'market_text',
                            'header' => 'market_text',
                            'format' => 'raw',
                    ],
                    [
                            'attribute'=>'benefit_text',
                            'header' => 'benefit_text',
                            'format' => 'raw',
                    ],
                    [
                            'attribute'=>'suku_bunga_kpr_text',
                            'header' => 'suku_bunga_kpr_text',
                            'format' => 'raw',
                    ],
                    [
                            'attribute'=>'suku_bunga_multiguna_text',
                            'header' => 'suku_bunga_multiguna_text',
                            'format' => 'raw',
                    ],
                    [
                            'attribute'=>'peserta_text',
                            'header' => 'peserta_text',
                            'format' => 'raw',
                    ],
                    [
                            'attribute'=>'keterangan',
                            'header' => 'keterangan',
                            'format' => 'raw',
                    ],
                ],
                'headers' => [
                    'created_at' => 'Date Created Content',
                ],
        ]);
    }

    public function actionImport(){
        $request = Yii::$app->request;
        if($request->isGet){
            return $this->redirect(['index']);

        }else{
            $modelExcel = Excel::findOne($request->post('Excel')['id']);
            $modelExcel->load($request->post());
            if(!$modelExcel->upload()){
                \Yii::$app->getSession()->setFlash('error', 'Upload failed');   
                return $this->redirect(['index']);
            }

            $data = $modelExcel->importExcel();
            if(empty($data)){
                \Yii::$app->getSession()->setFlash('error', 'wrong format');   
                
            }
            // var_dump($data);
            // die();
            $message = $this->importData($data);
            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');   
            // \Yii::$app->getSession()->setFlash('success', $message);   
            return $this->redirect(['index']);
        }
    }

    private function importData($data){
        $modelSebelumnya = null;
        foreach ($data as $item) {
            //Nama
            $model = Kpr::find()->where(['nama'=>$item["nama"]])->one();
            if(!empty($item["nama"])){
                if(empty($model)){
                    $model = new Kpr();
                    $model->nama = $item['nama'];
                }
                $model->order_number = $item["order_number"];
                $model->expired = $item["expired"];
                $model->profil_nasabah_text = $item["profil_nasabah_text"];
                $model->peruntukan_text = $item["peruntukan_text"];
                $model->market_text = $item["market_text"];
                $model->benefit_text = $item["benefit_text"];
                $model->suku_bunga_kpr_text = $item["suku_bunga_kpr_text"];
                $model->suku_bunga_multiguna_text = $item["suku_bunga_multiguna_text"];
                $model->peserta_text = $item["peserta_text"];
                $model->keterangan = $item["keterangan"];
            }
            if(!empty($model)){
                //Developer Tier
                $item['developerTiers'] = $this->convertDevTier($item['developerTiers']);
                //Profesi KPR
                $item['profesiKprs'] = $this->convertProfesi($item['profesiKprs']);
                //Suku Bunga
                $item['sukuBungas'] = $this->convertSukuBunga($item['sukuBungas']);
                //Target Market
                $item['targ3tMarkets'] = $this->convertTargetMarket($item['targ3tMarkets']); 
                //Perusahaan KPR Tier
                $item['perusahaanKprTiers'] = $this->convertPerusahaanKprTier($item['perusahaanKprTiers']);

                if($model->isNewRecord){
                    $model->triggerNotifikasi(json_encode($item),'create');
                }else{
                    $model->triggerNotifikasi(json_encode($item),'update');
                }
            }
        }
    }
    
    // public function actionTestKprDevTier($item){
    //     $items = explode(',', $item); // Array or selected records primary keys
    //     $items = array_map('trim',$items);
    //     $devtiers = DeveloperTier::find()->where(['nama'=>$items])->asArray()->all();
    //     return $devtiers;        
    // }
    private function convertDevTier($item){
        $items = explode(',', $item); // Array or selected records primary keys
        $items = array_map('trim',$items);
        $devtiers = DeveloperTier::find()->where(['nama'=>$items])->asArray()->all();
        return $devtiers;
    }

    private function convertProfesi($item){
        $items = explode(',', $item); // Array or selected records primary keys
        $items = array_map('trim',$items);
        $profesi = ProfesiKpr::find()->where(['nama'=>$items])->asArray()->all();
        return $profesi;
    }

    private function convertSukuBunga($item){
        $items = explode(',', $item); // Array or selected records primary keys
        $items = array_map('trim',$items);
        $sukuBungaCon = SukuBunga::find()->where(['nama'=>$items])->asArray()->all();
        return $sukuBungaCon;
    }

    private function convertTargetMarket($item){
        $items = explode(',', $item); // Array or selected records primary keys
        $items = array_map('trim',$items);
        $targetMarket = Targ3tMarket::find()->where(['name'=>$items])->asArray()->all();
        return $targetMarket;
    }

    private function convertPerusahaanKprTier($item){
        $items = explode(',', $item); // Array or selected records primary keys
        $items = array_map('trim',$items);
        $perusahaanKpr = PerusahaanKprTier::find()->where(['name'=>$items])->asArray()->all();
        return $perusahaanKpr;
    }    

    private function importDataOld($data){
        $modelSebelumnya = null;
        foreach ($data as $item) {
            //Nama
            $model = Kpr::find()->where(['nama'=>$item["nama"]])->one();
            if(!empty($item["nama"])){
                if(empty($model)){
                    $model = new Kpr();
                    $model->nama = $item['nama'];
                    $model->save(false);
                }
                $model->order_number = $item["order_number"];
                $model->expired = $item["expired"];
                $model->profil_nasabah_text = $item["profil_nasabah_text"];
                $model->peruntukan_text = $item["peruntukan_text"];
                $model->market_text = $item["market_text"];
                $model->benefit_text = $item["benefit_text"];
                $model->suku_bunga_kpr_text = $item["suku_bunga_kpr_text"];
                $model->suku_bunga_multiguna_text = $item["suku_bunga_multiguna_text"];
                $model->peserta_text = $item["peserta_text"];
                $model->keterangan = $item["keterangan"];
                $model->triggerNotifikasi(json_encode($sesudah),'create');
                if(!$model->save(false)){
                    echo '498';
                    var_dump($model->getErrors());
                    die();
                }
            }
            if(!empty($model)){
                //Developer Tier
                $modelSebelumnya = $model;
                foreach ($model->developerTiers as $devlama) {
                    $model->unlink('developerTiers',$devlama,true);
                }
                $targets = explode(',',$item["developerTiers"]);
                foreach ($targets as $target) {
                    $target = trim($target);
                    $targetModel = DeveloperTier::find()->where(['nama'=>$target])->one();
                    if(empty($targetModel)){
                        $targetModel = new DeveloperTier();
                        $targetModel->name = trim($target);
                        if($targetModel->save(false)){
                            echo '517';
                            var_dump($model->getErrors());
                            die();
                        }
                    }
                    $model->link('developerTiers',$targetModel);
                }
                //Profesi
                foreach ($model->profesiKprs as $profesilama) {
                    $model->unlink('profesiKprs',$profesilama,true);
                }
                $targets = explode(',',$item["profesiKprs"]);
                foreach ($targets as $target) {
                    $target = trim($target);
                    $targetModel = ProfesiKpr::find()->where(['nama'=>$target])->one();
                    if(empty($targetModel)){
                        $targetModel = new ProfesiKpr();
                        $targetModel->nama = trim($target);
                        if(!$targetModel->save(false)){
                            echo '534';
                            var_dump($model->getErrors());
                            die();
                        }
                    }
                    $model->link('profesiKprs',$targetModel);
                }
                //SukuBunga
                foreach ($model->sukuBungas as $bungalama) {
                    $model->unlink('sukuBungas',$bungalama,true);
                }
                $targets = explode(',',$item["sukuBungas"]);
                foreach ($targets as $target) {
                    $target = trim($target);
                    $targetModel = SukuBunga::find()->where(['nama'=>$target])->one();
                    if(empty($targetModel)){
                        $targetModel = new SukuBunga();
                        $targetModel->nama = trim($target);
                        if(!$targetModel->save(false)){
                            echo '552';
                            var_dump($model->getErrors());
                            die();
                        } 
                    }
                    $model->link('sukuBungas',$targetModel);
                }
                //Target Market
                foreach ($model->targ3tMarkets as $targetlama) {
                    $model->unlink('targ3tMarkets',$targetlama,true);
                }
                $targets = explode(',',$item["targ3tMarkets"]);
                foreach ($targets as $target) {
                    $target = trim($target);
                    $targetModel = Targ3tMarket::find()->where(['name'=>$target])->one();
                    if(empty($targetModel)){
                        $targetModel = new targ3tMarket();
                        $targetModel->name = trim($target);
                        if(!$targetModel->save(false)){
                            echo '570';
                            var_dump($model->getErrors());
                            die();
                        }
                    }
                    $model->link('targ3tMarkets',$targetModel);
                }
                //Perusahaan
                foreach ($model->perusahaanKprTiers as $tierlama) {
                    $model->unlink('perusahaanKprTiers',$tierlama,true);
                }
                $tiers = explode(',',$item["perusahaanKprTiers"]);
                foreach ($tiers as $tier) {
                    $tier = trim($tier);
                    $tierModel = PerusahaanKprTier::find()->where(['name'=>$tier])->one();
                    if(empty($tierModel)){
                        $tierModel = new PerusahaanKprTier();
                        $tierModel->name = trim($tier);
                        if(!$tierModel->save(false)){
                            echo '588';
                            var_dump($model->getErrors());
                            die();
                        }
                    }
                    $model->link('perusahaanKprTiers',$tierModel);
                }
                $model->triggerNotifikasi(json_encode($item),'create');
                var_dump($item);die();   
            }
        }
    }
}