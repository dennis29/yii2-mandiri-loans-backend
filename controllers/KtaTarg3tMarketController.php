<?php

namespace app\controllers;

use Yii;
use app\models\KtaTarg3tMarket;
use app\models\KtaTarg3tMarketSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KtaTarg3tMarketController implements the CRUD actions for KtaTarg3tMarket model.
 */
class KtaTarg3tMarketController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all KtaTarg3tMarket models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KtaTarg3tMarketSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KtaTarg3tMarket model.
     * @param string $kta_id
     * @param string $targ3t_market_id
     * @return mixed
     */
    public function actionView($kta_id, $targ3t_market_id)
    {
        $model = $this->findModel($kta_id, $targ3t_market_id);
        return $this->render('view', [
            'model' => $this->findModel($kta_id, $targ3t_market_id),
        ]);
    }

    /**
     * Creates a new KtaTarg3tMarket model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KtaTarg3tMarket();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'kta_id' => $model->kta_id, 'targ3t_market_id' => $model->targ3t_market_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing KtaTarg3tMarket model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kta_id
     * @param string $targ3t_market_id
     * @return mixed
     */
    public function actionUpdate($kta_id, $targ3t_market_id)
    {
        $model = $this->findModel($kta_id, $targ3t_market_id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'kta_id' => $model->kta_id, 'targ3t_market_id' => $model->targ3t_market_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing KtaTarg3tMarket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kta_id
     * @param string $targ3t_market_id
     * @return mixed
     */
    public function actionDelete($kta_id, $targ3t_market_id)
    {
        $this->findModel($kta_id, $targ3t_market_id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the KtaTarg3tMarket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kta_id
     * @param string $targ3t_market_id
     * @return KtaTarg3tMarket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kta_id, $targ3t_market_id)
    {
        if (($model = KtaTarg3tMarket::findOne(['kta_id' => $kta_id, 'targ3t_market_id' => $targ3t_market_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
