<?php

namespace app\controllers;

use Yii;
use app\models\UserReferensi;
use app\models\UserReferensiSearch;
use app\models\Area;
use app\models\Region;
use app\models\LogUser;
use app\models\Priviledge;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Excel;
use \yii\web\Response;
use yii\helpers\Json;

/**
 * UserReferensiController implements the CRUD actions for UserReferensi model.
 */
class UserReferensiController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // [
                    //     'allow' => true,
                    //     'actions'=>['my-white-list'],
                    //     'roles' => ['@'],
                    //     'matchCallback' => function($rules,$action){
                    //         return $this->isAcceptedRegion($rules,$action);
                    //         // return status= =12;
                    //     }
                    // ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rules,$action){
                            return $this->isAccepted($rules,$action);
                        }
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    private function isAccepted($rules,$action){
        //jika dia admin cek table priviledge
        $status = Yii::$app->user->identity->status;
        if(in_array($status, [5])){
            $cek = Priviledge::find()->where(['nama' => 'User White List','status'=>1])->one();
            if(!empty($cek)){
                return true;
            }else{
                return false;
            }
        }
        //jika approver return true
        if(in_array($status, [10])){
            return true;
        }else{
        //jika selain itu return false
            return false;
        }
        
    }

    private function isAcceptedRegion($rules,$action){
        //jika dia admin cek table priviledge
        $status = Yii::$app->user->identity->status;
        return $status ==12;
    }

    /**
     * Lists all UserReferensi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserReferensiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $excelModel = Excel::find()->where(['class'=>'userReferensi'])->one();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'excelModel' => $excelModel,
        ]);
    }

    /**
     * Displays a single UserReferensi model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->redirect(['index']);
        
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserReferensi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserReferensi();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            LogUser::logHitUser(LogUser::CREATE_WHITELIST,LogUser::SUCCESS,Yii::$app->user->identity->nip,Yii::$app->user->identity->id);
            \Yii::$app->getSession()->setFlash('success', 'Your data has been craeted');   
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UserReferensi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            LogUser::logHitUser(LogUser::UPDATE_WHITELIST,LogUser::SUCCESS,Yii::$app->user->identity->nip,Yii::$app->user->identity->id);
            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');   
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserReferensi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();
        LogUser::logHitUser(LogUser::DELETE_WHITELIST,LogUser::SUCCESS,Yii::$app->user->identity->nip,Yii::$app->user->identity->id);
        \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');   

        return $this->redirect(['index']);
    }

    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            // return $this->redirect(['index']);
            LogUser::logHitUser(LogUser::DELETE_WHITELIST,LogUser::SUCCESS,Yii::$app->user->identity->nip,Yii::$app->user->identity->id);
            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');
            return ['forceClose'=>true,'forceReload'=>'#kv-pjax-container-user-referensi'];
        }else{
            /*
            *   Process for non-ajax request
            */
            LogUser::logHitUser(LogUser::DELETE_WHITELIST,LogUser::SUCCESS,Yii::$app->user->identity->nip,Yii::$app->user->identity->id);
            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');
            return $this->redirect(['index']);
        }
       
    }

    public function actionDeleteAll()
    {
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $model = null;
        try{
            $models = UserReferensi::find()->all();
            foreach ($models as $item) {
                # code...
                $model = $item;
                $item->delete();
            }
            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');
            LogUser::logHitUser(LogUser::DELETE_WHITELIST,LogUser::SUCCESS,Yii::$app->user->identity->nip,Yii::$app->user->identity->id);
            $transaction->commit();
        } catch (\Exception $e) {
            if(!empty($model)){

                \Yii::$app->getSession()->setFlash('error', $model->name." gagal dihapus");   
            }else{
                
                \Yii::$app->getSession()->setFlash('error', "error");   
            }
            $transaction->rollBack();
            // throw $e;
        } catch (\Throwable $e) {
            \Yii::$app->getSession()->setFlash('error', $e->getMessage());   
            $transaction->rollBack();
            // throw $e;
        }
        return $this->redirect(['index']);
    }

    
    /**
     * Finds the UserReferensi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return UserReferensi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserReferensi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImport(){
        $request = Yii::$app->request;
        if($request->isGet){
            return $this->redirect(['index']);

        }else{
            $modelExcel = Excel::findOne($request->post('Excel')['id']);
            $modelExcel->load($request->post());
            if(!$modelExcel->upload()){
                \Yii::$app->getSession()->setFlash('error', 'Upload failed');   
                return $this->redirect(['index']);
            }

            $data = $modelExcel->importExcel();
            if(empty($data)){
                \Yii::$app->getSession()->setFlash('error', 'wrong format');   
                
            }
            // var_dump($data);
            // die();
            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try{

                UserReferensi::deleteAll();
                $this->importData($data);
                \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');   
                $transaction->commit();
                $total = count($data);
                $pesan = "Import User White List berhasil dilakukan sebanyak ".$total." data";
                LogUser::logHitUser(LogUser::IMPORT_WHITELIST,LogUser::SUCCESS,Yii::$app->user->identity->nip,Yii::$app->user->identity->id,$pesan);
            } catch (\Exception $e) {
                \Yii::$app->getSession()->setFlash('error', 'error');   
                $transaction->rollBack();
                // throw $e;
            } catch (\Throwable $e) {
                \Yii::$app->getSession()->setFlash('error', 'error');   
                $transaction->rollBack();
                // throw $e;
            }
            return $this->redirect(['index']);
        }
    }

    private function importData($data){
        $groupModel = null;
        $groupModel2 = null;
        foreach ($data as $item) {
            $groupModel = Area::find()->where(['name'=>$item["Area"]])->one();
            if(empty($groupModel)){
                $groupModel = new Area();
                $groupModel->name = $item["Area"];
                $groupModel->save();
            }
            $model = UserReferensi::find()->where(['nip'=>$item["Nip"]])->one();
            if(empty($model)){
                $model = new UserReferensi();
                $model->area_id = $groupModel->id;
            }
            $groupModel2 = Region::find()->where(['name'=>$item["Region"]])->one();
            if(empty($groupModel2)){
                $groupModel2 = new Region();
                $groupModel2->name = $item["Region"];
                $groupModel2->save();
            }
            $model->nip = $item["Nip"];
            if(!empty($item["Nama"])){
                $model->name = $item["Nama"];
            }
            if(!empty($item["Contact"])){
                $model->contact = $item["Contact"];
            }
            $model->dob = $item["DoB"];
            if(!empty($groupModel)){
                $model->area_id = $groupModel->id;
            }
            if(!empty($groupModel2)){
                $model->region_id = $groupModel2->id;
            }
            $model->save(false);
        }
    }

    public function actionExport(){
        ob_end_clean();
        
        \moonland\phpexcel\Excel::export([
            'models' => UserReferensi::find()->with(['region','area'])->orderBy('Nip')->all(),
            'fileName' => 'userreferensi',
                'columns' => [
                    [
                        'attribute'=>'nip',
                        'header' => 'Nip'
                    ],
                    [
                        'attribute'=>'name',
                        'header' => 'Nama'
                    ],
                    [
                        'attribute'=>'contact',
                        'header' => 'Contact'
                    ],
                    [
                        'attribute'=>'dob',
                        'header' => 'DoB'
                    ],
                    [
                        'attribute'=>'area_id',
                        'header' => 'Area',
                        'format' => 'text',
                             'value' => function($model) {
                                if(!empty($model->area)){
                                    return $model->area->name;
                                }else{
                                    return "";
                                }
                            }
                    ],
                    [
                            'attribute' => 'region_id',
                            'header' => 'Region',
                            'format' => 'text',
                             'value' => function($model) {
                                if(!empty($model->region)){
                                    return $model->region->name;
                                }else{
                                    return "";
                                }
                            }
                    ],
                ],
                'headers' => [
                    'created_at' => 'Date Created Content',
                ],
        ]);        
    }

    public function actionSubcat() {
    
    $out = [];
    if (isset($_POST['depdrop_parents'])) {
        $parents = $_POST['depdrop_parents'];
        if ($parents != null) {
            $cat_id = $parents[0];
            $out = UserReferensi::getAreaDep($cat_id); 
            // the getSubCatList function will query the database based on the
            // cat_id and return an array like below:
            // [
            //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
            //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
            // ]
            echo Json::encode(['output'=>$out, 'selected'=>'']);
            return;
        }
    }
    echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    // public function actionMyWhiteList()
    // {
    //     $dataProvider = new ActiveDataProvider([
    //         'query' => UserReferensi::find()->orderBy(['nip' => SORT_ASC])->where(['region_id' => Yii::$app->user->identity->region_id]),
    //         // 'query' => Notifikasi::find()->orderBy(['created_at' => SORT_DESC]),
    //     ]);
    //     $searchModel = new UserReferensiSearch();
    //     $excelModel = Excel::find()->where(['class'=>'userReferensi'])->one();

    //     return $this->render('index-by-region', [
    //         'dataProvider' => $dataProvider,
    //         'searchModel' => $searchModel,
    //         'excelModel' => $excelModel
    //     ]);
    // }

}