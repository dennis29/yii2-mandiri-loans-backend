<?php

namespace app\controllers;

use Yii;
use app\models\Priviledge;
use app\models\LogUser;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PriviledgeController implements the CRUD actions for Priviledge model.
 */
class PrivilegeController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'except' => [],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rules,$action){
                            return $this->isAccepted($rules,$action);
                        } 
                    ],
                ],
            ],
        ];
    }

    private function isAccepted($rules,$action){
        //jika dia admin cek table priviledge
        $status = Yii::$app->user->identity->status;
        if(in_array($status, [5])){
            $cek = Priviledge::find()->where(['nama' => 'Privilege','status'=>1])->one();
            if(!empty($cek)){
                return true;
            }else{
                return false;
            }
        }
        //jika approver return true
        if(in_array($status, [10])){
            return true;
        }else{
        //jika selain itu return false
            return false;
        }
        
    }

    /**
     * Lists all Priviledge models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Priviledge::find()->orderBy(['created_at' => SORT_ASC]),
            'pagination' => array('pageSize' => 50),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Priviledge model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Priviledge model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Priviledge();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Priviledge model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Priviledge model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the Priviledge model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Priviledge the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Priviledge::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSave(){
        $request = Yii::$app->request;
        $nama = $request->post('nama');
        if(empty($nama)){
            $yes = [];
            $nots = Priviledge::find()->asArray()->select('id')->column();
            // var_dump($nots);die();

        }else{
            $yes = Priviledge::find()->where(['id' => $nama])->asArray()->select('id')->column();
            $nots = Priviledge::find()->where(['not in','id',$nama])->asArray()->select('id')->column();
            
        }
        $this->triggerLog($yes,$nots);
        foreach ($yes as $key) {
            if(!empty($key)){   
                $model = $this->findModel($key);
                $model->status = 1;
                $model->save();
            }
        }
        foreach ($nots as $key) {
            if(!empty($key)){
                $model = $this->findModel($key);
                $model->status = 0;
                $model->save();

            }
        }
        \Yii::$app->getSession()->setFlash('success', 'Berhasil disimpan');   
        return $this->redirect(['index']);
    }

    private function triggerLog($yes,$nots){
        $yes_lama = Priviledge::find()->where(['status' => 1])->asArray()->select('id')->column();
        $not_lama = Priviledge::find()->where(['status' => 0])->asArray()->select('id')->column();

        $yang_berubah_dari_yes_ke_no = array_diff($nots, $not_lama);
        $yang_berubah_dari_no_ke_yes = array_diff($yes, $yes_lama);

        foreach ($yang_berubah_dari_yes_ke_no as $key ) {
                $model = $this->findModel($key);
                LogUser::logHitUser(LogUser::EDIT_PRIVILEGE,LogUser::SUCCESS,Yii::$app->user->identity->nip,Yii::$app->user->identity->id,"Terdapat perubahan privilege pada ".$model->nama." menjadi tidak boleh");
        }

        foreach ($yang_berubah_dari_no_ke_yes as $key ) {
                $model = $this->findModel($key);
                LogUser::logHitUser(LogUser::EDIT_PRIVILEGE,LogUser::SUCCESS,Yii::$app->user->identity->nip,Yii::$app->user->identity->id,"Terdapat perubahan privilege pada ".$model->nama." menjadi boleh");
        }
    }
}
