<?php

namespace app\controllers;

use Yii;
use app\models\Notifikasi;
use app\models\Developer;
use app\models\DeveloperGroup;
use app\models\DeveloperTier;
use app\models\NotifikasiSearch;
use app\models\Kta;
use app\models\KtaRules;
use app\models\Kpr;
use app\models\Proyek;
use app\models\SukuBunga;
use app\models\SukuBungaRules;
use app\models\Perusahaan;
use app\models\PerusahaanKpr;
use app\models\ProfesiKpr;
use app\models\Targ3tMarket;
use app\models\PerusahaanKprTier;
use app\models\PerusahaanGroup;
use app\models\PerusahaanTier;
use app\models\Priviledge;
use \yii\web\Response;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NotifikasiController implements the CRUD actions for Notifikasi model.
 */
class NotifikasiController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-respon' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'except' => [],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rules,$action){
                            return $this->isAccepted($rules,$action);
                        } 
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Notifikasi models.
     * @return mixed
     */

    private function isAccepted($rules,$action){
        //jika dia admin cek table priviledge
        $status = Yii::$app->user->identity->status;
        if(in_array($status, [5])){
            $cek = Priviledge::find()->where(['nama' => 'Approval','status'=>1])->one();
            if(!empty($cek)){
                return true;
            }else{
                return false;
            }
        }
        //jika approver return true
        if(in_array($status, [10])){
            return true;
        }else{
        //jika selain itu return false
            return false;
        }
        
    }
    public function actionIndex()
    {
        $searchModel = new NotifikasiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // $dataProvider = new ActiveDataProvider([
        //     'query' => Notifikasi::find()->orderBy(['created_at' => SORT_DESC])->where(['user_id' => Yii::$app->user->identity->id]),
        //     // 'query' => Notifikasi::find()->orderBy(['created_at' => SORT_DESC]),
        // ]);
        $items = Notifikasi::find()->where(['status'=>0,'user_id' => Yii::$app->user->identity->id])->all();
        // foreach ($items as $item) {
        //     $item->is_read = 1;
        //     $item->save();
        //     if(!$item->save()){
        //         echo json_encode($item->getErrors());
        //         die();
        //     }
        // }
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Notifikasi model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->redirect(['index']);
        // $model = $this->findModel($id);
        // return $this->render('view', [
        //     'model' => $this->findModel($id),
        // ]);
    }

    /**
     * Creates a new Notifikasi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Notifikasi();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Notifikasi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Notifikasi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#kv-pjax-container-notifikasi'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }
    public function actionBulkRespon()
    {        
        $request = Yii::$app->request;
        $aksi = $request->post( 'aksi' ); // Array or selected records primary keys
        if($aksi == "approve"){
            $this->actionBulkApprove();
        }else
        if($aksi == "reject"){
            $this->actionBulkReject();

        }
       
    }

    public function actionApproveAll()
    {   
        $models = Notifikasi::find()->where(['user_id' => Yii::$app->user->identity->id,'status'=>0])->all();
        $item = null;
            foreach ( $models as $model) {
                $item = $model;
                $modelSebelum = json_decode($model->sebelum,true);        
                // var_dump($modelSebelum['id']);die();
                $this->actionChangeApproved($model->id,$modelSebelum['id']);
            }

            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        
       
    }

    public function actionRejectAll()
    {   
        $models = Notifikasi::find()->where(['user_id' => Yii::$app->user->identity->id,'status'=>0])->all();
        $item = null;
            foreach ( $models as $model) {
                $item = $model;
                $modelSebelum = json_decode($model->sebelum,true);        
                // var_dump($modelSebelum['id']);die();
                $this->actionChangeReject($model->id,$modelSebelum['id']);
            }

            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        
       
    }

    public function actionBulkApprove()
    {   
        $request = Yii::$app->request;
        $pks = $request->post('selection'); // Array or selected records primary keys
        if(empty($pks)){
                    \Yii::$app->getSession()->setFlash('warning', 'Empty selection');   
            //redirect kehalaman
            return $this->redirect(['index']);

        }
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $modelSebelum = json_decode($model->sebelum,true);        
            // var_dump($modelSebelum['id']);die();
            $this->actionChangeApproved($model->id,$modelSebelum['id']);
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#kv-pjax-container-notifikasi'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    public function actionBulkReject()
    {   
        $request = Yii::$app->request;
        $pks = $request->post('selection'); // Array or selected records primary keys
        if(empty($pks)){
            \Yii::$app->getSession()->setFlash('warning', 'Empty selection');   
            return $this->redirect(['index']);
        }
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $modelSebelum = json_decode($model->sebelum,true);        
            // var_dump($modelSebelum['id']);die();
            $this->actionChangeReject($model->id,$modelSebelum['id']);
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#kv-pjax-container-notifikasi'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    
    /**
     * Finds the Notifikasi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Notifikasi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Notifikasi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionChangeApproved($id,$objId=null){
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try{
            $modelNotif = Notifikasi::findOne($id);
            $modelNotif->status = 1;
            $modelNotif->save();
            if($modelNotif->code==Notifikasi::Code_KPR){
                $this->approveKpr($objId,json_decode($modelNotif->sesudah,true));
            }else if($modelNotif->code==Notifikasi::Code_KTA){
                $this->approveKta($objId,json_decode($modelNotif->sesudah,true));
            }else if($modelNotif->code==Notifikasi::Code_SB){
                $this->approveSukubunga($objId,json_decode($modelNotif->sesudah,true));
            }else if($modelNotif->code==Notifikasi::Code_PROYEK){
                $this->approveProyek($objId,json_decode($modelNotif->sesudah,true));
            }
            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');   
            $transaction->commit();
        } catch (\Exception $e) {
            if(!empty($item)){

                \Yii::$app->getSession()->setFlash('error', $e->getMessage());   
                // \Yii::$app->getSession()->setFlash('error', $item->nama." gagal dihapus karena masih ada diproyek");   
            }else{
                
                \Yii::$app->getSession()->setFlash('error', $e->getMessage());   
            }
            $transaction->rollBack();
            // throw $e;
        } catch (\Throwable $e) {
            \Yii::$app->getSession()->setFlash('error', $e->getMessage());   
            $transaction->rollBack();
            // throw $e;
        }
        // else if($modelNotif->code==Notifikasi::Code_PERUSAHAANLIST){
        //     $this->approvePerusahaanList($objId,json_decode($modelNotif->sesudah,true));
        // }else if($modelNotif->code==Notifikasi::Code_PERUSAHAANGROUP){
        //     $this->approvePerusahaanGroup($objId,json_decode($modelNotif->sesudah,true));
        // }else if($modelNotif->code==Notifikasi::Code_PERUSAHAANTIER){
        //     $this->approvePerusahaanTier($objId,json_decode($modelNotif->sesudah,true));
        // }else if($modelNotif->code==Notifikasi::Code_DEVELOPERLIST){
        //     $this->approveDeveloperList($objId,json_decode($modelNotif->sesudah,true));
        // }else if($modelNotif->code==Notifikasi::Code_DEVELOPERGROUP){
        //     $this->approveDeveloperGroup($objId,json_decode($modelNotif->sesudah,true));
        // }else if($modelNotif->code==Notifikasi::Code_DEVELOPERTIER){
        //     $this->approveDeveloperTier($objId,json_decode($modelNotif->sesudah,true));
        // }else if($modelNotif->code==Notifikasi::Code_PERUSAHAANKPR){
        //     $this->approvePerusahaanKpr($objId,json_decode($modelNotif->sesudah,true));
        // }else if($modelNotif->code==Notifikasi::Code_PERUSAHAANKPRTIER){
        //     $this->approvePerusahaanKprTier($objId,json_decode($modelNotif->sesudah,true));
        // }
        //notifikasi sukses
        //redirect kehalaman
        return $this->redirect(['index']);
    }

    private function approveKpr($id,$model2){
        if(empty($model2)){
            Kpr::findOne($id)->deleteWithRelated();
            return;
        }
        if(empty($id)){
            $model = new Kpr();
        }else{
            $model = Kpr::find()->where(['id'=>$id])->one();
        }
        $model->nama = $model2['nama'];
        $model->order_number = $model2['order_number'];
        $model->expired = $model2['expired'];
        $model->keterangan = $model2['keterangan'];
        $model->profil_nasabah_text = $model2['profil_nasabah_text'];
        $model->peruntukan_text = $model2['peruntukan_text'];
        $model->market_text = $model2['market_text'];
        $model->benefit_text = $model2['benefit_text'];
        $model->suku_bunga_kpr_text = $model2['suku_bunga_kpr_text'];
        $model->suku_bunga_multiguna_text = $model2['suku_bunga_multiguna_text'];
        $model->peserta_text = $model2['peserta_text'];
        $model->user_id = Yii::$app->user->identity->id;
        // // $model->developerTiers = $model2['developerTiers'];
        // // $model->profesiKprs = $model2['profesiKprs'];
        // // $model->sukuBungas = $model2['sukuBungas'];
        // // $model->targ3tMarkets = $model2['targ3tMarkets'];
        // // $model->perusahaanKprTiers = $model2['perusahaanKprTiers'];
        $model->is_approved = 1;
            // die();
        if(!$model->save()){
            echo json_encode($model->getErrors());
            die();
        }else{
            //save all relation here
            foreach ($model->developerTiers as $item) {
                $model->unlink('developerTiers',$item,true);
            }
            if(!empty($model2['developerTiers'])){
                foreach ($model2['developerTiers'] as $item) {
                    $objItem = DeveloperTier::findOne($item['id']); 
                    // echo 'item '.json_encode($item);
                    $model->link('developerTiers',$objItem);
                }
            }

            foreach ($model->profesiKprs as $item) {
                $model->unlink('profesiKprs',$item,true);
            }
            if(!empty($model2['profesiKprs'])){
                foreach ($model2['profesiKprs'] as $item) {
                    $objItem = ProfesiKpr::findOne($item['id']); 
                    // echo 'item '.json_encode($item);
                    $model->link('profesiKprs',$objItem);
                }
            }

            foreach ($model->sukuBungas as $item) {
                $model->unlink('sukuBungas',$item,true);
            }
            if(!empty($model2['sukuBungas'])){
                foreach ($model2['sukuBungas'] as $item) {
                    $objItem = SukuBunga::findOne($item['id']); 
                    // echo 'item '.json_encode($item);
                    $model->link('sukuBungas',$objItem);
                }
            }

            foreach ($model->targ3tMarkets as $item) {
                $model->unlink('targ3tMarkets',$item,true);
            }
            if(!empty($model2['targ3tMarkets'])){
                foreach ($model2['targ3tMarkets'] as $item) {
                    $objItem = Targ3tMarket::findOne($item['id']); 
                    // echo 'item '.json_encode($item);
                    $model->link('targ3tMarkets',$objItem);
                }
            }


            foreach ($model->perusahaanKprTiers as $item) {
                $model->unlink('perusahaanKprTiers',$item,true);
            }
            if(!empty($model2['perusahaanKprTiers'])){
                foreach ($model2['perusahaanKprTiers'] as $item) {
                    $objItem = PerusahaanKprTier::findOne($item['id']); 
                    // echo 'item '.json_encode($item);
                    $model->link('perusahaanKprTiers',$objItem);
                }
            }
        }
        // foreach ($model->perusahaanKprTiers as $item) {
        //     $model->unlink('perusahaanKprTiers',$item,true);
        // }
        // if(!empty($model2['perusahaanKprTiers'])){
        //     foreach ($model2['perusahaanKprTiers'] as $item) {
        //         $model->link('perusahaanKprTiers',$item);
        //     }
        // }
    
    }

    private function approveProyek($id,$model2){
        if(empty($model2)){
            Proyek::findOne($id)->deleteWithRelated();
            return;
        }
        if(empty($id)){
            $model = new Proyek();
        }else{
            $model = Proyek::find()->where(['id'=>$id])->one();
        }
        $model->nama = $model2['nama'];
        // var_dump($model2);die();
        $model->tanggal_jatuh_tempo = $model2['tanggal_jatuh_tempo'];
        $model->developer_id = $model2['developer_id'];
        if(!empty($model2['region_id'])){
            $model->region_id = $model2['region_id'];
        }else{
            $model->area_id = NULL;
        }
        if(!empty($model2['area_id'])){
            $model->area_id = $model2['area_id'];
        }else{
            $model->area_id = NULL;
        }
        $model->min_harga = $model2['min_harga'];
        $model->max_harga = $model2['max_harga'];
        $model->website = $model2['website'];
        $model->longitude = $model2['longitude'];
        $model->latitude = $model2['latitude'];
        $model->user_id = Yii::$app->user->identity->id;
        // $model->location = $model2['location'];
        $model->is_approved = 1;
        if(!$model->save()){
            echo json_encode($model->getErrors());
            die();
        }else{

        }
    }

    // private function approvePerusahaanList($id,$model2){
    //     if(empty($id)){
    //         $model = new Perusahaan();
    //     }else{

    //         $model = Perusahaan::find()->where(['id'=>$id])->one();
    //     }
    //     $model->name = $model2['name'];
    //     $model->perusahaan_tier_id = $model2['perusahaan_tier_id'];
    //     $model->perusahaan_group_id = $model2['perusahaan_group_id'];
    //     $model->is_approved = 1;
    //     if(!$model->save()){
    //         echo json_encode($model->getErrors());
    //         die();
    //     }else{

    //     }
    // }

    // private function approvePerusahaanGroup($id,$model2){
    //     $model = PerusahaanGroup::find()->where(['id'=>$id])->one();
    //     $model->name = $model2['name'];
    //     $model->is_approved = 1;
    //     if(!$model->save()){
    //         echo json_encode($model->getErrors());
    //         die();
    //     }else{

    //     }
    // }

    // private function approvePerusahaanTier($id,$model2){
    //     $model = PerusahaanTier::find()->where(['id'=>$id])->one();
    //     $model->nama = $model2['nama'];
    //     $model->is_approved = 1;
    //     if(!$model->save(false)){
    //         echo json_encode($model->getErrors());
    //         die();
    //     }else{

    //     }
    // }

    // private function approveDeveloperList($id,$model2){
    //     $model = Developer::find()->where(['id'=>$id])->one();
    //     $model->nama = $model2['nama'];
    //     $model->developer_tier_id = $model2['developer_tier_id'];
    //     $model->developer_group_id = $model2['developer_group_id'];
    //     $model->is_approved = 1;
    //     if(!$model->save()){
    //         echo json_encode($model->getErrors());
    //         die();
    //     }else{

    //     }
    // }

    // private function approveDeveloperGroup($id,$model2){
    //     $model = DeveloperGroup::find()->where(['id'=>$id])->one();
    //     $model->name = $model2['name'];
    //     $model->text = $model2['text'];
    //     $model->is_approved = 1;
    //     if(!$model->save()){
    //         echo json_encode($model->getErrors());
    //         die();
    //     }else{

    //     }
    // }

    // private function approveDeveloperTier($id,$model2){
    //     $model = DeveloperTier::find()->where(['id'=>$id])->one();
    //     $model->nama = $model2['nama'];
    //     $model->is_approved = 1;
    //     if(!$model->save()){
    //         echo json_encode($model->getErrors());
    //         die();
    //     }else{

    //     }
    // }

    // private function approvePerusahaanKpr($id,$model2){
    //     $model = PerusahaanKpr::find()->where(['id'=>$id])->one();
    //     $model->name = $model2['name'];
    //     $model->group_id = $model2['group_id'];
    //     $model->perusahaan_kpr_tier_id = $model2['perusahaan_kpr_tier_id'];
    //     $model->is_approved = 1;
    //     if(!$model->save()){
    //         echo json_encode($model->getErrors());
    //         die();
    //     }else{

    //     }
    // }
    private function approveKta($id,$model2){
        if(empty($model2)){
            Kta::findOne($id)->deleteWithRelated();
            return;
        }
        if(empty($id)){
            $model = new Kta();
        }else{
            $model = Kta::findOne($id);
        }
        $model->nama = $model2['nama'];
        $model->expired = $model2['expired'];
        $model->keterangan = $model2['keterangan'];
        $model->pop_up = $model2['pop_up'];
        $model->target_market_text = $model2['target_market_text'];
        $model->ketentuan_umum_text = $model2['ketentuan_umum_text'];
        $model->limit_kredit_text = $model2['limit_kredit_text'];
        $model->user_id = Yii::$app->user->identity->id;
        $model->jangka_waktu_kredit_text = $model2['jangka_waktu_kredit_text'];
        $model->suku_bunga_text = $model2['suku_bunga_text'];
        $model->ketentuan_dbr_text = $model2['ketentuan_dbr_text'];
        $model->is_approved = 1;
            // die();
        if(!$model->save()){
            echo json_encode($model->getErrors());
            die();
        }else{
            //save all relation here
            foreach ($model->perusahaanTiers as $item) {
                $model->unlink('perusahaanTiers',$item,true);
            }
            if(!empty($model2['perusahaanTiers'])){
                foreach ($model2['perusahaanTiers'] as $item) {
                    $objItem = PerusahaanTier::findOne($item['id']); 
                    // echo 'item '.json_encode($item);
                    $model->link('perusahaanTiers',$objItem);
                }
            }

            foreach ($model->targ3tMarkets as $item) {
                $model->unlink('targ3tMarkets',$item,true);
            }
            if(!empty($model2['targ3tMarkets'])){
                foreach ($model2['targ3tMarkets'] as $item) {
                    $objItem = Targ3tMarket::findOne($item['id']); 
                    // echo 'item '.json_encode($item);
                    $model->link('targ3tMarkets',$objItem);
                }
            }
            foreach ($model->ktaRules as $item) {
                $item->delete();
            }
            if(!empty($model2['ktaRules'])){
                foreach ($model2['ktaRules'] as $item) {
                    $obj = new KtaRules();
                    $obj->kta_id = $model->id;
                    $obj->min_tenor = $item['min_tenor'];
                    $obj->max_tenor = $item['max_tenor'];
                    $obj->suku_bunga_floating = $item['suku_bunga_floating'];
                    $obj->suku_bunga_flat = $item['suku_bunga_flat'];
                    $obj->limit_min = $item['limit_min'];
                    $obj->limit_max = $item['limit_max'];
                    $obj->income_min = $item['income_min'];
                    $obj->income_max = $item['income_max'];
                    $obj->max_dbr = $item['max_dbr'];
                    if(!$obj->save(false)){
                        var_dump($obj->getErrors());
                        echo "596";
                        die();
                    }
                }
            }
        }
    }
    private function approveSukubunga($id,$model2){
        if(empty($model2)){
            SukuBunga::findOne($id)->deleteWithRelated();
            return;
        }
        if(empty($id)){
            $model = new SukuBunga();
        }else{
            $model = SukuBunga::findOne($id);
        }
        $model->nama = $model2['nama'];
        $model->user_id = Yii::$app->user->identity->id;
        $model->is_approved = 1;
        // $model->save();
        if(!$model->save()){
            echo json_encode($model->getErrors());
            die();
        }else{
            //save all relation here
        foreach ($model->sukuBungaRules as $item) {
                $item->delete();
            }
            if(!empty($model2['sukuBungaRules'])){
                foreach ($model2['sukuBungaRules'] as $item) {
                    $obj = new SukuBungaRules();
                    $obj->suku_bunga_id = $model->id;
                    $obj->is_multiguna = $item['is_multiguna'];
                    $obj->order_number = $item['order_number'];
                    $obj->suku_bunga_fixed = $item['suku_bunga_fixed'];
                    $obj->tahun_fixed = $item['tahun_fixed'];
                    $obj->min_tenor = $item['min_tenor'];
                    $obj->save();
                }
            }
        }
    }

    // private function approvePerusahaanKprTier($id,$model2){
    //     $model = PerusahaanKprTier::find()->where(['id'=>$id])->one();
    //     $model->name = $model2['name'];
    //     $model->is_approved = 1;
    //     if(!$model->save()){
    //         echo json_encode($model->getErrors());
    //         die();
    //     }else{

    //     }
    // }


    public function actionChangeReject($id,$objId=null){ 
        $modelNotif = Notifikasi::findOne($id);
        $modelNotif->status = 2;
        $modelNotif->save();
        if(!empty($objId)){
            try{

                if($modelNotif->code== Notifikasi::Code_KPR){
                    $this->rejectKpr($objId);
                } else 
                if($modelNotif->code== Notifikasi::Code_KTA){
                    $this->rejectKta($objId);
                } else 
                if($modelNotif->code== Notifikasi::Code_PROYEK){
                    $this->rejectProyek($objId);
                }else
                if ($modelNotif->code== Notifikasi::Code_SB){
                    $this->rejectSukubunga($objId);

                }
            } catch (\Exception $e) {
                //do nothing
            }
        }
        //notifikasi sukses
        \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');   
        //redirect kehalaman
        return $this->redirect(['index']);
    }

    private function rejectKpr($id){
        $model = Kpr::findOne($id);
        $model->is_approved = 1;
        $model->save();
    }
    private function rejectKta($id){
        $model = Kta::findOne($id);
        $model->is_approved = 1;
        $model->save();
    }
    private function rejectSukubunga($id){
        $model = SukuBunga::findOne($id);
        $model->is_approved = 1;
        $model->save();
    }
    private function rejectProyek($id){
        $model = Proyek::findOne($id);
        $model->is_approved = 1;
        $model->save();
    }

    public function actionTestMail(){
        // \Yii::getLogger()->log('test', \yii\log\Logger::LEVEL_ERROR);
        // $model = \app\models\User::findOne('24');
        // echo $model->sendVerification(true);
        echo \Yii::$app
            ->mailer
            ->compose(
                ['html' => 'verificationToken-html'],
                ['user' => \app\models\User::find()->one()]
            )
            ->setFrom('anas@4nets.com')
            ->setTo('farhan0syakir@gmail.com')
            ->setSubject('Terdapat perubahan pada ')
            // ->setHtmlBody('<b>'.Html::a('Verify',[Url::to(['site/verify','id'=>$this->id, 'verify'=>$this->verify_token],true)]).'</b>')
            ->send();   
        // var_dump($model->email);
        die();
    }
    public function actionCountAll(){
        echo Notifikasi::find()->where(['status'=>0,'user_id' => Yii::$app->user->identity->id])->count();
    }

}
