<?php

namespace app\controllers;

use Yii;
use app\models\UserMtf;
use app\models\UserMtfSearch;
use app\models\Area;
use app\models\Region;
use app\models\Priviledge;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Excel;
use \yii\web\Response;
use yii\helpers\Json;

/**
 * UserMtfController implements the CRUD actions for UserMtf model.
 */
class UserMtfController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rules,$action){
                            return $this->isAccepted($rules,$action);
                        }
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    private function isAccepted($rules,$action){
        //jika dia admin cek table priviledge
        $status = Yii::$app->user->identity->status;
        if(in_array($status, [5])){
            $cek = Priviledge::find()->where(['nama' => 'PIC MTF','status'=>1])->one();
            if(!empty($cek)){
                return true;
            }else{
                return false;
            }
        }
        //jika approver return true
        if(in_array($status, [10])){
            return true;
        }else{
        //jika selain itu return false
            return false;
        }
        
    }

    /**
     * Lists all UserMtf models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserMtfSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $excelModel = Excel::find()->where(['class'=>'userMtf'])->one();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'excelModel' => $excelModel,
        ]);
    }

    /**
     * Displays a single UserMtf model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->redirect(['index']);
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserMtf model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserMtf();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UserMtf model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserMtf model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            // return $this->redirect(['index']);
            return ['forceClose'=>true,'forceReload'=>'#kv-pjax-container-user-mtf'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    public function actionDeleteAll()
    {
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $model = null;
        try{
            $models = UserMtf::find()->all();
            foreach ($models as $item) {
                # code...
                $model = $item;
                $item->delete();
            }
            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');
            $transaction->commit();
        } catch (\Exception $e) {
            if(!empty($model)){

                \Yii::$app->getSession()->setFlash('error', $model->nama." gagal dihapus");   
            }else{
                
                \Yii::$app->getSession()->setFlash('error', "error");   
            }
            $transaction->rollBack();
            // throw $e;
        } catch (\Throwable $e) {
            \Yii::$app->getSession()->setFlash('error', $e->getMessage());   
            $transaction->rollBack();
            // throw $e;
        }
        return $this->redirect(['index']);
    }

    
    /**
     * Finds the UserMtf model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return UserMtf the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserMtf::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionImport(){
        $request = Yii::$app->request;
        if($request->isGet){
            return $this->redirect(['index']);

        }else{
            $modelExcel = Excel::findOne($request->post('Excel')['id']);
            $modelExcel->load($request->post());
            if(!$modelExcel->upload()){
                \Yii::$app->getSession()->setFlash('error', 'Upload failed');   
                return $this->redirect(['index']);
            }

            $data = $modelExcel->importExcel();
            if(empty($data)){
                \Yii::$app->getSession()->setFlash('error', 'wrong format');   
                
            }
            // var_dump($data);
            // die();
            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try{
                UserMtf::deleteAll();
                $this->importData($data);
                \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');
                $transaction->commit();
            } catch (\Exception $e) {
                \Yii::$app->getSession()->setFlash('error', 'error');   
                $transaction->rollBack();
                // throw $e;
            } catch (\Throwable $e) {
                \Yii::$app->getSession()->setFlash('error', 'error');   
                $transaction->rollBack();
                // throw $e;
            }   
            return $this->redirect(['index']);
        }
    }

    private function importData($data){
        $groupModel = null;
        $groupModel2 = null;
        foreach ($data as $item) {
            $groupModel = Area::find()->where(['name'=>$item["Area"]])->one();
            if(empty($groupModel)){
                $groupModel = new Area();
                $groupModel->name = $item["Area"];
                $groupModel->save();
            }
            $model = UserMtf::find()->where(['area_id'=>$groupModel->id])->one();
            // $model = UserMtf::find()->where(['nip'=>$item["Nip"]])->one();
            if(empty($model)){
                $model = new UserMtf();
                $model->area_id = $groupModel->id;
                
            }
            $groupModel2 = Region::find()->where(['name'=>$item["Region"]])->one();
            if(empty($groupModel2)){
                $groupModel2 = new Region();
                $groupModel2->name = $item["Region"];
                $groupModel2->save();
            }
            
            $model->nip = $item["Nip"];
            $model->contact = $item["Contact"];
            if(!empty($groupModel)){
                $model->area_id = $groupModel->id;
            }
            if(!empty($groupModel2)){
                $model->region_id = $groupModel2->id;
            }
            $model->slm = $item["SLM"];
            $model->slm_contact = $item["SLM Contact"];

            $model->save(false);
        }
    }

    public function actionExport(){
        ob_end_clean();

        \moonland\phpexcel\Excel::export([
            'models' => UserMtf::find()->orderBy('nip')->all(),
            'fileName' => 'usermtf',
                'columns' => [
                    [
                        'attribute'=>'nip',
                        'header' => 'Nip'
                    ],
                    [
                        'attribute'=>'contact',
                        'header' => 'Contact'
                    ],
                    [
                        'attribute'=>'area_id',
                        'header' => 'Area',
                        'format' => 'text',
                             'value' => function($model) {
                                if(!empty($model->area)){
                                    return $model->area->name;
                                }else{
                                    return "";
                                }
                            }
                    ],
                    [
                            'attribute' => 'region_id',
                            'header' => 'Region',
                            'format' => 'text',
                             'value' => function($model) {
                                if(!empty($model->region)){
                                    return $model->region->name;
                                }else{
                                    return "";
                                }
                            }
                    ],
                    [
                        'attribute'=>'slm',
                        'header' => 'SLM'
                    ],
                    [
                        'attribute'=>'slm_contact',
                        'header' => 'SLM Contact'
                    ],
                ],
                'headers' => [
                    'created_at' => 'Date Created Content',
                ],
        ]);        
    }

    public function actionSubcat() {
    
    $out = [];
    if (isset($_POST['depdrop_parents'])) {
        $parents = $_POST['depdrop_parents'];
        if ($parents != null) {
            $cat_id = $parents[0];
            $out = UserMtf::getAreaDep($cat_id); 
            // the getSubCatList function will query the database based on the
            // cat_id and return an array like below:
            // [
            //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
            //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
            // ]
            echo Json::encode(['output'=>$out, 'selected'=>'']);
            return;
        }
    }
    echo Json::encode(['output'=>'', 'selected'=>'']);
    }
    
}