<?php

namespace app\controllers;

use Yii;
use app\models\Kta;
use app\models\KtaSearch;
use app\models\Priviledge;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Excel;
use app\models\KtaRules;
use app\models\Targ3tMarket;
use app\models\KtaTarg3tMarket;
use app\models\PerusahaanTier;
use app\models\KtaPerusahaanTier;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;



/**
 * KtaController implements the CRUD actions for Kta model.
 */
class KtaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'except' => [],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rules,$action){
                            return $this->isAccepted($rules,$action);
                        } 
                    ],
                ],
            ],
        ];
    }

    private function isAccepted($rules,$action){
        //jika dia admin cek table priviledge
        $status = Yii::$app->user->identity->status;
        if(in_array($status, [5])){
            $cek = Priviledge::find()->where(['nama' => 'KTA','status'=>1])->one();
            if(!empty($cek)){
                return true;
            }else{
                return false;
            }
        }
        //jika approver return true
        if(in_array($status, [10])){
            return true;
        }else{
        //jika selain itu return false
            return false;
        }
        
    }

    /**
     * Lists all Kta models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KtaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $excelModel = Excel::find()->where(['class'=>'kta'])->one();
        $modelLink = new Kta();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'excelModel' => $excelModel,
            'model' => $modelLink
        ]);
    }

    /**
     * Displays a single Kta model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->redirect(['index']);
        $model = $this->findModel($id);
        $providerKtaPerusahaanTier = new \yii\data\ArrayDataProvider([
            'allModels' => $model->ktaPerusahaanTiers,
        ]);
        $providerKtaRules = new \yii\data\ArrayDataProvider([
            'allModels' => $model->ktaRules,
        ]);
        $providerKtaTarg3tMarket = new \yii\data\ArrayDataProvider([
            'allModels' => $model->ktaTarg3tMarkets,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerKtaPerusahaanTier' => $providerKtaPerusahaanTier,
            'providerKtaRules' => $providerKtaRules,
            'providerKtaTarg3tMarket' => $providerKtaTarg3tMarket,
        ]);
    }

    /**
     * Creates a new Kta model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Kta();
        $post = $request->post();
        if ($model->loadAll(Yii::$app->request->post()) /*&& $model->saveAll()*/) {
            $sesudah= $post['Kta'];
            if(!empty($post['KtaPerusahaanTier'])){
                $ids = [];
                foreach ($post['KtaPerusahaanTier'] as $item) {
                    $ids[] = $item['perusahaan_tier_id'];
                }
                $items = PerusahaanTier::find()->where(['id'=>$ids])->all();
                $sesudah['perusahaanTiers'] = ArrayHelper::toArray($items);
            }
            if(!empty($post['KtaTarg3tMarket'])){
                $ids = [];
                foreach ($post['KtaTarg3tMarket'] as $item) {
                    $ids[] = $item['targ3t_market_id'];
                }
                $items = Targ3tMarket::find()->where(['id'=>$ids])->all();
                $sesudah['targ3tMarkets'] = ArrayHelper::toArray($items);
                
            }
            if(!empty($post['KtaRules'])){
                $sesudah['ktaRules'] = $post['KtaRules'];
            }
            
            $model->triggerNotifikasi(json_encode($sesudah),'create');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Kta model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // $model = $this->findModel($id);
        // $model2 = $this->findModel($id);
        // // if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
        // //     return $this->redirect(['view', 'id' => $model->id]);
        // // }
        // $post = Yii::$app->request->post();

        // if ($model->loadAll($post)) {
        //     // $model['kprDeveloperTiers'] = 
        //     // var_dump($model->kprDeveloperTiers);
        //     // die();
        //     $sebelumArr = [];
        //     $sebelumArr = ArrayHelper::toArray($model2);
        //     $sebelumArr['perusahaanTiers'] = ArrayHelper::toArray($model2->perusahaanTiers);
        //     $sebelumArr['targ3tMarkets'] = ArrayHelper::toArray($model2->targ3tMarkets);
        //     $sebelumArr['ktaRules'] = ArrayHelper::toArray($model2->ktaRules);
        //     $model->is_approved = 0;
        //     $model->saveAll();
        //     $sebelum = json_encode($sebelumArr);
        //     if(!empty($post["KtaPerusahaanTier"])){
        //         foreach ($model->perusahaanTiers as $item) {
        //             $model->unlink("perusahaanTiers",$item,true);                                
        //         }
        //         foreach ($post["KtaPerusahaanTier"] as $item) {
        //             // echo var_dump($item['perusahaan_tier_id']).'   ';
        //             // die();
        //             $modelTier = PerusahaanTier::findOne($item["perusahaan_tier_id"]);
        //             $model->link("perusahaanTiers",$modelTier);                                
        //         }
        //     }

        //     $sebelum = json_encode($sebelumArr);
        //     if(!empty($post["KtaTarg3tMarket"])){
        //         foreach ($model->targ3tMarkets as $item) {
        //             $model->unlink("targ3tMarkets",$item,true);                                
        //         }
        //         foreach ($post["KtaTarg3tMarket"] as $item) {
        //             // echo var_dump($item['perusahaan_tier_id']).'   ';
        //             // die();
        //             $modelTier = Targ3tMarket::findOne($item["targ3t_market_id"]);
        //             $model->link("targ3tMarkets",$modelTier);                                
        //         }
        //     }

        //     // echo json_encode( ArrayHelper::toArray($model));
        //     // die();
        //     $model->triggerNotifikasi($sebelum);

        //     \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');  
        //     return $this->redirect(['index']);
        // }
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if ($model->loadAll($post)) {
            // $model->saveAll();
            
            // $sesudah = [];
            // array_merge($sesudah, $post['Kpr']);
            $sesudah= $post['Kta'];
            if(!empty($post['KtaPerusahaanTier'])){
                $ids = [];
                foreach ($post['KtaPerusahaanTier'] as $item) {
                    $ids[] = $item['perusahaan_tier_id'];
                }
                $items = PerusahaanTier::find()->where(['id'=>$ids])->all();
                $sesudah['perusahaanTiers'] = ArrayHelper::toArray($items);
            }
            if(!empty($post['KtaTarg3tMarket'])){
                $ids = [];
                foreach ($post['KtaTarg3tMarket'] as $item) {
                    $ids[] = $item['targ3t_market_id'];
                }
                $items = Targ3tMarket::find()->where(['id'=>$ids])->all();
                $sesudah['targ3tMarkets'] = ArrayHelper::toArray($items);
                
            }
            if(!empty($post['KtaRules'])){
                $sesudah['ktaRules'] = $post['KtaRules'];
            }
            
            $model->triggerNotifikasi(json_encode($sesudah));
            $model2 = $this->findModel($id);
            $model2->is_approved = 0;
            $model2->save();
            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');  
            return $this->redirect(['index']);
        }else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Kta model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // $this->findModel($id)->deleteWithRelated();
        $this->findModel($id)->triggerNotifikasi("",'delete');
        return $this->redirect(['index'], 200);
    }

    public function actionDeletePage($id){
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if ($model->loadAll($post)) {
            $sesudah= $post['Kta'];
            
            $model->triggerNotifikasi("",'delete');    
            return $this->redirect(['index']);
        }else {
            return $this->render('delete-page', [
                'model' => $model,
            ]);
        }
    }

    
    /**
     * Finds the Kta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Kta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kta::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for KtaPerusahaanTier
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddKtaPerusahaanTier()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('KtaPerusahaanTier');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formKtaPerusahaanTier', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for KtaRules
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddKtaRules()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('KtaRules');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formKtaRules', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for KtaTarg3tMarket
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddKtaTarg3tMarket()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('KtaTarg3tMarket');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formKtaTarg3tMarket', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionExport(){
         $model = Kta::find()->All();
        $filename = 'Data-'.Date('YmdGis').'-Mahasiswa.xls';
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=kta.xls");
        echo '<table border="1" width="100%">
            <thead>
                <tr>
                    <th>nama</th>
                    <th>expired</th>
                    <th>keterangan</th>
                    <th>pop_up</th>
                    <th>target_market_text</th>
                    <th>ketentuan_umum_text</th>
                    <th>limit_kredit_text</th>
                    <th>jangka_waktu_kredit_text</th>
                    <th>suku_bunga_text</th>
                    <th>ketentuan_dbr_text</th>
                    <th>Perusahaan_Tier</th>
                    <th>Target_Market</th>
                    <th>Min_Income</th>
                    <th>Max_Income</th>
                    <th>Min_Tenor</th>
                    <th>Max_Tenor</th>
                    <th>Min_Limit</th>
                    <th>Max_Limit</th>
                    <th>Sb_Floating</th>
                    <th>Sb_Flat</th>
                    <th>Dbr</th>
                </tr>
            </thead>';
            foreach($model as $data){
                echo '
                    <tr>
                        <td>'.$data->nama.'</td>
                        <td>'.$data->expired.'</td>
                        <td>'.Html::encode($data->keterangan).'</td>
                        <td>'.$data->pop_up.'</td>
                        <td>'.Html::encode($data->target_market_text).'</td>
                        <td>'.Html::encode($data->ketentuan_umum_text).'</td>
                        <td>'.Html::encode($data->limit_kredit_text).'</td>
                        <td>'.Html::encode($data->jangka_waktu_kredit_text).'</td>
                        <td>'.Html::encode($data->suku_bunga_text).'</td>
                        <td>'.Html::encode($data->ketentuan_dbr_text).'</td>
                        <td>'.$this->implodeTier($data).'</td>
                        <td>'.$this->implodeTargetMarket($data).'</td>
                    
                ';
                $rules = $data->ktaRules;
                $count245 = 0;
                foreach ($rules as $item) {
                    echo '<td>'.$item->income_min.'</td>';
                    echo '<td>'.$item->income_max.'</td>';
                    echo '<td>'.$item->min_tenor.'</td>';
                    echo '<td>'.$item->max_tenor.'</td>';
                    echo '<td>'.$item->limit_min.'</td>';
                    echo '<td>'.$item->limit_max.'</td>';
                    echo '<td>'.$item->suku_bunga_floating.'</td>';
                    echo '<td>'.$item->suku_bunga_flat.'</td>';
                    echo '<td>'.$item->max_dbr.'</td>';
                    if($count245==count($rules)-1){
                        echo '</tr>';
                    }else{
                        echo '</tr><tr>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                    }
                    $count245++;

                }
                if(count($rules)==0){
                    echo '</tr>';
                    
                }
            }
        echo '</table>';
        // die();

    }

    private function implodeTier($model){
        if(empty($model->perusahaanTiers)){
            return "";
        }   else{
            $temp = [];
            foreach ($model->perusahaanTiers as $key) {
                $temp[] = $key->nama;
            }
            return implode(", ", $temp);
        }   
    }

    private function implodeTargetMarket($model){
        if(empty($model->targ3tMarkets)){
            return "";
        }   else{
            $temp = [];
            foreach ($model->targ3tMarkets as $key) {
                $temp[] = $key->name;
            }
            return implode(", ", $temp);
        } 
    }


    public function actionImport(){
        $request = Yii::$app->request;
        if($request->isGet){
            return $this->redirect(['index']);

        }else{
            $modelExcel = Excel::findOne($request->post('Excel')['id']);
            $modelExcel->load($request->post());
            if(!$modelExcel->upload()){
                \Yii::$app->getSession()->setFlash('error', 'Upload failed');   
                return $this->redirect(['index']);
            }

            $data = $modelExcel->importExcel();
            if(empty($data)){
                \Yii::$app->getSession()->setFlash('error', 'wrong format');   
                
            }
            // var_dump($data);
            // die();
            $message = $this->importData($data);
            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');   
            // \Yii::$app->getSession()->setFlash('success', $message);   
            return $this->redirect(['index']);
        }
    }

    private function importData($data){
        $modelSebelumnya = null;
        $itemSebelumnya = null;
        $countRules = -1; 
        $counter = 0;
        foreach ($data as $item) {//perbaris
            $model = Kta::find()->where(['nama'=>$item["nama"]])->one();
            if(!empty($item["nama"])){
                if(!empty($modelSebelumnya)){

                    if($modelSebelumnya->isNewRecord){
                        $modelSebelumnya->triggerNotifikasi(json_encode($itemSebelumnya),'create');
                    }else{
                        $modelSebelumnya->triggerNotifikasi(json_encode($itemSebelumnya),'update');
                    }
                }
                if(empty($model)){
                    $model = new Kta();
                    $itemSebelumnya['nama'] = $item['nama'];
                }
                $itemSebelumnya["expired"] = $item["expired"];
                $itemSebelumnya["keterangan"] = $item["keterangan"];
                $itemSebelumnya["pop_up"] = $item['pop_up'];
                $itemSebelumnya["target_market_text"] = $item['target_market_text'];
                $itemSebelumnya["ketentuan_umum_text"] = $item['ketentuan_umum_text'];
                $itemSebelumnya["limit_kredit_text"] = $item['limit_kredit_text'];
                $itemSebelumnya["jangka_waktu_kredit_text"] = $item['jangka_waktu_kredit_text'];
                $itemSebelumnya["suku_bunga_text"] = $item['suku_bunga_text'];
                $itemSebelumnya["ketentuan_dbr_text"] = $item['ketentuan_dbr_text'];
                // var_dump($item);die();
                $modelSebelumnya = $model;
                $itemSebelumnya = $item;
                // KTA RULES
                $countRules = KtaRules::find()->where(['kta_id'=>$model->id])->count();
                //Target Market
                $itemSebelumnya['targ3tMarkets'] = $this->convertTargetMarket($item['Target_Market']);
                //Perusahaan KPR Tier
                $itemSebelumnya['perusahaanTiers'] = $this->convertPerusahaanTier($item['Perusahaan_Tier']);
            }
            $itemSebelumnya['ktaRules'][] = $this->convertKtaRules($item);
            // $counter++;
        }
            if(!empty($modelSebelumnya)){
                if($modelSebelumnya->isNewRecord){
                    $modelSebelumnya->triggerNotifikasi(json_encode($itemSebelumnya),'create');
                }else{
                    $modelSebelumnya->triggerNotifikasi(json_encode($itemSebelumnya),'update');
                }

            }

    }
    private function convertTargetMarket($item){
        $items = explode(',', $item); // Array or selected records primary keys
        $items = array_map('trim',$items);
        $targetMarket = Targ3tMarket::find()->where(['name'=>$items])->asArray()->all();
        return $targetMarket;
    }
    private function convertPerusahaanTier($item){
        $items = explode(',', $item); // Array or selected records primary keys
        $items = array_map('trim',$items);
        $perusahaanTier = PerusahaanTier::find()->where(['nama'=>$items])->asArray()->all();
        return $perusahaanTier;
    }

    private function convertKtaRules($item){
        $rules = [];
        if(!empty($item["Sb_Flat"])){
            // var_dump($model);die();
            // $rules['kta_id'] = $kta_id;
            if(!empty($item["Min_Tenor"])){
                $rules['min_tenor'] = $item["Min_Tenor"];
            }else{
                $rules['min_tenor'] = -1;
            }
            // intval($rules['min_tenor']);
            if(!empty($item['Max_Tenor'])){
                $rules['max_tenor'] = $item["Max_Tenor"];
            }else{
                $rules['max_tenor'] = -1;
            }
            // intval($rules['max_tenor']);
            if(!empty($item["Min_Income"])){
                $rules['income_min'] = $item["Min_Income"];
            }else{
                $rules['income_min'] = -1;
            }
            // var_dump($item['Max_Income']);die();
            if(!empty($item["Max_Income"])){
                if($item["Max_Income"] != "?"){
                    $rules['income_max'] = $item["Max_Income"];
                }else{
                    $rules['income_max'] = -1;
                }
            }else{
                $rules['income_max'] = -1;
            }
            // intval($rules['income_max']);
            if(!empty($item["Min_Limit"])){
                $rules['limit_min'] = $item["Min_Limit"];
            }else{
                $rules['limit_min'] = -1;
            } 
            // intval($rules['limit_min']);
            if(!empty($item["Max_Limit"])){
                $rules['limit_max'] = $item["Max_Limit"];
            }else{
                $rules['limit_max'] = -1;
            } 
            // intval($rules['limit_max']);
            if(!empty($item["Sb_Flat"])){
                $rules['suku_bunga_flat'] = $item["Sb_Flat"];
            }else{
                $rules['suku_bunga_flat'] = -1;
            } 
            // intval($rules['suku_bunga_flat']);
            if(!empty($item["Sb_Floating"])){
                $rules['suku_bunga_floating'] = $item["Sb_Floating"];
            }else{
                $rules['suku_bunga_floating'] = -1;
            }
            // intval($rules['suku_bunga_floating']);
            if(!empty($item["Dbr"])){
                $rules['max_dbr'] = $item["Dbr"];
            }else{
                $rules['max_dbr'] = -1;
            }
            // intval($rules['max_dbr']);
            return $rules;
        }
    }


    private function importDataOld($data){
        $modelSebelumnya = null;
        foreach ($data as $item) {
            $model = Kta::find()->where(['nama'=>$item["Nama"]])->one();
            if(!empty($item["Nama"])){
                if(empty($model)){
                    $model = new Kta();
                    $model->nama = $item['Nama'];
                    // $model->keterangan = $item['Keterangan'];
                    // $model->save(false);
                }
                $model->keterangan = $item["Keterangan"];
                $model->pop_up = $item['Pop Up'];
                if(!$model->save(false)){
                    echo '456';
                    var_dump($model->getErrors());
                    die();
                }
            }
            if(!empty($model)){
                $modelSebelumnya = $model;
                foreach ($model->targ3tMarkets as $targetlama) {
                    $model->unlink('targ3tMarkets',$targetlama,true);
                }
                $targets = explode(',',$item["Target_Market"]);
                foreach ($targets as $target) {
                    $target = trim($target);
                    $targetModel = Targ3tMarket::find()->where(['name'=>$target])->one();
                    if(empty($targetModel)){
                        $targetModel = new targ3tMarket();
                        $targetModel->name = trim($target);
                        $targetModel->save(false);
                    }
                    $model->link('targ3tMarkets',$targetModel);
                }

                foreach ($model->perusahaanTiers as $tierlama) {
                    $model->unlink('perusahaanTiers',$tierlama,true);
                }
                $tiers = explode(',',$item["Perusahaan_Tier"]);
                foreach ($tiers as $tier) {
                    $tier = trim($tier);
                    $tierModel = PerusahaanTier::find()->where(['nama'=>$tier])->one();
                    if(empty($tierModel)){
                        $tierModel = new PerusahaanTier();
                        $tierModel->nama = trim($tier);
                        $tierModel->save(false);
                    }
                    $model->link('perusahaanTiers',$tierModel);
                }

                foreach ($model->ktaRules as $ruleslama) {
                   $ruleslama->delete();
                }
                
            }

            if(!empty($item["Sb_Flat"])){
                $rules = new KtaRules();
                $rules->kta_id = $modelSebelumnya->id;
                $rules->min_tenor = $item["Min_Tenor"];
                $rules->max_tenor = $item["Max_Tenor"];
                $rules->income_min = $item["Min_Income"];
                $rules->income_max = $item["Max_Income"];
                $rules->limit_min = $item["Min_Limit"]; 
                $rules->limit_max = $item["Max_Limit"]; 
                $rules->suku_bunga_flat = $item["Sb_Flat"];
                $rules->suku_bunga_floating = $item["Sb_Floating"];
                $rules->max_dbr = $item["Dbr"]; 
                $rules->save(false);
            }else{
                
            }
        }
    }
}