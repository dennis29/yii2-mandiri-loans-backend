<?php

namespace app\controllers\api;

use Yii;
use app\models\Intro;
use app\models\IntroSearch;
use app\models\Kta;
use app\models\KtaRules;
use app\models\Targ3tMarket;
use app\models\SukuBungaRules;
use app\models\Area;
use app\models\Region;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\controllers\Financial;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\PasswordResetRequestForm;
use yii\base\ErrorException;

/**
 * IntroController implements the CRUD actions for Intro model.
 */
class KtaController extends BaseApiController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
           
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ]
        ];
    }

   

   

    public function actionSearch($profile_type = "", $perusahaan_id = "",$is_payroll=0,$is_perusahaan=0)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $request = Yii::$app->request;
        $result = ['code'=>200,'message'=>'Data berhasil ditemukan','data'=>null];

        $ktas = \app\models\Kta::find();

        $targetArr = [];
        $targetName = [
            '1' => Targ3tMarket::Code_Nasabah_Prioritas,//nasahah prio
            '2' => Targ3tMarket::Code_Nasabah_Payroll
        ];

        // if ($profile_type == '1' || $profile_type == '2') {
        //     // echo 'asd';die();
        //     array_push($targetArr, $targetName[$profile_type]);
        //     $ktas = $ktas ->leftJoin('kta_targ3t_market', 'kta_id = kta.id');
        //     $ktas = $ktas->andWhere(['targ3t_market_id' => $targetName[$profile_type]]);
        //     // $ktas = $ktas->joinWith('targ3tMarkets')
        //     //              ->where(['targ3t_market.name' => $targetName[$profile_type]]);

        // } else if ($is_perusahaan) {
        //     $ktas = $ktas->joinWith('perusahaanTiers')
        //                  ->leftJoin('perusahaan', 'perusahaan_tier.id = perusahaan.perusahaan_tier_id')
        //                  ->andWhere(['perusahaan.id' => $perusahaan_id]);

        // }
        if ($profile_type == '1') {
            // array_push($targetArr, $targetName[$profile_type]);
            $ktas = $ktas ->leftJoin('kta_targ3t_market', 'kta_targ3t_market.kta_id = kta.id');
            $ktas = $ktas->andWhere(['targ3t_market_id' => Targ3tMarket::Code_Nasabah_Prioritas]);
        }else
        if ($profile_type == '2') {
            // array_push($targetArr, $targetName[$profile_type]);
            $ktas = $ktas ->leftJoin('kta_targ3t_market', 'kta_targ3t_market.kta_id = kta.id');
            $ktas = $ktas->andWhere(['targ3t_market_id' => Targ3tMarket::Code_Wiraswasta]);
        }else
        if ($profile_type == '3') {
            if ($is_payroll) {
                // echo 'adasd';die();
                // array_push($targetArr, $targetName[$profile_type]);
                $ktas = $ktas ->leftJoin('kta_targ3t_market', 'kta_targ3t_market.kta_id = kta.id');
                $ktas = $ktas->andWhere(['targ3t_market_id' => Targ3tMarket::Code_Nasabah_Payroll]);
            }else{
                $ktas = $ktas ->leftJoin('kta_targ3t_market', 'kta_targ3t_market.kta_id = kta.id');
                $ktas = $ktas->andWhere(['targ3t_market_id' => Targ3tMarket::Code_Nasabah_Non_Payroll]);

            }
            if ($is_perusahaan) {
                if(!empty($perusahaan_id)){
                    
                $ktas = $ktas->joinWith('perusahaanTiers')
                         ->leftJoin('perusahaan', 'perusahaan_tier.id = perusahaan.perusahaan_tier_id')
                         ->andWhere(['perusahaan.id' => $perusahaan_id]);
                }
            }


        }
        // echo json_encode($targetArr);die();
        $ktas = $ktas->all();
        $temp = [];
        $result['data'] = [];
        if(empty($ktas)){
            $result['message'] = "Data tidak ditemukan";
        }else{
            $result['message'] = "Berhasil";
        }
        foreach ($ktas as $kta) {
            $temp['id'] = $kta->id;
            $temp['name'] = $kta->nama;
            $temp['expired'] = $kta->expired;
            $temp['keterangan'] = $kta->keterangan;
            $temp['pop_up'] = $kta->pop_up;
            $temp['target_market_text'] = $kta->target_market_text;
            $temp['ketentuan_umum_text'] = $kta->ketentuan_umum_text;
            $temp['limit_kredit_text'] = $kta->limit_kredit_text;
            $temp['jangka_waktu_kredit_text'] = $kta->jangka_waktu_kredit_text;
            $temp['suku_bunga_text'] = $kta->suku_bunga_text;
            $temp['ketentuan_dbr_text'] = $kta->ketentuan_dbr_text;
            $temp['target_market'] = $kta->targ3tMarketsName;
            // $temp['keterangan'] = 'lorem ipsum sir amit lorom';
            $temp['perusahaan_tier'] = [];
            $temp_is_karyawan = false;
            $temp_is_profesional = false;
            foreach ($kta->targ3tMarketsName as $key) {
                if($key->id == Targ3tMarket::Code_Pegawai){
                    $temp_is_karyawan = true;
                }
                if($key->id == Targ3tMarket::Code_Wiraswasta){
                    $temp_is_profesional = true;
                }
            }
            foreach ($kta->perusahaanTiersName as $key) {
                $dev = [];
                $dev['id'] = $key->id;
                $dev['name'] = $key->nama; 
                array_push($temp['perusahaan_tier'], $dev);
            }
            $rules = $kta->ktaRules;
            $temp['rules']=[];
            $temp2 = [];
            foreach ($rules as $rule) {
                // $temp2['id'] = $rule->id;
                $limit = $this->messageBuildMinMax($rule->limit_min,$rule->limit_max,'limit');
                $income =  $this->messageBuildMinMax($rule->income_min,$rule->income_max,'income');
                $temp2['income'] = $income;
                $temp2['limit'] = $limit;
                $temp2['id'] = $rule->id;
                $temp2['min_limit'] = $rule->limit_min;
                $temp2['max_limit'] = $rule->limit_max;
                $temp2['min_income'] = $rule->income_min;
                $temp2['max_income'] = $rule->income_max;
                $temp2['min_tenor'] = $rule->min_tenor;
                $temp2['max_tenor'] = $rule->max_tenor;
                $temp2['suku_bunga_floating'] = $rule->suku_bunga_floating;
                $temp2['suku_bunga_flat'] = $rule->suku_bunga_flat;
                $temp2['max_dbr'] = $rule->max_dbr;
                array_push($temp['rules'], $temp2);
            }
            $temp['peruntukan_str'] = $this->messagePeruntukan($temp);
            $temp['min_tenor'] = $this->countMinTenor($temp);
            $temp['max_tenor'] = $this->countMaxTenor($temp);
            $temp['suku_bunga_flat'] = $this->countMinSukuBunga($temp);
            $temp['max_limit'] = $this->countMaxLimit($temp);
            // $temp['suku_bunga_floating'] = $this->countMinSukuBungaFloating($temp);
            $temp['min_dbr'] = $this->countMinDbr($temp);
            $temp['max_dbr'] = $this->countMaxDbr($temp);
            $temp['suku_bunga_str'] = "mulai dari ".$temp['suku_bunga_flat']."%";

            // if($is_karyawan==Kta::ALL){

            //     array_push($result['data'], $temp);
            // }else{
            //     if($is_karyawan){
            //         if($temp_is_karyawan){
            //             array_push($result['data'], $temp);
            //         }
            //     }else{
            //         if($temp_is_profesional){
            //             array_push($result['data'], $temp);
            //         }
            //     }

            // }
            array_push($result['data'], $temp);
        }

        return $result;
    }

    private function countMaxDbr($input){
        $result = 0;
        if(!empty($input['rules'])){
            $rules = $input['rules'];
            $min = -1;
            foreach ($rules as $rule) {
                $min = max($min,$rule['max_dbr']) ;
            }
            $result = $min;
        }
        return $result;

    }
    private function countMinDbr($input){
    	$result = 0;
    	if(!empty($input['rules'])){
    		$rules = $input['rules'];
    		$min = 2147483647;
    		foreach ($rules as $rule) {
    			$min = min($min,$rule['max_dbr']) ;
    		}
    		$result = $min;
    	}
    	return $result;

    }   

	private function countMinSukuBunga($input){
    	$result = 0;
    	if(!empty($input['rules'])){
    		$rules = $input['rules'];
    		$min = 2147483647;
    		foreach ($rules as $rule) {
    			$min = min($min,$rule['suku_bunga_flat']) ;
    		}
    		$result = $min;
    	}
    	return $result;

    }   

    private function countMinTenor($input){
    	$result = 0;
    	if(!empty($input['rules'])){
    		$rules = $input['rules'];
    		$min = 2147483647;
    		foreach ($rules as $rule) {
    			$min = min($min,$rule['min_tenor']) ;
    		}
    		$result = $min;
    	}
    	return $result;

    }

    private function countMaxTenor($input){
    	$result = 0;
    	if(!empty($input['rules'])){
    		$rules = $input['rules'];
    		$min = -1;
    		foreach ($rules as $rule) {
    			$min = max($min,$rule['max_tenor']) ;
    		}
    		$result = $min;
    	}
    	return $result;

    }

    private function countMaxLimit($input){
    	$result = 0;
    	if(!empty($input['rules'])){
    		$rules = $input['rules'];
    		$min = -1;
    		foreach ($rules as $rule) {
    			$min = max($min,$rule['max_limit']) ;
    		}
    		$result = $min;
    	}
    	return $result;

    }
    private function messagePeruntukan($input){
        $result = "";
        foreach ($input['perusahaan_tier'] as $key) {
            $result .= $key["name"].", ";
        }
        foreach ($input['target_market'] as $key) {
            $result .= $key["name"].", ";
        }
        return preg_replace('/(.*),/','$1',$result);
    }

    public function messageBuildMinMax($min,$max,$str){
        $result = "-";
        if($min<0 && $max <0){
            return $result;
        }else
        if($min<0){
            return $str." <= ".number_format($max);

        }else
        if($max<0){
            return number_format($min)."<=".$str;

        }else{
            return number_format($min)." < ".$str." <".number_format($max);
        }
    }


    public function actionCalculatorAngsuranByProgram()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $jsonRequest = json_decode(file_get_contents('php://input'));
        $result = ['code'=>200,'message'=>'Data berhasil ditemukan','data'=>null];

        $limit = $jsonRequest->limit;
        // $income = $jsonRequest->income;
        $tenor = $jsonRequest->tenor;
        $program_id = $jsonRequest->program_id;

        $temp = [];
        $kta = KtaRules::find()
            ->andWhere(['kta_id' => $program_id])
            ->andWhere("limit_min <= ".$limit ." or limit_min = -1")
            ->andWhere("limit_max >= ".$limit ." or limit_max = -1")
            ->andWhere("min_tenor <= ".($tenor/12) ." or min_tenor = -1")
            ->andWhere("max_tenor >= ".($tenor /12)." or max_tenor = -1")
            // ->andWhere("income_min < ".$income ." or income_max = -1")
            // ->andWhere("income_max > ".$income ." or income_max = -1")
            // ->asArray()
            ->one();
            // var_dump($kta);die();
        // var_dump($kta);die();
        if(empty($kta)){
            $result = ['code'=>400,'message'=>'program not available for submitted limit','data'=>null];
            return $result;
        }
        $result['data']['kalkulasi'] = $this->actionCalculateKta($limit,$tenor,$kta->suku_bunga_flat);
        $result['data']['suku_bunga'] = $kta->suku_bunga_flat."%";
        $result['data']['angsuran'] = count($result['data']['kalkulasi']) === 0 ? -1 : $result['data']['kalkulasi'][0]['angsuran'];

            // echo "ssds";die();
        return $result;
    }

    public function actionCalculatorAngsuranBySukubunga()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $jsonRequest = json_decode(file_get_contents('php://input'));
        $result = ['code'=>200,'message'=>'Data berhasil ditemukan','data'=>null];

        $limit = $jsonRequest->limit;
        $tenor = $jsonRequest->tenor;
        $sukuBunga = $jsonRequest->suku_bunga;

        $temp = [];

        $result['data']['kalkulasi'] = $this->actionCalculateKta($limit,$tenor,$sukuBunga);
        $result['data']['suku_bunga'] = $sukuBunga."%";
        $result['data']['angsuran'] = count($result['data']['kalkulasi']) === 0 ? -1 : $result['data']['kalkulasi'][0]['angsuran'];
        
        return $result;
    }

    public function actionCalculatorLimit()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $jsonRequest = json_decode(file_get_contents('php://input'));
        $result = ['code'=>200,'message'=>'Data berhasil ditemukan','data'=>[]];

        $income = $jsonRequest->income;
        // $limit = $jsonRequest->limit;
        $tenor = $jsonRequest->tenor;
        $angsuran_lain = $jsonRequest->angsuran_lain;
        $program_id = $jsonRequest->program_id;
        $suku_bunga_flat = $jsonRequest->suku_bunga_flat;
        // $perusahaan_id = $jsonRequest->perusahaan_id;

        // public function calculateLimitKpr($income=30000000, $bulan=120, $suku_bunga=9.75,$pekerjaan = 1, $angsuran_lain = 0){
        // $kta = Kta::find()->joinWith('perusahaanTiers')
        //                  ->leftJoin('perusahaan', 'perusahaan_tier.id = perusahaan.perusahaan_tier_id')
        //                  ->where(['perusahaan.id' => $perusahaan_id])->one();
        $ktarules = KtaRules::find()
                        // ->where(['kta_id' => $kta->id])
                        ->where(['kta_id' => $program_id])
                        // ->andWhere("limit_min <= ".$limit."  or limit_min = -1" )
                        // ->andWhere("limit_max >= ".$limit ." or limit_max = -1")
                        ->andWhere(['suku_bunga_flat'=>$suku_bunga_flat])
                        ->andWhere("min_tenor <= ".($tenor/12) ." or min_tenor = -1")
                        ->andWhere("max_tenor >= ".($tenor/12) ." or max_tenor = -1")
                        ->andWhere("income_min <= ".($income-$angsuran_lain) ." or income_min = -1")
                        ->andWhere("income_max >= ".($income-$angsuran_lain) ." or income_max = -1")
                        ->one() ;
        // var_dump($kta->id);
        // var_dump($ktarules);
        // die();
        if(empty($ktarules)){

            $result = ['code'=>400,'message'=>"your income not available for the program",'data'=>null];
        }else
        if($ktarules->max_tenor*12 < $tenor){
            $result = [
                'code'=>400,
                'message'=>"max tenor is ".($ktarules->max_tenor*12)." month(s)",
                'data'=>null
            ];
        }else
        if($ktarules->max_dbr/100 <= $angsuran_lain/$income){
            $result = [
                'code'=>400,
                'message'=>"max dbr is ".($ktarules->max_dbr)." %",
                'data'=>null
            ];
            
        }else{

            try{
                // echo " bunga".$ktarules->suku_bunga_flat;
                // echo " max dbr ".$ktarules->max_dbr;
                // die();
                $result_limit = $this->calculateLimit($income,$tenor,$suku_bunga_flat,$ktarules->max_dbr/100,$angsuran_lain,$ktarules->max_tenor*12);
                // $result_limit =$ktarules->limit_max;
                // $result['data']= $result_limit;
                // var_dump($ktarules->max_tenor*12);die();
                // var_dump($result_limit);die();
                // $result['data']['max_angsuran']= 0;
                $result['data']['max_angsuran']= $result_limit['max_angsuran'];
                $result['data']['max_limit']= min($ktarules->limit_max, $result_limit['max_limit']);
                $result['data']['dbr']= $ktarules->max_dbr;
                $result['data']['suku_bunga']= $ktarules->suku_bunga_flat;
                $result['data']['max_tenor'] = $ktarules->max_tenor*12;
                // if($result['data']['is_approved']){
                //     $result['message'] = "permohonan dapat disetujui";
                // }else{
                //     $result['message'] = "permohonan tidak dapat disetujui";

                // }
            }catch(\yii\base\Exception $e){
                $result = ['code'=>400,'message'=>$e->getMessage(),'data'=>null];
            }catch(\yii\base\ErrorException $e){
                $result = ['code'=>400,'message'=>$e->getMessage(),'data'=>null];
                
            }
        }
        return $result;
    }

    public function calculateLimitOld($income=30000000, $bulan=120, $suku_bunga=9.75,$dbr=0.6,$angsuran_lain = 1000000,$limit,$max_tenor){
        //pekerjaan
        // 1 = pegawai
        // 2 = wirausaha
        // =PV(D8/12,D6,-(D4-D12)*H6)
        
        $h6 = $dbr;
        $finance = new Financial();
        $result = [];
        $max_angsuran = $income*$dbr-$angsuran_lain;
        $result['max_angsuran'] = $max_angsuran; 
        $result['max_limit'] = round(abs($finance->PV(($suku_bunga/100)/12,$max_tenor,$max_angsuran)));
        $angsuran = round(abs($finance->PMT(($suku_bunga/100)/12,$bulan,$limit))); 
        $result['angsuran'] = $angsuran;
        $result['dbr'] = $dbr*100;  
        $result['suku_bunga'] = $suku_bunga;  
        $result['is_approved'] = 1;
        if($max_angsuran < $angsuran){
            $result['is_approved'] = 0;

        }
        return $result;
    }

        public function calculateLimit($income=30000000, $bulan=120, $suku_bunga=9.75,$dbr=0.6,$angsuran_lain = 1000000,$max_tenor){
        //pekerjaan
        // 1 = pegawai
        // 2 = wirausaha
        // =PV(D8/12,D6,-(D4-D12)*H6)

        if($income<=$angsuran_lain){
            throw new \yii\web\HttpException(400, 'angsuran lain melebihi income'); 
        }
        $h6 = $dbr;
        $finance = new Financial();
        $result = [];
        $max_angsuran = $income*$dbr-$angsuran_lain;
        $result['max_angsuran'] = $max_angsuran; 
        // var_dump($suku_bunga);
        // var_dump($bulan);
        // var_dump($max_angsuran);
        // die();
        $result['max_limit'] = round(abs($finance->PV(($suku_bunga/100)/12,$bulan,-$max_angsuran)));
        $result['dbr'] = $dbr*100;  
        $result['suku_bunga'] = $suku_bunga;  
        // $result['is_approved'] = 1;
        return $result;
    }


    public function actionCalculateKta($saldo=212000000, $tenor=10,$suku_bunga=15.25){
        $d_col = [0];
        $e_col = [0];
        $f_col = [0];
        $h_col = [$saldo];
        $finance = new Financial();
        $result = [];
        for ($i=1; $i <= $tenor; $i++) { //skip 0
            $f_col[$i] = $finance->PMT(($suku_bunga/100)/12,$tenor-$i+1,-1*$h_col[$i-1]);
            // $pokoks[$i] =10;
            $e_col[$i] = (($suku_bunga/100)/12)*$h_col[$i-1];
            $d_col[$i] = $f_col[$i]-$e_col[$i];
            $h_col[$i] = $h_col[$i-1]-$d_col[$i];
            // echo "c = ".$i." d = ".number_format($d_col[$i]);
            // echo " e = ".number_format($e_col[$i])." f = ". number_format($f_col[$i])." h = ".number_format($h_col[$i])."<br/>";
            array_push($result, [
                'bulan'=>$i,
                'pokok'=>round($d_col[$i]),
                'bunga'=>round($e_col[$i]),
                'angsuran'=>round($f_col[$i]),
                'saldo'=>round($h_col[$i])
            ]);
        }
        // var_dump($result);
        return $result;
    }
    public function actionDetailPerusahaan($perusahaan_id=null ){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = ['code'=>200,'message'=>'Data berhasil ditemukan','data'=>[]];
        try{
            $property = $this->getMaxPropertyKta($perusahaan_id);
            $peruModel = \app\models\Perusahaan::findOne($perusahaan_id);
            $res_peru = [];
            $res_peru['name'] = $peruModel->name;
            $res_peru['max_limit'] = $property['limit_max'];
            // $res_peru['program_marketing_str'] = $property['program_marketing_str'];
            $res_peru['program_marketing'] = $property['program_marketing'];
            $res_peru['max_tenor'] = $property['tenor_max'];
            $res_peru['max_dbr'] = $property['dbr_max'];
            $result['data'] = $res_peru;
        }catch(\yii\base\Exception $e){
            $result = ['code'=>400,'message'=>$e->getMessage(),'data'=>null];
        }catch(\yii\base\ErrorException $e){
            $result = ['code'=>400,'message'=>$e->getMessage(),'data'=>null];
            
        }
        return $result;
    }

   
    private function getMaxPropertyKta($perusahaan_id){
        $result = [
            'limit_max'=>null,
            'tenor_max'=>null,
            'dbr_max'=>null,
            'program_marketing'=>[],
            // 'harga_min'=>null,
            // 'harga_max'=>null,
        ];
        $kta = Kta::find()->joinWith('perusahaanTiers')
                         ->leftJoin('perusahaan', 'perusahaan_tier.id = perusahaan.perusahaan_tier_id')
                         ->where(['perusahaan.id' => $perusahaan_id])->all();
        
        if(empty($kta)){
            return $result;
        }
        $ktaids = [];
        $ktanames = [];
        $program_marketings = $this->retrieveProperty($kta);
        foreach ($kta as $item) {
            array_push($ktaids, $item->id);
            array_push($ktanames, $item->nama);
        //     $temp = [];
        //     $temp['id'] = $item->id;
        //     $temp['name'] = $item->nama;

            // array_push($program_marketings, $temp);
        }

        // echo $kta;
        // die();
        $result['limit_max'] = $this->getLimitMax($ktaids);
        $result['tenor_max'] = $this->getTenorMax($ktaids);
        $result['dbr_max'] = $this->getDbrMax($ktaids);
        $result['program_marketing_str'] = implode(',', $ktanames);
        $result['program_marketing'] = $program_marketings;
        return $result;
    }

    private function retrieveProperty($ktas){
        $result= [];
        foreach ($ktas as $kta) {
            $temp['id'] = $kta->id;
            $temp['name'] = $kta->nama;
            $temp['expired'] = $kta->expired;
            $temp['target_market'] = $kta->targ3tMarketsName;
            $temp['keterangan'] = 'lorem ipsum sir amit lorom';
            $temp['perusahaan_tier'] = [];
            $temp_is_karyawan = false;
            $temp_is_profesional = false;
            foreach ($kta->targ3tMarketsName as $key) {
                if($key->id == Targ3tMarket::Code_Pegawai){
                    $temp_is_karyawan = true;
                }
                if($key->id == Targ3tMarket::Code_Wiraswasta){
                    $temp_is_profesional = true;
                }
            }
            foreach ($kta->perusahaanTiersName as $key) {
                $dev = [];
                $dev['id'] = $key->id;
                $dev['name'] = $key->nama; 
                array_push($temp['perusahaan_tier'], $dev);
            }
            $rules = $kta->ktaRules;
            $temp['rules']=[];
            $temp2 = [];
            foreach ($rules as $rule) {
                // $temp2['id'] = $rule->id;
                $limit = $this->messageBuildMinMax($rule->limit_min,$rule->limit_max,'limit');
                $income =  $this->messageBuildMinMax($rule->income_min,$rule->income_max,'income');
                $temp2['income'] = $income;
                $temp2['limit'] = $limit;
                $temp2['min_limit'] = $rule->limit_min;
                $temp2['max_limit'] = $rule->limit_max;
                $temp2['max_limit'] = $rule->income_max;
                $temp2['max_limit'] = $rule->income_max;
                $temp2['min_tenor'] = $rule->min_tenor;
                $temp2['max_tenor'] = $rule->max_tenor;
                $temp2['suku_bunga_floating'] = $rule->suku_bunga_floating;
                $temp2['suku_bunga_flat'] = $rule->suku_bunga_flat;
                $temp2['max_dbr'] = $rule->max_dbr;
                array_push($temp['rules'], $temp2);
            }
            $temp['peruntukan_str'] = $this->messagePeruntukan($temp);
            $temp['min_tenor'] = $this->countMinTenor($temp);
            $temp['max_tenor'] = $this->countMaxTenor($temp);
            $temp['suku_bunga_flat'] = $this->countMinSukuBunga($temp);
            $temp['max_limit'] = $this->countMaxLimit($temp);
            // $temp['suku_bunga_floating'] = $this->countMinSukuBungaFloating($temp);
            $temp['min_dbr'] = $this->countMinDbr($temp);
            $temp['suku_bunga_str'] = "mulai dari ".$temp['suku_bunga_flat']."%";

            
            array_push($result, $temp);
            
        }
        return $result;
    }
    private function getLimitMax($kta_id){
        $limit_max = -1;
        $rules = KtaRules::find()->where(['kta_id' => $kta_id,'limit_max'=>-1])->one();
        if(empty($rules)){
            $limit_max = KtaRules::find()->where(['kta_id' => $kta_id])->max('limit_max');
        }
        return $limit_max;

    }

    private function getTenorMax($kta_id){
        $tenor_max = -1;
        $rules = KtaRules::find()->where(['kta_id' => $kta_id,'max_tenor'=>-1])->one();
        if(empty($rules)){
            $tenor_max = KtaRules::find()->where(['kta_id' => $kta_id])->max('max_tenor');
        }
        return $tenor_max;  

    }

    private function getDbrMax($kta_id){
        $dbr = -1;
        $rules = KtaRules::find()->where(['kta_id' => $kta_id,'max_dbr'=>-1])->one();
        if(empty($rules)){
            $dbr = KtaRules::find()->where(['kta_id' => $kta_id])->max('max_dbr');
        }
        return $dbr;

    }
}