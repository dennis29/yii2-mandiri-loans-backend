<?php

namespace app\controllers\api;

use Yii;
use app\models\Intro;
use app\models\IntroSearch;
use app\models\TargetMarket;
use app\models\SukuBungaRules;
use app\models\Area;
use app\models\Region;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\PasswordResetRequestForm;
use yii\data\ActiveDataProvider;

/**
 * IntroController implements the CRUD actions for Intro model.
 */
class KkbController extends BaseApiController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
           
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ]
        ];
    }

    

   public function actionSlides()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $result = ['code'=>200,'message'=>'data berhasil diload','data'=>null];
        $slidesKkbs = \app\models\SliderKkb::find()->all();

        $temp = ['link' =>'', 'image_name' => ''];
        $result['data'] = [];

        if(empty($slidesKkbs)){
            $result['message'] = "empty data found";
        }else{
            $result['message'] = "success get data";
        }

        foreach ($slidesKkbs as $slideKkb) {
            $temp['link'] = \yii\helpers\Url::to($slideKkb->image,true);
            $temp['image_name'] = $slideKkb->image_name;

            array_push($result['data'], $temp);
        }

        return $result;
    }

    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $result = ['code'=>200,'message'=>'data berhasil diload','data'=>null];
        // $kkbs = \app\models\Kkb::find()->all();
        $kkbs = \app\models\Kkb::find()
            ->andWhere(["!=","id","5bfca27a235b11e7ad35000d3aa12d79"])
            ->all();
        $kkb_links = \app\models\KkbLink::find()->all();

        $temp = ['text' => '','link' =>'', 'image_name' => ''];
        $result['data'] = [];

        if(empty($kkbs) && empty($kkb_links)){
            $result['message'] = "empty data found";
            return $result;
        }else{
            $result['message'] = "success get data";
            $result['data'] = [];
            $result['data']['kkbs'] = [];
            $result['data']['links'] = [];
        }
        foreach ($kkbs as $item) {
            # code...
            $reguler = [
                'id' => $item->id,
                'code' => $item->code,
                'title' => $item->title,
                'image' => \yii\helpers\Url::to($item->image,true),
                'content' => $item->text,
                'apply_link' => $item->link,
            ];
            $result['data']['kkbs'][] = $reguler;
        }
        // $default = \app\models\Kkb::findOne('5bfca27a235b11e7ad35000d3aa12d79');
        // $result['data']['apply_link'] =  $default->link_utama;
        foreach ($kkb_links as $item) {
            # code...
            $reguler = [
                'id' => $item->id,
                'title' => $item->title,
                'link' => $item->link,
            ];
            $result['data']['links'][] = $reguler;
        }
        return $result;
    }

    public function actionIndex2()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $result = ['code'=>200,'message'=>'data berhasil diload','data'=>null];
        // $kkbs = \app\models\Kkb::find()->all();
        $kkb_reguler = \app\models\Kkb::find()->where(['code'=>'reguler'])->one();
        $kkb_multiguna = \app\models\Kkb::find()->where(['code'=>'multiguna'])->one();

        $temp = ['text' => '','link' =>'', 'image_name' => ''];
        $result['data'] = [];

        if(empty($kkb_reguler)||empty($kkb_multiguna)){
            $result['message'] = "empty data found";
            return $result;
        }else{
            $result['message'] = "success get data";
        }
        $reguler = [
            'title' => $kkb_reguler->title,
            'image' => \yii\helpers\Url::to($kkb_reguler->image,true),
            'content' => $kkb_reguler->text,
            'apply_link' => $kkb_reguler->link,
        ];
        $multiguna = [
            'title' => $kkb_multiguna->title,
            'image' => \yii\helpers\Url::to($kkb_multiguna->image,true),
            'content' => $kkb_multiguna->text,
            'apply_link' => $kkb_multiguna->link,
        ];

        $result['data'] = [
            'apply_link' => $kkb_reguler->link_utama,
            'reguler' => $reguler,
            'multiguna' => $multiguna,
        ];
        
        return $result;
    }

    public function actionMtfContacts($area_id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $result = ['code'=>200,'message'=>'data berhasil diload','data'=>null];
        $users = \app\models\UserMtf::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $users,
        ]);
        if(!empty($area_id)){
            $users->andFilterWhere(['area_id'=>$area_id]);
        }
        $users->all();

        $temp = [];
        $result['data'] = [];

        if(empty($users)){
            $result['message'] = "empty data found";
        }else{
            $result['message'] = "success get data";
        }

        foreach ($dataProvider->getModels() as $user) {
            $temp['name'] = $user->nip;
            $temp['contact'] = $user->contact;
            $temp['slm'] = $user->slm;
            $temp['slm_contact'] = $user->slm_contact;
            $temp['area_id'] = $user->area_id;

            array_push($result['data'], $temp);
        }

        return $result;
    }

    public function actionMufContacts($area_id=null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $result = ['code'=>200,'message'=>'data berhasil diload','data'=>null];
        $users = \app\models\UserMuf::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $users,
        ]);
        if(!empty($area_id)){
            $users->andFilterWhere(['area_id'=>$area_id]);
        }
        $users->all();

        $temp = [];
        $result['data'] = [];

        if(empty($users)){
            $result['message'] = "empty data found";
        }else{
            $result['message'] = "success get data";
        }

        foreach ($dataProvider->getModels() as $user) {
            $temp['name'] = $user->nip;
            $temp['contact'] = $user->contact;
            $temp['slm'] = $user->slm;
            $temp['slm_contact'] = $user->slm_contact;
            $temp['area_id'] = $user->area_id;

            array_push($result['data'], $temp);
        }

        return $result;
    }


}