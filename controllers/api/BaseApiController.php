<?php

namespace app\controllers\api;

use Yii;
use app\controllers\Financial;
use app\models\Intro;
use app\models\IntroSearch;
use app\models\Kpr;
use app\models\TargetMarket;
use app\models\SukuBunga;
use app\models\SukuBungaRules;
use app\models\Area;
use app\models\Region;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\PasswordResetRequestForm;
use app\models\RateAsuransi;
use yii\base\ErrorException;

/**
* IntroController implements the CRUD actions for Intro model.
*/
class BaseApiController extends Controller
{
/**
 * @inheritdoc
 */
    public function behaviors()
    {
        return [
           
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ]
        ];
    }

    public function beforeAction($action)
    {
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $this->enableCsrfValidation = false;

        // $user_id = Yii::$app->request->get('user_id');
        // $token = Yii::$app->request->get('token');
        // echo empty($token);die();
        $request= Yii::$app->request;
        $cookies = $_COOKIE;
        $user_id = $cookies["mandiri-user"];
        $token = $cookies["mandiri-token"];

        $result = ['code'=>403,'message'=>$user_id,'data'=>null];
        // echo  'keluar'; die();
        if(empty($user_id)){
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $result = ['code'=>403,'message'=>'UserID tidak boleh kosong sdjfgkssdf','data'=>$_COOKIE];
            // $result = ['code'=>403,'message'=>json_encode($token),'data'=>json_encode($cookies)];
            echo json_encode($result);
            return false;
            // echo  'masuk';die();
        }
        if(empty($token)){
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $result = ['code'=>403,'message'=>'Token tidak boleh kosong','data'=>null];
            echo json_encode($result);
            return false;
        }
        $tokenDB = User::find()->select('token')->where(['id'=> $user_id])->one()->token;
        if($tokenDB != $token){
            // var_dump($tokenDB);die();
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            // $result = ['code'=>403,'message'=>'Token tidak valid','data'=>null];
            $result = ['code'=>403,'message'=>'Token db = '.$tokenDB." token yang dikirim = ".$token,'data'=>null];
            echo json_encode($result);
            return false;
        }
        // try{
            return parent::beforeAction($action);
        // }catch(yii\web\NotFoundHttpException $e){
        //     $result = ['code'=>404,'message'=>'tidak ditemukan','data'=>null];
        //     return false;
        // }
    }
}