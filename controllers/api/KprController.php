<?php

namespace app\controllers\api;

use Yii;
use app\controllers\Financial;
use app\models\Intro;
use app\models\IntroSearch;
use app\models\Kpr;
use app\models\TargetMarket;
use app\models\SukuBunga;
use app\models\SukuBungaRules;
use app\models\Area;
use app\models\Region;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\PasswordResetRequestForm;
use app\models\RateAsuransi;
use yii\base\ErrorException;

/**
* IntroController implements the CRUD actions for Intro model.
*/
class KprController extends BaseApiController
{
/**
 * @inheritdoc
 */
public function behaviors()
{
    return [
       
        'corsFilter' => [
            'class' => \yii\filters\Cors::className(),
        ]
    ];
}



public function actionProfesi()
{
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

    $result = ['code'=>200,'message'=>'Data berhasil ditemukan','data'=>null];
    $profesis = \app\models\ProfesiKpr::find()->all();

    $temp = ['name' => '','id' =>''];
    $result['data'] = [];

    if(empty($profesis)){
        $result['message'] = "Data tidak ditemukan";
    }else{
        $result['message'] = "Berhasil";
    }

    foreach ($profesis as $profesi) {
        $temp['name'] = $profesi->nama;
        $temp['id'] = $profesi->id;

        array_push($result['data'], $temp);
    }

    return $result;
}

public function actionSearch($type = "", $profile_type = "", $perusahaan_id = "", $profesi_id = "")
{
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

    $request = Yii::$app->request;
    $result = ['code'=>200,'message'=>'Data berhasil ditemukan','data'=>null];

    $kprs = \app\models\Kpr::find()->with(['sukuBungas','developerTiers']);
    $temp = [];
    $kpr_temp = [];
    $result['data'] = [];

    $targetName = [
        '1' => 'Nasabah Prioritas',
        '2' => 'Nasabah Payroll'
    ];

    $targetId = [
        '1' => '81cc8131fd8a11e6acda94dbc9b18a6e',
        '2' => '905ca29ef4f711e69e6094dbc9b18a6e'
    ];

    if ($profile_type === '1' || $profile_type === '2') {
        // $kprs = $kprs->joinWith('targ3tMarkets')
                     // ->where(['targ3t_market.name' => $targetName[$profile_type]]);

        $kprs = $kprs->joinWith('targ3tMarkets')
     ->where(['targ3t_market.id' => $targetId[$profile_type]]);

    } else if ($profile_type === '3') {
        // $kprs = $kprs
        //             ->joinWith('developerTiers')
        //             ->leftJoin('developer', 'developer_tier.id = developer.developer_tier_id')
                    // ->where(['developer.id' => $developer_id]);
        $kprs = $kprs
                    ->joinWith('perusahaanKprTiers')
                    ->leftJoin('perusahaan_kpr', 'perusahaan_kpr_tier.id = perusahaan_kpr.perusahaan_kpr_tier_id')
                    ->where(['perusahaan_kpr.id' => $perusahaan_id]);

    } else if ($profile_type === '4') {
        $kprs = $kprs->joinWith('profesiKprs')
                     ->where(['profesi_kpr.id' => $profesi_id]);
    }

    $is_multiguna = false;
    $kpr_temp = $kprs->orderBy('order_number')->all();
    if(empty($kpr_temp)){
        $result['message'] = "Data tidak ditemukan";
    }else{
        $result['message'] = "Berhasil";

        foreach ($kpr_temp as $kpr) {
            $temp['id'] = $kpr->id;
            // $temp['is_multiguna'] = $kpr->is_multiguna === 0 ? false : true;
            $temp['name'] = $kpr->nama;
            $temp['summary_multiguna_text'] = $kpr->profil_nasabah_text;
            $temp['peruntukan_text'] = $kpr->peruntukan_text;
            $temp['market_text'] = $kpr->market_text;
            $temp['benefit_text'] = $kpr->benefit_text;
            $temp['expired'] = $kpr->expired;
            $temp['peserta_text'] = $kpr->peserta_text;
            $temp['suku_bunga_multiguna_text'] = $kpr->suku_bunga_multiguna_text;
            $temp['suku_bunga_kpr_text'] = $kpr->suku_bunga_kpr_text;
            $temp['summary_kpr_text'] = $kpr->keterangan;
            $temp['sukuBunga'] = [];
            $arrRule = [];

            // $temp['target_market'] = $kpr->targ3tMarketsName;

            // $temp['developer_tier'] = [];
            // foreach ($kpr->developerTiersName as $key) {
            //     $dev = [];
            //     $dev['id'] = $key->id;
            //     $dev['name'] = $key->nama; 
            //     array_push($temp['developer_tier'], $dev);
            // }
            // $temp['profesi'] = [];
            // foreach ($kpr->profesiKprsName as $key) {
            //     $dev = [];
            //     $dev['id'] = $key->id;
            //     $dev['name'] = $key->nama; 
            //     array_push($temp['profesi'], $dev);
            // }
            $countMultiRulesPm = 0;
            $countKprRulesPm = 0;
            foreach ($kpr->sukuBungas as $sb) {
                $temp2 =[] ;
                $temp2['id'] = $sb->id;
                $temp2['name'] = $sb->nama;
                $temp2['is_reguler'] = $sb->is_reguler;
                $temp2['rules'] = [];
                $countMultiRules = 0;
                $countKprRules = 0;
                foreach ($sb->sukuBungaRules as $rule) {
                    # code...
                    $temp3['id'] = $rule->id; 
                    $temp3['is_multiguna'] = $rule->is_multiguna; 
                    if($rule->is_multiguna==0){
                        $countKprRules++;
                    }else{
                        $countMultiRules++;
                    }
                    $temp3['suku_bunga_fixed'] = $rule->suku_bunga_fixed; 
                    $temp3['tahun_fixed'] = $rule->tahun_fixed; 
                    $temp3['min_tenor'] = $rule->min_tenor; 
                    array_push($temp2['rules'], $temp3);
                    array_push($arrRule, $temp3);
                }
                $temp2['is_kpr'] = 0;
                if($countKprRules>0){
                    $temp2['is_kpr'] = 1;
                    $countKprRulesPm++;
                }
                $temp2['is_multiguna'] = 0;
                if($countMultiRules>0){
                    $temp2['is_multiguna'] = 1;
                    $countMultiRulesPm++;
                }
                // $temp2['multiguna_str'] = $this->messageSukuBunga($temp2,1);
                // $temp2['kpr_str'] = $this->messageSukuBunga($temp2,0);
                array_push($temp['sukuBunga'], $temp2);
                # code...
            }
            // $rules = $kpr->kprRules;
            if(count($arrRule)>0){

                usort($arrRule, function ($a, $b) { return strnatcmp($a['suku_bunga_fixed'], $b['suku_bunga_fixed']); });

                $temp['suku_bunga_fixed'] = $arrRule[0]['suku_bunga_fixed'];
                usort($arrRule, function ($a, $b) { return strnatcmp($a['tahun_fixed'], $b['tahun_fixed']); });
                $temp['tahun_fixed'] = $arrRule[0]['tahun_fixed'];
                $temp['min_tenor'] = $arrRule[0]['min_tenor'];
                $temp['min_tenor_str'] = $arrRule[0]['min_tenor']." tahun";
            }

            //jangan di push kalau tipenya ga sesuai
            // echo $type;die();
            $temp['is_kpr'] = 0;
            if($countKprRulesPm>0){
                $temp['is_kpr'] = 1;
            }
            $temp['is_multiguna'] = 0;
            if($countMultiRulesPm>0){
                $temp['is_multiguna'] = 1;
            }
            // $temp['peruntukan_str'] = $this->messagePeruntukan($temp);
            // $temp['expired'] = $kpr->expired;
            if($type == SukuBungaRules::TYPE_MULTIGUNA){
                $temp['suku_bunga_text'] = $kpr->suku_bunga_multiguna_text;
            }else{
                $temp['suku_bunga_text'] = $kpr->suku_bunga_kpr_text;

            }
            if($type!=""){
                // $kprs = $kprs->andWhere(['is_multiguna'=> $type]);
                if($type == SukuBungaRules::TYPE_KPR){
                    if($countKprRulesPm>0){
                        array_push($result['data'], $temp);
                    }
                }else{//tipemulti
                    if($countMultiRulesPm>0){
                        array_push($result['data'], $temp);
                    }
                }
            }else{
                array_push($result['data'], $temp);
            }
        }
    }
    return $result;
}

private function messageSukuBunga($input,$is_multiguna){
    $result = "";
    $rules = SukuBungaRules::find()
                    ->where(['suku_bunga_id'=>$input["id"],
                        'is_multiguna'=>$is_multiguna
                        ])
                    ->all();
    // echo json_encode($rules);
    // die();
    if(!empty($rules)){
        // if($input["is_reguler"]==0){
            $counter= 0;
            foreach ($rules as $key) {
                if($counter==0){
                    $result .= $key["suku_bunga_fixed"]. "% untuk ".$key["tahun_fixed"]." tahun pertama<br/>";
                }else{
                    $result .= $key["suku_bunga_fixed"]. "% untuk ".$key["tahun_fixed"]." tahun berikutnya<br/>";

                }
                $counter++;
            }
        // }else{
        //     foreach ($rules as $key) {
        //         $result .= $key["suku_bunga_fixed"]. "% ".$key["tahun_fixed"]." tahun untuk tenor minimal ".$key["min_tenor"];
        //     }
        // }
    }
    $result = preg_replace('/(.*)\<br\/\>/','$1',$result);
    return $result;
    // return "halo";
}

private function messageSukuBungaText($result,$sukuBunga){
    $result2 = "";
    if(empty($sukuBunga[0])){
        return "-";
    }else{
        $result2 = $sukuBunga[0]['suku_bunga']."% untuk ".$sukuBunga[0]['tenor']." tahun pertama";
        if(!empty($sukuBunga[1])){
            $result2 .= '<br/>'.$sukuBunga[1]['suku_bunga']."% untuk ".$sukuBunga[1]['tenor']." tahun berikutnya";
        }
    }
    return $result2;
}

private function messagePeruntukan($input){
    $result = "";
    foreach ($input['developer_tier'] as $key) {
        $result .= $key["name"].", ";
    }
    foreach ($input['target_market'] as $key) {
        $result .= $key["name"].", ";
    }
    return preg_replace('/(.*),/','$1',$result);
}
public function actionCalculatorAngsuranBySukubunga()
{
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

    $jsonRequest = json_decode(file_get_contents('php://input'));
    $result = ['code'=>200,'message'=>'Data berhasil ditemukan','data'=>null];
    try{

        // $hitung_type = $jsonRequest->hitung_type;
        $limit = $jsonRequest->limit;
        $tenor = $jsonRequest->tenor;
        $sukuBunga = json_decode(json_encode($jsonRequest->suku_bunga), true);

        $result['data'] = [];
        // usort($sukuBunga, function ($a, $b) { return strnatcmp($a['suku_bunga'], $b['suku_bunga']); });
        $result['data']['suku_bunga'] = count($sukuBunga) === 0 ? -1 : $sukuBunga[0]['suku_bunga'];

        $result['data']['kalkulasi'] = $this->calculateKpr($limit, $tenor, $sukuBunga);
    }catch(\yii\base\Exception $e){
        $result = ['code'=>400,'message'=>$e->getMessage(),'data'=>null];
        return $result;    
    }
    $result['data']['angsuran'] = count($result['data']['kalkulasi']) === 0 ? -1 : $result['data']['kalkulasi'][0]['angsuran'];
    $result['data']['suku_bunga'] = $this->messageSukuBungaText($result,$sukuBunga);
    $result['data']['angsuran_text'] = $this->getMessageAngsuranText($result,$sukuBunga);
    

    return $result;
}

private function getMessageAngsuranText($result,$sukuBunga){
    if(count($result['data']['kalkulasi']) === 0){
        return "-";
    }else{
        $result2 = 'Rp. '.number_format(round($result['data']['kalkulasi'][0]['angsuran']),0,",",".")." untuk ".$sukuBunga[0]['tenor']." tahun pertama";
        if(!empty($sukuBunga[1])){
            $counter = count($result['data']['kalkulasi']);
            $result2 .= '<br/>Rp. '.number_format(round($result['data']['kalkulasi'][$counter-1]['angsuran']),0,",",".")." untuk ".$sukuBunga[1]['tenor']." tahun berikutnya";
        }
    }
    return $result2;
}
public function actionCalculatorLimit()
{
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

    $jsonRequest = json_decode(file_get_contents('php://input'));
    $result = ['code'=>200,'message'=>'Data berhasil ditemukan','data'=>null];

    $income = $jsonRequest->income;
    $suku_bunga = $jsonRequest->suku_bunga;
    $tenor = $jsonRequest->tenor;
    $profesi = $jsonRequest->profesi;
    $angsuran_lain = $jsonRequest->angsuran_lain;

    // public function calculateLimitKpr($income=30000000, $bulan=120, $suku_bunga=9.75,$pekerjaan = 1, $angsuran_lain = 0){
    try{
        $result['data'] = $this->calculateLimit($income,$tenor,$suku_bunga,$profesi,$angsuran_lain);
    }catch(\yii\base\Exception $e){
        $result = ['code'=>400,'message'=>$e->getMessage(),'data'=>[]];
        
    }
    return $result;
}


public function actionCalculatorAngsuranByProgram() 
{
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

    $jsonRequest = json_decode(file_get_contents('php://input'));
    $request = Yii::$app->request;
    $result = ['code'=>200,'message'=>'Data berhasil ditemukan','data'=>null];
    // try{

        // $hitung_type = $jsonRequest->hitung_type;
        $limit = $jsonRequest->limit;
        $tenor = $jsonRequest->tenor;
        // $program_id = $jsonRequest->program_id;
        $suku_bunga_id = $jsonRequest->suku_bunga_id;
        $is_multiguna = $jsonRequest->is_multiguna;
        $suku_bunga = SukuBunga::findOne($suku_bunga_id);
        // $program = \app\models\Kpr::find()->where(['id' => $program_id])->one();
        $result['data'] = [];

        $sukuBunga = [];
        // if(empty($program)){
        //     $result['message'] = "program not found";

        // }else{

            // if($suku_bunga->is_reguler){
            if(false){
                if($is_multiguna){
                    $rules = $suku_bunga->sukuBungaRulesMultiguna;

                }else{
                    $rules = $suku_bunga->sukuBungaRulesKpr;
                    
                }
                // $rules = $program->sukuBungaRules;
                usort($rules, function ($a, $b) {return $a->order_number < $b->order_number; } );
                // $chosen = $rules[count($rules)-1];
                // foreach ($rules as $rule) {
                //     if($tenor > $rule->min_tenor){
                //        $chosen = $rule; 
                //         break;
                //     }
                // }
                $tmp = [];
                $tmp['suku_bunga'] = $chosen->suku_bunga_fixed;
                // $tmp['tenor'] = $tenor/12;
                $tmp['tenor'] = $chosen->min_tenor;

                array_push($sukuBunga, $tmp);
            }else{
                if($is_multiguna){
                    $rules = $suku_bunga->sukuBungaRulesMultiguna;

                }else{
                    $rules = $suku_bunga->sukuBungaRulesKpr;
                    
                }
                foreach ($rules as $rule) {
                    $tmp = [];
                    $tmp['suku_bunga'] = $rule->suku_bunga_fixed;
                    $tmp['tenor'] = $rule->tahun_fixed;

                    array_push($sukuBunga, $tmp);
                }
            }
            // var_dump($suku_bunga->sukuBungaRulesMultiguna);die();
            usort($sukuBunga, function ($a, $b) { return strnatcmp($a['suku_bunga'], $b['suku_bunga']); });
            $result['data']['suku_bunga'] = count($sukuBunga) === 0 ? -1 : $sukuBunga[0]['suku_bunga'];


            try{
                $result['data']['kalkulasi'] = $this->calculateKpr($limit, $tenor, $sukuBunga);
                // $result['data']['kalkulasi'] = [$limit, $tenor, $sukuBunga];
            }catch(\yii\base\Exception $e){
                $result = ['code'=>400,'message'=>$e->getMessage(),'data'=>null];
                return $result;
            }
            $result['data']['angsuran'] = count($result['data']['kalkulasi']) === 0 ? -1 : $result['data']['kalkulasi'][0]['angsuran'];
            $result['data']['suku_bunga'] =  $this->messageSukuBungaText($result,$sukuBunga);
            $result['data']['angsuran_text'] = $this->getMessageAngsuranText($result,$sukuBunga);

        // }
    // } catch(ErrorException $e){
    //     $result['code'] = 500;
    //     $result['message'] = $e->getMessage();
    //     $result['data'] = null;
    // }
    return $result;
}

private function calculateKpr($saldo, $tenor, $arr=null){//tenor dalam bulan tapi tenor yang ada di arr dalam tahun wkwwk
    if(empty($arr)){
        $arr = [['suku_bunga'=>7.75,'tenor'=>2],['suku_bunga'=>9.25,'tenor'=>2]];
    }
    $tenorAwal = $tenor;
    $totalTenor = 0;
    //kolom c tenor
    //kolom d suku bunga
    $c_col = [0];
    $d_col = [0];
    foreach ($arr as $sb) {
        $totalTenor += $sb['tenor'];
        for ($i=0; $i < $sb['tenor']*12 ; $i++) {
            array_push($c_col, $tenor--);
            array_push($d_col, $sb['suku_bunga']/100);
        }
    }
    if($totalTenor*12 > $tenorAwal){
        throw new \yii\web\HttpException(400, 'Tenor harus lebih panjang dari masa fixed rate'); 
    }
    //kolom_n
    $pokoks = [0];
    //kolom_o
    $bungas = [0];
    //kolom_p
    $angsurans = [0];
    //kolom_q
    $saldos = [$saldo];

    $result = [];
    $finance = new Financial();
    for ($i=1; $i < count($c_col); $i++) { //skip 0
        $pokoks[$i] = $finance->PPMT($d_col[$i]/12,1,$c_col[$i],-1*$saldos[$i-1]);
        // $pokoks[$i] =10;
        $bungas[$i] = $saldos[$i-1]/12*$d_col[$i];
        $angsurans[$i] = $pokoks[$i]+$bungas[$i];
        $saldos[$i] = $saldos[$i-1] - $pokoks[$i];

        // DEBUG
        // echo "c = ".$c_col[$i]." d = ".$d_col[$i];
        // echo " n = ".number_format($pokoks[$i])." o = ". number_format($bungas[$i])." p = ".number_format($angsurans[$i])." q= ".number_format($saldos[$i])."<br/>";

        array_push($result, [
            'bulan'=>$i,
            'pokok'=>round($pokoks[$i]),
            'bunga'=>round($bungas[$i]),
            'angsuran'=>round($angsurans[$i]),
            'saldo'=>round($saldos[$i])
        ]);
    }
    // var_dump($result);
    return $result;
}


public function calculateLimit($income=30000000, $bulan=120, $suku_bunga=9.75,$pekerjaan = 1, $angsuran_lain = 0){
    //pekerjaan
    // 1 = pegawai
    // 2 = wirausaha
    // =PV(D8/12,D6,-(D4-D12)*H6)
    $h4 = 0;
    if($pekerjaan==1){
        if($income>10000000){
            $h4 = 65/100;
        }else if($income > 5000000){
            $h4 = 55/100;

        }else{
            $h4 = 50/100;
        }
    }else{
        if($income>30000000){
            $h4 = 60/100;
        }else{
            $h4 = 50/100;
        }
    }
    $h5 = $angsuran_lain/$income;
    $h6 = $h4-$h5;
    $finance = new Financial();
    $result = $finance->PV(($suku_bunga/100)/12,$bulan,-1*(($income*$h4)-$angsuran_lain));
    return $result;
}

public function actionCalculatorTakeover()
{
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    
    $jsonRequest = json_decode(file_get_contents('php://input'), true);
    $result = ['code'=>200,'message'=>'Data berhasil ditemukan','data'=>null];

    // $existing = $jsonRequest['existing'];
    // $mandiri = $jsonRequest['mandiri'];
    // $usia = $jsonRequest['usia'];
    try{

        // $result['data'] = $this->calculateTakeover();
        // return $jsonRequest['mandiri']['suku_bunga'][1];die();
        $pekerjaan = $jsonRequest['pekerjaan'];
        $usia = $jsonRequest['usia'];
        $tenor_tahun = $jsonRequest['mandiri']['suku_bunga'][0]['tenor']/12;
        if(!empty($jsonRequest['mandiri']['suku_bunga'][1])){
            $tenor_tahun += $jsonRequest['mandiri']['suku_bunga'][1]['tenor']/12;
        }
        $total = $jsonRequest['mandiri']['tenor']/12 +$usia;
        if($pekerjaan == 1){//pegawai max usia 55
            $max = 55;
            if($total>$max){

                $result = ['code'=>400,'message'=>'Total tenor ditambah usia melebihi '.$max.' tahun','data'=>null];
                return $result;
            }
        }else{//pengusaha max usia 60
            $max = 60;
            if($total>$max){
                $result = ['code'=>400,'message'=>'Total tenor ditambah usia melebihi '.$max.' tahun','data'=>null];
                return $result;
            }
        }
        if(!array_key_exists('is_cover_asuransi', $jsonRequest)){
            $jsonRequest['is_cover_asuransi'] =1;
        }
        $pekerjaan = $jsonRequest['pekerjaan'];
        $result['data'] = $this->calculateTakeover($jsonRequest);
    }catch(\yii\base\Exception $e){
        $result = ['code'=>400,'message'=>$e->getMessage(),'data'=>null];
        
    }catch(\yii\base\ErrorException $e){
        $result = ['code'=>400,'message'=>'Error','data'=>null];

    }
    
    return $result;
}

private function calculateTakeover($input = null){
    //usia
    // 1 30 - 34
    // 2 35 - 39
    // 3 40 - 44
    // 4 45 - 49 tahun
    // 5 50 - 54 tahun
    // 6 55 - 59 tahun
    //         {
    //    "existing":
    //    {
    //       "outstanding": 1000000000,
    //       "tenor": 120,
    //       "suku_bunga": 13.99
    //    },
    //    "mandiri":
    //    {
    //       "limit": 1000000000,
    //       "tenor": 108,
    //       "suku_bunga":
    //         [
    //          {
    //             "tenor":12,
    //             "persen":6
                
    //          },
    //          {
    //             "persen":6.75,
    //             "tenor":24
    //          }
    //       ]
    //    },
    //    "usia": 30,
    //    "type":1,
    //    "pekerjaan":1
    // }
        $i30 = $input['type'];
        $i6 = $input['mandiri']['limit'];
        $k27 = $input['usia'];
        $i8 = $input['mandiri']['tenor'];
        $p8 = $i8/12;
        $b6 = $input['existing']['outstanding'];
        $b8 = $input['existing']['tenor'];
        $b10 = $input['existing']['suku_bunga'];
        $i10 = $input['mandiri']['suku_bunga'][0]['persen'];

        $b12 = $this->getAngsuranBankLain($b10,$b8,$b6); //bener
        $b14 = $this->getAngsuranTotalAngsuranBankLain($b12,$b8);//bener
        $i18_i16 = $this->getAngsuranFixed($input);//bener
        $i20 = $this->getAngsuranFloat($input,$i18_i16); //bener 14731273 14,731,273 
        $i22 = $this->getTotalAngsuran($i18_i16,$i20,$input); //bener 1500932015  1,500,932,016 

        $k29 = $this->getBiayaProvisi($i30,$i6);//ini hati2 //bener
        $k30 = $this->getBiayaAdmin($i30); //bener
        $k31 = 0;//bener
        if($input['is_cover_asuransi']==1){
            $k31 = $this->getPremiAsuransi($k27,$p8,$i6);//bener
        }
        $k32 = $this->getAsuransiKerugian($i6);//bener
        $k33 = $this->getBiayaPerjanjianKredit($i6);//bener
        $k34 = $this->getBiayaSertifikat(); //bener
        $k35 = $this->getBiayaPengikatan($i6);//bener
        $k36 = array_sum([$k29,$k30,$k31,$k32,$k33,$k34,$k35]);
        $k38 = $k36 + $i22; 
        $k39 = $b14;
        $k40 = $k39 - $k38;


        $result['estimasi_keuntungan'] = round($k40);
        $result['total_angsuran_bank_lain'] = round($k39);
        $result['total_angsuran_mandiri'] = $k38;
        $result['total_biaya_take_over'] = $k36;
        $result['biaya_pengikatan_hak_tanggungan'] = $k35;
        $result['biaya_sertifikat'] = $k34;
        $result['biaya_perjanjian_kredit'] = $k33;
        $result['biaya_asuransi_kerugian'] = $k32;
        $result['premi_asuransi'] = $k31;
        $result['biaya_admin'] = $k30;
        $result['biaya_provisi'] = $k29;
        $result['total_angsuran'] = $i22;
        $result['angsuran_fixed'] = $i18_i16;
        $result['angsuran_float'] = round($i20);
        $result['angsuran_bank_lain'] = round($b12);
        $result['total_angsuran'] = $i22;

        return $result;


}

private function getBiayaPerjanjianKredit($i6){
    return 0.001*$i6;
}
private function getBiayaSertifikat(){
    return 250000;
}
private function getBiayaPengikatan($i6){
    $q26 = 0.002*$i6;
    $q27 = 850000;  
    if($q26<850000){
        return $q27+2500000;
    }else{
        return $q26+2500000;
    }
}
private function getAsuransiKerugian($i6){
    return 0.005*$i6;
}
private function getPremiAsuransi($k27,$p8,$i6){
    $p8 = intval($p8);
    $rate_asuransi = RateAsuransi::find()->select('tenor_'.$p8)->where(['umur'=>$k27])->asArray()->one();
    // var_dump($k27);
    // var_dump($p8);
    // var_dump($rate_asuransi['tenor_'.$p8]);die();
    return $rate_asuransi['tenor_'.$p8]*$i6/1000;

}
private function getBiayaAdmin($i30 ){
    if($i30==Kpr::Type_KPR_Takeover){
        return 250000;
    }else if($i30==Kpr::Type_KPR_Top_Up){
        return 500000;
    }else{
        return 750000;
    }
}
private function getBiayaProvisi($i30,$i6){
    if($i30==Kpr::Type_KPR_Takeover){
        //0.5
        return $i6*0.5/100;
    }else if($i30==Kpr::Type_Multiguna_Takeover){
        //1%
        return $i6*1/100;
    }else{
        return 0;
    }
}
private function getTotalAngsuran($i18_i16,$i20,$input){
    $result = 0;
    $i8 = $input['mandiri']['tenor'];
    $n10 = $input['mandiri']['suku_bunga'][0]['tenor'];
    $result+= $i18_i16[0]*$n10;
    $n12 = 0;

    if(!empty($input['mandiri']['suku_bunga'][1])){
        $n12 = $input['mandiri']['suku_bunga'][1]['tenor'];
        $result+= $i18_i16[1]*$n12;
    }
    $n14 = $i8 - $n10 - $n12;
    $result+= $i20*$n14;
    return round($result);
}

private function getAngsuranFloat($input,$i18_i16){
    $i14 = 13.25/100;
    $i6 = $input['mandiri']['limit'];

    $i8 = $input['mandiri']['tenor'];
    $n10 = $input['mandiri']['suku_bunga'][0]['tenor'];
    $n12 = 0;
    $i18 = 0;
    $i16 = $i18_i16[0];
    if(!empty($input['mandiri']['suku_bunga'][1])){
        $i18 = $i18_i16[1];
        $n12 = $input['mandiri']['suku_bunga'][1]['tenor'];
        $i12 = $input['mandiri']['suku_bunga'][1]['persen'];
    }
    $i10 = $input['mandiri']['suku_bunga'][0]['persen'];
    $finance = new Financial();

    $k19 = ($i16>0)?-$this->CUMIPMT($i10/12/100,$i8,$i6,1,$n10,0):0;
    //k19 57649134.029904  57,649,134 

    $b29 = -($i16*$n10-$i6-$k19);
    
    if(!empty($input['mandiri']['suku_bunga'][1])){
        $k21 = ($i16>$n12)?-$this->CUMIPMT($i12/12/100,$i8-$n10,$b29,1,$n12,0):0;
    }else{
        $k21= 0;
    }
    //b29 913580134.0299  913,580,138.46 
    //k21 111675035.67647 111,675,036 
    $b30 = -($i18*$n12-$k21-$b29);
    //b30 729043833.70637  729,043,835.51 
    $result = -$finance->PMT($i14/12,$i8-$n10-$n12,$b30);
    // echo $result;
    //result 14731273 14,731,273 
    // die();
    return $result;
}

private function getAngsuranBankLain($b10,$b8,$b6){
    // echo $b10." ";
    // echo $b8." ";
    // echo $b6;
    // die();
    $finance = new Financial();
    return $finance->PMT($b10/100/12,$b8,-$b6);
}

private function getAngsuranTotalAngsuranBankLain($b12,$b8){
    return $b12*$b8;
}

private function getAngsuranFixed($input){
    $finance = new Financial();
    $result = [];
    $suku_bunga_arr = $input['mandiri']['suku_bunga'];
    $i6 = $input['mandiri']['limit'];
    $i8 = $input['mandiri']['tenor'];
    $n10 = $suku_bunga_arr[0]['tenor'];
    $i10 = $suku_bunga_arr[0]['persen'];
    if(!empty($suku_bunga_arr[1])){
        $n12 = $suku_bunga_arr[1]['tenor'];
        $i12 = $suku_bunga_arr[1]['persen'];
        $k19 = -$this->CUMIPMT($i10/100/12,$i8,$i6,1,$n10,0);
    }
    // $tenor = $suku_bunga_arr[0]['tenor'];
    $i16 = $finance->PMT($i10/12/100,$i8,-$i6);
    if(!empty($suku_bunga_arr[1])){
        $b29 =-($i16*$n10-$i6-$k19);
        $i18 = $finance->PMT($i12/12/100,$i8-$n10,-$b29);
    }
    
        array_push($result,round($i16));
    if(!empty($suku_bunga_arr[1])){
        array_push($result,round($i18));
    }
    // var_dump($i10);
    // var_dump($i8);
    // var_dump($i6);
    // die();
    return $result;
}   
 
private function calculateTakeoverOld($input = null){
    //usia
    // 1 30 - 34
    // 2 35 - 39
    // 3 40 - 44
    // 4 45 - 49 tahun
    // 5 50 - 54 tahun
    // 6 55 - 59 tahun
    if(empty($input)){
        $input = [
            'existing' => [
                'outstanding' => 1000000000,
                'tenor' => 60,
                'suku_bunga' => 13,
            ],
            'mandiri' => [
                'limit' => 20000000000,
                'tenor' => 240,
                'suku_bunga' => [
                    ['persen' => 9.75, 'tenor'=>60],
                    ['persen' => 9.75, 'tenor'=>60],
                ],

            ],
            'usia' => 4
        ];
    }
    $total_tenor = $input['mandiri']['tenor'];
    $tenor_fixed = $input['mandiri']['suku_bunga']['tenor'];
    $tenor_float = $total_tenor-$tenor_fixed;
    $total = $input['mandiri']['limit'];
    $finance = new Financial();
    // $finance = new \app\helpers\Classes\PHPExcel\Calculation\Financial();
    $suku_bunga_float = 15/100;
    $suku_bunga_fixed = $input['mandiri']['suku_bunga']['persen']/100;
    $angsuran_fixed = ($suku_bunga_fixed>0)?$finance->PMT($suku_bunga_fixed/12,$total_tenor,-$total):0;
    //=IF(I14>0,-CUMIPMT(I10/12,I8,I6,1,N10,0),0)
    $total_bunga = ($angsuran_fixed>0)?-$this->CUMIPMT($suku_bunga_fixed/12,$total_tenor,$total,1,$tenor_fixed,0):0;
    $sisa_masa_fixed = $total+$total_bunga-($angsuran_fixed*$tenor_fixed);
    $angsuran_float = ($suku_bunga_float>0)?-$finance->PMT($suku_bunga_float/12,$tenor_float,$sisa_masa_fixed):0;
    $total_angsuran = $angsuran_fixed*$tenor_fixed+$angsuran_float*$tenor_float;
    $result = [];
    $provisi = 0.5*$input['mandiri']['limit']/100;
    $admin = 250000;
    $premi = $this->getPremi($input['usia'],$input['mandiri']['tenor'],$input['mandiri']['limit']);
    $kerugian_asuransi = 0.5*$input['mandiri']['limit']/100;
    $perjanjian_kredit = 0.1*$input['mandiri']['limit']/100;
    $sertifikat = 250000;
    $pengikatan_hak_tanggungan = $this->getBpth(0.2*$input['mandiri']['limit']/100);
    $arr_take_over = [$provisi,$admin,$premi,$kerugian_asuransi,$perjanjian_kredit,$sertifikat];
    $biaya_take_over = array_sum($arr_take_over);
    $total_mandiri = $total_angsuran + $biaya_take_over;

    $outstanding = $input['existing']['outstanding'];
    $tenor_bank_lain = $input['existing']['tenor'];
    $suku_bunga_bank_lain = $input['existing']['suku_bunga'];
    $total_bank_lain = $this->totalBankLain($outstanding,$tenor_bank_lain,$suku_bunga_bank_lain);
    $keuntungan = $total_mandiri - $total_bank_lain;

    $result = [
        'biaya_take_over' => $biaya_take_over,
        'total_angsuran' => $total_mandiri,
        'total_bank_lain' => $total_bank_lain,
        'keuntungan' => $keuntungan
    ];
    // var_dump($result);
    return $result;
    // var_dump($arr_take_over);
}
private function totalBankLain($outstanding,$tenor,$suku_bunga){
    $finance = new Financial();
    $suku_bunga = $suku_bunga/100;
    $angsuran = $finance->PMT($suku_bunga/12,$tenor,-$outstanding);
    return $angsuran*$tenor;
}
private function CUMIPMT($rate, $nper, $pv, $start, $end, $type = 0){
    $finance = new Financial();
    $interest = 0;
    for ($per = $start; $per <= $end; ++$per) {
        $interest += $finance->IPMT($rate, $per, $nper, $pv, 0, $type);
    }

    return $interest;
}
private function getBpth($amt){
    $result = 0;
    if($amt<850000){
        $result = 850000;
    }else{
        $result = $amt;
    }
    return $result+2500000;
}


private function getPremi($golongan_usia,$tenor,$limit){
    //             60  120 180
    // 30 - 34 tahun   0.335%  0.705%  1.075%
    // 35 - 39 tahun   0.425%  0.935%  1.495%
    // 40 - 44 tahun   0.610%  1.355%  2.250%
    // 45 - 49 tahun   0.950%  2.170%  3.500%
    // 50 - 54 tahun   1.505%  3.410%  5.445%
    // 55 - 59 tahun   2.280%  5.225%  tenor tidak diperbolehkan
    $persen = 0;
    if($golongan_usia==1){
        if($tenor==60){
            $persen = 0.335;
        }else if($tenor==180){
            $persen = 1.075;
        }else {
            $persen = 0.705;
        }
    }else if($golongan_usia==2){
        if($tenor==60){
            $persen = 0.425;
        }else if($tenor==180){
            $persen = 1.495;
        }else {
            $persen = 0.935;
        }
    }else if($golongan_usia==3){
        if($tenor==60){
            $persen = 0.610;
        }else if($tenor==180){
            $persen = 2.250;
        }else {
            $persen = 1.355;
        }
    }else if($golongan_usia==4){
        if($tenor==60){
            $persen = 0.95;
        }else if($tenor==180){
            $persen = 3.5;
        }else {
            $persen = 2.17;
        }
    }else if($golongan_usia==5){
        if($tenor==60){
            $persen = 1.505;
        }else if($tenor==180){
            $persen = 5.445;
        }else {
            $persen = 3.410;
        }
    }else if($golongan_usia==6){
        if($tenor==60){
            $persen = 2.28;
        }else if($tenor==180){
             throw new Exception('not available');
        }else {
            $persen = 5.225;
        }
    }
   return $persen*$limit/100;
}

public function actionSearchPerusahaan()
{
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

    $request = Yii::$app->request;
    $result = ['code'=>200,'message'=>'Data berhasil ditemukan','data'=>null];
    $name = $request->get('name');
    $perus = \app\models\PerusahaanKpr::find();
    if(!empty($name)){
        $perus ->andFilterWhere(['like', 'name', $name]);
    }
    $perus = $perus->all();
    $temp = ['id' => '','name' =>'' ];
    $result['data'] = [];
    if(empty($perus)){
        $result['message'] = "Data tidak ditemukan";
    }else{
        $result['message'] = "Berhasil";
    }
    foreach ($perus as $peru) {
        $temp['id'] = $peru->id;
        $temp['name'] = $peru->name;
        array_push($result['data'], $temp);
    }

    return $result;
}

public function actionSearchSukubunga($type = ""){
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

    $request = Yii::$app->request;
    $result = ['code'=>200,'message'=>'Data berhasil ditemukan','data'=>null];
    $temp = [];
    foreach (SukuBunga::find()->orderBy('nama')->all() as $sb) {
        $temp2 =[] ;
        $temp2['id'] = $sb->id;
        $temp2['name'] = $sb->nama;
        $temp2['is_reguler'] = $sb->is_reguler;
        $temp2['rules'] = [];
        $countMultiRules = 0;
        $countKprRules = 0;
        foreach ($sb->sukuBungaRules as $rule) {
            # code...
            $temp3['id'] = $rule->id; 
            $temp3['is_multiguna'] = $rule->is_multiguna; 
            if($rule->is_multiguna==0){
                $countKprRules++;
            }else{
                $countMultiRules++;
            }
            $temp3['suku_bunga_fixed'] = $rule->suku_bunga_fixed; 
            $temp3['tahun_fixed'] = $rule->tahun_fixed; 
            $temp3['min_tenor'] = $rule->min_tenor; 
            array_push($temp2['rules'], $temp3);
            // array_push($arrRule, $temp3);
        }
        $temp2['multiguna_str'] = $this->messageSukuBunga($temp2,1);
        $temp2['kpr_str'] = $this->messageSukuBunga($temp2,0);
        $temp2['is_kpr'] = 0;
        if($countKprRules>0){
            $temp2['is_kpr'] = 1;
        }
        $temp2['is_multiguna'] = 0;
        if($countMultiRules>0){
            $temp2['is_multiguna'] = 1;
        }
        if(!empty($type)){
            // $kprs = $kprs->andWhere(['is_multiguna'=> $type]);
            if($type == SukuBungaRules::TYPE_KPR){
                if($countKprRules>0){
                    array_push($temp, $temp2);
                }
            }else{//tipemulti
                if($countMultiRules>0){
                    array_push($temp, $temp2);
                }
            }
        }else{
            array_push($temp, $temp2);
        }
        # code...
    }
    $result['data'] = $temp;
    return $result;
}

    public function actionPromos() 
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = ['code'=>200,'message'=>'Data berhasil ditemukan','data'=>null];
        $result['data'] = [];
        $promos = \app\models\Promo::find()->orderBy('order_number')->all();
        foreach ($promos as $promo) {
        if(empty($promos)){
            $result['message'] = "Data tidak ditemukan";
        }else{
            $result['message'] = "Berhasil";
        }
            # code...
            //masukin id
            //masukin title
            $promoTemp = [];
            $promoTemp['id'] = $promo->id; 
            //get title
            $promoTemp['title'] = $promo->title; 
            $pdgs = \app\models\PromoDeveloperGroup::find()
                      ->With(['promo','developerGroup'])
                      ->where(['promo_id'=>$promo->id])
                      ->all();
            $developer_groups = [];
            //foreach promodeveloper
            foreach ($pdgs as $pdg) {
                # code...
                $temp['id'] = $pdg->developer_group_id;
                // get developer name
                $temp['name'] = $pdg->developerGroup->name;
                // get text
                $temp['text'] = $pdg->text;
                $developer_groups[] = $temp;
                // $data[] = $developer_groups[];
            }
            $promoTemp['developer_group'] = $developer_groups;
            array_push($result['data'], $promoTemp);
            // $data[] = $promoTemp;
        }
        return $result;
    }

}