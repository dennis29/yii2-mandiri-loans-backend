<?php
namespace app\controllers\api;

use Yii;
use app\models\Intro;
use app\models\IntroSearch;
use app\models\TargetMarket;
use app\models\Kpr;
use app\models\SukuBungaRules;
use app\models\User;
use app\models\Area;
use app\models\Region;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\PasswordResetRequestForm;

/**
 * IntroController implements the CRUD actions for Intro model.
 */
class TestController extends Controller
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                    ]
                ]
            ]
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionMax()
    {
        // echo 'adadsda';
        $input = [];
        $input['rules'][] = 11110;
        // $input['rules'][] = 1;
        // $input['rules'][] = 2;
        // $input['rules'][] = 3;
        // $input['rules'][] = 4;
        // $input['rules'][] = 5;

        echo $this->countMaxTenor($input);
    }

    private function countMaxTenor($input){
        $result = 0;
        if(!empty($input['rules'])){
            $rules = $input['rules'];
            $min = -1;
            foreach ($rules as $rule) {
                $min = max($min,$rule) ;
            }
            $result = $min;
        }
        return $result;

    }
}