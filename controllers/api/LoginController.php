<?php

namespace app\controllers\api;

use Yii;
use app\models\Intro;
use app\models\IntroSearch;
use app\models\TargetMarket;
use app\models\Kpr;
use app\models\SukuBungaRules;
use app\models\User;
use app\models\LogUser;
use app\models\Area;
use app\models\Region;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\PasswordResetRequestForm;

/**
 * IntroController implements the CRUD actions for Intro model.
 */
class MasterController extends Controller
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                    ]
                ]
            ]
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
	
	public function actionIndex()
    {
        echo 3333;
    }

    public function actionLogin()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $result = ['code'=>401,'message'=>'login not valid','data'=>null];
        $request = Yii::$app->request;
        if ($request->isPost){
            // return \yii\helpers\Json::encode($request);die();
            // var_dump($_POST);die();

            // THIS, coba cari helpernya di yii2 (kali aja ada)
            // nip, password
            $jsonRequest = json_decode(file_get_contents('php://input'));

                // $username = $request->post('username');
            $nip = $jsonRequest->nip;

            $modelUser = \app\models\User::findByUsername($nip);

            if($modelUser!=null){
                // $password = $request->post('password');
                //cek if user referensi still exist
                $userReferensi = \app\models\UserReferensi::find()->where(['nip'=>$nip])->one();
                if(empty($userReferensi)){
                    LogUser::logHitUser(LogUser::USER_WHITELIST_NOT_FOUND,LogUser::FAIL,$nip);
                    $result = [
                            'code'=>400,
                            'message'=>'User tidak ditemukan',
                            'data'=>[
                            ]
                    ];
                    return $result;
                } 
                $password = $jsonRequest->password;

                if($modelUser->validatePasswordMobile($password)){
                    // $modelUser->mobile_device_id = $request->post('mobile_device_id');
                    // $modelUser->save();
                    if(in_array($modelUser->status,[User::STATUS_MOBILE,User::STATUS_VERIFIED])){
                        $area_id = "";
                        $region_id = "";
                        if(!empty($modelUser->area)){
                            $area_id = $modelUser->area->id;

                        }
                        if(!empty($modelUser->region)){
                            
                            $region_id = $modelUser->region->id;
                        }
                        LogUser::logHitUser(LogUser::STATUS_SUCCESS,LogUser::SUCCESS,$modelUser->nip,$modelUser->id);
                        $tokens = "";
                        if(in_array($modelUser->status,[User::STATUS_VERIFIED])){
                            $tokens = Yii::$app->getSecurity()->generateRandomString(32);
                            $userModel = User::find()->where(['nip' =>$nip])->one();
                            $userModel->token = $tokens;
                            $userModel->save();
                        }
                        $result = [
                            'code'=>200,
                            'message'=>'Login berhasil',
                            'data'=>[
                                'id'=>$modelUser->id,
                                'is_verified'=>$modelUser->getIsVerified(),
                                'area_id'=>$area_id,
                                // 'area'=>$modelUser->area->name,
                                'region_id'=>$region_id,
                                'token' => $tokens,
                                // 'region'=>$modelUser->region->name,
                                // 'full_name'=>$modelUser->full_name,
                                // 'unit_kerja_id'=>$modelUser->unit_kerja,
                            ]
                        ];
                    }
                }else{
                    LogUser::logHitUser(LogUser::WRONG_PASSWORD,LogUser::FAIL,$modelUser->nip,$modelUser->id);
                    $result['message']='Password salah';
                    $result['code']=401;
                }
            }else{
                LogUser::logHitUser(LogUser::NIP_NOT_FOUND,LogUser::FAIL,$nip);
                $result['code']=400;
                $result['message']='Nip tidak ditemukan';
            }
        }
        return $result;
    }

    public function actionIntro()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $result = ['code'=>200,'message'=>'Data berhasil diload','data'=>null];
        $intros = \app\models\Intro::find()->orderBy('order_number')->all();
        $temp = ['text' => '','link' =>'' ];
        $result['data'] = [];
        if(empty($intros)){
            $result['message'] = "Data tidak ditemukan";
        }else{
            $result['message'] = "Berhasil";
        }
        foreach ($intros as $intro) {
            $temp['text'] = $intro->text;
            $temp['title'] = $intro->title;
            $temp['link'] = \yii\helpers\Url::to($intro->image,true);
            $temp['image_name'] = $intro->image_name;
            array_push($result['data'], $temp);
        }

        return $result;
    }

    public function actionSlides()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $result = ['code'=>200,'message'=>'Data berhasil diload','data'=>null];
        $intros = \app\models\Slider::find()->orderBy('order_number')->all();
        $temp = ['link' =>'' ];
        $result['data'] = [];
        if(empty($intros)){
            $result['message'] = "Data tidak ditemukan";
        }else{
            $result['message'] = "Berhasil";
        }
        foreach ($intros as $intro) {
            $temp['link'] = \yii\helpers\Url::to($intro->image,true);
            $temp['image_name'] = $intro->image_name;
            array_push($result['data'], $temp);
        }

        return $result;
    }

    public function actionResendEmail(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = ['code'=>400,'message'=>'bad request','data'=>null];
        $request = Yii::$app->request;
        $jsonRequest = json_decode(file_get_contents('php://input'));
        if ($request->isPost){
            $user_id = $jsonRequest->user_id;
            $model = User::findOne($user_id);
            if(empty($model)){
                $result = ['code'=>400,'message'=>'User tidak ditemukan','data'=>null];
            }else{
                $model->sendVerification(true);
                $result = ['code'=>200,'message'=>'Tolong cek email','data'=>null];
            }
        }
        return $result;
    }

    public function actionRegister()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $result = ['code'=>200,'message'=>'login not valid','data'=>null];
        $request = Yii::$app->request;
        $jsonRequest = json_decode(file_get_contents('php://input'));
        if ($request->isPost){
            // return \yii\helpers\Json::encode($request);die();
            // var_dump($_POST);die();
            $nip = $jsonRequest->nip;
            $modelUserReferensi = \app\models\UserReferensi::find()->where(['nip'=>$nip])->one();

            if($modelUserReferensi==null){
                $result = ['code'=>400,'message'=>'NIP/Sales Code anda tidak terdaftar pada database kami. Silakan kontak PIC database Region anda','data'=>null];
                return $result;
            // }else if(trim($modelUserReferensi->contact)!=trim($jsonRequest->phone)){
            //     $result = ['code'=>400,'message'=>'Nomor hp anda tidak sesuai dengan database kami. Silakan kontak PIC database Region anda.','data'=>null];
            //     return $result;
            }else if(trim(date("dmY",strtotime($modelUserReferensi->dob)))!=trim($jsonRequest->dob)){
                $result = ['code'=>400,'message'=>'Data anda tidak sesuai dengan database kami. Silakan kontak PIC database Region anda','data'=>(trim(date("dmY",strtotime($modelUserReferensi->dob))))];
                return $result;
            }
            $modelUser = new \app\models\SignupForm();
            $modelUser->nip = $jsonRequest->nip;
            // $modelUser->name = $jsonRequest->name;
            $modelUser->password = $jsonRequest->password;
            $modelUser->retype_password = $jsonRequest->password;
            $modelUser->email = $jsonRequest->email;
            // $modelUser->dob = date("dmY",$jsonRequest->dob);
            $modelUser->dob = preg_replace("/^(\d{2})(\d{2})(\d{4})$/", "$1-$2-$3", $jsonRequest->dob);
            $modelUser->phone_number = $jsonRequest->phone;
            // var_dump($modelUser);die();
            if(!empty($modelUserReferensi)){
                $modelUser->name = $modelUserReferensi->name;
            }
            if(!empty($modelUserReferensi->region)){
                $modelUser->region_id = $modelUserReferensi->region->id;
            }
            if(!empty($modelUserReferensi->area)){
                $modelUser->area_id = $modelUserReferensi->area->id;
            }
            if($user = $modelUser->signupMobileApi()){
                $user->sendVerification();
                $result = [
                    'code'=>200,
                    'message'=>'Registrasi berhasil, silahkan cek e-mail untuk verifikasi',
                    'data'=>[
                        'id'=>$user->id,
                    ]
                ];

            }else{
                $result['code']=400;
                $result['data']=null;
                foreach ($modelUser->errors as $d) {
                    // $result['data'] .= $d[0] .' ';
                    $result['message']=$d[0];
                }
                    // $result['data']['message']=$modelUser->errors;
            }
        }
        return $result;
    }
    public function actionForgotPassword()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = ['code'=>400,'message'=>'Email tidak valid','data'=>null];
        $model = new PasswordResetRequestForm();
        $request = Yii::$app->request;
        $jsonRequest = json_decode(file_get_contents('php://input'));
        $model->email = $jsonRequest->email;
        if ( $model->validate()) {
            if ($model->sendEmail()) {
                $result = ['code'=>200,'message'=>'Sukses! silahkan periksa email','data'=>null];
            } else {
                $result = ['code'=>500,'message'=>'Email gagal dikirim','data'=>null];

            }
        }

        return $result;
    }

    public function checkIfNipExist($nip){
        $modelUserReferensi = \app\models\UserReferensi::find()->where(['nip'=>$nip])->one();
        if(empty($modelUserReferensi)){
            return false;
        }else{
            return true;
        }

    }
    public function actionSearchPerusahaan()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $request = Yii::$app->request;
        $result = ['code'=>200,'message'=>'Data berhasil diload','data'=>null];
        $name = $request->get('name');
        $perus = \app\models\Perusahaan::find();
        if(!empty($name)){
            $perus ->andFilterWhere(['like', 'name', $name]);
        }
        $perus = $perus->orderBy('name')->all();
        $temp = ['id' => '','name' =>'' ];
        $result['data'] = [];
        if(empty($perus)){
            $result['message'] = "Data tidak ditemukan";
        }else{
            $result['message'] = "Success";
        }
        foreach ($perus as $peru) {
            $temp['id'] = $peru->id;
            $temp['name'] = $peru->name;
            array_push($result['data'], $temp);
        }

        return $result;
    }

    public function actionSearchDeveloper()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $request = Yii::$app->request;
        $result = ['code'=>200,'message'=>'Data berhasil diload','data'=>null];
        $name = $request->get('name');
        $perus = \app\models\Developer::find();
        if(!empty($name)){
            $perus ->andFilterWhere(['like', 'nama', $name]);
        }
        $perus = $perus->all();
        $temp = ['id' => '','name' =>'' ];
        $result['data'] = [];
        if(empty($perus)){
            $result['message'] = "Data tidak ditemukan";
        }else{
            $result['message'] = "Berhasil";
        }
        foreach ($perus as $peru) {
            $temp['id'] = $peru->id;
            $temp['name'] = $peru->nama;
            $temp['text'] = $peru->text;
            array_push($result['data'], $temp);
        }

        return $result;
    }

    public function actionSearchDeveloperGroup($is_publish=null,$name=null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $request = Yii::$app->request;
        $result = ['code'=>200,'message'=>'Data berhasil diload','data'=>null];
        // $name = $request->get('name');
        $perus = \app\models\DeveloperGroup::find();
        if(!empty($is_publish)){
            $perus ->andFilterWhere(['is_publish'=> $is_publish]);
        }
        if(!empty($name)){
            $perus ->andFilterWhere(['like', 'name', $name]);
        }
        $perus = $perus->orderBy('name')->all();
        $temp = ['id' => '','name' =>'' ];
        $result['data'] = [];
        if(empty($perus)){
            $result['message'] = "Data tidak ditemukan";
        }else{
            $result['message'] = "Berhasil";
        }
        foreach ($perus as $peru) {
            $temp['id'] = $peru->id;
            $temp['name'] = $peru->name;
            $temp['text'] = $peru->text;
            array_push($result['data'], $temp);
        }

        return $result;
    }

    public function actionRewards()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $result = ['code'=>200,'message'=>'Data berhasil diload','data'=>null];
        $rewards = \app\models\Reward::find()->all();
        $temp = ['title' => '','image' =>'','content' =>'' ];
        $result['data'] = [];

        if(empty($rewards)){
            $result['message'] = "Data tidak ditemukan";
        }else{
            $result['message'] = "Berhasil";
        }
        foreach ($rewards as $reward) {
            $temp['id'] = $reward->id;
            $temp['content'] = $reward->text;
            $temp['image'] = \yii\helpers\Url::to($reward->image,true);
            $temp['title'] = $reward->title;
            array_push($result['data'], $temp);
        }

        return $result;
    }

    public function actionSearchProyek($proyek_name = "", $developer_name = "", $min_price = "", $max_price = "",$location = "")
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $request = Yii::$app->request;
        $result = ['code'=>200,'message'=>'Data berhasil diload','data'=>null];

        $proyeks = \app\models\Proyek::find();

        if (!empty($proyek_name)) {
            $proyeks = $proyeks->where(['like', 'proyek.nama', $proyek_name]);
            // $proyeks = $proyeks->where(['id'=>$proyek_id]);
        } 
        if (!empty($developer_name)) {
            $proyeks = $proyeks->joinWith(['developer'])
                            ->leftJoin('developer_group', 'developer_group.id = developer.developer_group_id')->addSelect(['developer_group.name','proyek.*'])
                            // ;
                           ->andFilterWhere(['like','developer_group.name', $developer_name]);
        }
        if (!empty($min_price)) {
            $proyek = $proyeks->andWhere("min_harga >= ".$min_price."  or min_harga is null");
        }
        if (!empty($max_price)) {
            $proyek = $proyeks->andWhere("max_harga <= ".$max_price."  or max_harga is null");

        }
        if (!empty($location)) {
            $proyek = $proyeks->joinWith(['area'])->andWhere(['like','area.name',$location]);

        }

    

        $proyeks = $proyeks->limit(20)->orderBy('proyek.nama')->all();
                           // echo $developer_name;die();

        $temp = [];
        $result['data'] = [];

        if(empty($proyeks)){
            $result['message'] = "Data tidak ditemukan";
        }else{
            $result['message'] = "Berhasil";
        }

        foreach ($proyeks as $proyek) {
            $temp['id'] = $proyek->id;
            $temp['name'] = $proyek->nama;
            $temp['developer_name'] = $proyek->developer->nama;
            $temp['developer_group'] = "";
            if(!empty($proyek->developer)){
                if(!empty($proyek->developer->developerGroup)){
                    $temp['developer_group'] = $proyek->developer->developerGroup->name;
                }
            }
            $temp['tanggal_jatuh_tempo'] = date('d-m-Y',strtotime($proyek->tanggal_jatuh_tempo));
            $temp['developer_tier'] = (!empty($proyek->developerTier))?$proyek->developerTier->nama:"";
            $temp['area'] = (!empty($proyek->area))?$proyek->area->name:"";
            $temp['region'] = (!empty($proyek->region))?$proyek->region->name:"";
            // $temp['kta_name'] = $proyek->kta->nama;
            $temp['program_marketing_text'] = $this->messageProgramMarketing($proyek);
            // $temp['program_marketing'] = $this->arrProgramMarketing($proyek);
            $temp['min_harga'] = $proyek->min_harga;
            $temp['max_harga'] = $proyek->max_harga;
            $temp['longitude'] = $proyek->longitude;
            $temp['latitude'] = $proyek->latitude;
            // $temp['location'] = $proyek->location;
            $temp['website'] = $proyek->website;

            array_push($result['data'], $temp);
        }

        return $result;
    }

    private function arrProgramMarketing($proyek){
        $developerTier = $this->getDeveloperTier($proyek);
        if(empty($developerTier)){
            return "";
        }
        $pm = Kpr::find()->joinWith('developerTiers')->andWhere(['developer_tier_id'=>$developerTier->id]);
        $pm = $pm->orderBy('order_number');
        $pm = $pm->all();
        if(empty($pm)){
            $result['message'] = "Data tidak ditemukan";
        }else{
            $result['message'] = "Berhasil";

            foreach ($pm as $kpr) {
                $temp['id'] = $kpr->id;
                // $temp['is_multiguna'] = $kpr->is_multiguna === 0 ? false : true;
                $temp['name'] = $kpr->nama;
                $temp['profil_nasabah_text'] = $kpr->profil_nasabah_text;
                $temp['peruntukan_text'] = $kpr->peruntukan_text;
                $temp['market_text'] = $kpr->market_text;
                $temp['benefit_text'] = $kpr->benefit_text;
                $temp['peserta_text'] = $kpr->peserta_text;
                // $temp['keterangan'] = $kpr->keterangan;
                $temp['sukuBunga'] = [];
                $arrRule = [];

                // $temp['target_market'] = $kpr->targ3tMarketsName;

                // $temp['developer_tier'] = [];
                // foreach ($kpr->developerTiersName as $key) {
                //     $dev = [];
                //     $dev['id'] = $key->id;
                //     $dev['name'] = $key->nama; 
                //     array_push($temp['developer_tier'], $dev);
                // }
                // $temp['profesi'] = [];
                // foreach ($kpr->profesiKprsName as $key) {
                //     $dev = [];
                //     $dev['id'] = $key->id;
                //     $dev['name'] = $key->nama; 
                //     array_push($temp['profesi'], $dev);
                // }
                foreach ($kpr->sukuBungas as $sb) {
                    $temp2 =[] ;
                    $temp2['id'] = $sb->id;
                    $temp2['name'] = $sb->nama;
                    $temp2['is_reguler'] = $sb->is_reguler;
                    $temp2['rules'] = [];
                    $countMultiRules = 0;
                    $countKprRules = 0;
                    foreach ($sb->sukuBungaRules as $rule) {
                        # code...
                        $temp3['id'] = $rule->id; 
                        $temp3['is_multiguna'] = $rule->is_multiguna; 
                        if($rule->is_multiguna==0){
                            $countKprRules++;
                        }else{
                            $countMultiRules++;
                        }
                        $temp3['suku_bunga_fixed'] = $rule->suku_bunga_fixed; 
                        $temp3['tahun_fixed'] = $rule->tahun_fixed; 
                        $temp3['min_tenor'] = $rule->min_tenor; 
                        array_push($temp2['rules'], $temp3);
                        array_push($arrRule, $temp3);
                    }
                    // $temp2['multiguna_str'] = $this->messageSukuBunga($temp2,1);
                    // $temp2['kpr_str'] = $this->messageSukuBunga($temp2,0);
                    array_push($temp['sukuBunga'], $temp2);
                    # code...
                }
                // $rules = $kpr->kprRules;
                if(count($arrRule)>0){

                    usort($arrRule, function ($a, $b) { return strnatcmp($a['suku_bunga_fixed'], $b['suku_bunga_fixed']); });

                    $temp['suku_bunga_fixed'] = $arrRule[0]['suku_bunga_fixed'];
                    usort($arrRule, function ($a, $b) { return strnatcmp($a['tahun_fixed'], $b['tahun_fixed']); });
                    $temp['tahun_fixed'] = $arrRule[0]['tahun_fixed'];
                    $temp['min_tenor'] = $arrRule[0]['min_tenor'];
                    $temp['min_tenor_str'] = $arrRule[0]['min_tenor']." tahun";
                }

                //jangan di push kalau tipenya ga sesuai
                // echo $type;die();
                $temp['is_kpr'] = 0;
                if($countKprRules>0){
                    $temp['is_kpr'] = 1;
                }
                $temp['is_multiguna'] = 0;
                if($countMultiRules>0){
                    $temp['is_multiguna'] = 1;
                }
                // $temp['peruntukan_str'] = $this->messagePeruntukan($temp);
                // $temp['expired'] = $kpr->expired;
                // if($type == SukuBungaRules::TYPE_MULTIGUNA){
                    $temp['suku_bunga_text_kpr'] = $kpr->suku_bunga_multiguna_text;
                // }else{
                    $temp['suku_bunga_text_multiguna'] = $kpr->suku_bunga_kpr_text;

                // }
                // if(!empty($type)){
                //     // $kprs = $kprs->andWhere(['is_multiguna'=> $type]);
                //     if($type == SukuBungaRules::TYPE_KPR){
                //         if($countKprRules>0){
                //             array_push($result['data'], $temp);
                //         }
                //     }else{//tipemulti
                //         if($countMultiRules>0){
                //             array_push($result['data'], $temp);
                //         }
                //     }
                // }else{
                    array_push($result, $temp);
                // }
            }
        }
        // return $result;
        // $result = [];
        // foreach ($pm as $item) {
            // array_push($result, $item);
        // }/
        return $result;
    }
    private function getDeveloperTier($proyek){
        if(!empty($proyek->developer)){
            if(!empty($proyek->developer->developerTier)){
                return $proyek->developer->developerTier;
            }else{
                return null;
            }
        }else{
            return null;
        }
    }
    private function messageProgramMarketing($proyek){
        $developerTier = $this->getDeveloperTier($proyek);
        if(empty($developerTier)){
            return "";
        }
        $pm = Kpr::find()->joinWith('developerTiers')->where(['developer_tier_id'=>$developerTier->id])
        ->orderBy('order_number')
        ->all();
        $result = [];
        foreach ($pm as $item) {
            array_push($result, $item->nama);
        }
        return "<ul><li>".implode("</li><li>",$result)."</li></ul>";
        // return implode(", ",$result);
    }

    public function actionArea($region_id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $result = ['code'=>200,'message'=>'Data berhasil diload','data'=>null];
        $areas = \app\models\Area::find()->where(['region_id'=>$region_id])->orderBy('name')->all();

        $temp = ['name' => '','id' =>''];
        $result['data'] = [];

        if(empty($areas)){
            $result['message'] = "Data tidak ditemukan";
        }else{
            $result['message'] = "Berhasil";
        }

        foreach ($areas as $area) {
            $temp['name'] = $area->name;
            $temp['id'] = $area->id;

            array_push($result['data'], $temp);
        }

        return $result;
    }

    public function actionRegion()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $result = ['code'=>200,'message'=>'Data berhasil diload','data'=>null];
        $regions = \app\models\Region::find()->all();

        $temp = ['name' => '','id' =>''];
        $result['data'] = [];

        if(empty($regions)){
            $result['message'] = "Data tidak ditemukan";
        }else{
            $result['message'] = "Berhasil";
        }

        foreach ($regions as $region) {
            $temp['name'] = $region->name;
            $temp['id'] = $region->id;

            array_push($result['data'], $temp);
        }

        return $result;
    }

    public function actionProfesiKpr()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $result = ['code'=>200,'message'=>'Data berhasil diload','data'=>null];
        $profesis = \app\models\ProfesiKpr::find()->all();

        $temp = ['id' => '','name' =>'' ];
        $result['data'] = [];

        if(empty($profesis)){
            $result['message'] = "Data tidak ditemukan";
        }else{
            $result['message'] = "Berhasil";
        }

        foreach ($profesis as $profesi) {
            $temp['id'] = $profesi->id;
            $temp['name'] = $profesi->nama;

            array_push($result['data'], $temp);
        }

        return $result;
    }

    public function actionMenuKta()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $result = ['code'=>200,'message'=>'Data berhasil diload','data'=>null];
        $ktas = \app\models\MenuKta::find()->all();

        $temp = ['text' => '','link' =>'' ];
        $result['data'] = [];

        if(empty($ktas)){
            $result['message'] = "Data tidak ditemukan";
        }else{
            $result['message'] = "Berhasil";
        }

        foreach ($ktas as $kta) {
            $temp['title'] = $kta->title;
            $temp['text'] = $kta->text;
            $temp['link'] = \yii\helpers\Url::to($kta->image,true);
            $temp['image_name'] = $kta->image_name;

            array_push($result['data'], $temp);
        }

        return $result;
    }
    
    
    public function actionPdfMaster($code=null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $result = ['code'=>200,'message'=>'Data berhasil diload','data'=>null];
        $query = \app\models\PdfMaster::find();
        if(!empty($code)){
            $query = $query->where(['code'=>$code]);
        }
        $query = $query->all();

        $temp = [];
        $result['data'] = [];

        if(empty($query)){
            $result['message'] = "Data tidak ditemukan";
        }else{
            $result['message'] = "Berhasil";
        }

        foreach ($query as $item) {
            $temp['name'] = $item->file_name;
            $temp['file'] = \yii\helpers\Url::to($item->file_location,true);

            array_push($result['data'], $temp);
        }

        return $result;
    }

    public function actionLogHit(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $jsonRequest = json_decode(file_get_contents('php://input'));
        $request = Yii::$app->request;

        $result = ['code'=>200,'message'=>'Data berhasil diload','data'=>null];
        try{
            // $code = "-";
            // if(!empty($jsonRequest->code)){
            $code = $jsonRequest->code;
            // }
            $ip_address = $request->userIp;
            
            $user_id = $jsonRequest->user_id;
            $platform = $jsonRequest->platform;

            $obj = new \app\models\LogDashboard();
            $obj->code = $code;
            $obj->ip_address = $ip_address;
            $obj->user_id = $user_id;
            $obj->platform = $platform;
            $obj->save(false);
            // $result['message'] = $ip_address;
        }catch(\yii\base\ErrorException $e){
            $result['code'] = '400';
            $result['message'] = $e->getMessage();

        }

        return $result;
    }

    public function actionAreaByName()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $request = Yii::$app->request;
        $result = ['code'=>200,'message'=>'Data berhasil diload','data'=>null];
        $name = $request->get('name');
        $perus = \app\models\Area::find();
        if(!empty($name)){
            $perus ->andFilterWhere(['like', 'name', $name]);
        }
        $perus = $perus->orderBy('name')->all();
        $temp = ['id' => '','name' =>'' ];
        $result['data'] = [];
        if(empty($perus)){
            $result['message'] = "Data tidak ditemukan";
        }else{
            $result['message'] = "Berhasil";
        }
        foreach ($perus as $peru) {
            $temp['id'] = $peru->id;
            $temp['name'] = $peru->name;
            array_push($result['data'], $temp);
        }

        return $result;
    }

    public function actionEditUser()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $result = ['code'=>200,'message'=>'LOgin tidak berhasil','data'=>null];
        $request = Yii::$app->request;
        $jsonRequest = json_decode(file_get_contents('php://input'));
        try{

            if ($request->isPost){
                // return \yii\helpers\Json::encode($request);die();
                // var_dump($_POST);die();
                $id = $jsonRequest->id;
                $modelUser = \app\models\User::findOne($id);

                
                $nip = $jsonRequest->nip;
                // $modelUser->name = $jsonRequest->name;
                $modelUser->email = $jsonRequest->email;
                $modelUser->dob = $jsonRequest->dob;
                $modelUser->phone_number = $jsonRequest->phone;
                // $modelUser->region_id = $jsonRequest->region;
                // $modelUser->area_id = $jsonRequest->area;
                if($modelUser->save()){
                    $modelUser->sendVerification(true);
                    $result = [
                        'code'=>200,
                        'message'=>'Perubahan berhasil, silahkan cek email untuk verifikasi',
                        'data'=>[]
                    ];

                }else{
                    $result['code']=400;
                    $result['data']=null;
                    foreach ($modelUser->errors as $d) {
                        $result['data'] .= $d[0] .' ';
                    }
                        // $result['data']['message']=$modelUser->errors;
                }
            }
        
        }catch(\yii\base\ErrorException $e){
            $result['code'] = '400';
            $result['message'] = $e->getMessage();

        }
        return $result;
    }

    public function actionChangePassword()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $result = ['code'=>200,'message'=>'Ubah password gagal','data'=>null];
        $request = Yii::$app->request;
        $jsonRequest = json_decode(file_get_contents('php://input'));
        // var_dump($jsonRequest->user_id);
        // die();
        try{

            if ($request->isPost){
                // return \yii\helpers\Json::encode($request);die();
                // var_dump($_POST);die();
                $id = $jsonRequest->user_id;
                $modelUser = \app\models\User::findOne($id);

                
                // $nip = $jsonRequest->nip;
                // $modelUser->name = $jsonRequest->name;
                // $modelUser->user_id = $jsonRequest->user_id;
                //if user exist
                if($modelUser != null){
                    $password = $jsonRequest->password;
                    //if passsword true
                    if($modelUser->validatePasswordMobile($password)){
                        //change password
                        $new_password = $jsonRequest->new_password;
                        $modelUser->setPassword($new_password);
                        $modelUser->removePasswordResetToken();
                        if($modelUser->save()){
                            // $modelUser->sendVerification(true);
                            LogUser::logHitUser(LogUser::CHANGE_PASSWORD_SUCCESS,LogUser::SUCCESS,$modelUser->nip,$modelUser->id);
                            $result = [
                                'code'=>200,
                                'message'=>'Ganti password berhasil',
                                'data'=>null
                            ];
                        }else{
                            $result['code'] =400;
                            $result['message'] ="Ubah password gagal";
                        }
                    }else{
                        LogUser::logHitUser(LogUser::CHANGE_PASSWORD_FAIL_P,LogUser::FAIL,$modelUser->nip,$modelUser->id);
                        $result['code'] =400;
                        $result['message'] ="Password salah";
                    }
                }else{
                    LogUser::logHitUser(LogUser::CHANGE_PASSWORD_FAIL_U,LogUser::FAIL,$jsonRequest->user_id);
                    $result['code'] =400;
                    $result['message'] ="User tidak ditemukan";
                }
                //return kasih message
                

                
            
            }
        
        }catch(\yii\base\ErrorException $e){
            $result['code'] = '400';
            $result['message'] = $e->getMessage();

        }
        return $result;
    }

    public function actionVerifyToken(){
        $request= Yii::$app->request;
        $cookies = $_COOKIE;
        $user_id = $cookies["mandiri-user"];
        $token = $cookies["mandiri-token"];
        if(!$user_id){
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $result = ['code'=>403,'message'=>'UserID tidak boleh kosong','data'=>null];
            echo json_encode($result);die();
        }
        if(empty($token)){
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $result = ['code'=>403,'message'=>'Token tidak boleh kosong','data'=>null];
            echo json_encode($result);die();
        }    
        $tokenDB = User::find()->select('token')->where(['id'=> $user_id])->one()->token;
        if($token != $tokenDB){
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $result = ['code'=>403,'message'=>'Token tidak valid','data'=>null];
            echo json_encode($result);die();
        }else{
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $result = ['code'=>200,'message'=>'Token valid','data'=>null];
            echo json_encode($result);die(); 
        }
    }   
}
