<?php

namespace app\controllers;

use Yii;
use app\models\KprTarg3tMarket;
use app\models\KprTarg3tMarketSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KprTarg3tMarketController implements the CRUD actions for KprTarg3tMarket model.
 */
class KprTarg3tMarketController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all KprTarg3tMarket models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KprTarg3tMarketSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KprTarg3tMarket model.
     * @param string $kpr_id
     * @param string $targ3t_market_id
     * @return mixed
     */
    public function actionView($kpr_id, $targ3t_market_id)
    {
        $model = $this->findModel($kpr_id, $targ3t_market_id);
        return $this->render('view', [
            'model' => $this->findModel($kpr_id, $targ3t_market_id),
        ]);
    }

    /**
     * Creates a new KprTarg3tMarket model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KprTarg3tMarket();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'kpr_id' => $model->kpr_id, 'targ3t_market_id' => $model->targ3t_market_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing KprTarg3tMarket model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kpr_id
     * @param string $targ3t_market_id
     * @return mixed
     */
    public function actionUpdate($kpr_id, $targ3t_market_id)
    {
        $model = $this->findModel($kpr_id, $targ3t_market_id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'kpr_id' => $model->kpr_id, 'targ3t_market_id' => $model->targ3t_market_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing KprTarg3tMarket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kpr_id
     * @param string $targ3t_market_id
     * @return mixed
     */
    public function actionDelete($kpr_id, $targ3t_market_id)
    {
        $this->findModel($kpr_id, $targ3t_market_id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the KprTarg3tMarket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kpr_id
     * @param string $targ3t_market_id
     * @return KprTarg3tMarket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kpr_id, $targ3t_market_id)
    {
        if (($model = KprTarg3tMarket::findOne(['kpr_id' => $kpr_id, 'targ3t_market_id' => $targ3t_market_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
