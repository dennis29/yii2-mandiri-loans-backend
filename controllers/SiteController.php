<?php
namespace app\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\LoginForm;
use app\models\LogUser;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\SignupForm;
use app\models\ContactForm;
use app\models\User;
use yii\helpers\Url;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup','verify'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionTestEmail()
    {
        $data =Yii::$app
            ->mailer
            ->compose()
            ->setFrom('supernbrd91@gmail.com')
            ->setTo('farhan0syakir@gmail.com')
            ->setSubject('Password reset for ')
            ->send();
        var_dump($data);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $userRegionUrl = Url::toRoute(['/user-region']);
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            LogUser::logHitUser(LogUser::STATUS_SUCCESS,LogUser::SUCCESS,Yii::$app->user->identity->nip,Yii::$app->user->identity->id);
            // echo "string login";
            //     die();
            //if user region redirect to my whitelist
            if(Yii::$app->user->identity->status == 12){
                // var_dump($userRegionUrl);
                // die();
                return $this->redirect($userRegionUrl);
            }
            return $this->goBack();
        } else {
            $post = Yii::$app->request->post();
            if(!empty($post)){
                LogUser::logHitUser(LogUser::NIP_NOT_FOUND,LogUser::FAIL,$post['LoginForm']['username']);
            }
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                echo 'success';die();
                // return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
                echo 'success';die();
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function beforeAction($action)
    {
        if ($action->id == 'login-mobile') {
            $this->enableCsrfValidation = false;
            // echo "farhan";
            // die();
        }

        return parent::beforeAction($action);
    }

    public function actionLoginMobile()
    {
        $result = ['is_success'=>false,'message'=>'login not valid','user'=>null];
        $request = Yii::$app->request;
        if ($request->isPost){
            // return \yii\helpers\Json::encode($request);die();
            // var_dump($_POST);die();
            $username = $request->post('username');
            $modelUser = \app\models\User::findByUsername($username);
            if($modelUser!=null){
                if($modelUser->validatePassword($request->post('password'))){
                    // $modelUser->mobile_device_id = $request->post('mobile_device_id');
                    // $modelUser->save();
                    LogUser::logHitUser(LogUser::STATUS_SUCCESS,LogUser::SUCCESS,$modelUser->name,$modelUser->id);
                    $result = [
                        'is_success'=>true,
                        'message'=>'login success',
                        'user'=>[
                            'id'=>$modelUser->id,
                            // 'full_name'=>$modelUser->full_name,
                            // 'unit_kerja_id'=>$modelUser->unit_kerja,
                        ]
                    ];
                }else{
                    LogUser::logHitUser(LogUser::WRONG_PASSWORD,LogUser::FAIL,$modelUser->name,$modelUser->id);
                    $result['message']='wrong password';
                }
            }else{
                LogUser::logHitUser(LogUser::NIP_NOT_FOUND,LogUser::FAIL,$modelUser->name,$modelUser->id);
                $result['message']='username not exist';
            }
        }
        return \yii\helpers\Json::encode($result);
    }
    public function actionVerify($id,$verify){
        $user = User::findOne($id);
        if($user->verify_token == $verify){
            $user->verify_token = null;
            $user->status = User::STATUS_VERIFIED;
            $user->save(false);
            echo 'verification success';
            //redirect ke clgbankmandiri.com
            // return $this->redirect('http://www.clgbankmandiri.com');
            //sementara ke google.com
            return $this->redirect('http://www.google.com');
            // die();
        }else{
            echo 'verification failed';
            //redirect ke clgbankmandiri.com
            // return $this->redirect('http://www.clgbankmandiri.com');
            //sementara ke google.com
            return $this->redirect('http://www.google.com');
            // die();
        }
    }
}
