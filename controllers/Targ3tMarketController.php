<?php

namespace app\controllers;

use Yii;
use app\models\Targ3tMarket;
use app\models\Targ3tMarketSearch;
use app\models\Priviledge;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Excel;
use \yii\web\Response;


/**
 * Targ3tMarketController implements the CRUD actions for Targ3tMarket model.
 */
class Targ3tMarketController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rules,$action){
                            return $this->isAccepted($rules,$action);
                        }
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    private function isAccepted($rules,$action){
        //jika dia admin cek table priviledge
        $status = Yii::$app->user->identity->status;
        if(in_array($status, [5])){
            $cek = Priviledge::find()->where(['nama' => 'Target Market','status'=>1])->one();
            if(!empty($cek)){
                return true;
            }else{
                return false;
            }
        }
        //jika approver return true
        if(in_array($status, [10])){
            return true;
        }else{
        //jika selain itu return false
            return false;
        }
        
    }

    /**
     * Lists all Targ3tMarket models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Targ3tMarketSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $excelModel = Excel::find()->where(['class'=>'targetMarket'])->one();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'excelModel' => $excelModel,
        ]);
    }

    /**
     * Displays a single Targ3tMarket model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->redirect(['index']);
        
        $model = $this->findModel($id);
        $providerKprTarg3tMarket = new \yii\data\ArrayDataProvider([
            'allModels' => $model->kprTarg3tMarkets,
        ]);
        $providerKtaTarg3tMarket = new \yii\data\ArrayDataProvider([
            'allModels' => $model->ktaTarg3tMarkets,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerKprTarg3tMarket' => $providerKprTarg3tMarket,
            'providerKtaTarg3tMarket' => $providerKtaTarg3tMarket,
        ]);
    }

    /**
     * Creates a new Targ3tMarket model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Targ3tMarket();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Targ3tMarket model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Targ3tMarket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(in_array($id,[Targ3tMarket::Code_Wiraswasta,Targ3tMarket::Code_Nasabah_Prioritas,Targ3tMarket::Code_Nasabah_Payroll,Targ3tMarket::Code_Pegawai])){
            return;
        }
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    public function actionBulkDelete()
    {        
        try{

            $request = Yii::$app->request;
            $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
            foreach ( $pks as $pk ) {
                $model = $this->findModel($pk);
                $model->delete();
            }

            if($request->isAjax){
                /*
                *   Process for ajax request
                */
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['forceClose'=>true,'forceReload'=>'#kv-pjax-container-targ3t-market'];
            }else{
                /*
                *   Process for non-ajax request
                */
                return $this->redirect(['index']);
            }
        }catch(\yii\db\IntegrityException $e){
            \Yii::$app->getSession()->setFlash('danger', 'Cannot delete because linked');   
            
            return $this->redirect(['index']);
        }
       
    }

    public function actionDeleteAll()
    {
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $model = null;
        try{
            $models = Targ3tMarket::find()->all();
            foreach ($models as $item) {
                # code...
                $model = $item;
                $item->delete();
            }
            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');
            $transaction->commit();
        } catch (\Exception $e) {
            if(!empty($model)){

                \Yii::$app->getSession()->setFlash('error', $model->name." gagal dihapus karena masih ada di KPR");   
            }else{
                
                \Yii::$app->getSession()->setFlash('error', "error");   
            }
            $transaction->rollBack();
            // throw $e;
        } catch (\Throwable $e) {
            \Yii::$app->getSession()->setFlash('error', $e->getMessage());   
            $transaction->rollBack();
            // throw $e;
        }
        return $this->redirect(['index']);
    }

    
    /**
     * Finds the Targ3tMarket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Targ3tMarket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Targ3tMarket::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for KprTarg3tMarket
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddKprTarg3tMarket()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('KprTarg3tMarket');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formKprTarg3tMarket', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for KtaTarg3tMarket
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddKtaTarg3tMarket()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('KtaTarg3tMarket');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formKtaTarg3tMarket', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImport(){
        $request = Yii::$app->request;
        if($request->isGet){
            return $this->redirect(['index']);

        }else{
            $modelExcel = Excel::findOne($request->post('Excel')['id']);
            $modelExcel->load($request->post());
            if(!$modelExcel->upload()){
                \Yii::$app->getSession()->setFlash('error', 'Upload failed');   
                return $this->redirect(['index']);
            }

            $data = $modelExcel->importExcel();
            if(empty($data)){
                \Yii::$app->getSession()->setFlash('error', 'wrong format');   
                
            }
            // var_dump($data);
            // die();
            $this->importData($data);
            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');   
            return $this->redirect(['index']);
        }
    }

    private function importData($data){
        $groupModel = null;
        foreach ($data as $item) {
            $model = Targ3tMarket::find()->where(['name'=>$item["Name"]])->one();
            if(empty($model)){
                $model = new Targ3tMarket();
            }
            $model->name = $item["Name"];
            $model->desc = $item["Description"];
            $model->save(false);
        }
    }

    public function actionExport(){
        \moonland\phpexcel\Excel::export([
            'models' => Targ3tMarket::find()->orderBy('name')->all(),
            'fileName' => 'targetmarket',
                'columns' => [
                    [
                        'attribute'=>'name',
                        'header' => 'Name'
                    ],
                    [
                        'attribute'=>'desc',
                        'header' => 'Description'
                    ],
                ],
                'headers' => [
                    'created_at' => 'Date Created Content',
                ],
        ]);        
    }
}
