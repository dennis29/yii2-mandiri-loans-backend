<?php

namespace app\controllers;

use Yii;
use app\models\KprDeveloperTier;
use app\models\KprDeveloperTierSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KprDeveloperTierController implements the CRUD actions for KprDeveloperTier model.
 */
class KprDeveloperTierController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all KprDeveloperTier models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KprDeveloperTierSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KprDeveloperTier model.
     * @param string $kpr_id
     * @param string $developer_tier_id
     * @return mixed
     */
    public function actionView($kpr_id, $developer_tier_id)
    {
        $model = $this->findModel($kpr_id, $developer_tier_id);
        return $this->render('view', [
            'model' => $this->findModel($kpr_id, $developer_tier_id),
        ]);
    }

    /**
     * Creates a new KprDeveloperTier model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KprDeveloperTier();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'kpr_id' => $model->kpr_id, 'developer_tier_id' => $model->developer_tier_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing KprDeveloperTier model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kpr_id
     * @param string $developer_tier_id
     * @return mixed
     */
    public function actionUpdate($kpr_id, $developer_tier_id)
    {
        $model = $this->findModel($kpr_id, $developer_tier_id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'kpr_id' => $model->kpr_id, 'developer_tier_id' => $model->developer_tier_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing KprDeveloperTier model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kpr_id
     * @param string $developer_tier_id
     * @return mixed
     */
    public function actionDelete($kpr_id, $developer_tier_id)
    {
        $this->findModel($kpr_id, $developer_tier_id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the KprDeveloperTier model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kpr_id
     * @param string $developer_tier_id
     * @return KprDeveloperTier the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kpr_id, $developer_tier_id)
    {
        if (($model = KprDeveloperTier::findOne(['kpr_id' => $kpr_id, 'developer_tier_id' => $developer_tier_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
