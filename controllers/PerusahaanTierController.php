<?php

namespace app\controllers;

use Yii;
use app\models\PerusahaanTier;
use app\models\PerusahaanTierSearch;
use app\models\Priviledge;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * PerusahaanTierController implements the CRUD actions for PerusahaanTier model.
 */
class PerusahaanTierController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rules,$action){
                            return $this->isAccepted($rules,$action);
                        }
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    private function isAccepted($rules,$action){
        //jika dia admin cek table priviledge
        $status = Yii::$app->user->identity->status;
        if(in_array($status, [5])){
            $cek = Priviledge::find()->where(['nama' => 'Tier Perusahaan List KTA','status'=>1])->one();
            if(!empty($cek)){
                return true;
            }else{
                return false;
            }
        }
        //jika approver return true
        if(in_array($status, [10])){
            return true;
        }else{
        //jika selain itu return false
            return false;
        }
        
    }

    /**
     * Lists all PerusahaanTier models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new PerusahaanTierSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single PerusahaanTier model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {   
        return $this->redirect(['index']);

        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "PerusahaanTier #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new PerusahaanTier model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new PerusahaanTier();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new PerusahaanTier",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new PerusahaanTier",
                    'content'=>'<span class="text-success">Create PerusahaanTier success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Create new PerusahaanTier",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing PerusahaanTier model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            $post = $request->post();
            if($request->isGet){
                return [
                    'title'=> "Update PerusahaanTier #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($post) && $model->save()){
                // $sesudah= $post['PerusahaanTier'];
                // $sesudah['id'] = $id;
                // $model->triggerNotifikasi(json_encode($sesudah));
                // $model2 = $this->findModel($id);
                // $model2->is_approved = 0;
                // $model2->save(false);
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
                
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "PerusahaanTier #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update PerusahaanTier #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            $post = $request->post();
            
            if ($model->load($post) && $model->save()) {
            //     //
            //     $sesudah= $post['PerusahaanTier'];
            //     $sesudah['id'] = $id;
            //     $model->triggerNotifikasi(json_encode($sesudah));
            //     $model2 = $this->findModel($id);
            //     $model2->is_approved = 0;
            //     $model2->save();
            
                return $this->redirect(['index']);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing PerusahaanTier model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing PerusahaanTier model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        try{

            $request = Yii::$app->request;
            $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
            foreach ( $pks as $pk ) {
                $model = $this->findModel($pk);
                $model->delete();
            }

            if($request->isAjax){
                /*
                *   Process for ajax request
                */
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
            }else{
                /*
                *   Process for non-ajax request
                */
                return $this->redirect(['index']);
            }
        }catch(\yii\db\IntegrityException $e){
            \Yii::$app->getSession()->setFlash('danger', 'Cannot delete because linked');   
            
            return $this->redirect(['index']);
        }
       
    }

    public function actionDeleteAll()
    {
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $model = null;
        try{
            $models = PerusahaanTier::find()->all();
            foreach ($models as $item) {
                # code...
                $model = $item;
                $item->delete();
            }
            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');
            $transaction->commit();
        } catch (\Exception $e) {
            if(!empty($model)){

                \Yii::$app->getSession()->setFlash('error', $model->nama." gagal dihapus karena masih ada di Perusahaan List");   
            }else{
                
                \Yii::$app->getSession()->setFlash('error', "error");   
            }
            $transaction->rollBack();
            // throw $e;
        } catch (\Throwable $e) {
            \Yii::$app->getSession()->setFlash('error', $e->getMessage());   
            $transaction->rollBack();
            // throw $e;
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the PerusahaanTier model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PerusahaanTier the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PerusahaanTier::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
