<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\Region;
use app\models\Area;
use app\models\SignupForm;
use app\models\UserSearch;
use app\models\UserMobileSearch;
use app\models\Priviledge;
use app\models\LogUser;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\helpers\Json;
use app\models\Excel;


/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions'=>['mobile','create-mobile','update','delete','bulk-delete','subcat','role','export-mobile'],
                        'roles' => ['@'],
                        'matchCallback' => function($rules,$action){
                            return $this->isAcceptedMobile($rules,$action);
                        }
                    ],
                    [
                        'allow' => true,
                        'actions'=>['index','create','update','delete','subcat','bulk-delete','role','export'],
                        'roles' => ['@'],
                        'matchCallback' => function($rules,$action){
                            return $this->isAccepted($rules,$action);
                        }
                    ],
                    [
                        'allow' => true,
                        'actions'=>['hash-this'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    private function isAccepted($rules,$action){
        //jika dia admin cek table priviledge
        $status = Yii::$app->user->identity->status;
        if(in_array($status, [5])){
            //ifaction mobile
            //else action non mobile
            $cek = Priviledge::find()->where(['nama' => 'Admin','status'=>1])->one();
            if(!empty($cek)){
                return true;
            }else{
                return false;
            }
        }
        //jika approver return true
        if(in_array($status, [10])){
            return true;
        }else{
        //jika selain itu return false
            return false;
        }
        
    }

    private function isAcceptedMobile($rules,$action){
        //jika dia admin cek table priviledge
        $status = Yii::$app->user->identity->status;
        if(in_array($status, [5])){
            //ifaction mobile
            //else action non mobile
            $cek = Priviledge::find()->where(['nama' => 'User Terdaftar','status'=>1])->one();
            if(!empty($cek)){
                return true;
            }else{
                return false;
            }
        }
        //jika approver return true
        if(in_array($status, [10])){
            return true;
        }else{
        //jika selain itu return false
            return false;
        }
        
    }

    public function actionHashThis(){
        $post = Yii::$app->request->post();
        $password_hashing = hash('sha256', md5($post['password']));
        return $password_hashing;
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'is_mobile'=>false
        ]);	
    }

    public function actionMobile()
    {    
        $searchModel = new UserMobileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'is_mobile'=>true
        ]);
    }


    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "User #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new User model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                // if (Yii::$app->getUser()->login($user)) {
                //     return $this->goHome();
                LogUser::logHitUser(LogUser::CREATE_ADMIN,LogUser::SUCCESS,Yii::$app->user->identity->nip,Yii::$app->user->identity->id);
                \Yii::$app->getSession()->setFlash('success', 'Your data has been craeted');   
                return $this->redirect(['index']);
                // }
            }
        }
            // Yii::$app->response->format = Response::FORMAT_JSON;

        // return [
        //             'title'=> "Create User",
        //             'content'=>$this->renderAjax('create', [
        //                 'model' => $model,
        //             ]),
        //     ];
        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            
            return [
                        'title'=> "Create User",
                        'content'=>$this->renderAjax('create', [
                            'model' => $model,
                        ]),
                ];
        }else{

            return $this->render('create', [
                'model' => $model,
            ]);
        }
       
    }

    public function actionCreateMobile()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signupMobile()) {
                // if (Yii::$app->getUser()->login($user)) {
                //     return $this->goHome();
                LogUser::logHitUser(LogUser::CREATE_MOBILE,LogUser::SUCCESS,Yii::$app->user->identity->nip,Yii::$app->user->identity->id);
                \Yii::$app->getSession()->setFlash('success', 'Your data has been craeted');   
                return $this->redirect(['mobile']);
                // }
            }
        }
            // Yii::$app->response->format = Response::FORMAT_JSON;

        // return [
        //             'title'=> "Create User",
        //             'content'=>$this->renderAjax('create', [
        //                 'model' => $model,
        //             ]),
        //     ];
        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            
            return [
                        'title'=> "Create User",
                        'content'=>$this->renderAjax('create', [
                            'model' => $model,
                        ]),
                ];
        }else{

            return $this->render('create', [
                'model' => $model,
            ]);
        }
       
    }

    public function actionUpdate($id)
    {
        $modelUser = $this->findModel($id);       

        $model = new SignupForm();
        $model->nip = $modelUser->nip;    
        $model->email = $modelUser->email;    
        $model->name = $modelUser->name;    
        $model->dob = $modelUser->dob;    
        $model->phone_number = $modelUser->phone_number;    
        $model->region_id = $modelUser->region_id;    
        $model->area_id = $modelUser->area_id;   
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->myupdate($id)) {
                // if (Yii::$app->getUser()->login($user)) {
                LogUser::logHitUser(LogUser::UPDATE_ADMIN,LogUser::SUCCESS,Yii::$app->user->identity->nip,Yii::$app->user->identity->id);
                \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');   
                if(in_array($user->status,[User::STATUS_MOBILE,User::STATUS_VERIFIED])){
                    //     return $this->goHome();
                return $this->redirect(['mobile']);    
                }else{
                //     return $this->goHome();
                return $this->redirect(['index']);
                }
            }
        }
        if(Yii::$app->request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                        'title'=> "Update User",
                        'content'=>$this->renderAjax('update', [
                            'model' => $model,
                            'status' =>$modelUser->status,
                        ]),
                ];
        }else{

            return $this->render('create', [
                'model' => $model,
                'status' =>$modelUser->status,
            ]);
        }
       
    }

    /**
     * Updates an existing User model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate2($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update User #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "User #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update User #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionRole($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id); 
        Yii::$app->response->format = Response::FORMAT_JSON;
        if(in_array($model->status,[10,5,12])){
            $list = [10=>'Approver',5=>'Admin',12=>'Admin Region'];
            
        }else{
            $list = [20=>'Not Verified',30=>'Verified'];

        }
        if($request->isGet){
            return [
            'title'=> "Change Role",
            'content'=>$this->renderAjax('changeRole', [
                'model' => $model,
                'list'=>$list
                ]),
            'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
            Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
            ];         
        }else if($model->load($request->post()) && $model->save()){
            LogUser::logHitUser(LogUser::CHANGEROLE_ADMIN,LogUser::SUCCESS,Yii::$app->user->identity->nip,Yii::$app->user->identity->id);
            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];

        }        
    }

    /**
     * Delete an existing User model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();
        LogUser::logHitUser(LogUser::DELETE_ADMIN,LogUser::SUCCESS,Yii::$app->user->identity->nip,Yii::$app->user->identity->id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing User model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSubcat() {

        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
                $regionModel = Region::findOne($cat_id);
                $out = $regionModel->getAreaOut(); 
            // the getSubCatList function will query the database based on the
            // cat_id and return an array like below:
            // [
            //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
            //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
            // ]
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    public function actionExport(){
        ob_end_clean();
        
        \moonland\phpexcel\Excel::export([
            'models' => User::find()->orderBy('Nip')->where(['status'=>[5,10,12]])->all(),
            'fileName' => 'useradmin',
                'columns' => [
                    [
                        'attribute'=>'nip',
                        'header' => 'Nip'
                    ],
                    [
                        'attribute'=>'name',
                        'header' => 'Nama'
                    ],
                    [
                        'attribute'=>'email',
                        'header' => 'Email'
                    ],
                    [
                        'attribute'=>'phone_number',
                        'header' => 'Phone_Number'
                    ],
                    [
                        'attribute'=>'dob',
                        'header' => 'DoB',
                        'format' => ['date', 'php:d-m-Y']
                    ],
                    [
                        'attribute'=>'area_id',
                        'header' => 'Area',
                        'format' => 'text',
                             'value' => function($model) {
                                if(!empty($model->area)){
                                    return $model->area->name;
                                }else{
                                    return "";
                                }
                            }
                    ],
                    [
                            'attribute' => 'region_id',
                            'header' => 'Region',
                            'format' => 'text',
                             'value' => function($model) {
                                if(!empty($model->region)){
                                    return $model->region->name;
                                }else{
                                    return "";
                                }
                            }
                    ],
                    [
                            'attribute' => 'status',
                            'header' => 'Status',
                            'format' => 'text',
                            'value'=>function($model){
                                if($model->status==5){
                                    return "Admin";

                                }
                                if($model->status==12){
                                    return "Admin Region";

                                }
                                if($model->status==10){
                                    return "Approver";
                                }else 
                                if($model->status==30){
                                    return "Verified";

                                }else if($model->status==20){
                                    return "Not Verified";

                                }
                            }
                    ],
                    [
                        'attribute'=>'created_at',
                        'header' => 'Timestamp',
                        'format' => 'datetime'
                    ],
                ],
                'headers' => [
                    'created_at' => 'Date Created Content',
                ],
        ]);        
    }

    public function actionExportMobile(){
        ob_end_clean();
        
        \moonland\phpexcel\Excel::export([
            'models' => User::find()->orderBy('Nip')->where(['status'=>[20,30]])->all(),
            'fileName' => 'userterdaftar',
                'columns' => [
                    [
                        'attribute'=>'nip',
                        'header' => 'Nip'
                    ],
                    [
                        'attribute'=>'name',
                        'header' => 'Nama'
                    ],
                    [
                        'attribute'=>'email',
                        'header' => 'Email'
                    ],
                    [
                        'attribute'=>'phone_number',
                        'header' => 'Phone_Number'
                    ],
                    [
                        'attribute'=>'dob',
                        'header' => 'DoB',
                        'format' => ['date', 'php:d-m-Y']
                    ],
                    [
                        'attribute'=>'area_id',
                        'header' => 'Area',
                        'format' => 'text',
                             'value' => function($model) {
                                if(!empty($model->area)){
                                    return $model->area->name;
                                }else{
                                    return "";
                                }
                            }
                    ],
                    [
                            'attribute' => 'region_id',
                            'header' => 'Region',
                            'format' => 'text',
                             'value' => function($model) {
                                if(!empty($model->region)){
                                    return $model->region->name;
                                }else{
                                    return "";
                                }
                            }
                    ],
                    [
                            'attribute' => 'status',
                            'header' => 'Status',
                            'format' => 'text',
                            'value'=>function($model){
                                if($model->status==30){
                                    return "Verified";
                                }elseif($model->status==20){
                                    return "Not Verified";
                                }
                            }
                    ],
                    [
                        'attribute'=>'created_at',
                        'header' => 'Timestamp',
                        'format' => 'datetime'
                    ],
                ],
                'headers' => [
                    'created_at' => 'Date Created Content',
                ],
        ]);        
    }
}