<?php

namespace app\controllers;

use Yii;
use app\models\Excel;
use app\models\Perusahaan;
use app\models\PerusahaanGroup;
use app\models\PerusahaanTier;
use app\models\PerusahaanSearch;
use app\models\Priviledge;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;


/**
 * PerusahaanController implements the CRUD actions for Perusahaan model.
 */
class PerusahaanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rules,$action){
                            return $this->isAccepted($rules,$action);
                        }
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    private function isAccepted($rules,$action){
        //jika dia admin cek table priviledge
        $status = Yii::$app->user->identity->status;
        if(in_array($status, [5])){
            $cek = Priviledge::find()->where(['nama' => 'Perusahaan List KTA','status'=>1])->one();
            if(!empty($cek)){
                return true;
            }else{
                return false;
            }
        }
        //jika approver return true
        if(in_array($status, [10])){
            return true;
        }else{
        //jika selain itu return false
            return false;
        }
        
    }

    /**
     * Lists all Perusahaan models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new PerusahaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $excelModel = Excel::find()->where(['class'=>'perusahaan'])->one();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'excelModel' => $excelModel,
        ]);
    }


    /**
     * Displays a single Perusahaan model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {   
        return $this->redirect(['index']);
        
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Perusahaan #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Perusahaan model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Perusahaan();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            $post = $request->post();

            if($request->isGet){
                return [
                    'title'=> "Create new Perusahaan",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($post) && $model->save()){
                // $sesudah= $post['Perusahaan'];
                // if(!empty($post['Perusahaan'])){
                //     $sesudah['perusahaanTier'] = PerusahaanTier::find()->where(['id'=>$sesudah['perusahaan_tier_id']])->asArray()->one();
                // }
                // if(!empty($post['Perusahaan'])){ 
                //     $sesudah['perusahaanGroup'] = PerusahaanGroup::find()->where(['id'=>$sesudah['perusahaan_group_id']])->asArray()->one();
                // }
                // $model->triggerNotifikasi(json_encode($sesudah),'create');
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new Perusahaan",
                    'content'=>'<span class="text-success">Create Perusahaan success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Create new Perusahaan",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            $post = $request->post();

            if ($model->load($post) && $model->save()) {
                // $sesudah= $post['Perusahaan'];
                // if(!empty($post['Perusahaan'])){
                //     $sesudah['perusahaanTier'] = PerusahaanTier::find()->where(['id'=>$sesudah['perusahaan_tier_id']])->asArray()->one();
                // }
                // if(!empty($post['Perusahaan'])){ 
                //     $sesudah['perusahaanGroup'] = PerusahaanGroup::find()->where(['id'=>$sesudah['perusahaan_group_id']])->asArray()->one();
                // }
                // // if(!empty($model->perusahaan_tier_id)){
                // //     $item = $model->perusahaan_tier_id;
                // //     $sesudah['perusahaanTier'] = ArrayHelper::toArray($item);
                // // }

                // // if(!empty($model->perusahaan_group_id)){
                // //     $item = $model->perusahaan_group_id;
                // //     $sesudah['perusahaanGroup'] = ArrayHelper::toArray($item);
                // // }
                // $model->triggerNotifikasi(json_encode($sesudah),'create');
                return $this->redirect(['index']);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Perusahaan model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            $post = $request->post();
            if($request->isGet){
                return [
                    'title'=> "Update Perusahaan #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($post) && $model->save()){
                //
                // $sesudah= $post['Perusahaan'];
                // $sesudah['id'] = $id;
                // if(!empty($post['Perusahaan'])){
                //     $sesudah['perusahaanTier'] = PerusahaanTier::find()->where(['id'=>$sesudah['perusahaan_tier_id']])->asArray()->one();
                // }
                // if(!empty($post['Perusahaan'])){ 
                //     $sesudah['perusahaanGroup'] = PerusahaanGroup::find()->where(['id'=>$sesudah['perusahaan_group_id']])->asArray()->one();
                // }
                // $model->triggerNotifikasi(json_encode($sesudah));
                // $model2 = $this->findModel($id);
                // $model2->is_approved = 0;
                // $model2->save();
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
                
                
            }else{

                 return [
                    'title'=> "Update Perusahaan #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            $post = $request->post();
            
            if ($model->load($post) && $model->save()) {
                //
                // $sesudah= $post['Perusahaan'];
                // $sesudah['id'] = $id;
                // if(!empty($post['Perusahaan'])){
                //     $sesudah['perusahaanTier'] = PerusahaanTier::find()->where(['id'=>$sesudah['perusahaan_tier_id']])->asArray()->one();
                // }
                // if(!empty($post['Perusahaan'])){ 
                //     $sesudah['perusahaanGroup'] = PerusahaanGroup::find()->where(['id'=>$sesudah['perusahaan_group_id']])->asArray()->one();
                // }
                // $model->triggerNotifikasi(json_encode($sesudah));
                // $model2 = $this->findModel($id);
                // $model2->is_approved = 0;
                // $model2->save();

                return $this->redirect(['index']);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Perusahaan model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Perusahaan model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        try{

            $request = Yii::$app->request;
            $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
            foreach ( $pks as $pk ) {
                $model = $this->findModel($pk);
                $model->delete();
            }

            if($request->isAjax){
                /*
                *   Process for ajax request
                */
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
            }else{
                /*
                *   Process for non-ajax request
                */
                return $this->redirect(['index']);
            }
        }catch(\yii\db\IntegrityException $e){
            \Yii::$app->getSession()->setFlash('danger', 'Cannot delete because linked');   
            
            return $this->redirect(['index']);
        }
       
    }

    public function actionDeleteAll()
    {
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $model = null;
        try{
            $models = Perusahaan::find()->all();
            foreach ($models as $item) {
                # code...
                $model = $item;
                $item->delete();
            }
            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');
            $transaction->commit();
        } catch (\Exception $e) {
            if(!empty($model)){

                \Yii::$app->getSession()->setFlash('error', $model->nama." gagal dihapus");   
            }else{
                
                \Yii::$app->getSession()->setFlash('error', "error");   
            }
            $transaction->rollBack();
            // throw $e;
        } catch (\Throwable $e) {
            \Yii::$app->getSession()->setFlash('error', $e->getMessage());   
            $transaction->rollBack();
            // throw $e;
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Perusahaan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Perusahaan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Perusahaan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImport(){
        $request = Yii::$app->request;
        if($request->isGet){
            return $this->redirect(['index']);

        }else{
            $modelExcel = Excel::findOne($request->post('Excel')['id']);
            $modelExcel->load($request->post());
            if(!$modelExcel->upload()){
                \Yii::$app->getSession()->setFlash('error', 'Upload failed');   
                return $this->redirect(['index']);
            }

            $data = $modelExcel->importExcel();
            if(empty($data)){
                \Yii::$app->getSession()->setFlash('error', 'wrong format');   
                
            }
            // var_dump($data);
            // die();
            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try{
                Perusahaan::deleteAll();
                $this->importData($data);
                \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');
                $transaction->commit();   
            } catch (\Exception $e) {
                \Yii::$app->getSession()->setFlash('error', 'error');   
                $transaction->rollBack();
                // throw $e;
            } catch (\Throwable $e) {
                \Yii::$app->getSession()->setFlash('error', 'error');   
                $transaction->rollBack();
                // throw $e;
            }
            return $this->redirect(['index']);
        }
    }

    private function importDataInit($data){
        $groupModel = null;
        foreach ($data as $item) {
            if(!empty($item["no"])){
                $groupModel = PerusahaanGroup::find()->where(['name'=>$item["perusahaan"]])->one();
                if(empty($groupModel)){
                    $groupModel = new PerusahaanGroup();
                }
                $groupModel->name = $item["perusahaan"];
                $groupModel->save(false);
            }else{
                $model = Perusahaan::find()->where(['name'=>$item["perusahaan"]])->one();
                if(empty($model)){
                    $model = new Perusahaan();
                }
                $model->name = $item["perusahaan"];
                $model->perusahaan_group_id = $groupModel->id;
                $model->save(false);
            }
        }
    }

    private function importData($data){
        $groupModel = null;
        foreach ($data as $item) {
            $model = Perusahaan::find()->where(['name'=>$item["Name"]])->one();
            if(empty($model)){
                $model = new Perusahaan();
            }
            $groupModel = PerusahaanGroup::find()->where(['name'=>$item["Group"]])->one();
            if(empty($groupModel)){
                $groupModel = new PerusahaanGroup();
                $groupModel->name = $item["Group"];
                $groupModel->save();
            }

            $tierModel = PerusahaanTier::find()->where(['nama'=>$item["Tier"]])->one();
            if(empty($tierModel)){
                $tierModel = new PerusahaanTier();
                $tierModel->nama = $item["Tier"];
                $tierModel->save();
            }
            $model->name = $item["Name"];
            // $model->max_limit = $item["Max_Limit"];
            // $model->max_tenor = $item["Max_Tenor"];
            // $model->dbr = $item["Dbr"];
            if(!empty($groupModel)){
                $model->perusahaan_group_id = $groupModel->id;
            }
            if(!empty($tierModel)){
                $model->perusahaan_tier_id = $tierModel->id;
            }
            $model->save(false);
        }
    }

    public function actionExport(){
        ob_end_clean();
        
        \moonland\phpexcel\Excel::export([
            'models' => Perusahaan::find()->with(['perusahaanGroup','perusahaanTier'])->orderBy('perusahaan_group_id')->all(),
            'fileName' => 'perusahaan',
                'columns' => [
                    [
                            'attribute' => 'name',
                            'header' => 'Name',
                            'format' => 'text',
                            // 'value' => function($model) {
                            //     return ExampleClass::removeText('example', $model->content);
                            // },
                    ],
                    [
                            'attribute' => 'perusahaan_group_id',
                            'header' => 'Group',
                            'format' => 'text',
                             'value' => function($model) {
                                if(empty($model->perusahaanGroup)){
                                    return "";
                                }else{
                                    return $model->perusahaanGroup->name;
                                }
                            }
                    ],
                    [
                            'header' => 'Tier',
                            'format' => 'text',
                             'value' => function($model) {
                                 if(empty($model->perusahaanTier)){
                                    return "";
                                }   else{
                                    return $model->perusahaanTier->nama;
                                }
                            }
                    ],
                    // [
                    //     'attribute'=>'max_limit',
                    //     'header' => 'Max_Limit'
                    // ],
                    // [
                    //     'attribute'=>'max_tenor',
                    //     'header' => 'Max_Tenor'
                    // ],
                    // 'dbr',
                ],
                'headers' => [
                    'created_at' => 'Date Created Content',
                ],
        ]);        
    }
}
