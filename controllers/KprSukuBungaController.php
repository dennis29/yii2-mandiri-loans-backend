<?php

namespace app\controllers;

use Yii;
use app\models\KprSukuBunga;
use app\models\KprSukuBungaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KprSukuBungaController implements the CRUD actions for KprSukuBunga model.
 */
class KprSukuBungaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all KprSukuBunga models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KprSukuBungaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KprSukuBunga model.
     * @param string $kpr_id
     * @param integer $suku_bunga_id
     * @return mixed
     */
    public function actionView($kpr_id, $suku_bunga_id)
    {
        $model = $this->findModel($kpr_id, $suku_bunga_id);
        return $this->render('view', [
            'model' => $this->findModel($kpr_id, $suku_bunga_id),
        ]);
    }

    /**
     * Creates a new KprSukuBunga model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KprSukuBunga();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'kpr_id' => $model->kpr_id, 'suku_bunga_id' => $model->suku_bunga_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing KprSukuBunga model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kpr_id
     * @param integer $suku_bunga_id
     * @return mixed
     */
    public function actionUpdate($kpr_id, $suku_bunga_id)
    {
        $model = $this->findModel($kpr_id, $suku_bunga_id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'kpr_id' => $model->kpr_id, 'suku_bunga_id' => $model->suku_bunga_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing KprSukuBunga model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kpr_id
     * @param integer $suku_bunga_id
     * @return mixed
     */
    public function actionDelete($kpr_id, $suku_bunga_id)
    {
        $this->findModel($kpr_id, $suku_bunga_id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the KprSukuBunga model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kpr_id
     * @param integer $suku_bunga_id
     * @return KprSukuBunga the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kpr_id, $suku_bunga_id)
    {
        if (($model = KprSukuBunga::findOne(['kpr_id' => $kpr_id, 'suku_bunga_id' => $suku_bunga_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
