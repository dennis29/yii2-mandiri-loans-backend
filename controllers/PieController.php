<?php

namespace app\controllers;

use Yii;
use app\models\Income;
use app\models\LogDashboard;
use app\models\LogDashboardSearch;
use app\models\IncomeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use app\models\Priviledge;
use app\models\CsvForm;
use app\models\Tbl_pie;
use yii\helpers\Html;
use app\models\Excel;
use yii\web\UploadedFile;

/**
 * IncomeController implements the CRUD actions for Income model.
 */
class PieController extends Controller
{
	 public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'except' => [],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rules,$action){
                            return $this->isAccepted($rules,$action);
                        } 
                    ],
                ],
            ],
        ];
    }
    
    private function isAccepted($rules,$action){
        //jika dia admin cek table priviledge
        $status = Yii::$app->user->identity->status;
        if(in_array($status, [5])){
            $cek = Priviledge::find()->where(['nama' => 'Dashboard','status'=>1])->one();
            if(!empty($cek)){
                return true;
            }else{
                return false;
            }
        }
        //jika approver return true
        if(in_array($status, [10])){
            return true;
        }else{
        //jika selain itu return false
            return false;
        }
        
    }

    /**
     * Lists all Income models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $model = new CsvForm; 
        if($model->load(Yii::$app->request->post())){
			$file = UploadedFile::getInstance($model,'file');
			$filename = 'Data.'.$file->extension;
			$upload = $file->saveAs('uploads/'.$filename);
			if($upload){
				define('CSV_PATH','uploads/');
				$csv_file = CSV_PATH . $filename;
				$filecsv = file($csv_file);
                $array = array();
                $i=0;
				foreach($filecsv as $data){
					if($i>0)
					{
						$hasil = array_filter(explode(",",$data)); 
						$modelnew = new Tbl_pie;
						$modelnew->STATUS = $hasil[0];
						$modelnew->TAHUN = $hasil[1];
						$modelnew->CATEGORY = $hasil[2];
						$modelnew->Bandtier = $hasil[3];
						$value='';					
						for($j=4;$j<=count($hasil);$j++)
						{
						 $value .= $hasil[$j];
						}
						
						$modelnew->VALUE = str_replace('"','',$value);
						$modelnew->save(); 
					}
					
					$i++;
				}
				unlink('uploads/'.$filename);
			}
		}

        $searchModel = new LogDashboardSearch();
		$excelModel = Excel::find()->where(['class'=>'kta'])->one();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $LogDashboardSearch = Yii::$app->request->get('LogDashboardSearch');
        $start_date = $LogDashboardSearch['start_date'];
        $end_date = $LogDashboardSearch['end_date'];
        // var_dump($LogDashboardSearch['start_date']);
        // $logs = $dataProvider->getModels();
        // var_dump($dataProvider->query->andWhere(['code'=>LogDashboard::Code_KTA]));
        // die();
        $qqq = clone $dataProvider->query;
        $log_hit_kkb = $qqq->andWhere(['code'=>LogDashboard::Code_KKB])->count();
        $qqq = clone $dataProvider->query;
        $log_hit_kpr = $qqq->andWhere(['code'=>LogDashboard::Code_KPR])->count();
        $qqq = clone $dataProvider->query;
        $log_hit_kta = $qqq->andWhere(['code'=>LogDashboard::Code_KTA])->count();
        $log_total = $log_hit_kpr+$log_hit_kkb+$log_hit_kta;
        // $log_hit_kpr
        // $log_hit_kkb
        // $log_total
        // var_dump($log_total);
        $min_tanggal = LogDashboard::find()->min('created_at');
        $today = LogDashboard::find()->max('created_at');
        // var_dump($today);die();
        if(empty($start_date)){
            $date1=date_create($min_tanggal);
        }else{
            $date1=date_create($start_date);

        }
        if(empty($end_date)){
            $date2=date_create($today);
        }else{
            
            $date2=date_create($end_date);
        }
        $diff=date_diff($date1,$date2);
// echo $diff->format("%R%a days");
        // var_dump(date_timestamp_get($date1));
        // die();
        $countHari = $diff->format("%a");
        $categories_data = $this->getCategories($date1,$date2);
        $data_kpr = $this->getData(LogDashboard::Code_KPR,$dataProvider->query,$date1,$date2);
        $data_kta = $this->getData(LogDashboard::Code_KTA,$dataProvider->query,$date1,$date2);
        $data_kkb = $this->getData(LogDashboard::Code_KKB,$dataProvider->query,$date1,$date2);
        
        // var_dump($data_kta);
        // die();
        return $this->render('index', [
            // 'searchModel' => $searchModel,
            // 'dataProvider' => $dataProvider,
            'log_hit_kkb' => $log_hit_kkb,
            'log_hit_kpr' => $log_hit_kpr,
            'log_hit_kta' => $log_hit_kta,
            'log_total' => $log_total,
            'categories_data' => $categories_data,
            'data_kpr' =>$data_kpr,
            'data_kta' =>$data_kta,
            'data_kkb' =>$data_kkb,
            'searchModel' => $searchModel,
			'excelModel' => $excelModel,
			'model' => $model
        ]);
    }

    private function getData($code,$query,$date1,$date2){
        // $today = date("d");
        $diff=date_diff($date1,$date2);
        // echo $diff->format("%R%a days");
        $countHari = $diff->format("%a")+1;

        
        $today = strtotime('+1 day',date_timestamp_get($date2));
        $yesterday = date_timestamp_get($date2);
        // echo '<br>';
        // var_dump($yesterday);
        // die();

        // echo $today;
        $result = [];
        for ($i=0; $i < $countHari; $i++) { 
            $qqq = clone $query;
            $val = $qqq->andWhere(['code'=>$code])
            ->andWhere(['like','log_dashboard.created_at',date("Y-m-d",$today)])
            // ->andWhere(['>','log_dashboard.created_at',date("Y-m-d",$yesterday)])
            ->count();
            array_push($result, intval($val));
            $today = $yesterday;
            $yesterday = strtotime('-1 day', $today);
            // $val = LogDashboard::find()->one();
            // // $created_day = date("Y-m-d H:i:s",$val->created_at);
            // var_dump(date('d',strtotime($val->created_at)));
            // die();
            // echo $today == date('d',strtotime($val->created_at));
        }
        // var_dump(date("Y-m-d",$yesterday));
        // var_dump($code);
        // var_dump($result);
        // die();
        return array_reverse($result);
    }
    private function getCategories($date1,$date2){
        $diff=date_diff($date1,$date2);
        // echo $diff->format("%a days");
        $countHari = $diff->format("%a")+1;
        $result = [];
        $day = date_timestamp_get($date2);
        // $day = time();
        for ($i=0; $i < $countHari; $i++) { 
            array_push($result, date('d/m/Y',$day));
            $day = strtotime('-1 day', $day);
        }
        return array_reverse($result);
    }

    public function actionExport($date=null){
                $searchModel = new LogDashboardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $LogDashboardSearch = Yii::$app->request->get('LogDashboardSearch');
        
        $start_date = $LogDashboardSearch['start_date'];
        $end_date = $LogDashboardSearch['end_date'];

        $min_tanggal = LogDashboard::find()->min('created_at');
        $today = LogDashboard::find()->max('created_at');
        // var_dump($today);die();
        if(empty($start_date)){
            $date1=date_create($min_tanggal);
        }else{
            $date1=date_create($start_date);

        }
        if(empty($end_date)){
            $date2=date_create($today);
        }else{
            
            $date2=date_create($end_date);
        }
        
        $categories_data = $this->getCategories($date1,$date2);
        $data_kpr = $this->getData(LogDashboard::Code_KPR,$dataProvider->query,$date1,$date2);
        $data_kta = $this->getData(LogDashboard::Code_KTA,$dataProvider->query,$date1,$date2);
        $data_kkb = $this->getData(LogDashboard::Code_KKB,$dataProvider->query,$date1,$date2);

        $filename = 'Data-'.date('YmdGis').'-dashboard.xls';
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=".$filename);
        echo '<table border="1" width="100%">
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Kta</th>
                    <th>KPR</th>
                    <th>KKB</th>
                </tr>
            </thead>';
            for ($i=0; $i < sizeof($categories_data); $i++) { 
                echo '
                    <tr>
                        <td>'.$categories_data[$i].'</td>
                        <td>'.$data_kta[$i].'</td>
                        <td>'.$data_kpr[$i].'</td>
                        <td>'.$data_kkb[$i].'</td>
                    </tr>
                ';
            }
        echo '</table>';
    }

}