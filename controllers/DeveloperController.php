<?php

namespace app\controllers;

use Yii;
use app\models\Developer;
use app\models\DeveloperSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Excel;
use app\models\DeveloperGroup;
use app\models\DeveloperTier;
use app\models\Priviledge;
use yii\helpers\ArrayHelper;


/**
 * DeveloperController implements the CRUD actions for Developer model.
 */
class DeveloperController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],

            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'except' => [],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rules,$action){
                            return $this->isAccepted($rules,$action);
                        } 
                    ],
                ],
            ],
        ];
    }

    private function isAccepted($rules,$action){
        //jika dia admin cek table priviledge
        $status = Yii::$app->user->identity->status;
        if(in_array($status, [5])){
            $cek = Priviledge::find()->where(['nama' => 'Developer List','status'=>1])->one();
            if(!empty($cek)){
                return true;
            }else{
                return false;
            }
        }
        //jika approver return true
        if(in_array($status, [10])){
            return true;
        }else{
        //jika selain itu return false
            return false;
        }
        
    }

    /**
     * Lists all Developer models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new DeveloperSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $excelModel = Excel::find()->where(['class'=>'developer'])->one();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'excelModel' => $excelModel,
        ]);
    }


    /**
     * Displays a single Developer model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {   
        return $this->redirect(['index']);

        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Developer #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }


    /**
     * Creates a new Developer model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Developer();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new Developer",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new Developer",
                    'content'=>'<span class="text-success">Create Developer success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Create new Developer",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Developer model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            $post = $request->post();
            if($request->isGet){
                return [
                    'title'=> "Update Developer #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($post) && $model->save()){
                // $sesudah= $post['Developer'];
                // $sesudah['id'] = $id;
                // if(!empty($post['Developer'])){
                //     $sesudah['developerTier'] = DeveloperTier::find()->where(['id'=>$sesudah['developer_tier_id']])->asArray()->one();
                // }
                // if(!empty($post['Developer'])){ 
                //     $sesudah['developerGroup'] = DeveloperGroup::find()->where(['id'=>$sesudah['developer_group_id']])->asArray()->one();
                // }
                // $model->triggerNotifikasi(json_encode($sesudah));
                // $model2 = $this->findModel($id);
                // $model2->is_approved = 0;
                // $model2->save();
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
                
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Developer #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update Developer #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            $post = $request->post();
            
            if ($model->load($post) && $model->save()) {
                //
                // $sesudah= $post['Developer'];
                // $sesudah['id'] = $id;
                // if(!empty($post['Developer'])){
                //     $sesudah['developerTier'] = DeveloperTier::find()->where(['id'=>$sesudah['developer_tier_id']])->asArray()->one();
                // }
                // if(!empty($post['Developer'])){ 
                //     $sesudah['developerGroup'] = DeveloperGroup::find()->where(['id'=>$sesudah['developer_group_id']])->asArray()->one();
                // }
                // $model->triggerNotifikasi(json_encode($sesudah));
                // $model2 = $this->findModel($id);
                // $model2->is_approved = 0;
                // $model2->save();

                return $this->redirect(['index']);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Developer model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Developer model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        try{

            $request = Yii::$app->request;
            $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
            foreach ( $pks as $pk ) {
                $model = $this->findModel($pk);
                $model->delete();
            }

            if($request->isAjax){
                /*
                *   Process for ajax request
                */
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
            }else{
                /*
                *   Process for non-ajax request
                */
                return $this->redirect(['index']);
            }
        }catch(\yii\db\IntegrityException $e){
            \Yii::$app->getSession()->setFlash('danger', 'Cannot delete because linked');   
            
            return $this->redirect(['index']);
        }
       
    }

    public function actionDeleteAll()
    {
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $model = null;
        try{
            $models = Developer::find()->all();
            foreach ($models as $item) {
                # code...
                $model = $item;
                $item->delete();
            }
            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');
            $transaction->commit();
        } catch (\Exception $e) {
            if(!empty($model)){

                \Yii::$app->getSession()->setFlash('error', $model->nama." gagal dihapus karena masih ada diproyek");   
            }else{
                
                \Yii::$app->getSession()->setFlash('error', "error");   
            }
            $transaction->rollBack();
            // throw $e;
        } catch (\Throwable $e) {
            \Yii::$app->getSession()->setFlash('error', $e->getMessage());   
            $transaction->rollBack();
            // throw $e;
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Developer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Developer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Developer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImport(){
        $request = Yii::$app->request;
        if($request->isGet){
            return $this->redirect(['index']);

        }else{
            $modelExcel = Excel::findOne($request->post('Excel')['id']);
            $modelExcel->load($request->post());
            // var_dump($modelExcel);die();
            if(!$modelExcel->upload()){
                \Yii::$app->getSession()->setFlash('error', 'Upload failed');   
                return $this->redirect(['index']);
            }

            $data = $modelExcel->importExcel();
            if(empty($data)){
                \Yii::$app->getSession()->setFlash('error', 'wrong format');   
                
            }
            // var_dump($data);
            // die();
            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try{
                Developer::deleteAll();
                $this->importData($data);
                \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');   
                $transaction->commit();
            } catch (\Exception $e) {
                // \Yii::$app->getSession()->setFlash('error', $e->getMessage());   
                \Yii::$app->getSession()->setFlash('error', 'Proyek harus kosong');   
                $transaction->rollBack();
                // throw $e;
            } catch (\Throwable $e) {
                \Yii::$app->getSession()->setFlash('error', 'error');   
                $transaction->rollBack();
                // throw $e;
            }
            return $this->redirect(['index']);
        }
    }

    private function importData($data){
        $groupModel = null;
        foreach ($data as $item) {
            $model = Developer::find()->where(['nama'=>$item["Nama"]])->one();
            if(empty($model)){
                $model = new Developer();
            }
            $groupModel = DeveloperGroup::find()->where(['name'=>$item["Group"]])->one();
            if(empty($groupModel)){
                $groupModel = new DeveloperGroup();
                $groupModel->name = $item["Group"];
                $groupModel->save();
            }

            $tierModel = DeveloperTier::find()->where(['nama'=>$item["Tier"]])->one();
            if(empty($tierModel)){
                $tierModel = new Developer();
                $tierModel->nama = $item["Tier"];
                $tierModel->save();
            }
            $model->nama = $item["Nama"];
            if(!empty($groupModel)){
                $model->developer_group_id = $groupModel->id;
            }
            if(!empty($tierModel)){
                $model->developer_tier_id = $tierModel->id;
            }
            $model->save(false);
        }
    }

    public function actionExportNew(){
         $model = Developer::find()->orderBy('developer_group_id')->all();
        $filename = 'Data-'.Date('YmdGis').'-asdad.xls';
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=developer-group.xls");
        echo '<table border="1" width="100%">
            <thead>
                <tr>
                    <th>nama</th>
                    <th>developer-group</th>
                    <th>developer-tier</th>
                   
                </tr>
            </thead>';
            foreach($model as $data){
                $groupName = "";
                if(!empty($data->developerGroup)){
                    $groupName = $data->developerGroup->name;
                    
                }
                $tierName = "";
                if(!empty($data->developerTier)){
                    $tierName = $data->developerTier->nama;
                    
                }
                echo '
                    <tr>
                        <td>'.$data->nama.'</td>
                        <td>'.$groupName.'</td>
                        <td>'.$tierName.'</td>
                    
                ';
               
            }
        echo '</table>';
        // die();

    }
    public function actionExport(){
        ob_end_clean();
        
        \moonland\phpexcel\Excel::export([
            'models' => Developer::find()->with(['developerGroup','developerTier'])->orderBy('developer_group_id')->all(),
            'fileName' => 'developer',
                'columns' => [
                    [
                            'attribute' => 'nama',
                            'header' => 'Nama',
                            'format' => 'raw',
                            // 'value' => function($model) {
                            //     return ExampleClass::removeText('example', $model->content);
                            // },
                    ],
                    [
                            'attribute' => 'developer_group_id',
                            'header' => 'Group',
                            'format' => 'raw',
                            'value' => function($model) {
                                return $model->developerGroup->name;
                            }
                    ],
                    [
                            'header' => 'Tier',
                            'format' => 'raw',
                            'value' => function($model) {
                                 if(empty($model->developerTier)){
                                    return "";
                                }   else{
                                    return $model->developerTier->nama;
                                }
                            }
                    ],
                ],
                'headers' => [
                    'created_at' => 'Date Created Content',
                ],
        ]);        
    }
}
