<?php

namespace app\controllers;

use Yii;
use app\models\Area;
use app\models\Proyek;
use app\models\Developer;
use app\models\DeveloperGroup;
use app\models\DeveloperTier;
use app\models\ProyekSearch;
use app\models\Priviledge;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Excel;
use app\models\Kpr;
use app\models\Region;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * ProyekController implements the CRUD actions for Proyek model.
 */
class ProyekController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        // 'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'roles' => ['@'],
                        'matchCallback' => function($rules,$action){
                            return $this->isAccepted($rules,$action);
                        }
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    private function isAccepted($rules,$action){
        //jika dia admin cek table priviledge
        $status = Yii::$app->user->identity->status;
        if(in_array($status, [5])){
            $cek = Priviledge::find()->where(['nama' => 'Proyek','status'=>1])->one();
            if(!empty($cek)){
                return true;
            }else{
                return false;
            }
        }
        //jika approver return true
        if(in_array($status, [10])){
            return true;
        }else{
        //jika selain itu return false
            return false;
        }
        
    }

    /**
     * Lists all Proyek models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProyekSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $excelModel = Excel::find()->where(['class'=>'proyek'])->one();
        $modelLink = new Proyek();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'excelModel' => $excelModel,
            'model' => $modelLink
        ]);
    }

    /**
     * Displays a single Proyek model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->redirect(['index']);
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Proyek model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Proyek();
        $post = Yii::$app->request->post();
        if ($model->loadAll($post) /*&& $model->saveAll()*/) {
            $sesudah= $post['Proyek'];
            if(!empty($post['Proyek'])){
                $sesudah['developer'] = Developer::find()->where(['id'=>$sesudah['developer_id']])->asArray()->one();
            }
            if(!empty($post['Proyek'])){ 
                $sesudah['region'] = Region::find()->where(['id'=>$sesudah['region_id']])->asArray()->one();
            }
            if(!empty($post['Proyek'])){ 
                $sesudah['area'] = Area::find()->where(['id'=>$sesudah['area_id']])->asArray()->one();
            }
            
            $model->triggerNotifikasi(json_encode($sesudah),'create');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Proyek model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if ($model->loadAll($post)) {
            $sesudah= $post['Proyek'];
            if(!empty($post['Proyek'])){
                $sesudah['developer'] = Developer::find()->where(['id'=>$sesudah['developer_id']])->asArray()->one();
            }
            if(!empty($post['Proyek'])){ 
                $sesudah['region'] = Region::find()->where(['id'=>$sesudah['region_id']])->asArray()->one();
            }
            if(!empty($post['Proyek'])){ 
                $sesudah['area'] = Area::find()->where(['id'=>$sesudah['area_id']])->asArray()->one();
            }
            
            $model->triggerNotifikasi(json_encode($sesudah));
            $model2 = $this->findModel($id);
            $model2->is_approved = 0;
            $model2->save();

            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');  
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Proyek model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // $this->findModel($id)->deleteWithRelated();
        $this->findModel($id)->triggerNotifikasi("",'delete');
        return $this->redirect(['index']);
    }

    public function actionDeletePage($id){
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if ($model->loadAll($post)) {
            $sesudah= $post['Proyek'];
            
            $model->triggerNotifikasi("",'delete');    
            return $this->redirect(['index']);
        } else {
            return $this->render('delete-page', [
                'model' => $model,
            ]);
        }
    }

    
    /**
     * Finds the Proyek model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Proyek the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Proyek::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImport(){
        $request = Yii::$app->request;
        if($request->isGet){
            return $this->redirect(['index']);

        }else{
            $modelExcel = Excel::findOne($request->post('Excel')['id']);
            $modelExcel->load($request->post());
            if(!$modelExcel->upload()){
                \Yii::$app->getSession()->setFlash('error', 'Upload failed');   
                return $this->redirect(['index']);
            }

            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();
            $data = $modelExcel->importExcel();
            if(empty($data)){
                \Yii::$app->getSession()->setFlash('error', 'wrong format');   
                
            }
            try{
                // Proyek::deleteAll();
                // var_dump($data);die();
                $this->importData($data);
                $this->deleteApproval($data);
                // UserReferensi::deleteAll();
                \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');   
                $transaction->commit();
            } catch (\Exception $e) {
                \Yii::$app->getSession()->setFlash('error', $e->getMessage());   
                $transaction->rollBack();
                // throw $e;
            } catch (\Throwable $e) {
                \Yii::$app->getSession()->setFlash('error', $e->getMessage());   
                $transaction->rollBack();
                // throw $e;
            }
            // var_dump($data);
            // die();
            return $this->redirect(['index']);
        }
    }

    private function importDataOld($data){
        // $developerModel = null;
        foreach ($data as $item) {
            // $model = Proyek::find()->where(['nama'=>$item["Nama"]])->one();
            // if(empty($model)){
            //     $model = new Proyek();
            // }

            // $model->nama = $item["Nama"];
            // $model->tanggal_jatuh_tempo = $item["Tanggal_Jatuh_Tempo"];

            $this->saveItem($item);
            $this->saveDeveloper($item);
            $this->saveRegion($item);
            // $this->saveItem($item);

            // $developerModel = Developer::find()->where(['nama'=>$item["Developer"]])->one();
            // if(empty($developerModel)){
            //     $developerModel = new Developer();
            //     $developerModel->nama = $item["Developer"];
            //     $developerModel->save();
            // }

            // $kprModel = Kpr::find()->where(['nama'=>$item["Kpr"]])->one();
            // if(empty($kprModel)){
            //     $kprModel = new Kpr();
            //     $kprModel->nama = $item["Kpr"];
            //     $kprModel->save();
            // }

            // $regionModel = Region::find()->where(['name'=>$item["Region"]])->one();
            // if(empty($kprModel)){
            //     $kprModel = new Region();
            //     $kprModel->name = $item["Region"];
            //     $kprModel->save();
            // }

            // $model->min_harga = $item["Min_Harga"];
            // $model->max_harga = $item["Max_Harga"];
            // $model->longitude = $item["Longitude"];
            // $model->latitude = $item["Latitude"];
            // $model->location = $item["Location"];

            // if(!empty($developerModel)){
            //     $model->developer_id = $developerModel->id;
            // }
            // if(!empty($kprModel)){
            //     $model->kpr_id = $kprModel->id;
            // }
            // if(!empty($regionModel)){
            //     $model->region_id = $regionModel->id;
            // }
            // $model->save(false);
        }
    }

    public function saveDeveloperOld($item){
        $model = Proyek::find()->where(['nama'=>$item["Proyek"]])->one();

        $developerModel = Developer::find()->where(['nama'=>$item["Developer"]])->one();
        if(empty($developerModel)){
            $developerModel = new Developer();
            $developerModel->nama = $item["Developer"];
            
        }
        $groupModel = DeveloperGroup::find()->where(['name'=>$item["Group"]])->one();
        if(empty($groupModel)){
            $groupModel = new DeveloperGroup();
            $groupModel->name = $item["Group"];
            if(!$groupModel->save()){
                echo json_encode($groupModel->getErrors());
                die();
            }
        }

        $tierModel = DeveloperTier::find()->where(['nama'=>$item["Tier"]])->one();
        if(empty($tierModel)){
            $tierModel = new DeveloperTier();
            $tierModel->nama = $item["Tier"];
            if(!$tierModel->save()){
                echo json_encode($tierModel->getErrors());
                die();
            }
        }
        $developerModel->developer_tier_id = $tierModel->id;
        $developerModel->developer_group_id = $groupModel->id;
        if(!$developerModel->save()){
            echo json_encode($developerModel->getErrors());
            die();
        }   
        $model->developer_id = $developerModel->id;
        $model->developer_tier_id = $tierModel->id;
        $model->save();
    }

    public function saveRegionOld($item){
        if(!empty($item["Region"])){
            $model = Proyek::find()->where(['nama'=>$item["Proyek"]])->one();

            $regionModel = Region::find()->where(['name'=>$item["Region"]])->one();
            if(empty($regionModel)){
                $regionModel = new Region();
                $regionModel->name = $item["Region"];
                if(!$regionModel->save()){
                    echo json_encode($tierModel->getErrors());
                    die();
                }
            }
            if(!empty($item["Area"])){
                $areaModel = Area::find()->where(['name'=>$item["Area"]])->one();
                if(empty($areaModel)){
                    $areaModel = new Area();
                    $areaModel->name = $item["Area"];
                }
                    $areaModel->region_id = $regionModel->id;

                if(!$areaModel->save()){
                    echo json_encode($tierModel->getErrors());
                    die();
                }
                $model->area_id = $areaModel->id;
            }
            $model->region_id = $regionModel->id;
            if(!$model->save()){
                echo json_encode($model->getErrors());
                    die();
            }
        }
    }

     public function saveItemOld($item){
        $model = Proyek::find()->where(['nama'=>$item["Proyek"]])->one();
        if(empty($model)){
            $model = new Proyek();
            $model->nama = $item["Proyek"];
            $model->save();
        }
        $model->min_harga = $item["min_harga"];
        $model->max_harga = $item["max_harga"];
        $model->longitude = $item["longitude"];
        $model->latitude = $item["latitude"];
        // $model->location = $item["Location"];
        $model->website = $item["website"];
        if(!$model->save()){
            echo json_encode($model->getErrors());
                die();
        }
    }
    private function deleteApproval($data){
        $ids = [];
        foreach ($data as $item) {
            $model = Proyek::find()->where(['nama' =>$item["nama"]])->one();
            if(!empty($model)){
                $ids[] = $model->id;
                
                
            }
        }

        $notIn = Proyek::find()->where(['not in','id',$ids])->all();
        foreach ($notIn as $item) {
            $item->triggerNotifikasi("",'delete');
            
        }

    }

    private function importData($data){
        foreach ($data as $item) {
            //Nama
            $model = Proyek::find()->where(['nama' =>$item["nama"]])->one();
            if(empty($model)){   
                $model = new Proyek();
                $itemSebelumnya['nama'] = $item['nama'];
            }
            // $itemSebelumnya['min_harga'] = $item['min_harga'];
            // $itemSebelumnya['max_harga'] = $item['max_harga'];
            // $itemSebelumnya['website'] = $item['website'];
            // $itemSebelumnya['longitude'] = $item['longitude'];
            // $itemSebelumnya['latitude'] = $item['latitude'];
            // $itemSebelumnya['area_id'] = $item['area_id'];
            // $itemSebelumnya['region_id'] = $item['region_id'];
            // $itemSebelumnya = $item;
            $item['developer'] = $this->saveDeveloper($item);
            $item['developer_id'] = $item['developer']['id'];
            //dibuat array dahulu
            $item['region'] = $this->saveRegion($item);
            if(!empty($item['region'])){
                $item['region_id'] = $item['region']['id'];
            }
            //dibuat array dahulu
            $item['area'] = $this->saveArea($item);
            if(!empty($item['area'])){
                $item['area_id'] = $item['area']['id'];
            }
            // $item['area'] = $model->area;
                // var_dump($itemSebelumnya);die();
            if($model->isNewRecord){
                $model->triggerNotifikasi(json_encode($item),'create');
            }else{
                $model->triggerNotifikasi(json_encode($item),'update');
            }
        }
    }

    public function saveDeveloper($item){
        $model = Proyek::find()->where(['nama'=>$item["nama"]])->one();

        $developerModel = Developer::find()->where(['nama'=>$item["developer"]])->asArray()->one();
        if(empty($developerModel)){
                throw new \yii\base\Exception('Error in developer '.$item["developer"].' not exist'); 
            $developerModel = new Developer();
            $developerModel->nama = $item["developer"];
            
        }
        return $developerModel;
    }

    public function saveRegion($item){
        if(!empty($item["region"])){
            $model = Proyek::find()->where(['nama'=>$item["nama"]])->one();

            $regionModel = Region::find()->where(['name'=>$item["region"]])->asArray()->one();
            if(empty($regionModel)){
                throw new \yii\base\Exception('Error in region '.$item["region"].' not exist'); 

                $regionModel = new Region();
                $regionModel->name = $item["region"];
                // if(!$regionModel->save()){
                //     echo json_encode($tierModel->getErrors());
                //     die();
                // }
                return "";
            }else{

            return $regionModel;
            }
        }else{
            return "";
            
        }
    }

    public function saveArea($item){
        if(!empty($item["area"])){
            $model = Proyek::find()->where(['nama'=>$item["nama"]])->one();

            $areaModel = Area::find()->where(['name'=>$item["area"]])->asArray()->one();
            if(empty($areaModel)){
                throw new \yii\base\Exception('Error in area '.$item["area"].' not exist'); 

                $areaModel = new Area();
                $areaModel->name = $item["area"];
                // if(!$regionModel->save()){
                //     echo json_encode($tierModel->getErrors());
                //     die();
                // }
                return "";
            }else{

                return $areaModel;
            }
        }else{
            return "";
        }
    }

    public function actionExport(){
        ob_end_clean();
        
        \moonland\phpexcel\Excel::export([
            'models' => Proyek::find()->with(['developer','region','area'])->orderBy('developer_id')->all(),
            'fileName' => 'proyek',
                'columns' => [
                    // [
                    //     'attribute' => 'id',
                    //     'header' => 'id',
                    // ],
                    [
                        'attribute' => 'nama',
                        'header' => 'nama',
                    ],
                    [
                        'attribute' => 'tanggal_jatuh_tempo',
                        'header' => 'tanggal_jatuh_tempo',
                        // 'format' => 'text',
                    ],
                    [
                            'attribute' => 'developer_id',
                            'header' => 'developer',
                            'format' => 'raw',
                            'value' => function($model) {
                                if(!empty($model->developer)){

                                    return $model->developer->nama;
                                }else{
                                    return "";
                                }
                            }
                    ],
                    // [
                    //         // 'attribute' => 'developer_id',
                    //         'header' => 'Group',
                    //         'format' => 'text',
                    //         'value' => function($model) {
                    //             if(!empty($model->developer)){
                    //                 if(!empty($model->developer->developerGroup)){
                    //                     return $model->developer->developerGroup->name;
                    //                 }else{
                    //                     return "";
                    //                 }
                    //             }else{
                    //                 return "";
                    //             }
                    //         }
                    // ],
                    // [
                    //         // 'attribute' => 'developer_id',
                    //         'header' => 'Tier',
                    //         'format' => 'text',
                    //         'value' => function($model) {
                    //             if(!empty($model->developerTier)){
                    //                     return $model->developerTier->nama;
                    //             }else{
                    //                 return "";
                    //             }
                    //         }
                    // ],
                    // [
                    //         'attribute' => 'kpr_id',
                    //         'header' => 'Kpr',
                    //         'format' => 'text',
                    //         'value' => function($model) {
                    //             return $model->kpr->nama;
                    //         }
                    // ],
                    [
                            'attribute' => 'region_id',
                            'header' => 'region',
                            'format' => 'text',
                            'value' => function($model) {
                                if(!empty($model->region)){
                                    return $model->region->name;
                                }else{
                                    return "";
                                }
                            }
                    ],
                    [
                            'attribute' => 'area_id',
                            'header' => 'area',
                            'format' => 'text',
                            'value' => function($model) {
                                if(!empty($model->area)){
                                    return $model->area->name;
                                }else{
                                    return "";
                                }
                            }
                    ],
                    [
                        'attribute'=>'min_harga',
                        'header' => 'min_harga'
                    ],
                    [
                        'attribute'=>'max_harga',
                        'header' => 'max_harga'
                    ],
                    [
                        'attribute'=>'longitude',
                        'header' => 'longitude'
                    ],
                    [
                        'attribute'=>'latitude',
                        'header' => 'latitude'
                    ],
                    [
                        'attribute'=>'website',
                        'header' => 'website'
                    ],
                ],
                'headers' => [
                    'created_at' => 'Date Created Content',
                ],
        ]);        
    }

    public function actionSubcat() {
    
    $out = [];
    if (isset($_POST['depdrop_parents'])) {
        $parents = $_POST['depdrop_parents'];
        if ($parents != null) {
            $cat_id = $parents[0];
            $out = Proyek::getAreaDep($cat_id); 
            // the getSubCatList function will query the database based on the
            // cat_id and return an array like below:
            // [
            //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
            //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
            // ]
            echo Json::encode(['output'=>$out, 'selected'=>'']);
            return;
        }
    }
    echo Json::encode(['output'=>'', 'selected'=>'']);
    }

}