<?php

namespace app\controllers;

use Yii;
use app\models\SukuBunga;
use app\models\SukuBungaRules;
use app\models\SukuBungaSearch;
use app\models\Priviledge;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\Excel;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * SukuBungaController implements the CRUD actions for SukuBunga model.
 */
class SukuBungaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'add-kpr-suku-bunga','test-arr', 'add-suku-bunga-rules','add-suku-bunga-rules-kpr','add-suku-bunga-rules-multiguna','import','export','delete-page'],
                        'roles' => ['@'],
                        'matchCallback' => function($rules,$action){
                            return $this->isAccepted($rules,$action);
                        }
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    private function isAccepted($rules,$action){
        //jika dia admin cek table priviledge
        $status = Yii::$app->user->identity->status;
        if(in_array($status, [5])){
            $cek = Priviledge::find()->where(['nama' => 'Suku Bunga KPR','status'=>1])->one();
            if(!empty($cek)){
                return true;
            }else{
                return false;
            }
        }
        //jika approver return true
        if(in_array($status, [10])){
            return true;
        }else{
        //jika selain itu return false
            return false;
        }
        
    }

    /**
     * Lists all SukuBunga models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SukuBungaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $excelModel = Excel::find()->where(['class'=>'sukuBunga'])->one();
        $modelLink = new SukuBunga();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'excelModel' => $excelModel,
            'model' => $modelLink
        ]);
    }

    /**
     * Displays a single SukuBunga model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->redirect(['index']);

        // $model = $this->findModel($id);
        // $providerKprSukuBunga = new \yii\data\ArrayDataProvider([
        //     'allModels' => $model->kprSukuBungas,
        // ]);
        // $providerSukuBungaRules = new \yii\data\ArrayDataProvider([
        //     'allModels' => $model->sukuBungaRules,
        // ]);
        // return $this->render('view', [
        //     'model' => $this->findModel($id),
        //     'providerKprSukuBunga' => $providerKprSukuBunga,
        //     'providerSukuBungaRules' => $providerSukuBungaRules,
        // ]);
    }

    /**
     * Creates a new SukuBunga model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new SukuBunga();
        $post = Yii::$app->request->post();
        if ($model->loadAll($post) /*&& $model->save()*/) {
            // if(!empty($post['SukuBungaRules'])){
            //     $model->saveRules($post['SukuBungaRules']);
            // }else{
            //     $model->saveRules(null);
            // }
            $sesudah= $post['SukuBunga'];
            if(!empty($post['SukuBungaRules'])){
                $sesudah['sukuBungaRules'] = $post['SukuBungaRules'];
                
            }
            $model->triggerNotifikasi(json_encode($sesudah),'create');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionDeletePage($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();

        if ($model->loadAll($post) /*&& $model->save()*/) {
            $sesudah= $post['SukuBunga'];
            
            $model->triggerNotifikasi("",'delete');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('delete-page', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Updates an existing SukuBunga model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // $model = $this->findModel($id);
        // $model2 = $this->findModel($id);
        // $post = Yii::$app->request->post();
        // if ($model->loadAll($post)) {
        //     // $model['kprDeveloperTiers'] = 
        //     // var_dump($model->kprDeveloperTiers);
        //     // die();
        //     $sebelumArr = [];
        //     $sebelumArr = ArrayHelper::toArray($model2);
        //     $sebelumArr['sukuBungaRules'] = ArrayHelper::toArray($model2->sukuBungaRules);
        //     $model->is_approved = 0;
        //     $model->saveAll();
            
        //     $sebelum = json_encode($sebelumArr);

        //     if ($model->loadAll($post)) {
        //         if(!empty($post['SukuBungaRules'])){
        //             $model->saveRules($post['SukuBungaRules']);
        //         }else{
        //             $model->saveRules(null);
        //         }
        //         // return $this->redirect(['view', 'id' => $model->id]);
        //     }
        //     $model->triggerNotifikasi($sebelum);

        // \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');  
        //     return $this->redirect(['index']);
        // }
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if ($model->loadAll($post)) {
            $sesudah= $post['SukuBunga'];
            $sesudah['sukuBungaRules'] = $post['SukuBungaRules'];
            $model->triggerNotifikasi(json_encode($sesudah));
            $model2 = $this->findModel($id);
            $model2->is_approved = 0;
            $model2->save();
            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');  
            return $this->redirect(['index']);
        }else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SukuBunga model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // $this->findModel($id)->deleteWithRelated();
        $this->findModel($id)->triggerNotifikasi("",'delete');
        return $this->redirect(['index']);
    }

    
    /**
     * Finds the SukuBunga model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return SukuBunga the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SukuBunga::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for KprSukuBunga
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddKprSukuBunga()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('KprSukuBunga');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formKprSukuBunga', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for SukuBungaRules
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddSukuBungaRules()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('SukuBungaRules');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formSukuBungaRules', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAddSukuBungaRulesKpr()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('SukuBungaRules');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formSukuBungaRulesKpr', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionAddSukuBungaRulesMultiguna()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('SukuBungaRules');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formSukuBungaRulesMultiguna', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionExport(){
        $model = SukuBunga::find()->All();
        $filename = 'Data-'.Date('YmdGis').'-Mahasiswa.xls';
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=sukubungakpr.xls");
        echo '<table border="1" width="100%">
            <thead>
                <tr>
                    <th>id</th>
                    <th>nama</th>
                    <th>KPR</th>
                    <th>Order_Number</th>
                    <th>Suku_Bunga_Fixed</th>
                    <th>Tahun_Fixed</th>
                    <th>Min_Tenor</th>
                    <th>Multiguna</th>
                    <th>Order_Number_M</th>
                    <th>Suku_Bunga_Fixed_M</th>
                    <th>Tahun_Fixed_M</th>
                    <th>Min_Tenor_M</th>
                </tr>
            </thead>';

            foreach($model as $data){
                // $this->generateContentExcel($data);
                $id = Html::encode($data->id);
                $nama = Html::encode($data->nama);
                $rules_kpr = $data->sukuBungaRulesKpr;
                $rules_multiguna = $data->sukuBungaRulesMultiguna;
                //baris pertama
                //print nama
                    echo '
                            <tr>
                                <td>'.$id.'</td>
                                <td>'.$nama.'</td>
                        ';

                for ($count=0;$count<max(count($rules_kpr),count($rules_multiguna));$count++){
                    if($count != 0){
                        echo '
                                    <tr><td></td>
                                        <td></td>';
                        }
                    if(!empty($rules_kpr[$count])){
                        echo '
                                    <td>'.$rules_kpr[$count]->is_multiguna.'</td>
                                    <td>'.$rules_kpr[$count]->order_number.'</td>
                                    <td>'.$rules_kpr[$count]->suku_bunga_fixed.'</td>
                                    <td>'.$rules_kpr[$count]->tahun_fixed.'</td>
                                    <td>'.$rules_kpr[$count]->min_tenor.'</td>
                            ';
                    }else{
                            echo '<td></td> <td></td> <td></td> <td></td> <td></td>';
                    }
                    if(!empty($rules_multiguna[$count])){
                        echo '
                                    <td>'.$rules_multiguna[$count]->is_multiguna.'</td>
                                    <td>'.$rules_multiguna[$count]->order_number.'</td>
                                    <td>'.$rules_multiguna[$count]->suku_bunga_fixed.'</td>
                                    <td>'.$rules_multiguna[$count]->tahun_fixed.'</td>
                                    <td>'.$rules_multiguna[$count]->min_tenor.'</td>
                                </tr>    
                            ';
                    }else{
                            echo '<td></td> <td></td> <td></td> <td></td> <td></td> </tr>';
                    }
                }
            }
        echo '</table>';
        // die();
    }

    // private function generateContentExcel($data){ 
    //     $nama = $data->nama;
    //     $rules_kpr = $data->sukuBungaRulesKpr;
    //     $rules_multiguna = $data->sukuBungaRulesMultiguna;
    //     //baris pertama
    //     //print nama
    //             echo '
    //                     <tr>
    //                         <td>'.$nama.'</td>
    //             ';

    //     for ($count=0;$count<max(count($rules_kpr),count($rules_multiguna));$count++){
    //         if($count != 0){
    //         echo '
    //                         <td></td>';
    //         }
    //         if(!empty($rules_kpr[$count])){
    //             echo '
    //                         <td>'.$rules_kpr[$count]->is_multiguna.'</td>
    //                         <td>'.$rules_kpr[$count]->order_number.'</td>
    //                         <td>'.$rules_kpr[$count]->suku_bunga_fixed.'</td>
    //                         <td>'.$rules_kpr[$count]->tahun_fixed.'</td>
    //                         <td>'.$rules_kpr[$count]->min_tenor.'</td>
    //                 ';
    //             }else{
    //                 echo '<td></td> <td></td> <td></td> <td></td> <td></td>';
    //             }
    //         if(!empty($rules_multiguna[$count])){
    //             echo '
    //                         <td>'.$rules_multiguna[$count]->is_multiguna.'</td>
    //                         <td>'.$rules_multiguna[$count]->order_number.'</td>
    //                         <td>'.$rules_multiguna[$count]->suku_bunga_fixed.'</td>
    //                         <td>'.$rules_multiguna[$count]->tahun_fixed.'</td>
    //                         <td>'.$rules_multiguna[$count]->min_tenor.'</td>
    //                     </tr>    
    //                 ';
    //             }else{
    //                 echo '<td></td> <td></td> <td></td> <td></td> <td></td> </tr>';
    //             }
    //         }
    // }

    public function actionImport(){
        $request = Yii::$app->request;
        if($request->isGet){
            return $this->redirect(['index']);

        }else{
            $modelExcel = Excel::findOne($request->post('Excel')['id']);
            $modelExcel->load($request->post());
            if(!$modelExcel->upload()){
                \Yii::$app->getSession()->setFlash('error', 'Upload failed');   
                return $this->redirect(['index']);
            }

            $data = $modelExcel->importExcel();
            if(empty($data)){
                \Yii::$app->getSession()->setFlash('error', 'wrong format');   
                
            }
            // var_dump($data);
            // die();
            $message = $this->importData($data);
            \Yii::$app->getSession()->setFlash('success', 'Your data has been updated');   
            // \Yii::$app->getSession()->setFlash('success', $message);   
            return $this->redirect(['index']);
        }
    }

    private function importData($data){
        $modelSebelumnya = null;
        $itemSebelumnya = null;
        $countRules = -1;
        $itemSebelumnya['sukuBungaRules'] = [];
        foreach ($data as $item) {
            $model = SukuBunga::find()->where(['nama'=>$item["nama"]])->one();
            //Nama
            if(!empty($item["nama"])){
                if(!empty($modelSebelumnya)){

                    if($modelSebelumnya->isNewRecord){
                        $modelSebelumnya->triggerNotifikasi(json_encode($itemSebelumnya),'create');
                    }else{
                        $modelSebelumnya->triggerNotifikasi(json_encode($itemSebelumnya),'update');
                    }
                }
                if(empty($model)){
                    $model = new SukuBunga();
                    $itemSebelumnya['nama'] = $item['nama'];
                    // $model->save(false);
                // echo $itemSebelumnya['nama'].'<br>';
                }
                $modelSebelumnya = $model;
                $itemSebelumnya = $item;
                $countRules = SukuBungaRules::find()->where(['suku_bunga_id'=>$model->id])->count();
            }
            $sbkpr = $this->convertSukuBungaRulesKpr($item);
            $sbmulti = $this->convertSukuBungaRulesMulti($item);
            if(!empty($sbkpr)){
                $itemSebelumnya['sukuBungaRules'][] = $sbkpr;
                
            }
            if(!empty($sbmulti)){
                $itemSebelumnya['sukuBungaRules'][] = $sbmulti;
            }
        }
        if(!empty($modelSebelumnya)){
            if($modelSebelumnya->isNewRecord){
                // echo 'sadasdasd';die();
                $modelSebelumnya->triggerNotifikasi(json_encode($itemSebelumnya),'create');
            }else{
                $modelSebelumnya->triggerNotifikasi(json_encode($itemSebelumnya),'update');
            }
        }
    }
    
    private function convertSukuBungaRulesKpr($item){
        $rules = [];
        if(!empty ($item['Order_Number'])){
            $rules['is_multiguna'] = $item["KPR"];
            $rules['order_number'] = $item["Order_Number"];
            $rules['suku_bunga_fixed'] = $item["Suku_Bunga_Fixed"];
            $rules['tahun_fixed'] = $item["Tahun_Fixed"];
            $rules['min_tenor'] = $item["Min_Tenor"];
            return $rules;
        }
        return null;
    }
    private function convertSukuBungaRulesMulti($item){
        $rulesMulti =[];
        if(!empty($item['Multiguna'])){
            $rulesMulti['is_multiguna'] = $item["Multiguna"];
            $rulesMulti['order_number'] = $item["Order_Number_M"];
            $rulesMulti['suku_bunga_fixed'] = $item["Suku_Bunga_Fixed_M"];
            $rulesMulti['tahun_fixed'] = $item["Tahun_Fixed_M"];
            $rulesMulti['min_tenor'] = $item["Min_Tenor_M"];
            return $rulesMulti;
        }
        return null;
    }

    private function importDataOld($data){
        $modelSebelumnya = null;
        foreach ($data as $item) {
            $model = SukuBunga::find()->where(['nama'=>$item["Nama"]])->one();
            // if(empty($item["Name"])){
            //     $model= $modelSebelumnya;
            // }
            if(!empty($item["Nama"])){
                if(empty($model)){
                    $model = new SukuBunga();
                    $model->nama = $item['Nama'];
                    $model->save(false);
                }
                foreach ($model->sukuBungaRules as $rulesKprlama) {
                   $rulesKprlama->delete();
                }
            }
            if(empty($item["Nama"])){
                $rulesKpr = new SukuBungaRules();
                $rulesKpr->suku_bunga_id = $modelSebelumnya->id;
                $rulesKpr->is_multiguna = $item["KPR"];
                $rulesKpr->order_number = $item["Order_Number"];
                $rulesKpr->suku_bunga_fixed = $item["Suku_Bunga_Fixed"];
                $rulesKpr->tahun_fixed = $item["Tahun_Fixed"];
                $rulesKpr->min_tenor = $item["Min_Tenor"];
                $rulesKpr->save(false);
                $rulesMulti = new SukuBungaRules();
                $rulesMulti->suku_bunga_id = $modelSebelumnya->id;
                $rulesMulti->is_multiguna = $item["Multiguna"];
                $rulesMulti->order_number = $item["Order_Number_M"];
                $rulesMulti->suku_bunga_fixed = $item["Suku_Bunga_Fixed_M"];
                $rulesMulti->tahun_fixed = $item["Tahun_Fixed_M"];
                $rulesMulti->min_tenor = $item["Min_Tenor_M"];
                $rulesMulti->save(false);
            }
            if(!empty($model)){
                $modelSebelumnya = $model;
                $rulesKpr = new SukuBungaRules();
                $rulesKpr->suku_bunga_id = $modelSebelumnya->id;
                $rulesKpr->is_multiguna = $item["KPR"];
                $rulesKpr->order_number = $item["Order_Number"];
                $rulesKpr->suku_bunga_fixed = $item["Suku_Bunga_Fixed"];
                $rulesKpr->tahun_fixed = $item["Tahun_Fixed"];
                $rulesKpr->min_tenor = $item["Min_Tenor"];
                $rulesKpr->save(false);
            }
            if(!empty($model)){
                $modelSebelumnya = $model;

                $rulesMulti = new SukuBungaRules();
                $rulesMulti->suku_bunga_id = $modelSebelumnya->id;
                $rulesMulti->is_multiguna = $item["Multiguna"];
                $rulesMulti->order_number = $item["Order_Number_M"];
                $rulesMulti->suku_bunga_fixed = $item["Suku_Bunga_Fixed_M"];
                $rulesMulti->tahun_fixed = $item["Tahun_Fixed_M"];
                $rulesMulti->min_tenor = $item["Min_Tenor_M"];
                $rulesMulti->save(false);
            }
        }
    }
}