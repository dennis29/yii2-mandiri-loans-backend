<?php

namespace app\controllers;

use Yii;
use app\models\KtaPerusahaanTier;
use app\models\KtaPerusahaanTierSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KtaPerusahaanTierController implements the CRUD actions for KtaPerusahaanTier model.
 */
class KtaPerusahaanTierController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all KtaPerusahaanTier models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KtaPerusahaanTierSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KtaPerusahaanTier model.
     * @param string $kta_id
     * @param string $perusahaan_tier_id
     * @return mixed
     */
    public function actionView($kta_id, $perusahaan_tier_id)
    {
        $model = $this->findModel($kta_id, $perusahaan_tier_id);
        return $this->render('view', [
            'model' => $this->findModel($kta_id, $perusahaan_tier_id),
        ]);
    }

    /**
     * Creates a new KtaPerusahaanTier model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KtaPerusahaanTier();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'kta_id' => $model->kta_id, 'perusahaan_tier_id' => $model->perusahaan_tier_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing KtaPerusahaanTier model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $kta_id
     * @param string $perusahaan_tier_id
     * @return mixed
     */
    public function actionUpdate($kta_id, $perusahaan_tier_id)
    {
        $model = $this->findModel($kta_id, $perusahaan_tier_id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'kta_id' => $model->kta_id, 'perusahaan_tier_id' => $model->perusahaan_tier_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing KtaPerusahaanTier model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $kta_id
     * @param string $perusahaan_tier_id
     * @return mixed
     */
    public function actionDelete($kta_id, $perusahaan_tier_id)
    {
        $this->findModel($kta_id, $perusahaan_tier_id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the KtaPerusahaanTier model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $kta_id
     * @param string $perusahaan_tier_id
     * @return KtaPerusahaanTier the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($kta_id, $perusahaan_tier_id)
    {
        if (($model = KtaPerusahaanTier::findOne(['kta_id' => $kta_id, 'perusahaan_tier_id' => $perusahaan_tier_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
