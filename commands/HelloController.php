<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";
    }

    public function actionSeedKtaRules($amount = 10)
    {
    	$ktas = \yii\helpers\ArrayHelper::getColumn(\app\models\Kta::find()->asArray()->all(), 'id');
    	$incomes = \yii\helpers\ArrayHelper::getColumn(\app\models\Income::find()->asArray()->all(), 'id');
    	$limits = \yii\helpers\ArrayHelper::getColumn(\app\models\Limit::find()->asArray()->all(), 'id');
		for ($i=0; $i < $amount; $i++) {
			$model = new \app\models\KtaRules;
			$model->income_id = $incomes[array_rand($incomes)];
			$model->limit_id = $limits[array_rand($limits)];
			$model->kta_id = $ktas[array_rand($ktas)];
			$model->min_tenor = 1; 
			$model->max_tenor = rand(5,10); 
			$model->suku_bunga_floating = rand(1875,3025)/100; 
			$model->suku_bunga_flat = rand(70,143)/100; 
			$model->max_dbr = rand(30,50); 
			//generate profesi
			//random jumlah profesi
			if(!$model->save()){
				var_dump($model->errors);
			}
			$randomLim = rand(1,4);
	    	$tiers = \app\models\PerusahaanTier::find()->orderBy(new \yii\db\Expression('rand()'))->limit($randomLim)->all();
	    	foreach ($tiers as $key) {
				$model->link('perusahaanTiers',$key);
	    	}

	    	$randomLim = rand(1,2);
	    	$profesi = \app\models\TargetMarket::find()->orderBy(new \yii\db\Expression('rand()'))->limit($randomLim)->all();
	    	foreach ($profesi as $key) {
				$model->link('targetMarkets',$key);
	    	}
			//generate tiers

    	}
    }
    public function actionSeedPerusahaan(){
    	$perus = [ 	  	
			"Elnusa Kawasan Komersial ",
			"Elnusa Petro Teknik ",
			"ELNUSA PRIMA ELEKTRIKA ",
			"Elnusa Workover Services ",
			"Geodipa Energi ",
			"Hotel Patra Jasa ",
			"Nippon Sokubai Indonesia "
		];

		foreach ($perus as $key ) {
			$model = new \app\models\Perusahaan;
			$model->name = $key;
			$model->max_limit = 100000000;
			$model->max_tenor = 8;
			$model->dbr = 40;
			$model->perusahaan_tier_id= "88dc7e2fe19d11e698c794dbc9b18a6e";
			$model->perusahaan_group_id = "951df531e1ff11e68cda94dbc9b18a6e";
			$model->save();
		}
    }

    public function actionSeedDeveloper(){
    	$model = null;
    	$perus = [ 
    	"ANGKASA PURA 1"
,"ANGKASA PURA LOGISTIC"
,"ANGKASA PURA HOTEL"
,"ANGKASA PURA I DAPEN "
,"ANGKASA PURA I YYS."
,"ANGKASA PURA PROPERTY"
,"ANGKASA PURA SUPPORT"
,"KESEJAHTERAAN ANGKASA PURA I YYS."
		];

		foreach ($perus as $key ) {
			$model = new \app\models\Developer;
			$model->nama = $key;
			$model->link = "google.com";
			$model->developer_tier_id= "4fbc8c17e72711e6ad8600ac81a198fa";
			$model->developer_group_id = "990293bbe72711e6ad8600ac81a198fa";
			$model->save();
		}
    }

    public function actionSeedKprRules($amount = 10)
    {
    	$kprs = \yii\helpers\ArrayHelper::getColumn(\app\models\Kpr::find()->asArray()->all(), 'id');
    	$incomes = \yii\helpers\ArrayHelper::getColumn(\app\models\Income::find()->asArray()->all(), 'id');
    	$limits = \yii\helpers\ArrayHelper::getColumn(\app\models\Limit::find()->asArray()->all(), 'id');
		for ($i=0; $i < $amount; $i++) {
			$model = new \app\models\KprRules;
			$model->income_id = $incomes[array_rand($incomes)];
			$model->limit_id = $limits[array_rand($limits)];
			$model->kpr_id = $kprs[array_rand($kprs)];
			$model->min_tenor = 1; 
			$model->max_tenor = rand(5,10); 
			$model->suku_bunga_floating = rand(1875,3025)/100; 
			$model->suku_bunga_flat = rand(70,143)/100; 
			$model->max_dbr = rand(30,50); 
			//generate profesi
			//random jumlah profesi
			if(!$model->save()){
				var_dump($model->errors);
			}
			$randomLim = rand(1,4);
	    	$tiers = \app\models\DeveloperTier::find()->orderBy(new \yii\db\Expression('rand()'))->limit($randomLim)->all();
	    	foreach ($tiers as $key) {
				$model->link('developerTiers',$key);
	    	}

	    	$randomLim = rand(1,2);
	    	$profesi = \app\models\TargetMarket::find()->orderBy(new \yii\db\Expression('rand()'))->limit($randomLim)->all();
	    	foreach ($profesi as $key) {
				$model->link('targetMarkets',$key);
	    	}
			//generate tiers

    	}
    }

	 public function actionSeedProyek($amount = 10)
    {
    	$kprs = \yii\helpers\ArrayHelper::getColumn(\app\models\Kpr::find()->asArray()->all(), 'id');
    	$developer = \yii\helpers\ArrayHelper::getColumn(\app\models\Developer::find()->asArray()->all(), 'id');
    	$regions = \yii\helpers\ArrayHelper::getColumn(\app\models\Region::find()->asArray()->all(), 'id');
		$letters = array_merge(range('a','z'),range('A','Z'));
 
		$faker = \Faker\Factory::create();
		for ($i=0; $i < $amount; $i++) {
			$model = new \app\models\Proyek;
			$model->nama = $letters[rand(0,51)].' proyek ' . rand(1,500);
			$model->tanggal_jatuh_tempo =  $faker->date($format = 'Y-m-d');;
			$model->developer_id = $developer[array_rand($developer)];
			$model->kpr_id = $kprs[array_rand($kprs)];
			$model->min_harga = rand(50000000,100000000); 
			$model->max_harga = rand(190000000,100000000000);
			$model->longitude = $faker->longitude($min=95,$max=141);
			$model->latitude = $faker->latitude($min=-10,$max=6);
			$model->location = rand(1,8); 
			if(!$model->save()){
				var_dump($model->errors);
			}

    	}
    }

    public function actionSeedLog($amount = 10){
    	$user_ids = \yii\helpers\ArrayHelper::getColumn(\app\models\User::find()->asArray()->all(), 'id');

    	$ip_address = "1.1.1.1";

		for ($i=0; $i < $amount; $i++) {
    		$code = rand(1,3);
    		$user_id = $user_ids[array_rand($user_ids)];
			$obj = new \app\models\LogDashboard();
            $obj->code = $code;
            $obj->ip_address = $ip_address;
            $obj->user_id = $user_id;
            if(!$obj->save(false)){
				var_dump($obj->errors);
			}

		}

    }     
}
