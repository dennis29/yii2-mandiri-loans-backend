<?php

use yii\db\Migration;

/**
 * Handles adding message to table `log_user`.
 */
class m170607_032326_add_message_column_to_log_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('log_user', 'message', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('log_user', 'message');
    }
}
